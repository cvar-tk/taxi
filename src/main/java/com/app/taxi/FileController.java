package com.app.taxi;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileController {

	@GetMapping(value = { SingleTon.URL_FILE_PATH })
	public ResponseEntity<Resource> downloadFile(@PathVariable("file_name") String filename, HttpSession httpSession) {

		System.out.println(filename);

		//Session Check implement here..

		try {
			Path rootLocation = Paths.get("/usr/local/tomcat8/CLS_DATA/");

			Path file = rootLocation.resolve(filename);

			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {

				return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + resource.getFilename() + "\"").body(resource);

			} else {

				return ResponseEntity.badRequest().body(null);
			}

		} catch (Exception e) {

			return ResponseEntity.badRequest().body(null);

		}

	}
}
