package com.app.taxi;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserAndVehicleLocationService;
import com.app.taxi.service.UserService;

@Controller
public class CommonsController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private UserAndVehicleLocationService userAndVehicleLocationService;
	
	@Autowired
	private UserService userService; 

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_USER_LOCATION })
	private String POST_UPDATE_USER_LOCATION(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			userAndVehicleLocationService.updateUserLocation(user_id, latitude, longtitude);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}


	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_VEHICLE_LOCATION })
	private String POST_UPDATE_VEHICLE_LOCATION(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			userAndVehicleLocationService.updateVehicleLocation(user_id, latitude, longtitude);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}
	
	

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_USER_EMAIL})
	private String POST_UPDATE_EMAIL_ID(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			String email = request_obj.getString(SingleTon.EMAIL);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			userService.updateUserEmailId(user_id,email);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_GET_SERVER_TIME })
	private String TIME() {

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put(SingleTon.CURRENT_TIME,Calendar.getInstance(TimeZone.getTimeZone("IST")).getTimeInMillis());
			return jsonObject.toString();

		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			return jsonObject.toString();

		}
		
	}
	
	
}
