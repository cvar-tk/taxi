package com.app.taxi.service;

import java.util.Date;

import org.hibernate.loader.collection.OneToManyJoinWalker;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.OwnerDAO;

@Service("OwnerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class OwnerServiceImpl implements OwnerService{
	
	@Autowired
	private OwnerDAO ownerDAO;
	
	
	@Override
	public JSONObject getOwnerProfileDetails(Integer user_id) {
	
		
		return ownerDAO.getOwnerProfileDetails(user_id);
	}


	@Override
	public JSONObject getCarTripHistoryByDate(Integer user_id, Integer car_id, Date date1) {
		
		return ownerDAO.getCarTripHistoryByDate(user_id,car_id,date1);
	}


	@Override
	public JSONObject ViewTripHistoryByTripId(Integer trip_id) {
		
		return ownerDAO.ViewTripHistoryByTripId(trip_id);
	}


	@Override
	public JSONObject AdminViewTripHistoryByTripId(Integer trip_id) {
		
		return ownerDAO.AdminViewTripHistoryByTripId(trip_id);
	}


	@Override
	public JSONObject getCarSubscriberPlanDetails(Integer user_id, Integer car_id) {
		
		return ownerDAO.getCarSubscriberPlanDetails(user_id , car_id);
	}


	@Override
	public JSONObject updateCarSubscriberAutoRenewal(Integer user_id, Integer car_id, Integer plan_id,
			Integer auto_renewal) {
		
		return ownerDAO.updateCarSubscriberAutoRenewal(user_id , car_id , plan_id , auto_renewal);
	}


	
	//every 60 seconds method calling
	@Override
	@Scheduled(cron = "0 0/1 * * * *")
	public void UpdateAutorenewalByMIN() {
		
		ownerDAO.UpdateAutorenewalByMIN();
		
	}


	@Override
	public JSONArray getOwnerList(Integer page, Integer limit) {
		
		return ownerDAO.getOwnerList(page,limit);
	}


	@Override
	public Long getTotalCountOfOwners() {
	
		return ownerDAO.getTotalCountOfOwners();
	}

	
}
