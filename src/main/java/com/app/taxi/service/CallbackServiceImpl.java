package com.app.taxi.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.CMSDao;
import com.app.taxi.dao.CallbackDAO;

@Service("CallbackService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CallbackServiceImpl implements CallbackService {

	@Autowired
	private CallbackDAO callbackDAO;

	@Override
	public JSONObject addCallbackByVehicleId(Integer vehicle_id, Integer user_id , String address, Double latitude, Double longtitude) {
		
		return callbackDAO.addCallbackByVehicleId(vehicle_id,user_id , address , latitude , longtitude);
		
	} 
	

}
