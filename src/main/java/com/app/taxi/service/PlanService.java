package com.app.taxi.service;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.taxi.model.PlanMaster;

public interface PlanService {

	void addNewPlan(PlanMaster planMaster, Integer type_id);

	JSONArray getAllDemandPlans();

	JSONObject changePlanStatusById(Integer id);

	JSONArray getAllVehiclePlans();

	JSONArray getActiveRunningPlanList();

	JSONArray getActiveDemandPlanList();

	JSONObject subscribeRunningPlan(Integer user_id, Integer plan_id, Integer vehicle_id);

	JSONObject getPlanDetailsById(Integer plan_id);

	void EditPlanDetailsById(PlanMaster planMaster, Integer plan_id);

	void EditOnDemandPlanDetailsById(PlanMaster planMaster, Integer plan_id);

}
