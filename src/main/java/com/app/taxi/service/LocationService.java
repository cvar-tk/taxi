package com.app.taxi.service;

import org.json.JSONArray;

public interface LocationService {

	JSONArray getNearestVehicles(Double latitude, Double longtitude,Integer type);
	JSONArray getNearestVehiclesDetails(Double latitude, Double longtitude, Integer type);
}
