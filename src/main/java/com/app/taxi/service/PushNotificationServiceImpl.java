package com.app.taxi.service;

import java.util.List;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service("PushNotificationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PushNotificationServiceImpl implements PushNotificationService {

	private final String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	private final String FIREBASE_SERVER_KEY = "AAAAG3ibHj4:APA91bG9E4viLiUrYx9EyzkqBzXVEmM74qArMVgodoSeDwSiqayzBqFl2zuNbpZE89nrEK6rmN7HcXtu7qeKaE9X6lz2RYyuQiTAu3iBp8_M5hl-ItqvDb-v05D8E5pyWcLfmKnl-Fd74QzrHBziCaOUjRvJT_ggpg";

	@Override
	public void sendPushNotification(List<String> keys, String messageTitle, String message) {
		try {

			JSONObject msg = new JSONObject();

			msg.put("title", messageTitle);
			msg.put("body", message);
			msg.put("notificationType", "Test");

			keys.forEach(key -> {
				System.out.println("\nCalling fcm Server >>>>>>>");
				String response = callToFcmServer(msg, key);
				System.out.println("Got response from fcm Server : " + response + "\n\n");
			});
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private String callToFcmServer(JSONObject message, String receiverFcmKey) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.set("Authorization", "key=" + FIREBASE_SERVER_KEY);
			httpHeaders.set("Content-Type", "application/json");

			JSONObject json = new JSONObject();

			json.put("data", message);
			json.put("notification", message);
			json.put("to", receiverFcmKey);

			System.out.println("Sending :" + json.toString());

			HttpEntity<String> httpEntity = new HttpEntity<>(json.toString(), httpHeaders);
			return restTemplate.postForObject(FIREBASE_API_URL, httpEntity, String.class);

		} catch (Exception e) {

			e.printStackTrace();

			return null;

		}
	}
}
