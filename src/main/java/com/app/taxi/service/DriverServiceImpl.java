package com.app.taxi.service;

import java.math.BigInteger;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.DriverDao;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;

@Service("DriverService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverDao driverDao;

	@Override
	public JSONObject addnewDriverForApproval(UserMaster userMaster, WorkersCredential workersCredential,
			Integer owner_id) {

		return driverDao.addnewDriverForApproval(userMaster, workersCredential, owner_id);
	}

	@Override
	public JSONObject getDriverProfileDetails(Integer user_id) {

		return driverDao.getDriverProfileDetails(user_id);
	}

	@Override
	public JSONArray getNewDrivers(Integer page, Integer limit) {

		return driverDao.getNewDrivers(page, limit);
	}

	@Override
	public Long getTotalCountNewDrivers() {

		return driverDao.getTotalCountNewDrivers();
	}

	@Override
	public JSONObject getDriverDetailsByDriverId(Integer driver_id) {

		return driverDao.getDriverDetailsByDriverId(driver_id);
	}

	@Override
	public void updateDriverDetails(Integer driver_id, String driver_name, Date expiry, Date dob1,
			String license_no) {

		driverDao.updateDriverDetails(driver_id, driver_name,  expiry, dob1, license_no);

	}

	@Override
	public void updatedriverStatus(String reason, String comments, Integer driver_id, Integer status) {

		driverDao.updatedriverStatus(reason, comments, driver_id, status);
	}

	@Override
	public JSONArray getApprovedDriverList(Integer page, Integer limit) {

		return driverDao.getApprovedDriverList(page, limit);
	}

	@Override
	public Long getTotalCountApprovedDrivers() {

		return driverDao.getTotalCountApprovedDrivers();
	}

	@Override
	public JSONObject getDriverDetailsByLicenseNo(String license_number) {

		return driverDao.getDriverDetailsByLicenseNo(license_number);
	}

	@Override
	public JSONArray filterByDriverDate(Date fromdate, Date todate, Integer pageIndex) {

		return driverDao.filterByDriverDate(fromdate, todate, pageIndex);
	}

	@Override
	public Long getTotalCountApprovedDriverDate(Date fromdate, Date todate, Integer pageIndex) {

		return driverDao.getTotalCountApprovedDriverDate(fromdate, todate, pageIndex);
	}

	@Override
	public JSONObject ChangeDriverStatusToOnline(Integer user_id) {

		return driverDao.ChangeDriverStatusToOnline(user_id);

	}

	@Override
	public JSONObject ChangeDriverStatusToOffline(Integer user_id) {

		return driverDao.ChangeDriverStatusToOffOnline(user_id);

	}

	@Override
	public JSONObject selectCarByDriver(Integer user_id, Integer vehicle_id) {

		return driverDao.selectCarByDriver(user_id, vehicle_id);
	}

	@Override
	public Boolean checkDrvierAlreadyPickedAnyVehicle(Integer driver_id) {

		return driverDao.checkDrvierAlreadyPickedAnyVehicle(driver_id);
	}

	@Override
	public JSONObject getDriverCurrentStatusById(Integer user_id) {

		return driverDao.getDriverCurrentStatusById(user_id);
	}

	@Override
	public JSONObject getDriverONOFFStatusById(Integer user_id) {

		return driverDao.getDriverONOFFStatusById(user_id);
	}

	@Override
	public void updateExpiryDateByAdmin(Integer driver_id, Date expiry) {

		driverDao.updateExpiryDateByAdmin(driver_id, expiry);

	}

	@Override
	public void ChangeDriverStatusToDisable(Integer check_status, Integer driver_id) {

		driverDao.ChangeDriverStatusToDisable(check_status, driver_id);
	}

	@Override
	public JSONObject getUserLocationRequest(Integer user_id) {

		return driverDao.getUserLocationRequest(user_id);
	}

	@Override
	public JSONObject changeDriverSelfDisableStatus(Integer user_id) {
		
		return driverDao.changeDriverSelfDisableStatus(user_id);
		
	}

	@Override
	public JSONObject updateBaseKmDetails(Integer user_id, Double base_km, Double base_km_price,
			Double per_min_price, Double base_fair) {
		
		
		return driverDao.updateBaseKmDetails(user_id,base_km,base_km_price,per_min_price,base_fair);
	}

	
	@Override
	public JSONObject startTrip(JSONObject request,Integer user_id) {
	    return driverDao.startTrip(request,user_id);
	}
	
	@Override
	public JSONObject EndTrip(JSONObject request, Integer user_id) {

	return driverDao.EndTrip(request,user_id);
	}

	@Override
	public JSONObject deselectCarByDriverId(Integer user_id, Integer vehicle_id) {
	
		return driverDao.deselectCarByDriverId(user_id,vehicle_id);
		
		
	}

	@Override
	public JSONObject addExcistingDriverToOwner(BigInteger mobile, Integer user_id) {
		
		return driverDao.addExcistingDriverToOwner(mobile,user_id);
	}

	@Override
	public JSONObject getDriverTripHistory(Integer user_id) {
		
		return driverDao.getDriverTripHistory(user_id);
	}

	@Override
	public JSONObject checkCurrentPasswordByID(Integer user_id, String password) {
		
		return driverDao.checkCurrentPasswordByID(user_id , password);
	}

	@Override
	public JSONObject getOndemandPlanDetailaById(Integer user_id) {
		
		return driverDao.getOndemandPlanDetailaById(user_id);
	}

	@Override
	public JSONObject getManyTripLocationByLatitude(Integer user_id, Double latitude, Double longtitude) {
		
		
		return driverDao.getManyTripLocationByLatitude(user_id,latitude,longtitude);
		
	}

	@Override
	public void adminTerminateTripByTripId(Integer trip_id, Integer userid) {
		
		 driverDao.adminTerminateTripByTripId(trip_id,userid); 
		
	}

	
	
	
	
}
