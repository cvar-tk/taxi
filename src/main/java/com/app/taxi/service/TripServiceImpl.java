package com.app.taxi.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.CMSDao;
import com.app.taxi.dao.TripDAO;

@Service("TripService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TripServiceImpl implements TripService {

	@Autowired
	private TripDAO tripDAO;

	@Override
	public JSONObject getTripControlDetails(Integer id) {
		
		return tripDAO.getTripControlDetails(id);
	}

	@Override
	public void AdminUpdateTripControlDetails(Double base_control, Integer status) {
	
           tripDAO.AdminUpdateTripControlDetails(base_control,status);
		
	}

	@Override
	public JSONObject getUserTripDetailsByUserId(Integer user_id, Integer pageIndex) {
		
		return tripDAO.getUserTripDetailsByUserId(user_id,pageIndex);
	}

	@Override
	public JSONArray getTripMasterList(Integer page, Integer limit) {
	
		return tripDAO.getTripMasterList(page,limit);
	}

	@Override
	public Long getTotalCountTripMaster() {
		
		return tripDAO.getTotalCountTripMaster();
	}

	@Override
	public JSONObject getUserLiveTripDetailsByUserId(Integer user_id) {

		return tripDAO.getUserLiveTripDetailsByUserId(user_id);
	} 
	
	

}
