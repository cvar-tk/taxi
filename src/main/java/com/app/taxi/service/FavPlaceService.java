package com.app.taxi.service;

import org.json.JSONArray;

import com.app.taxi.model.FavPlace;

public interface FavPlaceService {

	void addPlace(FavPlace favPlace, Integer user_id);

	JSONArray getFavListById(Integer user_id);

	void deleteFavPlace(Integer user_id, Integer place_id);


}
