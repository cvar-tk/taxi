package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.UserAndVehicleLocationDao;

@Service("userAndVehicleLocationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserAndVehicleLocationServiceImpl implements UserAndVehicleLocationService {

	@Autowired
	private UserAndVehicleLocationDao userAndVehicleLocationDao;
	
	@Override
	public void updateUserLocation(Integer user_id, Double latitude, Double longtitude) {

		userAndVehicleLocationDao.updateUserLocation(user_id,latitude,longtitude);
	}

	@Override
	public void updateVehicleLocation(Integer user_id, Double latitude, Double longtitude) {

		userAndVehicleLocationDao.updateVehicleLocation(user_id, latitude, longtitude);
	}

}
