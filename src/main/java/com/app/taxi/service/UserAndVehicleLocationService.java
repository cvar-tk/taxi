package com.app.taxi.service;

public interface UserAndVehicleLocationService {

	public void updateUserLocation(Integer user_id, Double latitude, Double longtitude);

	public void updateVehicleLocation(Integer user_id, Double latitude, Double longtitude);

}
