package com.app.taxi.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.BackUpCodeDao;
import com.app.taxi.model.BackUpCode;

@Service("BackUpCodeService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class BackUpCodeServiceImpl implements BackUpCodeService {

	@Autowired
	private BackUpCodeDao backUpCodeDao;

	@Override
	public BackUpCode getBackUpCode(Integer user_id) {

		return backUpCodeDao.getBackUpCode(user_id);
	}

	@Override
	public void getnerateBackUpCode(Integer user_id) {

		backUpCodeDao.getnerateBackUpCode(user_id);

	}

}
