package com.app.taxi.service;

import java.math.BigInteger;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.taxi.model.RoleUserMaster;
import com.app.taxi.model.UserMaster;


public interface UserService {

	JSONObject getJSONResultForUserLogin(Double mobile, String password,Integer role);

	Object[] getAdminDetailsByCredential(String emailid, String password, Integer pin);

	void AddNewOwner(UserMaster userMaster);

	boolean checkAccountExistingByRole(BigInteger obj_phone, int role);

	void addBookmark(String book_id, Integer i, String image);

	JSONArray getBookMarksbyBookId(String book_id);

	JSONObject VerifyOwnerOTP(BigInteger obj_phone, String otp);

	void UploadNewPic(Integer integer, String filenamebyfile);

	JSONObject getDetailsBySessionId(String token);

	JSONObject UpdatePasswordByToken(String token, String password);

	Object VerifyDriverOTP(BigInteger obj_phone, String otp);

	void sendOTPByID(String phone, Integer role);

	JSONArray getAssosiatedDriverByOwnerId(Integer owner_id);

	JSONObject requestChangePassword(String email_id);

	void AddNewUser(UserMaster master);

	String getUserNameById(Integer user_id);

	JSONObject VerifyUserOTP(BigInteger obj_phone, String otp);

	void updateEmailAndUsername(Integer user_id, String name, String email);

	void updateUserEmailId(Integer user_id, String email);

	void UpdateUsernameToken(Integer user_id, String username);

	JSONArray getCustomerDetailsList(Integer page, Integer limit);

	Long getTotalCustomerCount();

	boolean checkEmailAlreadyExcist(String email);

	void adminCreateNewRoleManagement(RoleUserMaster userMaster, Integer id);

	JSONArray getMultiRoleUserList(Integer page, Integer limit);

	Long getTotalMultiRoleUserCount();

	JSONObject getMultiRoleDetailsById(Integer id);

	List<Object[]> getMultiRoleMaster();

	void updateRoleUserMasterDetailsById(Integer u_id, String name, BigInteger mobile, Integer security_pin,
			Integer role);

	void DeactiveRoleUserMasterDetailsById(Integer u_id, Integer status);

	boolean checkOwnerVerifiedById(Integer user_id);

	JSONObject VerifyForgetPasswordOTP(BigInteger obj_phone, String otp, Integer role);

	JSONObject getUserProfileDetails(Integer user_id);

	boolean checkCurrentPasswordByID(Integer id, String current_password);

	void updateChangePassword(Integer attribute, String new_password);

	boolean checkCurrentPinByID(Integer id, Integer current_pin);

	void UpdateChangePin(Integer id, Integer new_pin);




}
