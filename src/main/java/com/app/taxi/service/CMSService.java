package com.app.taxi.service;

public interface CMSService {

	String getPrivacyPolicy(Integer role);

	String getTermsAndCondition(Integer role);

	void updatePrivacyPolicyByAdmin(Integer role, String user_privacy);

	void updateTermsAndConditionByAdmin(Integer role, String terms);

}
