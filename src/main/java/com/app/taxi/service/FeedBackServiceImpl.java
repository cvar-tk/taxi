package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.FeedBackDAO;
import com.app.taxi.model.Feedback;

@Service("FeedBackService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FeedBackServiceImpl implements FeedBackService {

	@Autowired
	private FeedBackDAO feedBackDao;

	@Override
	public void AddUserFeedback(Feedback feedback, Integer user_id) {
		
		feedBackDao.AddUserFeedback(feedback,user_id);
		
	}

	
}
