package com.app.taxi.service;

import java.math.BigInteger;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;

public interface DriverService {

	public JSONObject addnewDriverForApproval(UserMaster userMaster, WorkersCredential workersCredential, Integer owner_id);

	public JSONObject getDriverProfileDetails(Integer user_id);

	public JSONArray getNewDrivers(Integer page, Integer limit);
	
	Long getTotalCountNewDrivers();

	public JSONObject getDriverDetailsByDriverId(Integer driver_id);

	void updateDriverDetails(Integer driver_id, String driver_name, Date expiry, Date dob1, String license_no);

	public void updatedriverStatus(String reason, String comments, Integer driver_id, Integer status);

	public JSONArray getApprovedDriverList(Integer page, Integer limit);

	public Long getTotalCountApprovedDrivers();

	public JSONObject getDriverDetailsByLicenseNo(String license_number);

	public JSONArray filterByDriverDate(Date fromdate, Date todate, Integer pageIndex);

	Long getTotalCountApprovedDriverDate(Date fromdate, Date todate, Integer pageIndex);

	JSONObject ChangeDriverStatusToOnline(Integer user_id);
	
	JSONObject ChangeDriverStatusToOffline(Integer user_id);

	public JSONObject selectCarByDriver(Integer user_id, Integer vehicle_id);

	Boolean checkDrvierAlreadyPickedAnyVehicle(Integer driver_id);

	public JSONObject getDriverCurrentStatusById(Integer user_id);

	public JSONObject getDriverONOFFStatusById(Integer user_id);

	public void updateExpiryDateByAdmin(Integer driver_id, Date expiry);

	public void ChangeDriverStatusToDisable(Integer check_status, Integer driver_id);

	public JSONObject getUserLocationRequest(Integer user_id);

	public JSONObject changeDriverSelfDisableStatus(Integer user_id);

	JSONObject updateBaseKmDetails(Integer user_id, Double base_km, Double base_km_price, Double per_min_price,
			Double per_km_price);
	
	public JSONObject startTrip(JSONObject request,Integer user_id);

	public JSONObject EndTrip(JSONObject request, Integer user_id);

	public JSONObject deselectCarByDriverId(Integer user_id, Integer vehicle_id);

	public JSONObject addExcistingDriverToOwner(BigInteger mobile, Integer user_id);

	public JSONObject getDriverTripHistory(Integer user_id);

	public JSONObject checkCurrentPasswordByID(Integer user_id, String password);

	public JSONObject getOndemandPlanDetailaById(Integer user_id);

	public JSONObject getManyTripLocationByLatitude(Integer user_id, Double latitude, Double longtitude);

	public void adminTerminateTripByTripId(Integer trip_id, Integer userid);




}
