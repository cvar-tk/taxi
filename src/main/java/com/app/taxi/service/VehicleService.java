package com.app.taxi.service;

import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleType;
import com.google.gson.JsonElement;


public interface VehicleService {

	void addNewVehicleforApproval(VehicleMaster vehicleMaster,Integer user_id);

	JSONArray getNewApprovalVehicles(Integer page, Integer limit);

	Long getTotalCountNewApprovalVehicles();

	JSONArray getApprovedVehicles(Integer page, Integer limit);

	JSONObject getVehicleRequestById(Integer car_id);

	JSONArray getAllVehicleBrands();

	JSONArray getVehicleModelsByMakeID(Integer make_id);

	void upDateVehicleStatusById(Integer vehicle_id, String reason, String comments, Integer status);

	void updateVehicleAfterApproval(VehicleMaster master);

	JSONArray getVehicleDetailsByOwnerId(Integer user_id);

	JSONObject getApprovedVehicleByID(Integer car_id);

	JSONArray filterByVehicleDate(Date fromdate, Date todate, Integer pageIndex);

	JSONObject getVechicleDetailsByVechicleNo(String vehicle_number);

	Long getTotalCountApprovedVehicles();

	Long getFilterTotalCountApprovedVehicles(Date fromdate, Date todate, Integer pageIndex);

	boolean checkVehicleSubscriptionStatusByVehicleId(Integer vehicle_id, Integer type);

	List<Object[]> getVehicleType();

	void updateVehicleInsuranceExpiryDate(Date insurance_expiry, Integer vehicle_id);



	JSONObject getVehicleDetailsByVehicleId(Integer vehicle_id, Double latitude, Double longtitude);

	List<Object[]> getVehicleColour();

	JSONObject getCarDetailsByCarId(Integer car_id);



}
