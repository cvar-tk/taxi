package com.app.taxi.service;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.LocationDao;

@Service("LocationService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationDao locationDao;

	@Override
	public JSONArray getNearestVehicles(Double latitude, Double longtitude, Integer type) {

		return locationDao.getNearestVehicles(latitude, longtitude, type);
	}

	@Override
	public JSONArray getNearestVehiclesDetails(Double latitude, Double longtitude, Integer type) {

		return locationDao.getNearestVehiclesDetails(latitude, longtitude, type);
	}
}
