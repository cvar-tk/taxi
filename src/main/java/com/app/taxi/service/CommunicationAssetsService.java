package com.app.taxi.service;

public interface CommunicationAssetsService {

	public void updateFCMToken(String fcm_token,Integer user_id);

}
