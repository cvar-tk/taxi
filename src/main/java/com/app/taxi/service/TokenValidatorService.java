package com.app.taxi.service;

public interface TokenValidatorService {

	public Integer getUserIdByToken(String token);
	
}
