package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.CommunicationAssetsDao;

@Service("CommunicationAssetsService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CommunicationAssetsServiceImpl implements CommunicationAssetsService{

	@Autowired
	private CommunicationAssetsDao communicationAssetsDao;
	
	
	@Override
	public void updateFCMToken(String fcm_token,Integer id) {

		communicationAssetsDao.updateFCMToken(fcm_token,id);
	}

	
	
	
}
