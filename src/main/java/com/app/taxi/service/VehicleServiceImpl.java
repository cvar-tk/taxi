package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.VehicleDao;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleType;

import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

@Service("VehicleService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleDao vehicleDao;

	@Override
	public void addNewVehicleforApproval(VehicleMaster vehicleMaster, Integer user_id) {

		vehicleDao.addNewVehicleForApproval(vehicleMaster, user_id);
	}

	@Override
	public JSONArray getNewApprovalVehicles(Integer page, Integer limit) {

		return vehicleDao.getNewApprovalVehicles(page, limit);
	}

	@Override
	public Long getTotalCountNewApprovalVehicles() {

		return vehicleDao.getNewApprovalVehicles();
	}

	@Override
	public JSONArray getApprovedVehicles(Integer page, Integer limit) {
		return vehicleDao.getApprovedVehicles(page, limit);
	}

	@Override
	public Long getTotalCountApprovedVehicles() {
		return vehicleDao.getTotalCountApprovedVehicles();
	}

	@Override
	public JSONObject getVehicleRequestById(Integer car_id) {

		return vehicleDao.getVehicleRequestById(car_id);
	}

	@Override
	public JSONArray getAllVehicleBrands() {

		return vehicleDao.getAllVehicleBrands();
	}

	@Override
	public JSONArray getVehicleModelsByMakeID(Integer make_id) {

		return vehicleDao.getVehicleModelsByMakeID(make_id);
	}

	@Override
	public void upDateVehicleStatusById(Integer vehicle_id,  String reason, String comments, Integer status) {

		vehicleDao.upDateVehicleStatusById(vehicle_id, reason,comments,status);

	}

	@Override
	public void updateVehicleAfterApproval(VehicleMaster master) {

		vehicleDao.updateVehicleAfterApproval(master);
	}

	@Override
	public JSONArray getVehicleDetailsByOwnerId(Integer user_id) {

		return vehicleDao.getVehicleDetailsByOwnerId(user_id);
	}

	@Override
	public JSONObject getApprovedVehicleByID(Integer car_id) {

		return vehicleDao.getApprovedVehicleByID(car_id);
	}

	@Override
	public JSONArray filterByVehicleDate(Date fromdate, Date todate ,  Integer pageIndex) {
		
		return vehicleDao.filterByVehicleDate(fromdate , todate , pageIndex);
	}

	@Override
	public JSONObject getVechicleDetailsByVechicleNo(String vehicle_number) {
		
		return vehicleDao.getVechicleDetailsByVechicleNo(vehicle_number);
	}

	@Override
	public Long getFilterTotalCountApprovedVehicles(Date fromdate, Date todate, Integer pageIndex) {

		return vehicleDao.getFilterTotalCountApprovedVehicles(fromdate,todate,pageIndex);
	}

	@Override
	public boolean checkVehicleSubscriptionStatusByVehicleId(Integer vehicle_id,Integer type) {

		return vehicleDao.checkVehicleSubscriptionStatusByVehicleId(vehicle_id,type);
	}

	@Override
	public List<Object[]> getVehicleType() {
		
		return vehicleDao.getVehicleType();
	}

	@Override
	public void updateVehicleInsuranceExpiryDate(Date insurance_expiry, Integer vehicle_id) {
		
		 vehicleDao.updateVehicleInsuranceExpiryDate(insurance_expiry,vehicle_id);
		
	}

	
	@Override
	public JSONObject getVehicleDetailsByVehicleId(Integer vehicle_id, Double latitude, Double longtitude) {
		return vehicleDao.getVehicleDetailsByVehicleId(vehicle_id,latitude,longtitude);
	}

	@Override
	public List<Object[]> getVehicleColour() {
		
		return vehicleDao.getVehicleColour();
	}

	@Override
	public JSONObject getCarDetailsByCarId(Integer car_id) {
		
		return vehicleDao.getCarDetailsByCarId(car_id);
	}

}
