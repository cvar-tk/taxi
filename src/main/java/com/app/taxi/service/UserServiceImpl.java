package com.app.taxi.service;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.UserDao;
import com.app.taxi.model.RoleUserMaster;
import com.app.taxi.model.UserMaster;

@Service("userService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao UserDao;

	@Override
	public JSONObject getJSONResultForUserLogin(Double mobile, String password, Integer role) {

		return UserDao.getJSONResultForUserLogin(mobile, password, role);
	}

	@Override
	public Object[] getAdminDetailsByCredential(String emailid, String password, Integer pin) {

		return UserDao.getAdminDetailsByCredential(emailid, password, pin);
	}

	@Override
	public void AddNewOwner(UserMaster userMaster) {

		UserDao.AddNewOwner(userMaster);

	}

	@Override
	public boolean checkAccountExistingByRole(BigInteger obj_phone, int role) {

		return UserDao.checkAccountExistingByRole(obj_phone, role);
	}

	@Override
	public void addBookmark(String book_id, Integer page_id, String image) {

		UserDao.addBookmark(book_id, page_id, image);

	}

	@Override
	public JSONArray getBookMarksbyBookId(String book_id) {
		return UserDao.getBookMarksbyBookId(book_id);
	}

	@Override
	public JSONObject VerifyOwnerOTP(BigInteger mobile, String otp) {
		return UserDao.VerifyOwnerOTP(mobile, otp);
	}

	@Override
	public void UploadNewPic(Integer id, String filenamebyfile) {
		UserDao.UploadNewPic(id, filenamebyfile);

	}

	@Override
	public JSONObject getDetailsBySessionId(String token) {

		return UserDao.getDetailsBySessionId(token);
	}

	@Override
	public JSONObject UpdatePasswordByToken(String token, String password) {
		// TODO Auto-generated method stub
		return UserDao.UpdatePasswordByToken(token, password);
	}

	@Override
	public Object VerifyDriverOTP(BigInteger obj_phone, String otp) {

		return UserDao.VerifyDriverOTP(obj_phone, otp);
	}

	@Override
	public void sendOTPByID(String phone, Integer role) {
		UserDao.sendOTPByPhone(phone, role);

	}

	@Override
	public JSONArray getAssosiatedDriverByOwnerId(Integer owner_id) {
		// TODO Auto-generated method stub
		return UserDao.getAssosiatedDriverByOwnerId(owner_id);
	}

	@Override
	public JSONObject requestChangePassword(String email_id) {

		return UserDao.requestChangePassword(email_id);
	}

	@Override
	public void AddNewUser(UserMaster master) {

		UserDao.AddNewUser(master);
	}

	@Override
	public String getUserNameById(Integer user_id) {

		return UserDao.getUserNameById(user_id);
	}

	@Override
	public JSONObject VerifyUserOTP(BigInteger obj_phone, String otp) {

		return UserDao.VerifyUserOTP(obj_phone, otp);
	}

	@Override
	public void updateEmailAndUsername(Integer user_id, String name, String email) {
		UserDao.updateEmailAndUsername(user_id, name, email);

	}

	@Override
	public void updateUserEmailId(Integer user_id, String email) {

		UserDao.updateUserEmailId(user_id, email);
	}

	@Override
	public void UpdateUsernameToken(Integer user_id, String username) {
		 UserDao.UpdateUsernameToken(user_id, username);
	}

	@Override
	public JSONArray getCustomerDetailsList(Integer page, Integer limit) {

		
		return UserDao.getCustomerDetailsList(page,limit);
	}

	@Override
	public Long getTotalCustomerCount() {

		return UserDao.getTotalCustomerCount();
	}

	@Override
	public boolean checkEmailAlreadyExcist(String email) {
		
		return UserDao.checkEmailAlreadyExcist(email);
	}

	@Override
	public void adminCreateNewRoleManagement(RoleUserMaster userMaster, Integer id) {

		UserDao.adminCreateNewRoleManagement(userMaster,id);
	}

	@Override
	public JSONArray getMultiRoleUserList(Integer page, Integer limit) {
		
		return UserDao.getMultiRoleUserList(page,limit);
	}

	@Override
	public Long getTotalMultiRoleUserCount() {

		return UserDao.getTotalMultiRoleUserCount();
	}

	@Override
	public JSONObject getMultiRoleDetailsById(Integer id) {
		
		return UserDao.getMultiRoleDetailsById(id);
	}

	@Override
	public List<Object[]> getMultiRoleMaster() {
	
		return UserDao.getMultiRoleMaster();
	}

	@Override
	public void updateRoleUserMasterDetailsById(Integer u_id, String name, BigInteger mobile, Integer security_pin,
			Integer role) {
		
		 UserDao.updateRoleUserMasterDetailsById(u_id,name,mobile,security_pin,role);
		
	}

	@Override
	public void DeactiveRoleUserMasterDetailsById(Integer u_id, Integer status) {
		
		UserDao.DeactiveRoleUserMasterDetailsById(u_id,status);
		
		
	}

	@Override
	public boolean checkOwnerVerifiedById(Integer user_id) {
	
		return UserDao.checkOwnerVerifiedById(user_id);
	}

	@Override
	public JSONObject VerifyForgetPasswordOTP(BigInteger obj_phone, String otp,Integer role) {
		
		return UserDao.VerifyForgetPasswordOTP(obj_phone,otp,role);
	}

	@Override
	public JSONObject getUserProfileDetails(Integer user_id) {
		
		return UserDao.getUserProfileDetails(user_id);
	}

	@Override
	public boolean checkCurrentPasswordByID(Integer id, String current_password) {
		
		return UserDao.checkCurrentPasswordByID(id,current_password);
	}

	@Override
	public void updateChangePassword(Integer id, String new_password) {
		
		UserDao.updateChangePassword(id,new_password);
		
	}

	@Override
	public boolean checkCurrentPinByID(Integer id, Integer current_pin) {
		
		return UserDao.checkCurrentPinByID(id,current_pin);
		
	}

	@Override
	public void UpdateChangePin(Integer id, Integer new_pin) {
		
        UserDao.UpdateChangePin(id , new_pin);
		
	}



}
