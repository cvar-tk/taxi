package com.app.taxi.service;

import org.json.JSONObject;

public interface CallbackService {

	JSONObject addCallbackByVehicleId(Integer vehicle_id, Integer user_id, String address, Double latitude, Double longtitude);

	
}
