package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.CMSDao;

@Service("CMSService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class CMSServiceImpl implements CMSService {

	@Autowired
	private CMSDao cmsDao; 
	
	@Override
	public String getPrivacyPolicy(Integer role) {

		return cmsDao.getPrivacyPolicy(role);
	}

	@Override
	public String getTermsAndCondition(Integer role) {

		return cmsDao.getTermsAndCondition(role);
	}

	@Override
	public void updatePrivacyPolicyByAdmin(Integer role, String user_privacy) {
		
		cmsDao.updatePrivacyPolicyByAdmin(role,user_privacy);
		
	}

	@Override
	public void updateTermsAndConditionByAdmin(Integer role, String terms) {
	
		cmsDao.updateTermsAndConditionByAdmin(role,terms);
		
	}

}
