package com.app.taxi.service;

import java.util.List;

public interface PushNotificationService {

	
	void sendPushNotification(List<String> keys, String messageTitle, String message);
}
