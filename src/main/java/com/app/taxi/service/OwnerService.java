package com.app.taxi.service;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public interface OwnerService {

	JSONObject getOwnerProfileDetails(Integer user_id);

	JSONObject getCarTripHistoryByDate(Integer user_id, Integer car_id, Date date1);

	JSONObject ViewTripHistoryByTripId(Integer trip_id);

	JSONObject AdminViewTripHistoryByTripId(Integer trip_id);

	JSONObject getCarSubscriberPlanDetails(Integer user_id, Integer car_id);

	JSONObject updateCarSubscriberAutoRenewal(Integer user_id, Integer car_id, Integer plan_id, Integer auto_renewal);

	void UpdateAutorenewalByMIN();

	JSONArray getOwnerList(Integer page, Integer limit);

	Long getTotalCountOfOwners();

 
}
