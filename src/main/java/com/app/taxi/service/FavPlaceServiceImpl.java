package com.app.taxi.service;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.FavPlaceDao;
import com.app.taxi.model.FavPlace;

@Service("FavPlaceService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class FavPlaceServiceImpl implements FavPlaceService {

	@Autowired
	private FavPlaceDao favPlaceDao;

	@Override
	public void addPlace(FavPlace favPlace, Integer user_id) {

		favPlaceDao.addPlace(favPlace, user_id);
	}

	@Override
	public JSONArray getFavListById(Integer user_id) {

		return favPlaceDao.getFavListById(user_id);
	}

	@Override
	public void deleteFavPlace(Integer user_id, Integer place_id) {

		favPlaceDao.deleteFavPlace(user_id, place_id);
	}

}
