package com.app.taxi.service;


import com.app.taxi.model.BackUpCode;

public interface BackUpCodeService {

	BackUpCode getBackUpCode(Integer user_id);


	void getnerateBackUpCode(Integer user_id);

}
