package com.app.taxi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.TokenValidatorDao;

@Service("tokenValidatorService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TokenValidatorServiceimpl  implements TokenValidatorService{

	@Autowired
	private TokenValidatorDao tokenValidatorDao; 
	
	
	@Override
	public Integer getUserIdByToken(String token) {

		return tokenValidatorDao.getUserIdByToken(token);
	}

}
