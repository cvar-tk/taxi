package com.app.taxi.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.dao.PlanDao;
import com.app.taxi.model.PlanMaster;

@Service("PlanService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PlanServiceImpl implements PlanService {

	@Autowired
	private PlanDao planDao;

	@Override
	public void addNewPlan(PlanMaster planMaster, Integer type_id) {
		planDao.addNewPlan(planMaster, type_id);
	}

	@Override
	public JSONArray getAllDemandPlans() {

		return planDao.getAllDemandPlans();
	}

	@Override
	public JSONObject changePlanStatusById(Integer id) {

		return planDao.changePlanStatusById(id);
	}

	@Override
	public JSONArray getAllVehiclePlans() {
		return planDao.getAllVehiclePlans();
	}

	@Override
	public JSONArray getActiveRunningPlanList() {

		return planDao.getActiveRunningPlanList();
	}

	@Override
	public JSONArray getActiveDemandPlanList() {

		return planDao.getActiveDemandPlanList();
	}

	@Override
	public JSONObject subscribeRunningPlan(Integer user_id, Integer plan_id, Integer vehicle_id) {

		return planDao.subscribeRunningPlan(user_id, plan_id, vehicle_id);
	}

	@Override
	public JSONObject getPlanDetailsById(Integer plan_id) {
		
		return planDao.getPlanDetailsById(plan_id);
	}

	@Override
	public void EditPlanDetailsById(PlanMaster planMaster, Integer plan_id) {
		
		planDao.EditPlanDetailsById(planMaster,plan_id);
		
	}

	@Override
	public void EditOnDemandPlanDetailsById(PlanMaster planMaster, Integer plan_id) {
		
		planDao.EditOnDemandPlanDetailsById(planMaster,plan_id);
		
	}

}
