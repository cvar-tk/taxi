package com.app.taxi;

import java.util.Calendar;
import java.util.TimeZone;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.google.gson.GsonBuilder;

@EnableAutoConfiguration
@Configuration
@EnableScheduling
public class Config {

	@Bean
	public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {

		return hemf.getSessionFactory();

	}

	@Bean
	public GsonBuilder gsonBuilder() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
		gsonBuilder.serializeNulls();
		return gsonBuilder;
	}

	@Bean
	public Calendar calendar() {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		return calendar;
	}

	

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {

		return new BCryptPasswordEncoder();
	}

	
}
