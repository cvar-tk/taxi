package com.app.taxi;

import org.json.JSONException;
import org.json.JSONObject;

public class Commons {

	public String APP_TECH_ERROR() {

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Something went wrong..! Try after sometime.");

			return jsonObject.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
