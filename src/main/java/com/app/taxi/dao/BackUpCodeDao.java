package com.app.taxi.dao;


import com.app.taxi.model.BackUpCode;

public interface BackUpCodeDao {

	public BackUpCode getBackUpCode(Integer user_id);
	
	public void getnerateBackUpCode(Integer user_id);


}
