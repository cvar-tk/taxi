package com.app.taxi.dao;

public interface UserAndVehicleLocationDao {

	void updateUserLocation(Integer user_id, Double latitude, Double longtitude);

	void updateVehicleLocation(Integer user_id, Double latitude, Double longtitude);
	
}
