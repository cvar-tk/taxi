package com.app.taxi.dao;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.app.taxi.SingleTon;
import com.app.taxi.model.PlanMaster;
import com.app.taxi.model.PlanTypeMaster;
import com.app.taxi.model.SubscribedList;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleMaster;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

@Repository("PlanDao")
@Transactional
@Component
public class PlanDaoImpl implements PlanDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GsonBuilder gsonBuilder;



	@Override 
	public void addNewPlan(PlanMaster planMaster, Integer type_id) {

		planMaster.setPt_type(sessionFactory.getCurrentSession().get(PlanTypeMaster.class, type_id));

		sessionFactory.getCurrentSession().save(planMaster);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONArray getAllDemandPlans() {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT pm_id,pm_title,pm_price,pm_comments,pm_demandtime_duration,status FROM PlanMaster WHERE pt_type.pt_id = :plantype AND status !=:status ORDER BY pm_id DESC");

		query.setParameter("plantype", 2);
		query.setParameter("status", 0);

		List<Object[]> list = query.list();

		try {

			for (Object[] object : list) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put(SingleTon.ID, object[0]);
				jsonObject.put(SingleTon.TITLE, object[1]);
				jsonObject.put(SingleTon.PRICE, object[2]);
				jsonObject.put(SingleTon.COMMENTS, object[3]);
				jsonObject.put(SingleTon.DURATION, object[4]);
				jsonObject.put(SingleTon.CONTENT_STATUS, object[5]);
				jsonArray.put(jsonObject);

			}

		} catch (JSONException e) {

			e.printStackTrace();

			return jsonArray;

		}

		return jsonArray;
	}

	@Override
	public JSONArray getAllVehiclePlans() {
		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT pm_id,pm_title,pm_price,pm_comments,pm_topup_duration,status FROM PlanMaster WHERE pt_type.pt_id = :plantype AND status !=:status ORDER BY pm_id DESC");

		query.setParameter("plantype", 1);
		query.setParameter("status", 0);

		List<Object[]> list = query.list();

		try {

			for (Object[] object : list) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put(SingleTon.ID, object[0]);
				jsonObject.put(SingleTon.TITLE, object[1]);
				jsonObject.put(SingleTon.PRICE, object[2]);
				jsonObject.put(SingleTon.COMMENTS, object[3]);
				jsonObject.put(SingleTon.DURATION, object[4]);
				jsonObject.put(SingleTon.CONTENT_STATUS, object[5]);
				jsonArray.put(jsonObject);

			}

		} catch (JSONException e) {

			e.printStackTrace();

			return jsonArray;

		}

		return jsonArray;
	}

	@Override
	public JSONObject changePlanStatusById(Integer id) {

		JSONObject jsonObject = new JSONObject();

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		PlanMaster planMaster = session.get(PlanMaster.class, id);

		if (planMaster == null) {
			tx.commit();
			session.close();
			return SingleTon.APP_TECH_ERROR(null);
		}

		Integer status = planMaster.getStatus();

		if (status == 1) {

			planMaster.setStatus(2);

			try {
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan De - Activated Successfully..!");
				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (status == 2) {

			planMaster.setStatus(1);

			try {
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Activated Successfully..!");
				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		session.update(planMaster);

		tx.commit();
		session.close();

		return jsonObject;
	}

	@Override
	public JSONArray getActiveRunningPlanList() {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT pm_id,pm_title,pm_price,pm_comments,pm_topup_duration,status FROM PlanMaster WHERE pt_type.pt_id = :plantype AND status !=:status ORDER BY pm_id DESC");

		query.setParameter("plantype", 1);
		query.setParameter("status", 0);

		List<Object[]> list = query.list();

		try {

			for (Object[] object : list) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put(SingleTon.ID, object[0]);
				jsonObject.put(SingleTon.TITLE, object[1]);
				jsonObject.put(SingleTon.PRICE, object[2]);
				jsonObject.put(SingleTon.COMMENTS, object[3]);
				jsonObject.put(SingleTon.DURATION, object[4]);
				jsonObject.put(SingleTon.CONTENT_STATUS, object[5]);
				jsonArray.put(jsonObject);

			}

		} catch (JSONException e) {

			e.printStackTrace();

			return jsonArray;

		}

		return jsonArray;
	}

	@Override
	public JSONArray getActiveDemandPlanList() {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT pm_id,pm_title,pm_price,pm_comments,pm_demandtime_duration,status FROM PlanMaster WHERE pt_type.pt_id = :plantype AND status !=:status ORDER BY pm_id DESC");

		query.setParameter("plantype", 2);
		query.setParameter("status", 0);

		List<Object[]> list = query.list();

		try {

			for (Object[] object : list) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put(SingleTon.ID, object[0]);
				jsonObject.put(SingleTon.TITLE, object[1]);
				jsonObject.put(SingleTon.PRICE, object[2]);
				jsonObject.put(SingleTon.COMMENTS, object[3]);
				jsonObject.put(SingleTon.DURATION, object[4]);
				jsonObject.put(SingleTon.CONTENT_STATUS, object[5]);
				jsonArray.put(jsonObject);

			}

		} catch (JSONException e) {

			e.printStackTrace();

			return jsonArray;

		}

		return jsonArray;
	}

	@Override
	public JSONObject subscribeRunningPlan(Integer user_id, Integer plan_id, Integer vehicle_id) {

		JSONObject jsonObject = new JSONObject();
		try {

			PlanMaster planMaster = sessionFactory.getCurrentSession().get(PlanMaster.class, plan_id);

			// checking the plan is exist
			if (planMaster == null || planMaster.getStatus() != 1) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Master is Empty.!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject;

				
			}

			Query query_role = sessionFactory.getCurrentSession()
					.createQuery("SELECT u_role.rm_id from UserMaster where u_id = :user_id");

			query_role.setParameter("user_id", user_id);

			Integer role = (Integer) query_role.uniqueResult();
			
			System.err.println(role);

			// checking the request from owner role or not
			if (role == null  ) {

				return SingleTon.APP_TECH_ERROR(null);

			}
			
			
			VehicleMaster vehicleMaster = sessionFactory.getCurrentSession().get(VehicleMaster.class, vehicle_id);
			
			if(vehicleMaster.getVhm_status() == 1) {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Car Not Approved Yet..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;
				
			}
			
			
			if(role == 2) {

				
				
			// Checking vehicle in the owner repo

			Query query_vehicle_owner = sessionFactory.getCurrentSession()
					.createQuery("SELECT vhm_owner.u_id from VehicleMaster where vhm_id = :vhm_id");

			query_vehicle_owner.setParameter("vhm_id", vehicle_id);

			Integer vehicle_owner_id = (Integer) query_vehicle_owner.uniqueResult();

			if (vehicle_owner_id == null || !vehicle_owner_id.equals(user_id)) {

				return SingleTon.APP_TECH_ERROR(null);


			}
			
			Query subcribe_vquery = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_vehicle_master.vhm_id= :v_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
			
			subcribe_vquery.setParameter("v_id", vehicle_id);
			subcribe_vquery.setParameter("type", 1);
			
			subcribe_vquery.setMaxResults(1);
			
            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));

			
			SubscribedList subscribedVehicle = (SubscribedList) subcribe_vquery.uniqueResult();
			
		

			if(subscribedVehicle == null || subscribedVehicle.getTo_date().before(calendar1.getTime())) {
			
			SubscribedList subscribedList = new SubscribedList();

			subscribedList.setSl_price(planMaster.getPm_price());
			subscribedList.setSl_type(planMaster.getPt_type());
			subscribedList.setSl_vehicle_master(
					(VehicleMaster) sessionFactory.getCurrentSession().get(VehicleMaster.class, vehicle_id));
			
		
			subscribedList.setSl_duration(planMaster.getPm_topup_duration());
		
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

			subscribedList.setFrom_date(calendar.getTime());

			calendar.add(Calendar.DATE, planMaster.getPm_topup_duration());

			subscribedList.setTo_date(calendar.getTime());
			subscribedList.setSl_auto(0);

			sessionFactory.getCurrentSession().save(subscribedList);

			

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success.!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;
			
			}else {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "You have a Plan already..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;
			
			}
			
			
			}else {
				
	            Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
				
				Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_driver.u_id= :driver_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
				
				subcribe_query.setParameter("driver_id", user_id);
				subcribe_query.setParameter("type", 2);

				subcribe_query.setMaxResults(1);
				
				SubscribedList subscribedList1 = (SubscribedList) subcribe_query.uniqueResult();
				
				
				if(subscribedList1 == null || subscribedList1.getTo_date().before(calendar1.getTime())) {
				
				
				SubscribedList subscribedList = new SubscribedList();

				subscribedList.setSl_price(planMaster.getPm_price());
				subscribedList.setSl_type(planMaster.getPt_type());
				
				subscribedList.setSl_duration(planMaster.getPm_demandtime_duration());
				
				subscribedList.setSl_driver((UserMaster) sessionFactory.getCurrentSession().get(UserMaster.class, user_id));
			
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

				subscribedList.setFrom_date(calendar.getTime());

				calendar.add(Calendar.MINUTE, planMaster.getPm_demandtime_duration());

				subscribedList.setTo_date(calendar.getTime());

				sessionFactory.getCurrentSession().save(subscribedList);

				

				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success.!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				return jsonObject;
				
				}else {
					
					jsonObject.put(SingleTon.CONTENT_STATUS, false);
					jsonObject.put(SingleTon.CONTENT_MESSAGE, "You have Plan a already..!");

					jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

					return jsonObject;
					
				}
				
				
			}

		} catch (JSONException e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public JSONObject getPlanDetailsById(Integer plan_id) {
		
		PlanMaster planMaster = sessionFactory.getCurrentSession().get(PlanMaster.class, plan_id);
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			
			jsonObject.put("plan_id", planMaster.getPm_id());
			jsonObject.put("plan_title", planMaster.getPm_title());
			jsonObject.put("plan_price", planMaster.getPm_price());
			jsonObject.put("plan_comments", planMaster.getPm_comments());
			jsonObject.put("plan_demand_duration", planMaster.getPm_demandtime_duration());
			jsonObject.put("plan_totup_duration", planMaster.getPm_topup_duration());
			jsonObject.put("plan_price", planMaster.getPm_price());
			jsonObject.put("plan_type", planMaster.getPt_type().getPt_title());
			jsonObject.put("plan_type_id", planMaster.getPt_type().getPt_id());
			jsonObject.put("status", planMaster.getStatus());


		}catch (Exception e) {

		 e.printStackTrace();
		}
	
		return jsonObject;
	}

	@Override
	public void EditPlanDetailsById(PlanMaster planMaster, Integer plan_id) {
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		PlanMaster planMaster2 = session.get(PlanMaster.class, plan_id);
		
		planMaster2.setPm_price(planMaster.getPm_price());
		planMaster2.setPm_comments(planMaster.getPm_comments());
		//planMaster2.setPm_demandtime_duration(planMaster.getPm_demandtime_duration());
		planMaster2.setPm_title(planMaster.getPm_title());
		planMaster2.setPm_topup_duration(planMaster.getPm_topup_duration());
        planMaster2.setStatus(planMaster.getStatus());		
        
        session.update(planMaster2);

        tx.commit();
        session.close();
        
	}

	@Override
	public void EditOnDemandPlanDetailsById(PlanMaster planMaster, Integer plan_id) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		PlanMaster planMaster2 = session.get(PlanMaster.class, plan_id);
		
		planMaster2.setPm_price(planMaster.getPm_price());
		planMaster2.setPm_comments(planMaster.getPm_comments());
		planMaster2.setPm_demandtime_duration(planMaster.getPm_demandtime_duration());
		planMaster2.setPm_title(planMaster.getPm_title());
		//planMaster2.setPm_topup_duration(planMaster.getPm_topup_duration());
        planMaster2.setStatus(planMaster.getStatus());		
        
        session.update(planMaster2);

        tx.commit();
        session.close();
        
		
	}

	
	
	
	
}
