package com.app.taxi.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.AdminControl;
import com.app.taxi.model.Feedback;
import com.app.taxi.model.SupportCMS;
import com.app.taxi.model.TripMaster;
import com.app.taxi.model.UserMaster;

@Repository("FeedBackDAO")
@Transactional
@Component
public class FeedBackDAOImpl implements FeedBackDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void AddUserFeedback(Feedback feedback, Integer user_id) {
		
		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);
		
		feedback.setUser(userMaster);
		
		sessionFactory.getCurrentSession().save(feedback);
		
		
	}

	
}
