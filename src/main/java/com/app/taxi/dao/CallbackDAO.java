package com.app.taxi.dao;

import org.json.JSONObject;

public interface CallbackDAO {

	JSONObject addCallbackByVehicleId(Integer vehicle_id, Integer user_id, String address, Double latitude, Double longtitude);

}
