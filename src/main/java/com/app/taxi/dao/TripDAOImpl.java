package com.app.taxi.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.AdminControl;
import com.app.taxi.model.SupportCMS;
import com.app.taxi.model.TripMaster;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleLocation;

@Repository("TripDAO")
@Transactional
@Component
public class TripDAOImpl implements TripDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public JSONObject getTripControlDetails(Integer id) {

		JSONObject jsonObject = new JSONObject();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT ctrl_id,base_km_control,base_km,per_min_price,gst,cgst,sgst FROM AdminControl WHERE ctrl_id = :id");

		query.setParameter("id", id);

		List<Object[]> obj = query.list();

		try {

			Object[] object = obj.get(0);

			jsonObject.put("ctrl_id", object[0]);
			jsonObject.put("base_control", object[1]);
			jsonObject.put("base_km", object[2]);
			jsonObject.put("per_min_price", object[3]);
			jsonObject.put("gst", object[4]);
			jsonObject.put("cgst", object[5]);
			jsonObject.put("sgst", object[6]);

			
			
			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void AdminUpdateTripControlDetails(Double base_control, Integer status) {

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		AdminControl adminControl = session.get(AdminControl.class, 1);

		if (status == 1) {
			
			int base = base_control.intValue();
			

			adminControl.setBase_km_control(base);

		}

		if (status == 2) {

			adminControl.setBase_km(base_control);

		}
		if (status == 3) {

			adminControl.setPer_min_price(base_control);
		}
		
		if (status == 5) {

			adminControl.setCgst(base_control);
		}
		
		if (status == 6) {

			adminControl.setSgst(base_control);
		}
		
		
		if (status == 4) {

			adminControl.setGst(base_control);
		}

		tx.commit();
		session.close();
		
	}
	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}

	@Override
	public JSONObject getUserTripDetailsByUserId(Integer user_id, Integer pageIndex) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			
			JSONArray jsonArray = new JSONArray();
			
			Query query = sessionFactory.getCurrentSession().createQuery("SELECT tr_id,customer.u_name,vehicle.vhm_number,vehicle.v_type.vt_text,driver.u_name,driver.u_pic,starting_point,ending_point,start_time,amount FROM TripMaster WHERE customer.u_id= :id and trip_status.ts_id= :status ORDER BY tr_id DESC");
			
			query.setMaxResults(10);
			query.setFirstResult(calculateOffset(pageIndex, 10));
			query.setParameter("id", user_id);
			query.setParameter("status", 2);

			List<Object[]> obj = query.list();
			
			for (Object[] objects : obj) {
				
				JSONObject jsonObject2 = new JSONObject();
				
				jsonObject2.put("trip_id", objects[0]);
				jsonObject2.put("customer_name", objects[1]);
				jsonObject2.put("vehicle_number", objects[2]);
				jsonObject2.put("vehicle_type", objects[3]);
				jsonObject2.put("driver_name", objects[4]);
				jsonObject2.put("driver_pic", SingleTon.IMAGE_PATH+objects[5]);
				jsonObject2.put("starting_point", objects[6]);
				jsonObject2.put("ending_point", objects[7]);
				jsonObject2.put("start_time", objects[8]);
				jsonObject2.put("amount", objects[9]);

				jsonArray.put(jsonObject2);

			}
			
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");
			jsonObject.put("data", jsonArray);
			jsonObject.put("pageIndex", pageIndex);
			
			return jsonObject;
			
		}catch (Exception e) {

		  e.printStackTrace();
		  
		  return SingleTon.APP_TECH_ERROR(null);
		
		}
		
		
		
	}

	@Override
	public JSONArray getTripMasterList(Integer page, Integer limit) {
		
		JSONArray jsonArray = new JSONArray();
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT tr_id,customer.u_name,vehicle.vhm_number,driver.u_name,driver.m_phone,start_time,end_time,amount,payment_method,trip_status.ts_id,trip_status.ts_title FROM TripMaster ORDER BY tr_id DESC");
		
		query.setMaxResults(limit);
		query.setFirstResult(calculateOffset(page, limit));
		
		
		List<Object[]> obj = query.list();
		try {
		for (Object[] objects : obj) {
			
			JSONObject jsonObject2 = new JSONObject();
			
			jsonObject2.put("trip_id", objects[0]);
			jsonObject2.put("customer_name", objects[1]);
			jsonObject2.put("vehicle_number", objects[2]);
			jsonObject2.put("driver_name", objects[3]);
			jsonObject2.put("driver_phone", objects[4]);
			jsonObject2.put("start_time", objects[5]);
			jsonObject2.put("end_time", objects[6]);
			jsonObject2.put("amount", objects[7]);
			jsonObject2.put("payment_type", objects[8]);
			jsonObject2.put("status_id", objects[9]);
			jsonObject2.put("status", objects[10]);

			jsonArray.put(jsonObject2);

		}
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return jsonArray;
	}

	@Override
	public Long getTotalCountTripMaster() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(tr_id) FROM TripMaster ORDER BY tr_id DESC");

		Long count = (Long) query.uniqueResult();
		
		return count;
		
	}

	@Override
	public JSONObject getUserLiveTripDetailsByUserId(Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);
		
		if(userMaster.getU_role().getrId() != 4 || userMaster.getU_status() != 2) {
			
			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Account Not Verified yet...!");
			
			return jsonObject;
			
			
		}
		
		Query trip_query = sessionFactory.getCurrentSession().createQuery("FROM TripMaster WHERE customer.u_id= :userid and trip_status.ts_id= :status ORDER BY tr_id DESC");
		
		trip_query.setParameter("userid",user_id);
		trip_query.setParameter("status", 1);
		trip_query.setMaxResults(1);
		
		TripMaster tripMaster = (TripMaster) trip_query.uniqueResult();
		
		if(tripMaster == null) {
			
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.TRIP_STATUS, false);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "User not in trip...!");
			
			return jsonObject;
			
			
		}
		
		
		jsonObject.put("trip_id", tripMaster.getTr_id());
		jsonObject.put("start_time", tripMaster.getStart_time());
		jsonObject.put("start_point", tripMaster.getStarting_point());
		jsonObject.put("end_point", tripMaster.getEnding_point());
		jsonObject.put("start_latitude", tripMaster.getStart_lat());
		jsonObject.put("start_longitude", tripMaster.getStart_lang());
		jsonObject.put("end_latitude", tripMaster.getEnd_lat());
		jsonObject.put("end_longitude", tripMaster.getEnd_lang());
        jsonObject.put("type_text", tripMaster.getVehicle().getV_type().getVt_text());
        jsonObject.put("type_id", tripMaster.getVehicle().getV_type().getVt_id());
        jsonObject.put("vehicle_colour_id", tripMaster.getVehicle().getVhm_colour().getC_id());
        jsonObject.put("vehicle_colour", tripMaster.getVehicle().getVhm_colour().getColour());

		
		Query loc_query = sessionFactory.getCurrentSession().createQuery("FROM VehicleLocation WHERE vl_vid.vhm_id= :vid ORDER BY vl_id DESC");
         loc_query.setParameter("vid", tripMaster.getVehicle().getVhm_id());
         loc_query.setMaxResults(1);
         
         VehicleLocation vehicleLocation = (VehicleLocation) loc_query.uniqueResult();
         
         if(vehicleLocation == null) {
        	 
 			return SingleTon.APP_TECH_ERROR(null);

         }
         
         jsonObject.put("vehicle_id", vehicleLocation.getVl_id());
         jsonObject.put("vehicle_latitude", vehicleLocation.getVl_lat());
         jsonObject.put("vehicle_longitude", vehicleLocation.getVl_lang());

		jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
	
		jsonObject.put(SingleTon.TRIP_STATUS, true);
		
		
		return jsonObject;
		
		
		
		}catch (Exception e) {
			
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null);
		}
	}

}
