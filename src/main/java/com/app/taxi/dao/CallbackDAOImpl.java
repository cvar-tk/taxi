package com.app.taxi.dao;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.Callbacks;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleMaster;

@Repository("CallbackDAO")
@Transactional
@Component
public class CallbackDAOImpl implements CallbackDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public JSONObject addCallbackByVehicleId(Integer vehicle_id, Integer user_id , String address, Double latitude, Double longtitude) {

		JSONObject jsonObject = new JSONObject();
		try {
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

		VehicleMaster vehicleMaster = sessionFactory.getCurrentSession().get(VehicleMaster.class, vehicle_id);

		if (vehicleMaster == null || vehicleMaster.getVhm_status() == 0 ) {

			jsonObject.put(SingleTon.CONTENT_STATUS, false);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Vehice Is Disabled...!");
	    	return jsonObject;
	    	

		}
		
		if(vehicleMaster.getVhm_status().equals("5")) {
		
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Car In Trip...!");
	    	return jsonObject;
	    	
			
		}
		
		if(vehicleMaster.getVhm_status().equals("4")) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Car In Offline...!");
	    	return jsonObject;
	    	
			
		}

		Query check_role = sessionFactory.getCurrentSession()
				.createQuery("FROM UserMaster WHERE u_id = :id and u_role.rm_id = :role and u_status= :status");

		check_role.setParameter("id", vehicleMaster.getVhm_owner().getU_id());

		check_role.setParameter("role", 2);
		check_role.setParameter("status", 3);

		UserMaster userMaster = (UserMaster) check_role.uniqueResult();

		if (userMaster == null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Car Not Mapped With Owner...!");
	    	return jsonObject;
		}

		Query user_query = sessionFactory.getCurrentSession()
				.createQuery("FROM UserMaster WHERE u_id= :uid and u_role.rm_id= :role and u_status= :status");

		user_query.setParameter("uid", user_id);

		user_query.setParameter("role", 4);
		user_query.setParameter("status", 2);

		UserMaster user_obj = (UserMaster) user_query.uniqueResult();

		if (user_obj.equals("0") || user_obj == null || user_obj.getU_status() == 1) {

			jsonObject.put(SingleTon.CONTENT_STATUS, false);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Not Verified...!");
	    	return jsonObject;

		}

		Callbacks callbacks = new Callbacks();

		callbacks.setUser_master(user_obj);
		callbacks.setVehicle_master(vehicleMaster);
		callbacks.setAddress(address);
		callbacks.setUser_lang(longtitude);
		callbacks.setUser_lat(latitude);
		callbacks.setCreated_date(calendar.getTime());

		sessionFactory.getCurrentSession().save(callbacks);
		
		
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
    	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		jsonObject.put(SingleTon.CONTENT_MESSAGE , "Success...!");
		return jsonObject;
		
		
		}catch (Exception e) {
			
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}
	}

}
