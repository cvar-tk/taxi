package com.app.taxi.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.SubscribedList;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleBrand;
import com.app.taxi.model.VehicleColour;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleModel;
import com.app.taxi.model.VehicleType;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

@Repository("VehicleDao")
@Transactional
@Component
public class VehicleDaoImpl implements VehicleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GsonBuilder gsonBuilder;

	@Override
	public void addNewVehicleForApproval(VehicleMaster vehicleMaster, Integer user_id) {
		vehicleMaster.setVhm_owner(sessionFactory.getCurrentSession().get(UserMaster.class, user_id));
		sessionFactory.getCurrentSession().save(vehicleMaster);
	}

	@Override
	public JSONArray getNewApprovalVehicles(Integer page, Integer limit) {
		try {
			JSONArray jsonArray = new JSONArray();

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT vhm_id,vhm_owner.u_name,vhm_number,vhm_registration_number,vhm_colour_hint,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone FROM VehicleMaster where vhm_status= :status order by vhm_id desc");

			query.setMaxResults(limit);
			query.setFirstResult(calculateOffset(page, limit));

			query.setInteger("status", 1);

			List<Object[]> objects = query.list();

			System.out.println(page + "p-l" + limit + "" + objects.size());

			for (Object[] object : objects) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_owner_name", object[1]);
				jsonObject.put("vhm_number", object[2]);
				jsonObject.put("vhm_registration_number", object[3]);
				jsonObject.put("vhm_colour_hint", object[4]);
				jsonObject.put("registration_date", object[5]);
				jsonObject.put("vhm_model_hint", object[6]);
				jsonObject.put("vhm_make_hint", object[7]);
				jsonObject.put("vhm_owner_no", object[8]);
				jsonArray.put(jsonObject);
			}

			return jsonArray;

		} catch (JSONException e) {
			e.printStackTrace();

			return null;
		}
	}

	@Override
	public Long getNewApprovalVehicles() {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("select count(*) from VehicleMaster where vhm_status=:status");
		query.setParameter("status", 1);

		Long count = (Long) query.uniqueResult();

		return count;
	}

	@Override
	public JSONArray getApprovedVehicles(Integer page, Integer limit) {
		try {
			JSONArray jsonArray = new JSONArray();

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT vhm_id,vhm_owner.u_name,vhm_number,vhm_registration_number,vhm_colour.colour,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone FROM VehicleMaster where vhm_status!=:disable_status AND vhm_status!=:new_status order by vhm_id desc");
			query.setMaxResults(limit);
			query.setFirstResult(calculateOffset(page, limit));

			query.setParameter("disable_status", 0);
			query.setParameter("new_status", 1);

			List<Object[]> objects = query.list();

			System.out.println(page + "p-l" + limit + "" + objects.size());

			for (Object[] object : objects) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_owner_name", object[1]);
				jsonObject.put("vhm_number", object[2]);
				jsonObject.put("vhm_registration_number", object[3]);
				jsonObject.put("vhm_colour_hint", object[4]);
				jsonObject.put("registration_date", object[5]);
				jsonObject.put("vhm_model_hint", object[6]);
				jsonObject.put("vhm_make_hint", object[7]);
				jsonObject.put("vhm_owner_no", object[8]);
				jsonArray.put(jsonObject);
			}

			return jsonArray;

		} catch (JSONException e) {
			e.printStackTrace();

			return null;
		}
	}

	@Override
	public Long getTotalCountApprovedVehicles() {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"select count(*) from VehicleMaster where vhm_status!=:disable_status AND vhm_status!=:new_status");
		query.setParameter("disable_status", 0);
		query.setParameter("new_status", 1);

		Long count = (Long) query.uniqueResult();

		return count;
	}

	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}

	@Override
	public JSONObject getVehicleRequestById(Integer car_id) {

		JSONObject jsonObject = new JSONObject();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vhm_id,vhm_name,vhm_number,vhm_registration_number,vhm_colour_hint,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone,v_assets.vma_insurance_exp_date,v_assets.vma_full_view,v_assets.vma_rc_book,v_assets.vma_number_view,v_assets.vma_insurance_img FROM VehicleMaster where vhm_status= :status AND vhm_id=:vhm_id");

		query.setInteger("status", 1);
		query.setInteger("vhm_id", car_id);

		List<Object[]> objects = query.list();
		if (objects.isEmpty()) {
			return null;
		}

		Object[] object = objects.get(0);

		try {

			DateFormat op_format = new SimpleDateFormat("MMMM dd, yyyy");

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date reg_date = format.parse(object[5].toString());
			Date ins_exp_date = format.parse(object[9].toString());

			System.out.println(reg_date);
			System.out.println(ins_exp_date);

			System.out.println(object[5].toString());
			System.out.println(object[9].toString());

			jsonObject.put("vhm_id", object[0]);
			jsonObject.put("vhm_owner_name", object[1]);
			jsonObject.put("vhm_number", object[2]);
			jsonObject.put("vhm_registration_number", object[3]);
			jsonObject.put("vhm_colour_hint", object[4]);
			jsonObject.put("registration_date", op_format.format(reg_date));
			jsonObject.put("vhm_model_hint", object[6]);
			jsonObject.put("vhm_make_hint", object[7]);
			jsonObject.put("vhm_owner_no", object[8]);
			jsonObject.put("vhm_insurance_exp", op_format.format(ins_exp_date));
			jsonObject.put("car_full_view", SingleTon.IMAGE_PATH+object[10]);
			jsonObject.put("car_rc_book", SingleTon.IMAGE_PATH+object[11]);
			jsonObject.put("car_name_board_img", SingleTon.IMAGE_PATH+object[12]);
			jsonObject.put("insurance_img", SingleTon.IMAGE_PATH+object[13]);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonObject;
	}

	@Override
	public JSONArray getAllVehicleBrands() {

		Query query = sessionFactory.getCurrentSession().createQuery("from VehicleBrand");

		List<VehicleBrand> VehicleBrands = (List<VehicleBrand>) query.list();
		System.out.println(VehicleBrands);

		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(gsonBuilder.create().toJson(VehicleBrands));
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return jsonArray;
	}

	@Override
	public JSONArray getVehicleModelsByMakeID(Integer make_id) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("from VehicleModel where vm_brand.vb_id=:brand_id");

		query.setParameter("brand_id", make_id);

		List<VehicleModel> vehicleModels = (List<VehicleModel>) query.list();

		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(gsonBuilder.create().toJson(vehicleModels));
		} catch (JSONException e) {

			e.printStackTrace();
		}
		return jsonArray;
	}

	@Override
	public void upDateVehicleStatusById(Integer vehicle_id, String reason, String comments, Integer status) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		VehicleMaster vehicleMaster = session.get(VehicleMaster.class, vehicle_id);
		vehicleMaster.setVhm_status(status);
		vehicleMaster.getV_assets().setReason(reason);
		vehicleMaster.getV_assets().setComments(comments);

		session.update(vehicleMaster);
		tx.commit();
		session.close();

	}

	@Override
	public void updateVehicleAfterApproval(VehicleMaster vehiclemaster_obj) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		VehicleMaster vehicleMaster = session.get(VehicleMaster.class, vehiclemaster_obj.getVhm_id());

		vehicleMaster.setVhm_number(vehiclemaster_obj.getVhm_number());
		vehicleMaster.setVhm_registration_number(vehiclemaster_obj.getVhm_registration_number());
		vehicleMaster.setVhm_name(vehiclemaster_obj.getVhm_name());
		vehicleMaster.setV_type(session.get(VehicleType.class, vehiclemaster_obj.getV_type().getVt_id()));
		vehicleMaster.setVhm_colour(session.get(VehicleColour.class, vehiclemaster_obj.getVhm_colour().getC_id()));
		vehicleMaster.setVhm_model(session.get(VehicleModel.class, vehiclemaster_obj.getVhm_model().getVm_id()));
		vehicleMaster.setRegistration_date(vehiclemaster_obj.getRegistration_date());
		vehicleMaster.getV_assets()
				.setVma_insurance_exp_date(vehiclemaster_obj.getV_assets().getVma_insurance_exp_date());
		vehicleMaster.setVhm_status(2);
		
		if(vehicleMaster.getVhm_owner().getU_status() == 2) {
			UserMaster userMaster = session.get(UserMaster.class, vehicleMaster.getVhm_owner().getU_id());
			userMaster.setU_status(3);
			
			vehicleMaster.setVhm_owner(userMaster);
			
		}
		
		
		session.update(vehicleMaster);
		tx.commit();
		session.close();
	}

	@Override
	public JSONArray getVehicleDetailsByOwnerId(Integer user_id) {

		JSONArray array = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vm.vhm_id,vm.vhm_number,vm.vhm_colour.colour,vm.vhm_status,vm.vhm_model_hint,vm.vhm_make_hint,vm.vhm_colour_hint FROM VehicleMaster vm LEFT JOIN vm.vhm_colour WHERE vm.vhm_owner.u_id=:user_id AND vm.vhm_status !=:delete_status");

		query.setParameter("delete_status", 0);
		query.setParameter("user_id", user_id);

		List<Object[]> list = query.list();

		try {
			for (Object[] object : list) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_number", object[1]);
				jsonObject.put("vhm_colour", object[2]);
				jsonObject.put("vhm_status", object[3]);
				jsonObject.put("vhm_model_hint", object[4]);
				jsonObject.put("vhm_make_hint", object[5]);
				jsonObject.put("vhm_colour_hint", object[6]);

				Query query2 = sessionFactory.getCurrentSession()
						.createQuery("select vhm_model from VehicleMaster where vhm_id =:id");
				query2.setParameter("id", object[0]);
				@SuppressWarnings("unchecked")
				List<VehicleModel> lst = query2.list();
				if (!lst.isEmpty()) {
					jsonObject.put("vhm_model", new JSONObject(gsonBuilder.create().toJson(lst.get(0))));

				}
				array.put(jsonObject);
			}

			return array;

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return array;

		}
	}

	@Override
	public JSONObject getApprovedVehicleByID(Integer car_id) {

		JSONObject jsonObject = new JSONObject();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vhm_id,vhm_name,vhm_number,vhm_registration_number,vhm_colour_hint,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone,v_assets.vma_insurance_exp_date,v_assets.vma_full_view,v_assets.vma_rc_book,v_assets.vma_number_view,v_assets.vma_insurance_img,vhm_colour.colour,vhm_model.vm_name,vhm_model.vm_brand.vb_name,v_type.vt_text FROM VehicleMaster where vhm_status= :status AND vhm_id=:vhm_id");

		query.setInteger("status", 2);
		query.setInteger("vhm_id", car_id);

		List<Object[]> objects = query.list();
		if (objects.isEmpty()) {
			return null;
		}

		Object[] object = objects.get(0);

		try {

			DateFormat op_format = new SimpleDateFormat("MMMM dd, yyyy");

			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date reg_date = format.parse(object[5].toString());
			Date ins_exp_date = format.parse(object[9].toString());

			System.out.println(reg_date);
			System.out.println(ins_exp_date);

			System.out.println(object[5].toString());
			System.out.println(object[9].toString());

			jsonObject.put("vhm_id", object[0]);
			jsonObject.put("vhm_owner_name", object[1]);
			jsonObject.put("vhm_number", object[2]);
			jsonObject.put("vhm_registration_number", object[3]);
			jsonObject.put("vhm_colour_hint", object[4]);
			jsonObject.put("registration_date", op_format.format(reg_date));
			jsonObject.put("vhm_model_hint", object[6]);
			jsonObject.put("vhm_make_hint", object[7]);
			jsonObject.put("vhm_owner_no", object[8]);
			jsonObject.put("vhm_insurance_exp", op_format.format(ins_exp_date));
			jsonObject.put("car_full_view", SingleTon.IMAGE_PATH+object[10]);
			jsonObject.put("car_rc_book", SingleTon.IMAGE_PATH+object[11]);
			jsonObject.put("car_name_board_img", SingleTon.IMAGE_PATH+object[12]);
			jsonObject.put("insurance_img", SingleTon.IMAGE_PATH+object[13]);
			jsonObject.put("vhm_colour", object[14]);
			jsonObject.put("vhm_model", object[15]);
			jsonObject.put("vhm_brand", object[16]);
			jsonObject.put("vhm_type", object[17]);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		return jsonObject;
	}

	@Override
	public JSONObject getVechicleDetailsByVechicleNo(String vehicle_number) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vhm_id,vhm_owner.u_name,vhm_number,vhm_registration_number,vhm_colour_hint,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone FROM VehicleMaster where vhm_status!=:disable_status AND vhm_status!=:new_status AND vhm_number= :number");

		query.setParameter("disable_status", 0);
		query.setParameter("new_status", 1);
		query.setParameter("number", vehicle_number);

		List<Object[]> objects = query.list();

		System.out.println(objects);
		JSONObject jsonObject = new JSONObject();
		for (Object[] object : objects) {

			try {
				jsonObject.put("vhm_owner_name", object[1]);
				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_number", object[2]);
				jsonObject.put("vhm_registration_number", object[3]);
				jsonObject.put("vhm_colour_hint", object[4]);
				jsonObject.put("registration_date", object[5]);
				jsonObject.put("vhm_model_hint", object[6]);
				jsonObject.put("vhm_make_hint", object[7]);
				jsonObject.put("vhm_owner_no", object[8]);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return jsonObject;

	}

	@Override
	public JSONArray filterByVehicleDate(Date fromdate, Date todate, Integer pageIndex) {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vhm_id,vhm_owner.u_name,vhm_number,vhm_registration_number,vhm_colour_hint,registration_date,vhm_model_hint,vhm_make_hint,vhm_owner.m_phone FROM VehicleMaster where created_date BETWEEN :fromdate AND :todate and vhm_status!=:disable_status AND vhm_status!=:new_status order by vhm_id desc");

		query.setMaxResults(10);
		query.setFirstResult(calculateOffset(pageIndex, 10));

		query.setParameter("fromdate", new java.sql.Date(fromdate.getTime()));
		query.setParameter("todate", new java.sql.Date(todate.getTime()));
		query.setParameter("disable_status", 0);
		query.setParameter("new_status", 1);

		List<Object[]> objects = query.list();

		for (Object[] object : objects) {

			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_owner_name", object[1]);
				jsonObject.put("vhm_number", object[2]);
				jsonObject.put("vhm_registration_number", object[3]);
				jsonObject.put("vhm_colour_hint", object[4]);
				jsonObject.put("registration_date", object[5]);
				jsonObject.put("vhm_model_hint", object[6]);
				jsonObject.put("vhm_make_hint", object[7]);
				jsonObject.put("vhm_owner_no", object[8]);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(jsonObject);
			System.out.println(jsonObject);
			System.out.println("******");

			System.out.println(jsonObject);
			jsonArray.put(jsonObject);
		}

		return jsonArray;
	}

	@Override
	public Long getFilterTotalCountApprovedVehicles(Date fromdate, Date todate, Integer pageIndex) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT count(vhm_id) FROM VehicleMaster where created_date BETWEEN :fromdate AND :todate and vhm_status!=:disable_status AND vhm_status!=:new_status order by vhm_id desc");

		query.setParameter("fromdate", new java.sql.Date(fromdate.getTime()));
		query.setParameter("todate", new java.sql.Date(todate.getTime()));
		query.setParameter("disable_status", 0);
		query.setParameter("new_status", 1);

		Long count = (Long) query.uniqueResult();

		return count;

	}

	@Override
	public Boolean checkVehicleSubscriptionStatusByVehicleId(Integer vehicle_id, Integer type) {

		Query query_subscribtion_list = sessionFactory.getCurrentSession().createQuery(
				"FROM SubscribedList WHERE sl_vehicle_master.vhm_id = :vehicle_id AND sl_type.pt_id =:type ORDER BY sl_id DESC");

		query_subscribtion_list.setParameter("vehicle_id", vehicle_id);
		query_subscribtion_list.setParameter("type", type);

		query_subscribtion_list.setMaxResults(1);

		SubscribedList subscription = (SubscribedList) query_subscribtion_list.uniqueResult();

		if (subscription == null) {

			return false;

		}

		if (subscription.getTo_date().after(Calendar.getInstance().getTime())) {

			return true;

		}

		return false;
	}

	@Override
	public List<Object[]> getVehicleType() {

		Query query = sessionFactory.getCurrentSession().createQuery("FROM VehicleType");

		return query.list();

	}

	@Override
	public void updateVehicleInsuranceExpiryDate(Date insurance_expiry, Integer vehicle_id) {

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		VehicleMaster vehicleMaster = session.get(VehicleMaster.class, vehicle_id);

		vehicleMaster.getV_assets().setVma_insurance_exp_date(insurance_expiry);

		tx.commit();
		session.close();

	}

	@Override
	public JSONObject getVehicleDetailsByVehicleId(Integer vehicle_id,Double latitude,Double longtitude) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT vhm_id,vhm_number,vhm_colour.colour,vhm_model.vm_name,vhm_model.vm_brand.vb_name,v_assets.vma_full_view,base_fair,per_km_price,vhm_picked_by,base_km,per_min_price FROM VehicleMaster where vhm_status!=:disable_status AND vhm_status!=:new_status AND vhm_id= :vhm_id");

		query.setParameter("disable_status", 0);
		query.setParameter("new_status", 1);
		query.setParameter("vhm_id", vehicle_id);

		query.setMaxResults(1);
		
		List<Object[]> objects = query.list();

		System.out.println(objects);
		JSONObject jsonObject = new JSONObject();
		for (Object[] object : objects) {

			try {

				jsonObject.put("vhm_id", object[0]);
				jsonObject.put("vhm_number", object[1]);
				jsonObject.put("vhm_colour", object[2]);
				jsonObject.put("vm_model", object[3]);
				jsonObject.put("vm_brand", object[4]);
				jsonObject.put("vma_full_view",SingleTon.IMAGE_PATH+ object[5]);
				jsonObject.put("base_fair", object[6]);
				jsonObject.put("per_km_price", object[7]);
				jsonObject.put("picked_by", object[8]);
				jsonObject.put("base_km", object[9]);
				jsonObject.put("per_min_price", object[10]);

				Integer driver_id = (Integer) object[8];

				Query query2 = sessionFactory.getCurrentSession()
						.createQuery("SELECT u_name,u_pic,m_phone FROM UserMaster where u_id =:driver_id");

				query2.setParameter("driver_id", driver_id);

				Object[] obj = (Object[]) query2.uniqueResult();

				jsonObject.put("driver_name", obj[0]);
				jsonObject.put("driver_pic", SingleTon.IMAGE_PATH+obj[1]);
				jsonObject.put("driver_phone", obj[2]);
				
				/*
				Query query3 = sessionFactory.getCurrentSession().createQuery("FROM VehicleLocation WHERE vl_vid.vhm_id =:vhm_id ");

				query3.setParameter("vhm_id", vehicle_id);
				*/
				
				SQLQuery distance_query = sessionFactory.getCurrentSession().createSQLQuery("SELECT SQRT( POW(69.1 * (vl_lat -"+ latitude+"), 2) + POW(69.1 * ("+longtitude+"- vl_lang) * COS(vl_lat / 57.3), 2)) AS distance FROM vehicle_location ORDER BY distance limit 1");
				
				Double distance = (Double) distance_query.uniqueResult();
				
			jsonObject.put("distance", distance);
				
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Success...!");
			
			return jsonObject;

			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return jsonObject;
	}

	@Override
	public List<Object[]> getVehicleColour() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("FROM VehicleColour");

		return query.list();
	
	}

	@Override
	public JSONObject getCarDetailsByCarId(Integer car_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT vm.vhm_id,vm.vhm_name,vm.vhm_number,vm.vhm_registration_number,vm.vhm_picked_by,vm.registration_date,vm.created_date,vm.vhm_status,vm.base_fair,vm.vhm_make_hint,vm.vhm_model_hint,vm.vhm_colour_hint,vm.vhm_owner.u_name,vm.vhm_colour.colour,vm.per_km_price,vm.base_km,vm.per_min_price,vm.vhm_model.vm_name,vm.vhm_model.vm_brand.vb_name FROM VehicleMaster vm LEFT JOIN vm.vhm_colour LEFT JOIN vm.vhm_model LEFT JOIN vm.vhm_model.vm_brand WHERE vhm_id= :id");
				
		query.setParameter("id", car_id);
		
		List<Object[]> obj = query.list();
		
		// check the user exist
					if (obj == null) {
						System.err.println("Def -Err- user not exit");
						return SingleTon.APP_TECH_ERROR(null);
					}
		
		try {
			
			for (Object[] objects : obj) {
				
				jsonObject.put("car_id", objects[0]);
				jsonObject.put("register_owner_name", objects[1]);
				jsonObject.put("car_number", objects[2]);
				jsonObject.put("register_number", objects[3]);
				jsonObject.put("register_date", objects[5]);
				jsonObject.put("created_date", objects[6]);
				jsonObject.put("car_status", objects[7]);
				jsonObject.put("base_fair", objects[8]);
				jsonObject.put("car_make_hint", objects[9]);
				jsonObject.put("car_model_hint", objects[10]);
				jsonObject.put("car_colour_hint", objects[11]);
				jsonObject.put("car_colour", objects[13]);
				jsonObject.put("per_km_price", objects[14]);
				jsonObject.put("base_km", objects[15]);
				jsonObject.put("per_min_price", objects[16]);
				jsonObject.put("car_model", objects[17]);
				jsonObject.put("car_make", objects[18]);

				jsonObject.put("picked_by", objects[4]);

				if(objects[4] != null) {
					
					Query driver_query = sessionFactory.getCurrentSession().createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id");
					
					driver_query.setParameter("id", objects[4]);
					
					String name = (String) driver_query.uniqueResult();
					
					jsonObject.put("driver_name", name);
					
				}
				
	           Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
				
				Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_vehicle_master.vhm_id= :v_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
				
				subcribe_query.setParameter("v_id", car_id);
				subcribe_query.setParameter("type", 1);

				subcribe_query.setMaxResults(1);
				
				SubscribedList subscribedList = (SubscribedList) subcribe_query.uniqueResult();
				
				if( subscribedList == null || subscribedList.getTo_date().before(calendar.getTime()) ) {
					
					jsonObject.put(SingleTon.VECHICLE_PLAN_CODE, 1007);
					jsonObject.put(SingleTon.VECHICLE_PLAN_STATUS, false);
					
					
				}else {
				jsonObject.put(SingleTon.VECHICLE_PLAN_CODE, 1007);
				jsonObject.put(SingleTon.VECHICLE_PLAN_STATUS, true);
				
			
				}
				
				
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.CONTENT_STATUS, true);
			
				
			}
			
			
		}catch (Exception e) {

			e.printStackTrace();
		
		}
		return jsonObject;
		
		
		
	}

}
