package com.app.taxi.dao;



import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.model.SupportCMS;

@Repository("CMSDao")
@Transactional
@Component
public class CMSDaoImpl implements CMSDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public String getPrivacyPolicy(Integer role) {

		Query query = sessionFactory.getCurrentSession().createQuery("SELECT privacy_policy FROM SupportCMS WHERE role_master.rm_id =:role_id");
		
		query.setParameter("role_id", role);
		
		return (String) query.uniqueResult();
	}

	@Override
	public String getTermsAndCondition(Integer role) {
	Query query = sessionFactory.getCurrentSession().createQuery("SELECT terms_condition FROM SupportCMS WHERE role_master.rm_id =:role_id");
		
		query.setParameter("role_id", role);
		
		return (String) query.uniqueResult();
	}

	@Override
	public void updatePrivacyPolicyByAdmin(Integer role, String user_privacy) {
		
		System.out.println(user_privacy);
		
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		Query query = session.createQuery("FROM SupportCMS where role_master.rm_id= :role");
		query.setParameter("role", role);
		
		SupportCMS supportCMS = (SupportCMS) query.list().get(0);
		
		System.out.println(supportCMS);
		
		
		supportCMS.setPrivacy_policy(user_privacy);
		
		session.update(supportCMS);
		
		tx.commit();
		session.close();
		
	}

	@Override
	public void updateTermsAndConditionByAdmin(Integer role, String terms) {
		
		
	Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		Query query = session.createQuery("FROM SupportCMS where role_master.rm_id= :role");
		query.setParameter("role", role);
		
		SupportCMS supportCMS = (SupportCMS) query.list().get(0);
		
		System.out.println(supportCMS);
		
		supportCMS.setTerms_condition(terms);
		
		session.update(supportCMS);
		
		tx.commit();
		session.close();
		
		
	}

}
