package com.app.taxi.dao;

public interface CMSDao {

	String getPrivacyPolicy(Integer role);

	String getTermsAndCondition(Integer role);

	void updatePrivacyPolicyByAdmin(Integer role, String user_privacy);

	void updateTermsAndConditionByAdmin(Integer role, String terms);

}
