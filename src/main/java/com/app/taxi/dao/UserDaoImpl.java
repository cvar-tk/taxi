package com.app.taxi.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.BookMark;
import com.app.taxi.model.MainWallet;
import com.app.taxi.model.MultiRoleMaster;
import com.app.taxi.model.RoleMaster;
import com.app.taxi.model.RoleUserMaster;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.UserSession;
import com.app.taxi.model.WorkersCredential;
import com.google.cloud.storage.Acl.User;

@Repository("userDao")
@Transactional
@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private TokenValidatorDao tokenValidatorDao;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public JSONObject getJSONResultForUserLogin(Double phone, String password, Integer role) {

		JSONObject result = new JSONObject();

		try {

			result.put(SingleTon.CONTENT_STATUS, false);

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id, w_credential.wc_password,u_status,m_phone from UserMaster WHERE m_phone in (:phone) AND u_role.rm_id in (:role)");

			query.setDouble("phone", phone);

			query.setInteger("role", role);

			System.out.println(query.getQueryString());

			List<Object[]> user_list = query.list();

			if (user_list.isEmpty()) {

				result.put(SingleTon.RESPONSE_CODE, 6002);

				result.put(SingleTon.CONTENT_MESSAGE, "Invalid mobile Number...!");

				return result;

			}

			Object[] user = user_list.get(0);

			if (user[2].equals(1)) {
				
				Session session = sessionFactory.openSession();
				Transaction tx =session.beginTransaction();
				
				UserMaster userMaster = session.get(UserMaster.class, (Integer)user[0]);
				
				Random random = new Random();

				String OTP = String.format("%06d", random.nextInt(999999));
				SingleTon.sendphone(user[3].toString(),
						"Your OTP is " + OTP + ".Don't share this OTP to anyone.this OTP is valid upto 5 Minutes. ");

				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
				calendar.add(Calendar.MINUTE, +5);
				
				userMaster.getW_credential().setWc_otp(OTP);
				userMaster.getW_credential().setWc_otp_expiry_time(calendar.getTime());
				
				tx.commit();
				session.close();
				
				result.put(SingleTon.CONTENT_MESSAGE, "Success..!");
				result.put(SingleTon.RESPONSE_CODE, 6006);

				return result;

			}

			if (user[2].equals(5)) {

				result.put(SingleTon.CONTENT_MESSAGE, "Your account disabled..Kindly contact support..!");
				result.put(SingleTon.RESPONSE_CODE, 6003);

				return result;

			}

			
			if (bCryptPasswordEncoder.matches(password, user[1].toString())) {

				result.put(SingleTon.CONTENT_STATUS, true);
				result.put(SingleTon.USER_ID, user[0]);

				String token = bCryptPasswordEncoder.encode(user[0].toString()) + "--"
						+ Calendar.getInstance().getTimeInMillis() + "--";
				result.put(SingleTon.CONTENT_MESSAGE, "Login Success..!");
				result.put(SingleTon.ACCESS_TOKEN, token);

				UserSession userSessions = new UserSession();
				userSessions.setSession_key(token);
				userSessions.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, (Integer) user[0]);
				userSessions.setUser_master(userMaster);

				sessionFactory.getCurrentSession().save(userSessions);
				result.put(SingleTon.CONTENT_STATUS, true);
				result.put(SingleTon.RESPONSE_CODE, 6001);
				return result;

			}

			result.put(SingleTon.CONTENT_MESSAGE, "Wrong Password...!");
			result.put(SingleTon.RESPONSE_CODE, 6003);

			return result;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public Object[] getAdminDetailsByCredential(String emailid, String password, Integer pin) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id, w_credential.wc_password,u_status from UserMaster WHERE u_email =:email AND u_security_pin in (:pin) AND u_role.rm_id in (:role)");

		query.setDouble("pin", pin);
		query.setInteger("role", 1);
		query.setString("email", emailid);

		List<Object[]> list = query.list();

		if (list.isEmpty()) {

			return null;

		}

		return list.get(0);
	}

	@Override
	public void AddNewOwner(UserMaster userMaster) {

		userMaster.setU_status(1);
		userMaster.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
		Random random = new Random();

		String OTP = String.format("%06d", random.nextInt(999999));
		SingleTon.sendphone(userMaster.getM_phone().toString(),
				"Your OTP is " + OTP + ".Don't share this OTP to anyone.this OTP is valid upto 5 Minutes. ");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		calendar.add(Calendar.MINUTE, +5);

		userMaster.getW_credential().setWc_otp(OTP);
		userMaster.getW_credential().setWc_otp_expiry_time(calendar.getTime());

		userMaster.setU_role(sessionFactory.getCurrentSession().get(RoleMaster.class, 2));

		sessionFactory.getCurrentSession().save(userMaster);
	}

	@Override
	public Boolean checkAccountExistingByRole(BigInteger obj_phone, int role) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("select count(*) from UserMaster where u_role.rm_id =:role and m_phone=:phone");

		query.setParameter("role", role);
		query.setParameter("phone", obj_phone);

		Long count = (Long) query.uniqueResult();

		if (count != 0) {

			return true;
		}

		return false;
	}

	@Override
	public JSONArray getBookMarksbyBookId(String book_id) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery(" from BookMark where book_id =:book_id order by bookmark_id ASC");

		query.setParameter("book_id", book_id);

		JSONArray jsonArray = new JSONArray();

		List<BookMark> bookMark = query.list();

		for (BookMark obj : bookMark) {

			JSONObject jsonObject = new JSONObject();

			try {

				jsonObject.put("page_id", obj.getBookmark_id());
				jsonObject.put("image", obj.getImage_code());
				jsonArray.put(jsonObject);

			} catch (JSONException e) {

				e.printStackTrace();

			}

		}

		return jsonArray;
	}

	@Override
	public void addBookmark(String book_id, Integer page_id, String image) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				" from BookMark where book_id =:book_id AND bookmark_id =:bookmark_id order by bookmark_id ASC");

		query.setParameter("book_id", book_id);
		query.setParameter("bookmark_id", page_id);

		if (query.list().isEmpty()) {

			BookMark bookMark = new BookMark();

			bookMark.setBook_id(book_id);
			bookMark.setBookmark_id(page_id);
			bookMark.setImage_code(image);

			sessionFactory.getCurrentSession().save(bookMark);
		}

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		Query query1 = session.createQuery(" from BookMark where book_id =:book_id AND bookmark_id =:bookmark_id");

		query1.setParameter("book_id", book_id);
		query1.setParameter("bookmark_id", page_id);

		BookMark bookMark = (BookMark) query1.list().get(0);

		bookMark.setBook_id(book_id);
		bookMark.setBookmark_id(page_id);
		bookMark.setImage_code(image);

		session.update(bookMark);

		tx.commit();
		session.close();

	}

	@Override
	public JSONObject VerifyOwnerOTP(BigInteger mobile, String otp) {
		JSONObject jsonObject = new JSONObject();
		try {

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT  u_id,w_credential.wc_otp,w_credential.wc_otp_expiry_time FROM UserMaster WHERE u_role.rm_id =:role AND m_phone=:phone");

			query.setParameter("role", 2);
			query.setParameter("phone", mobile);

			List<Object[]> list = query.list();

			if (query.list().isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No User registered with this mobile number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject;
			}

			Object[] object = (Object[]) query.list().get(0);

			System.out.println((Timestamp) object[2]);

			Calendar exp_time = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			exp_time.setTime((Timestamp) object[2]);

			if (otp.equals(object[1]) && Calendar.getInstance(TimeZone.getTimeZone("IST")).before(exp_time)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Verified");
				String token = bCryptPasswordEncoder.encode(object[0].toString()) + "--"
						+ Calendar.getInstance().getTimeInMillis() + "--";

				jsonObject.put(SingleTon.ACCESS_TOKEN, token);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				UserSession userSessions = new UserSession();
				userSessions.setSession_key(token);
				userSessions.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, (Integer) object[0]);
				userSessions.setUser_master(userMaster);

				sessionFactory.getCurrentSession().save(userSessions);

				Session session = sessionFactory.openSession();
				Transaction tx = session.beginTransaction();

				UserMaster O_userMaster = session.get(UserMaster.class, Integer.parseInt(object[0].toString()));

				O_userMaster.setU_status(2);
				
			/*	MainWallet mainWallet = new MainWallet();
				mainWallet.setClosing(0.0);
				mainWallet.setOpening(0.0);
				mainWallet.setOpposition(0.0);
				mainWallet.setPayment_type(1);
				
				Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));

				mainWallet.setCreated_date(calendar1.getTime());
				mainWallet.setUser(session.get(UserMaster.class, Integer.parseInt(object[0].toString())));
				
				session.save(mainWallet);*/
				

				tx.commit();
				session.close();
				return jsonObject;

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid OTP..!");

			return jsonObject;

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void UploadNewPic(Integer id, String filenamebyfile) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		UserMaster userMaster = session.get(UserMaster.class, id);
		userMaster.setU_pic(filenamebyfile);
		session.saveOrUpdate(userMaster);
		tx.commit();
		session.close();

	}

	@Override
	public JSONObject getDetailsBySessionId(String token) {

		try {
			JSONObject jsonObject = new JSONObject();

			Integer user_id = tokenValidatorDao.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired");
				return jsonObject;

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT m_phone,u_name,u_pic,u_email,u_role.rm_id,u_status from UserMaster WHERE u_id=:user_id");

			query.setParameter("user_id", user_id);

			Object[] object = (Object[]) query.list().get(0);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.MOBILE, object[0]);
			jsonObject.put(SingleTon.USER_NAME, object[1]);
			jsonObject.put(SingleTon.USER_PIC, SingleTon.IMAGE_PATH+object[2]);
			jsonObject.put(SingleTon.EMAIL, object[3]);
			jsonObject.put(SingleTon.ROLE, object[4]);
			jsonObject.put(SingleTon.USER_STATUS, object[5]);

			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public JSONObject UpdatePasswordByToken(String token, String password) {

		try {

			JSONObject jsonObject = new JSONObject();

			Integer user_id = tokenValidatorDao.getUserIdByToken(token);
			System.out.println(user_id);
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired");
				return jsonObject;
			}

			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			UserMaster userMaster = session.get(UserMaster.class, user_id);
			userMaster.getW_credential().setWc_password(bCryptPasswordEncoder.encode(password));
			session.saveOrUpdate(userMaster);
			tx.commit();
			session.close();

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password updated successfully..!");

			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public JSONObject VerifyDriverOTP(BigInteger mobile, String otp) {

		JSONObject jsonObject = new JSONObject();
		try {

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT  u_id,w_credential.wc_otp,w_credential.wc_otp_expiry_time FROM UserMaster WHERE u_role.rm_id =:role AND m_phone=:phone");

			query.setParameter("role", 3);
			query.setParameter("phone", mobile);

			List<Object[]> list = query.list();

			if (query.list().isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No User registered with this mobile number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject;
			}

			Object[] object = (Object[]) query.list().get(0);

			System.out.println((Timestamp) object[2]);

			Calendar exp_time = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			exp_time.setTime((Timestamp) object[2]);

			if (otp.equals(object[1]) && Calendar.getInstance(TimeZone.getTimeZone("IST")).before(exp_time)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Verified");
				String token = bCryptPasswordEncoder.encode(object[0].toString()) + "--"
						+ Calendar.getInstance().getTimeInMillis() + "--";

				jsonObject.put(SingleTon.ACCESS_TOKEN, token);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				UserSession userSessions = new UserSession();
				userSessions.setSession_key(token);
				userSessions.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, (Integer) object[0]);
				userSessions.setUser_master(userMaster);

				sessionFactory.getCurrentSession().save(userSessions);

				Session session = sessionFactory.openSession();
				Transaction tx = session.beginTransaction();

				UserMaster O_userMaster = session.get(UserMaster.class, Integer.parseInt(object[0].toString()));

				O_userMaster.setU_status(2);

				tx.commit();
				session.close();
				return jsonObject;

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid OTP..!");

			return jsonObject;

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void sendOTPByPhone(String phone, Integer role) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("from UserMaster where u_role.rm_id =:role and m_phone=:phone and u_status !=:status");

		query.setParameter("role", role);
		query.setParameter("phone", new BigInteger(phone));
		query.setParameter("status", 0);

		List<UserMaster> user = query.list();

		if (user.isEmpty()) {

			return;

		}
		
		System.err.println(user);
		
		
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster userMaster = session.get(UserMaster.class, user.get(0).getU_id());

		Random random = new Random();

		String OTP = String.format("%06d", random.nextInt(999999));
		SingleTon.sendphone(userMaster.getM_phone().toString(),
				"Your OTP is " + OTP + ".Don't share this OTP to anyone.this OTP is valid upto 5 Minutes. ");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		calendar.add(Calendar.MINUTE, +5);

		userMaster.getW_credential().setWc_otp(OTP);
		userMaster.getW_credential().setWc_otp_expiry_time(calendar.getTime());
		
		
		tx.commit();
		session.close();

	}

	@Override
	public JSONArray getAssosiatedDriverByOwnerId(Integer owner_id) {

		JSONArray jsonArray = new JSONArray();

		try {
			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id, m_phone, u_name, u_pic, u_status, u_role.rm_id,u_created_date FROM UserMaster WHERE mapped_with=:owner_id AND u_status !=:disable_status");
			query.setParameter("owner_id", owner_id);
			query.setParameter("disable_status", 0);

			List<Object[]> list = query.list();

			for (Object[] objects : list) {

				JSONObject jsonObject = new JSONObject();
				jsonObject.put("u_id", objects[0]);
				jsonObject.put("m_phone", objects[1]);
				jsonObject.put("u_name", objects[2]);
				jsonObject.put("u_pic", SingleTon.IMAGE_PATH+objects[3]);
				jsonObject.put("u_status", objects[4]);
				jsonObject.put("u_role", objects[5]);
				jsonObject.put("created_date", objects[6]);
				jsonArray.put(jsonObject);
			}

			return jsonArray;
		} catch (Exception e) {

			e.printStackTrace();
			return jsonArray;
		}
	}

	@Override
	public JSONObject requestChangePassword(String email_id) {

		JSONObject jsonObject = new JSONObject();

		try {
			Query query = sessionFactory.getCurrentSession()
					.createQuery("SELECT u_id FROM UserMaster WHERE u_email =:email AND u_role.rm_id=:role");
			query.setParameter("email", email_id);
			query.setParameter("role", 1);

			if (query.list().isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "This E-Mail not registered..!");
				return jsonObject;
			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE,
					"Password change informaion sent to the following e-mail id -<b>" + email_id + "</b>..!");
			return jsonObject;
		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}
	}

	@Override
	public void AddNewUser(UserMaster userMaster) {

		userMaster.setU_status(1);
		WorkersCredential credential = new WorkersCredential();
		userMaster.setW_credential(credential);
		userMaster.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
		Random random = new Random();

		String OTP = String.format("%06d", random.nextInt(999999));
		SingleTon.sendphone(userMaster.getM_phone().toString(),
				"Your OTP is " + OTP + ".Don't share this OTP to anyone.this OTP is valid upto 5 Minutes. ");

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

		userMaster.getW_credential().setWc_created_on(calendar.getTime());
		userMaster.getW_credential().setWc_updated_on(calendar.getTime());
		calendar.add(Calendar.MINUTE, +5);

		userMaster.getW_credential().setWc_otp(OTP);
		userMaster.getW_credential().setWc_otp_expiry_time(calendar.getTime());

		userMaster.setU_role(sessionFactory.getCurrentSession().get(RoleMaster.class, 4));

		sessionFactory.getCurrentSession().save(userMaster);
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT u_id FROM UserMaster ORDER BY u_id DESC");
		
		query.setMaxResults(1);
		
		Integer id = (Integer) query.uniqueResult();
		
		MainWallet mainWallet = new MainWallet();
		mainWallet.setClosing(0.0);
		mainWallet.setOpening(0.0);
		mainWallet.setOpposition(0.0);
		mainWallet.setPayment_type(1);
		
		Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));

		mainWallet.setCreated_date(calendar1.getTime());
		mainWallet.setUser(sessionFactory.getCurrentSession().get(UserMaster.class, id));
		
		sessionFactory.getCurrentSession().save(mainWallet);
		

	}

	@Override
	public String getUserNameById(Integer user_id) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT u_name from UserMaster where u_id = :user_id");

		query.setParameter("user_id", user_id);

		String name = (String) query.uniqueResult();

		if (name == null) {

			return null;
		}

		return name;
	}

	@Override
	public JSONObject VerifyUserOTP(BigInteger obj_phone, String otp) {

		JSONObject jsonObject = new JSONObject();
		try {

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id,w_credential.wc_otp,w_credential.wc_otp_expiry_time FROM UserMaster WHERE u_role.rm_id =:role AND m_phone=:phone");

			query.setParameter("role", 4);
			query.setParameter("phone", obj_phone);

			List<Object[]> list = query.list();

			if (query.list().isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No User registered with this mobile number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject;
			}

			Object[] object = (Object[]) query.list().get(0);

			System.out.println((Timestamp) object[2]);

			Calendar exp_time = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			exp_time.setTime((Timestamp) object[2]);

			if (otp.equals(object[1]) && Calendar.getInstance(TimeZone.getTimeZone("IST")).before(exp_time)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Verified");
				String token = bCryptPasswordEncoder.encode(object[0].toString()) + "--"
						+ Calendar.getInstance().getTimeInMillis() + "--";

				jsonObject.put(SingleTon.ACCESS_TOKEN, token);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				UserSession userSessions = new UserSession();
				userSessions.setSession_key(token);
				userSessions.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, (Integer) object[0]);
				userSessions.setUser_master(userMaster);

				sessionFactory.getCurrentSession().save(userSessions);

				Session session = sessionFactory.openSession();
				Transaction tx = session.beginTransaction();

				UserMaster O_userMaster = session.get(UserMaster.class, Integer.parseInt(object[0].toString()));

				O_userMaster.setU_status(2);
				
				

				tx.commit();
				session.close();
				return jsonObject;

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, false);

			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid OTP..!");

			return jsonObject;

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void updateEmailAndUsername(Integer user_id, String name, String email) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster O_userMaster = session.get(UserMaster.class, user_id);

		if (O_userMaster == null) {
			tx.commit();
			session.close();
			return;
		}

		O_userMaster.setU_name(name);
		O_userMaster.setU_email(email);

		tx.commit();
		session.close();

	}

	@Override
	public void updateUserEmailId(Integer user_id, String email) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster O_userMaster = session.get(UserMaster.class, user_id);

		if (O_userMaster == null) {
			tx.commit();
			session.close();
			return;
		}

		O_userMaster.setU_email(email);

		tx.commit();
		session.close();
	}

	@Override
	public void UpdateUsernameToken(Integer id, String username) {
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		UserMaster userMaster = session.get(UserMaster.class, id);

		if (userMaster == null) {
			tx.commit();
			session.close();
			return;
		}

		userMaster.setU_name(username);
		session.saveOrUpdate(userMaster);
		tx.commit();
		session.close();
	}
	
	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}

	@Override
	public JSONArray getCustomerDetailsList(Integer page, Integer limit) {

		JSONArray jsonArray = new JSONArray();	
			
			Query query = sessionFactory.getCurrentSession().createQuery("select u_id,u_name,m_phone,u_email from UserMaster where u_role.rm_id= :role and u_status= :status order by u_id desc");
			
			query.setMaxResults(limit);
			query.setFirstResult(calculateOffset(page, limit));

			query.setParameter("role", 4);
			query.setParameter("status", 2);
			
			List<Object[]> obj = query.list();
			
			if(obj.isEmpty()) {
				
			
				
			}
			
			try {
				for (Object[] objects : obj) {
					
					JSONObject jsonObject = new JSONObject();
					
					jsonObject.put("id", objects[0]);
					jsonObject.put("name", objects[1]);
					jsonObject.put("phone", objects[2]);
					jsonObject.put("email", objects[3]);
				
					
					jsonArray.put(jsonObject);
					
					
				}
				
				
			}catch (Exception e) {
	         e.printStackTrace();
	           }
			
		
			
			return jsonArray;
			
	}

	@Override
	public Long getTotalCustomerCount() {
		
		Query query = sessionFactory.getCurrentSession().createQuery("select count(u_id) from UserMaster where u_role.rm_id= :role and u_status= :status order by u_id desc");

		query.setParameter("role", 4);
		query.setParameter("status", 2);
		
		Long count = (Long) query.uniqueResult();
		
		return count;
		
	}

	@Override
	public boolean checkEmailAlreadyExcist(String email) {
		
		Query query = sessionFactory.getCurrentSession().createQuery("FROM RoleUserMaster WHERE u_email= :email");
		
		query.setParameter("email", email);
		
		List<Object[]> obj = query.list();
		
		if(obj.isEmpty()) {
			
			return true;
			
		}
		
		return false;
		
	}

	@Override
	public void adminCreateNewRoleManagement(RoleUserMaster userMaster, Integer id) {
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		
		userMaster.setU_created_date(calendar.getTime());
		userMaster.setPassword_update(calendar.getTime());
		userMaster.setPassword(bCryptPasswordEncoder.encode(userMaster.getPassword()));
		
		MultiRoleMaster multiRoleMaster= sessionFactory.getCurrentSession().get(MultiRoleMaster.class, userMaster.getU_role().getR_id());
		
		userMaster.setU_role(multiRoleMaster);
		
		UserMaster userMaster2 = sessionFactory.getCurrentSession().get(UserMaster.class, id);
		
		userMaster.setCreated_by(userMaster2);
		
		sessionFactory.getCurrentSession().saveOrUpdate(userMaster);
		
		
		
	}


	@Override
	public JSONArray getMultiRoleUserList(Integer page, Integer limit) {
		
		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT ur_id,u_name,m_phone,u_email,u_security_pin,u_status,u_role.multi_roles,u_created_date FROM RoleUserMaster order by ur_id DESC");

		query.setMaxResults(limit);
		query.setFirstResult(calculateOffset(page, limit));


		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}
		
		try {
			
			for (Object[] objects : obj) {
				
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("u_id", objects[0]);
				jsonObject.put("u_name", objects[1]);
				jsonObject.put("u_phone", objects[2]);
				jsonObject.put("u_email", objects[3]);
				jsonObject.put("security_pin", objects[4]);
				jsonObject.put("status", objects[5]);
				jsonObject.put("role", objects[6]);
				jsonObject.put("created_date", objects[7]);

				jsonArray.put(jsonObject);
				
			}
			
			
		}catch (Exception e) {

		  e.printStackTrace();
		  
		}
		
         return jsonArray;
	}

	@Override
	public Long getTotalMultiRoleUserCount() {
	  
		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT COUNT(ur_id) FROM RoleUserMaster");

		Long count = (Long) query.uniqueResult();

		return count;

	
	
	}

	@Override
	public JSONObject getMultiRoleDetailsById(Integer id) {
	
		JSONObject jsonObject = new JSONObject();
		
		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT ur_id,u_name,m_phone,u_email,u_security_pin,u_status,u_role.multi_roles,u_role.r_id,u_created_date FROM RoleUserMaster WHERE ur_id=:id order by ur_id DESC");

		query.setParameter("id", id);
		
		List<Object[]> obj = query.list();
		
		if (obj.isEmpty()) {

			return null;

		}
		
		try {
			
			for (Object[] objects : obj) {
				
				jsonObject.put("u_id", objects[0]);
				jsonObject.put("u_name", objects[1]);
				jsonObject.put("u_phone", objects[2]);
				jsonObject.put("u_email", objects[3]);
				jsonObject.put("security_pin", objects[4]);
				jsonObject.put("status", objects[5]);
				jsonObject.put("role", objects[6]);
				jsonObject.put("role_id", objects[7]);
				jsonObject.put("created_date", objects[8]);

				
				
			}
			
			
		}catch (Exception e) {

		  e.printStackTrace();
		  
		}
		
		return jsonObject;
		
		
	}

	@Override
	public List<Object[]> getMultiRoleMaster() {
	
		Query query = sessionFactory.getCurrentSession().createQuery("FROM MultiRoleMaster");

		return query.list();
	}

	@Override
	public void updateRoleUserMasterDetailsById(Integer u_id, String name, BigInteger mobile, Integer security_pin,
			Integer role) {
	
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		RoleUserMaster roleUserMaster = session.get(RoleUserMaster.class, u_id);
		
		roleUserMaster.setU_name(name);
		roleUserMaster.setM_phone(mobile);
		roleUserMaster.setU_security_pin(security_pin);
		
		MultiRoleMaster multiRoleMaster = session.get(MultiRoleMaster.class, role);
		roleUserMaster.setU_role(multiRoleMaster);
		
		session.update(roleUserMaster);
		
		tx.commit();
		session.close();
		
	}

	@Override
	public void DeactiveRoleUserMasterDetailsById(Integer u_id, Integer status) {
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		RoleUserMaster roleUserMaster = session.get(RoleUserMaster.class, u_id);
		
	    roleUserMaster.setU_status(status);
		
		session.update(roleUserMaster);
		
		tx.commit();
		session.close();
		
		
	}

	@Override
	public boolean checkOwnerVerifiedById(Integer user_id) {
		
		Query count_query  = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(vhm_id) FROM VehicleMaster WHERE vhm_owner.u_id= :picked_by");
		
		count_query.setParameter("picked_by", user_id);
		
		Long count = (Long) count_query.uniqueResult();
		
		System.out.println("fjg"+count);
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT u_status FROM UserMaster WHERE u_id= :id");
		
		query.setParameter("id", user_id);
		
		Integer status = (Integer) query.uniqueResult();
		
		if(status == 2 && count != 0 ) {
			
			return true;
			
		}
		return false;
		
	}

	@Override
	public JSONObject VerifyForgetPasswordOTP(BigInteger obj_phone, String otp,Integer role) {
		
		JSONObject jsonObject = new JSONObject();
		try {

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT  u_id,w_credential.wc_otp,w_credential.wc_otp_expiry_time FROM UserMaster WHERE u_role.rm_id =:role AND m_phone=:phone");

			query.setParameter("role", role);
			query.setParameter("phone", obj_phone);

			List<Object[]> list = query.list();

			if (query.list().isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No User registered with this mobile number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject;
			}

			Object[] object = (Object[]) query.list().get(0);

			System.out.println((Timestamp) object[2]);

			Calendar exp_time = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			exp_time.setTime((Timestamp) object[2]);

			if (otp.equals(object[1]) && Calendar.getInstance(TimeZone.getTimeZone("IST")).before(exp_time)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Verified");
				String token = bCryptPasswordEncoder.encode(object[0].toString()) + "--"
						+ Calendar.getInstance().getTimeInMillis() + "--";

				jsonObject.put(SingleTon.ACCESS_TOKEN, token);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				UserSession userSessions = new UserSession();
				userSessions.setSession_key(token);
				userSessions.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, (Integer) object[0]);
				userSessions.setUser_master(userMaster);

				sessionFactory.getCurrentSession().save(userSessions);

				Session session = sessionFactory.openSession();
				Transaction tx = session.beginTransaction();

				UserMaster O_userMaster = session.get(UserMaster.class, Integer.parseInt(object[0].toString()));

				//O_userMaster.setU_status(2);

				tx.commit();
				session.close();
				return jsonObject;

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid OTP..!");

			return jsonObject;

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}
	}

	@Override
	public JSONObject getUserProfileDetails(Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
			
		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);
		
		if(userMaster.getU_role().getrId() != 4) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "You are not a User..!");
			return jsonObject;

			
		}
		
		if(userMaster.getU_status() == 1) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Account Not Verified..!");
			return jsonObject;
		}
		
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT closing FROM MainWallet WHERE user.u_id= :id ORDER BY m_id DESC ");
		
		query.setParameter("id", user_id);
		query.setMaxResults(1);
		
		Double amount = (Double) query.uniqueResult();
		
		  jsonObject.put("u_id", userMaster.getU_id());
		  jsonObject.put("user_name", userMaster.getU_name());
		  jsonObject.put("user_mobile", userMaster.getM_phone());
		  jsonObject.put("user_email", userMaster.getU_email());
		  jsonObject.put("created_date", userMaster.getU_created_date());
		  jsonObject.put("wallet_amount", amount);

		    jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			
			return jsonObject;
			
			
		}catch (Exception e) {

		  e.printStackTrace();
		  
		  return SingleTon.APP_TECH_ERROR(null);
			
		}
	
	}

	@Override
	public boolean checkCurrentPasswordByID(Integer id, String current_password) {
		
		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, id);
		
		if(bCryptPasswordEncoder.matches(current_password, userMaster.getW_credential().getWc_password())) {
			
			return true;
		}
		
		return false;
		
	}

	@Override
	public void updateChangePassword(Integer id, String new_password) {
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
	
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		UserMaster userMaster = session.get(UserMaster.class, id);
		
		userMaster.getW_credential().setWc_password(bCryptPasswordEncoder.encode(new_password));
		userMaster.getW_credential().setWc_updated_on(calendar.getTime());
		
		session.update(userMaster);

		tx.commit();
		session.close();
		
	}

	@Override
	public boolean checkCurrentPinByID(Integer id, Integer current_pin) {
		
         UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, id);
         
		
		if(userMaster.getU_security_pin().equals(current_pin)) {
			
			return true;
		}
		
		return false;
		
		
	}

	@Override
	public void UpdateChangePin(Integer id, Integer new_pin) {
		
Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		UserMaster userMaster = session.get(UserMaster.class, id);
		
		userMaster.setU_security_pin(new_pin);
	
		session.update(userMaster);

		tx.commit();
		session.close();
	}

}
