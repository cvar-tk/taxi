package com.app.taxi.dao;

import java.util.Random;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.model.BackUpCode;
import com.app.taxi.model.UserMaster;
import com.app.taxi.service.BackUpCodeService;

@Repository("BackUpCodeDao")
@Transactional
@Component
public class BackUpCodeDaoImpl implements BackUpCodeDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private BackUpCodeService backUpCodeService;

	@Override
	public BackUpCode getBackUpCode(Integer user_id) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT bck_code from UserMaster WHERE u_id =:user_id");

		query.setParameter("user_id", user_id);

		BackUpCode backUpCode = (BackUpCode) query.uniqueResult();

		return backUpCode;
	}

	@Override
	public void getnerateBackUpCode(Integer user_id) {

		Random random = new Random();

		BackUpCode backUpCode = new BackUpCode();

		backUpCode.setBk_1(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_2(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_3(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_4(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_5(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_6(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_7(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_8(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_9(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setBk_10(Integer.parseInt(String.format("%04d", random.nextInt(10000))));
		backUpCode.setS_bk_1(1);
		backUpCode.setS_bk_2(1);
		backUpCode.setS_bk_3(1);
		backUpCode.setS_bk_4(1);
		backUpCode.setS_bk_5(1);
		backUpCode.setS_bk_6(1);
		backUpCode.setS_bk_7(1);
		backUpCode.setS_bk_8(1);
		backUpCode.setS_bk_9(1);
		backUpCode.setS_bk_10(1);

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster O_userMaster = session.get(UserMaster.class, user_id);

		if (O_userMaster == null) {
			tx.commit();
			session.close();
			return;
		}

		O_userMaster.setBck_code(backUpCode);

		tx.commit();
		session.close();

	}

}
