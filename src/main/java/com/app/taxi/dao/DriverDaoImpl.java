package com.app.taxi.dao;


import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.app.taxi.SingleTon;
import com.app.taxi.model.AdminControl;
import com.app.taxi.model.AdminWallet;
import com.app.taxi.model.BackUpCode;
import com.app.taxi.model.BookMark;
import com.app.taxi.model.MainWallet;
import com.app.taxi.model.RoleMaster;
import com.app.taxi.model.SubscribedList;
import com.app.taxi.model.TripMaster;
import com.app.taxi.model.TripStatus;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.WorkersCredential;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.UserService;
import com.app.taxi.service.VehicleService;
import com.google.cloud.storage.Acl.User;
import com.google.gson.GsonBuilder;

@Repository("driverDao")
@Transactional
@Component
public class DriverDaoImpl implements DriverDao {

	@Autowired
	private UserService userService;

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private DriverService driverService;

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private GsonBuilder gsonBuilder;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public JSONObject addnewDriverForApproval(UserMaster userMaster, WorkersCredential workersCredential,
			Integer owner_id) {

		try {

			JSONObject jsonObject = new JSONObject();

			if (userService.checkAccountExistingByRole(userMaster.getM_phone(), 3)) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "This mobile number already registered.Kindly login.!");
				return jsonObject;
			}

			userMaster.setU_status(1);

			userMaster.setU_created_date(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
			Random random = new Random();

			String OTP = String.format("%06d", random.nextInt(999999));

			SingleTon.sendphone(userMaster.getM_phone().toString(),
					"Your OTP is " + OTP + ".Don't share this OTP to anyone.this OTP is valid upto 5 Minutes. ");

			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

			calendar.add(Calendar.MINUTE, +5);

			userMaster.getW_credential().setWc_otp(OTP);
			userMaster.setMappedWith(owner_id);
			userMaster.getW_credential().setWc_otp_expiry_time(calendar.getTime());

			userMaster.setU_role(sessionFactory.getCurrentSession().get(RoleMaster.class, 3));

			workersCredential.setWc_updated_on(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

			workersCredential.setWc_created_on(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

			userMaster.setW_credential(workersCredential);

			sessionFactory.getCurrentSession().save(userMaster);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "User Registered..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}
	}

	@Override
	public JSONObject getDriverProfileDetails(Integer user_id) {

		JSONObject jsonObject = new JSONObject();

		try {

			Query query_user_role = sessionFactory.getCurrentSession()
					.createQuery("SELECT u_id,u_role.rm_id FROM UserMaster where u_id =:user_id");

			query_user_role.setParameter("user_id", user_id);

			List<Object[]> list = query_user_role.list();

			// check the user exist
			if (list == null) {
				System.err.println("Def -Err- DriverDao - user not exit");
				return SingleTon.APP_TECH_ERROR(null);
			}

			Object[] user_role_obj = list.get(0);

			// check the user role
			if (!user_role_obj[1].equals(3)) {

				System.err.println("Def -Err- DriverDao - user not a driver");

				return SingleTon.APP_TECH_ERROR(null);

			}

			Query query_driver_details = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id,u_name,m_phone,u_pic,u_email,u_status,MappedWith FROM UserMaster where u_id =:user_id");

			query_driver_details.setParameter("user_id", user_id);

			Object[] driver = (Object[]) query_driver_details.list().get(0);
			/*
			 * if (!driver[5].equals(4)) {
			 * 
			 * System.out.println(user_id);
			 * 
			 * System.err.println("Def -Err- DriverDao - user not a driver");
			 * 
			 * return SingleTon.APP_TECH_ERROR(null);
			 * 
			 * }
			 */
			jsonObject.put(SingleTon.USER_NAME, driver[1]);
			jsonObject.put(SingleTon.MOBILE, driver[2]);
			jsonObject.put(SingleTon.PROFILE_PICTURE, SingleTon.IMAGE_PATH+driver[3]);
			jsonObject.put(SingleTon.EMAIL, driver[4]);

			if (driver[6] == null) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
				return jsonObject;
			}

			jsonObject.put(SingleTon.LINKED_OWNER, userService.getUserNameById(Integer.parseInt(driver[6].toString())));

			Query query_car_list_by_owner = sessionFactory.getCurrentSession().createQuery(
					"SELECT vhm_id,vhm_number,vhm_colour.colour,vhm_status,vhm_model.vm_id,vhm_picked_by FROM VehicleMaster  WHERE vhm_owner.u_id =:owner_id AND vhm_status!=:vehicle_deactive_status AND vhm_status!=:vehicle_non_verify_status");

			query_car_list_by_owner.setParameter("owner_id", driver[6]);
			query_car_list_by_owner.setParameter("vehicle_deactive_status", 0);
			query_car_list_by_owner.setParameter("vehicle_non_verify_status", 1);

			List<Object[]> car_list = query_car_list_by_owner.list();

			JSONArray jsonArray = new JSONArray();

			for (Object[] car_object : car_list) {

				JSONObject object = new JSONObject();

				object.put(SingleTon.VEHICLE_NUMBER, car_object[1]);
				object.put(SingleTon.VEHICLE_COLOUR, car_object[2]);
				object.put(SingleTon.ID, car_object[0]);

				Integer picked_by = (Integer) car_object[5];
				
				//System.out.println(picked_by.equals(user_id) );
				//System.out.println(picked_by);

				if (picked_by != null && picked_by.equals(user_id)) {

					object.put(SingleTon.CONTENT_SELECT_STATUS, true);

				} else {

					object.put(SingleTon.CONTENT_SELECT_STATUS, false);

				}

				if (picked_by != null && !picked_by.equals(user_id)) {

					object.put(SingleTon.CONTENT_PICKED_STATUS, true);

				}else {
					
					object.put(SingleTon.CONTENT_PICKED_STATUS, false);

				}
				
				if (car_object[4] == null) {

					return SingleTon.APP_TECH_ERROR(null);
				}

				Query query_model_name = sessionFactory.getCurrentSession()
						.createQuery("SELECT vm_name,vm_brand.vb_name FROM VehicleModel where vm_id =:model_id");

				query_model_name.setParameter("model_id", car_object[4]);

				Object[] model = (Object[]) query_model_name.uniqueResult();

				object.put(SingleTon.CAR_MODEL_AND_BRAND, model[0] + "-" + model[1]);

				jsonArray.put(object);

			}

			jsonObject.put(SingleTon.VEHICLE_LIST, jsonArray);

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
			return jsonObject;

		} catch (JSONException e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public JSONArray getNewDrivers(Integer page, Integer limit) {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id,u_name,m_phone,driver_assets.dma_licence_number,driver_assets.dma_licence,MappedWith,driver_assets.dma_address_proof FROM UserMaster WHERE u_role.rm_id= :role and u_status= :status2 order by u_id DESC");

		query.setMaxResults(limit);
		query.setFirstResult(calculateOffset(page, limit));

		query.setParameter("role", 3);
		query.setParameter("status2", 2);

		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}

		try {
			for (Object[] objects : obj) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("d_id", objects[0]);
				jsonObject.put("d_name", objects[1]);
				jsonObject.put("d_phone", objects[2]);
				jsonObject.put("license_no", objects[3]);
				jsonObject.put("license", SingleTon.IMAGE_PATH+objects[4]);
				jsonObject.put("address_proof", SingleTon.IMAGE_PATH+objects[5]);

				Query name_query = sessionFactory.getCurrentSession()
						.createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id ");
				name_query.setParameter("id", (Integer) objects[5]);

				String name = (String) name_query.uniqueResult();

				System.out.println(name);

				jsonObject.put("owner_name", name);

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonArray;
	}

	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}

	@Override
	public Long getTotalCountNewDrivers() {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT COUNT(u_id) FROM UserMaster WHERE u_role.rm_id= :role and u_status= :status");

		query.setParameter("role", 3);
		query.setParameter("status", 1);

		Long count = (Long) query.uniqueResult();

		return count;
	}

	@Override
	public JSONObject getDriverDetailsByDriverId(Integer driver_id) {

		JSONObject jsonObject = new JSONObject();

		try {

			Query query_driver_details = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id,u_name,m_phone,u_pic,u_email,u_status,MappedWith,driver_assets.dma_licence_number,driver_assets.dma_licence,driver_assets.dma_address_proof,driver_assets.dma_dob,driver_assets.licence_exp,u_created_date,driver_assets.gov_proof_number FROM UserMaster WHERE u_id =:user_id");

			query_driver_details.setParameter("user_id", driver_id);

			List<Object[]> obj = query_driver_details.list();

			if (obj.isEmpty()) {

				return null;

			}

			Object[] object = obj.get(0);

			jsonObject.put("d_id", object[0]);
			jsonObject.put("d_name", object[1]);
			jsonObject.put("d_phone", object[2]);
			jsonObject.put("d_picture", SingleTon.IMAGE_PATH+object[3]);

			System.out.println("object4" + object[4]);

			if (object[4] == null) {

				jsonObject.put("d_email", "false");
			} else {
				jsonObject.put("d_email", object[4]);

			}

			jsonObject.put("d_status", object[5]);

			Query name_query = sessionFactory.getCurrentSession()
					.createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id ");
			name_query.setParameter("id", (Integer) object[6]);

			String name = (String) name_query.uniqueResult();

			System.out.println(name);

			jsonObject.put("owner_name", name);

			jsonObject.put("license_no", object[7]);
			jsonObject.put("license_image", SingleTon.IMAGE_PATH+object[8]);
			jsonObject.put("address_proof", SingleTon.IMAGE_PATH+object[9]);
			jsonObject.put("dateofbirth", object[10]);
			jsonObject.put("license_expiry", object[11]);
			jsonObject.put("created_date", object[12]);
			jsonObject.put("gov_proof", object[13]);

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT COUNT(*) FROM TripMaster WHERE trip_status.ts_id =:status and driver.u_id= :id");

			query.setParameter("status", 1);
			query.setParameter("id", driver_id);

			Long count = (Long) query.uniqueResult();

			// Check in trip or not
			if (count != 0) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1000);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver in a trip...!");
				return jsonObject;

			}

			// Driver In Vehicle
			if (driverService.checkDrvierAlreadyPickedAnyVehicle(driver_id)) {

				Query status_query = sessionFactory.getCurrentSession()
						.createQuery("SELECT vhm_status FROM VehicleMaster WHERE vhm_picked_by= :picked_by");

				status_query.setParameter("picked_by", driver_id);

				Integer status = (Integer) status_query.uniqueResult();

				if (status == 3) {
					jsonObject.put(SingleTon.ONLINE, true);
					jsonObject.put(SingleTon.CONTENT_MESSAGE, "Online");

				}

				if (status == 4) {

					jsonObject.put(SingleTon.ONLINE, false);
					jsonObject.put(SingleTon.CONTENT_MESSAGE, "Offline");

				}

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Already Picked Car...!");
				return jsonObject;

			}

			if (object[5].equals(2)) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1004);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Not Approved...!");
				return jsonObject;

			}
			// Driver Map Check
			if (object[6] == null) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1002);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver not mapped with any owner..!");
				return jsonObject;

			}

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1003);
			jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver In Idle...!");
			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public void updateDriverDetails(Integer driver_id, String driver_name, Date expiry, Date dob1,
			String license_no) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster userMaster = session.get(UserMaster.class, driver_id);

		
		userMaster.setU_name(driver_name);
		userMaster.getDriver_assets().setLicence_exp(expiry);
		userMaster.setU_status(4);
		userMaster.getDriver_assets().setDma_licence_number(license_no);
		userMaster.getDriver_assets().setDma_dob(dob1);

		session.update(userMaster);

		tx.commit();
		session.close();

	}

	@Override
	public void updatedriverStatus(String reason, String comments, Integer driver_id, Integer status) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster userMaster = session.get(UserMaster.class, driver_id);

		userMaster.getDriver_assets().setComments(comments);
		userMaster.getDriver_assets().setReason(reason);
		userMaster.setU_status(status);

		session.update(userMaster);

		tx.commit();
		session.close();

	}

	@Override
	public JSONArray getApprovedDriverList(Integer page, Integer limit) {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id,u_name,m_phone,driver_assets.dma_licence_number,driver_assets.dma_licence,MappedWith,driver_assets.dma_address_proof FROM UserMaster WHERE u_role.rm_id= :role and u_status= :status OR u_status= :s order by u_id desc");

		query.setMaxResults(limit);
		query.setFirstResult(calculateOffset(page, limit));

		query.setParameter("role", 3);
		query.setParameter("status", 4);
		query.setParameter("s", 5);

		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}

		try {
			for (Object[] objects : obj) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("d_id", objects[0]);
				jsonObject.put("d_name", objects[1]);
				jsonObject.put("d_phone", objects[2]);
				jsonObject.put("license_no", objects[3]);
				jsonObject.put("license", SingleTon.IMAGE_PATH+objects[4]);
				jsonObject.put("address_proof", SingleTon.IMAGE_PATH+objects[5]);
				
				
				System.err.println(SingleTon.IMAGE_PATH+objects[4]);
				System.err.println(SingleTon.IMAGE_PATH+objects[4]);

				Query name_query = sessionFactory.getCurrentSession()
						.createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id ");
				name_query.setParameter("id", (Integer) objects[5]);

				String name = (String) name_query.uniqueResult();

				System.out.println(name);

				jsonObject.put("owner_name", name);

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonArray;

	}

	@Override
	public Long getTotalCountApprovedDrivers() {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT COUNT(u_id) FROM UserMaster WHERE u_role.rm_id= :role and u_status= :status");

		query.setParameter("role", 3);
		query.setParameter("status", 4);

		Long count = (Long) query.uniqueResult();

		return count;
	}

	@Override
	public JSONObject getDriverDetailsByLicenseNo(String license_number) {

		JSONObject jsonObject = new JSONObject();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id,u_name,m_phone,driver_assets.dma_licence_number,driver_assets.dma_licence,MappedWith,driver_assets.dma_address_proof FROM UserMaster WHERE driver_assets.dma_licence_number= :license and u_status= :status and u_role.rm_id= :role");

		query.setParameter("license", license_number);
		query.setParameter("status", 4);
		query.setParameter("role", 3);

		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}

		try {
			for (Object[] objects : obj) {

				jsonObject.put("d_id", objects[0]);
				jsonObject.put("d_name", objects[1]);
				jsonObject.put("d_phone", objects[2]);
				jsonObject.put("license_no", objects[3]);
				jsonObject.put("license", SingleTon.IMAGE_PATH+objects[4]);
				jsonObject.put("address_proof", SingleTon.IMAGE_PATH+objects[5]);

				Query name_query = sessionFactory.getCurrentSession()
						.createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id ");
				name_query.setParameter("id", (Integer) objects[5]);

				String name = (String) name_query.uniqueResult();

				System.out.println(name);

				jsonObject.put("owner_name", name);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonObject;

	}

	@Override
	public JSONArray filterByDriverDate(Date fromdate, Date todate, Integer pageIndex) {

		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id,u_name,m_phone,driver_assets.dma_licence_number,driver_assets.dma_licence,MappedWith,driver_assets.dma_address_proof FROM UserMaster WHERE u_created_date BETWEEN :fromdate AND :todate and u_role.rm_id= :role and u_status= :status OR u_status= :s order by desc");

		query.setMaxResults(10);
		query.setFirstResult(calculateOffset(pageIndex, 10));

		query.setParameter("fromdate", new java.sql.Date(fromdate.getTime()));
		query.setParameter("todate", new java.sql.Date(todate.getTime()));
		query.setParameter("role", 3);
		query.setParameter("status", 4);
		query.setParameter("s", 5);

		System.out.println(query.getQueryString());

		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}

		try {
			for (Object[] objects : obj) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("d_id", objects[0]);
				jsonObject.put("d_name", objects[1]);
				jsonObject.put("d_phone", objects[2]);
				jsonObject.put("license_no", objects[3]);
				jsonObject.put("license", SingleTon.IMAGE_PATH+objects[4]);
				jsonObject.put("address_proof", SingleTon.IMAGE_PATH+objects[5]);

				Query name_query = sessionFactory.getCurrentSession()
						.createQuery("SELECT u_name FROM UserMaster WHERE u_id= :id ");
				name_query.setParameter("id", (Integer) objects[5]);

				String name = (String) name_query.uniqueResult();

				System.out.println(name);

				jsonObject.put("owner_name", name);

				jsonArray.put(jsonObject);

				System.out.println(jsonArray);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonArray;
	}

	@Override
	public Long getTotalCountApprovedDriverDate(Date fromdate, Date todate, Integer pageIndex) {

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT COUNT(u_id) FROM UserMaster WHERE u_created_date BETWEEN :fromdate AND :todate and u_role.rm_id= :role and u_status= :status ");

		query.setParameter("fromdate", new java.sql.Date(fromdate.getTime()));
		query.setParameter("todate", new java.sql.Date(todate.getTime()));
		query.setParameter("role", 3);
		query.setParameter("status", 4);

		Long count = (Long) query.uniqueResult();

		return count;

	}

	@Override
	public JSONObject ChangeDriverStatusToOnline(Integer user_id) {

		try {

			JSONObject jsonObject = new JSONObject();

			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();

			VehicleMaster vehicleMaster = (VehicleMaster) session
					.createQuery("FROM VehicleMaster WHERE vhm_picked_by =:picked_by")
					.setParameter("picked_by", user_id).uniqueResult();

			if (vehicleMaster == null || vehicleMaster.getVhm_status() != 4) {
				tx.commit();
				session.close();

				return SingleTon.APP_TECH_ERROR(null);
			}

			vehicleMaster.setVhm_status(3);
			session.update(vehicleMaster);

			tx.commit();

			session.close();

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}
	}

	@Override
	public JSONObject ChangeDriverStatusToOffOnline(Integer driver_id) {

		try {

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT COUNT(*) FROM TripMaster WHERE driver.u_id =:driver_id AND trip_status.ts_id =:trip_status");
			query.setParameter("trip_status", 1);
			query.setParameter("driver_id", driver_id);

			Long count = (Long) query.uniqueResult();

			if (count != 0) {

				return SingleTon.APP_TECH_ERROR(null);
			}

			UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, driver_id);

			if (userMaster == null || userMaster.getU_role().getrId() != 3) {

				return SingleTon.APP_TECH_ERROR(null);
			}

			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();

			VehicleMaster vehicleMaster = (VehicleMaster) session
					.createQuery("FROM VehicleMaster WHERE vhm_picked_by =:picked_by")
					.setParameter("picked_by", driver_id).setMaxResults(1).uniqueResult();

			if (vehicleMaster == null) {

				tx.commit();
				session.close();

				return SingleTon.APP_TECH_ERROR(null);
			}

			if (vehicleMaster.getVhm_status() != 3) {

				tx.commit();
				session.close();
				return SingleTon.APP_TECH_ERROR(null);

			}

			vehicleMaster.setVhm_status(4);

			session.update(vehicleMaster);

			tx.commit();

			session.close();

			JSONObject jsonObject = new JSONObject();

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}
	}

	@Override
	public JSONObject selectCarByDriver(Integer user_id, Integer vehicle_id) {

		JSONObject jsonObject = new JSONObject();

		
		try {
			// Driver check
			UserMaster user_master = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);

			if (user_master == null || user_master.getU_role().getrId() != 3 || user_master.getU_status() != 4) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Account Not Approved..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;

			}

			// Driver owner with status
			Integer mapper_owner = user_master.getMappedWith();
			
			UserMaster mapped_owner_obj = sessionFactory.getCurrentSession().get(UserMaster.class, mapper_owner);
			
			if (mapper_owner == null || mapped_owner_obj == null || mapped_owner_obj.getU_status() != 3) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Mapped owner not fould or owner not verified yet or blocked..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;
				//return SingleTon.APP_TECH_ERROR("Mapped owner not fould or owner not verified yet or blocked");
			}

			// Car Check
			VehicleMaster vehicleMaster = sessionFactory.getCurrentSession().get(VehicleMaster.class, vehicle_id);

			if (vehicleMaster == null || vehicleMaster.getVhm_owner().getU_id() != mapper_owner) {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Mapped with wrong owner..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;
				//return SingleTon.APP_TECH_ERROR("Mapped wrong owner");

			}
			// Vehicle owner check

			// in subscription check
			if (!vehicleService.checkVehicleSubscriptionStatusByVehicleId(vehicle_id, 1)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No subscription in car.please Add subscription..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;
				
				//return SingleTon.APP_TECH_ERROR("No subscription in car");

			}

			Integer picked_by = vehicleMaster.getVhm_picked_by();

			// vehicle free check
			if (driverService.checkDrvierAlreadyPickedAnyVehicle(user_id)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver already mapped with some other vehicle..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;
			

			}

			// vehicle free check
			if (picked_by != null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "vehicle already engaged with driver..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject;
				//return SingleTon.APP_TECH_ERROR("vehicle already engaged with driver");

			}

			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();

			VehicleMaster vMaster = session.get(VehicleMaster.class, vehicle_id);

			vMaster.setVhm_picked_by(user_id);
			vMaster.setVhm_status(4);

			session.update(vMaster);

			tx.commit();

			session.close();


			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;

		} catch (JSONException e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	public Boolean checkDrvierAlreadyPickedAnyVehicle(Integer driver_id) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT COUNT(*) FROM VehicleMaster WHERE vhm_picked_by= :pickby");

		query.setParameter("pickby", driver_id);

		Long count = (Long) query.uniqueResult();

		System.out.println(count);

		if (count == 0) {

			return false;

		}

		return true;

	}

	@Override
	public JSONObject getDriverCurrentStatusById(Integer driver_id) {

		JSONObject jsonObject = new JSONObject();

		try {

			// Driver check
			UserMaster user_master = sessionFactory.getCurrentSession().get(UserMaster.class, driver_id);

			if (user_master == null || user_master.getU_role().getrId() != 3) {

				return SingleTon.APP_TECH_ERROR(null);

			}

			if (user_master.getU_status() == 5) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE,
						"Your Account has been blocked by admin.Kindly contant support...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;

			}

			Query query = sessionFactory.getCurrentSession().createQuery(
					"FROM TripMaster WHERE trip_status.ts_id =:status and driver.u_id= :id ORDER BY tr_id DESC");

					query.setParameter("status", 1);
					query.setParameter("id", driver_id);

					TripMaster tripMaster = (TripMaster) query.uniqueResult();

					// Check in trip or not
					if (tripMaster != null) {

					JSONObject trip_obj = new JSONObject();
                    trip_obj.put(SingleTon.TRIP_ID, tripMaster.getTr_id());
					trip_obj.put(SingleTon.BASE_FAIR, tripMaster.getBase_fair());
					//trip_obj.put(SingleTon.MOBILE, tripMaster.getCustomer().getM_phone());
					trip_obj.put(SingleTon.STARTING_POINT, tripMaster.getStarting_point());
					trip_obj.put(SingleTon.ENDING_POINT, tripMaster.getEnding_point());
					trip_obj.put(SingleTon.STARTING_LAT, tripMaster.getStart_lat());
					trip_obj.put(SingleTon.STARTING_LANG, tripMaster.getStart_lang());
					trip_obj.put(SingleTon.ENDING_LAT, tripMaster.getEnd_lat());
					trip_obj.put(SingleTon.ENDING_LANG, tripMaster.getEnd_lang());
					trip_obj.put(SingleTon.BASE_MIN_PRICE, tripMaster.getPer_min_price());
					trip_obj.put(SingleTon.ESTMATED_AMOUNT, tripMaster.getEst_amount());
					trip_obj.put(SingleTon.BASE_KM, tripMaster.getBase_km());
					trip_obj.put(SingleTon.BASE_KM_PRICE, tripMaster.getPer_km_price());
					trip_obj.put(SingleTon.GST, tripMaster.getTax_percent());
					trip_obj.put(SingleTon.TOTAL_KM, tripMaster.getTotal_km());
					trip_obj.put(SingleTon.START_TIME, tripMaster.getStart_time());
					
                    jsonObject.put(SingleTon.TRIP_DETAILS, trip_obj);
					jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
					jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1000);
					jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver in a trip...!");
					return jsonObject;

					}
			// Driver In Vehicle
			if (driverService.checkDrvierAlreadyPickedAnyVehicle(driver_id)) {
				
				Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
				
				Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_driver.u_id= :driver_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
				
				subcribe_query.setParameter("driver_id", driver_id);
				subcribe_query.setParameter("type", 2);

				subcribe_query.setMaxResults(1);
				
				SubscribedList subscribedList = (SubscribedList) subcribe_query.uniqueResult();
				
				if( subscribedList == null || subscribedList.getTo_date().before(calendar.getTime()) ) {
					
					jsonObject.put(SingleTon.DRIVER_ONDEMAND_CODE, 1007);
					jsonObject.put(SingleTon.DRIVER_ONDEMAND_STATUS, false);
					
					jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
					jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1001);
					jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Already Picked Car...!");
					return jsonObject;	
					
				}
				jsonObject.put(SingleTon.DRIVER_ONDEMAND_CODE, 1007);
				jsonObject.put(SingleTon.DRIVER_ONDEMAND_STATUS, true);
				
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Already Picked Car...!");
				return jsonObject;

			}

			if (user_master.getU_status() == 2) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1004);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Not Approved...!");
				return jsonObject;

			}
			// Driver Map Check
			if (user_master.getMappedWith() == null) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1002);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver not mapped with any owner..!");
				return jsonObject;

			}

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.DRIVER_ACTIVITY_CODE, 1003);
			jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver In Idle...!");
			return jsonObject;

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public JSONObject getDriverONOFFStatusById(Integer driver_id) {

		JSONObject jsonObject = new JSONObject();
		try {

			// Driver check
			UserMaster user_master = sessionFactory.getCurrentSession().get(UserMaster.class, driver_id);

			if (user_master == null || user_master.getU_role().getrId() != 3 || user_master.getU_status() != 4) {

				return SingleTon.APP_TECH_ERROR(null);

			}

			Query query_status = sessionFactory.getCurrentSession()
					.createQuery("SELECT vhm_status FROM VehicleMaster WHERE vhm_picked_by =:driver_id");

			query_status.setParameter("driver_id", driver_id);

			Integer integer = (Integer) query_status.uniqueResult();

			if (integer == null) {
				return SingleTon.APP_TECH_ERROR(null);
			}

			Query query = sessionFactory.getCurrentSession().createQuery(
					"SELECT vhm_id,base_km,base_fair,per_km_price,per_min_price FROM VehicleMaster WHERE vhm_picked_by =:driver_id");
			query.setParameter("driver_id", driver_id);

			Object[] datas = (Object[]) query.uniqueResult();

			jsonObject.put(SingleTon.VEHICLE_ID, datas[0]);
			jsonObject.put(SingleTon.BASE_KM, datas[1]);
			jsonObject.put(SingleTon.BASE_FAIR, datas[2]);
			jsonObject.put(SingleTon.BASE_KM_PRICE, datas[3]);
			jsonObject.put(SingleTon.BASE_MIN_PRICE, datas[4]);
			
			Query License_query = sessionFactory.getCurrentSession().createQuery("SELECT driver_assets.licence_exp FROM UserMaster WHERE u_id= :id");
			
			License_query.setParameter("id", driver_id);
			
			Date date = (Date) License_query.uniqueResult();
			
			jsonObject.put("license_expiry", date);

			String data = gsonBuilder.create().toJson(sessionFactory.getCurrentSession().get(AdminControl.class, 1));

			jsonObject.put("admin_ctrl", new JSONObject(data));

			// Driver On-Line check
			if (integer == 3) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.ONLINE, true);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver In online...!");
				return jsonObject;

			} else {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				jsonObject.put(SingleTon.ONLINE, false);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver In offline...!");
				return jsonObject;

			}

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void updateExpiryDateByAdmin(Integer driver_id, Date expiry) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster userMaster = session.get(UserMaster.class, driver_id);

		if (userMaster == null || userMaster.getU_role().getrId() != 3 || userMaster.getU_status() != 3) {

		}

		userMaster.getDriver_assets().setLicence_exp(expiry);

		session.update(userMaster);

		tx.commit();

		session.close();

	}

	@Override
	public void ChangeDriverStatusToDisable(Integer check_status, Integer driver_id) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		UserMaster userMaster = session.get(UserMaster.class, driver_id);

		userMaster.setU_status(check_status);

		session.update(userMaster);

		tx.commit();
		session.close();

	}

	@Override
	public JSONObject getUserLocationRequest(Integer driver_id) {

		JSONObject jsonObject = new JSONObject();
		try {
		JSONArray jsonArray = new JSONArray();

		Query id_query = sessionFactory.getCurrentSession()
				.createQuery("SELECT vhm_id FROM VehicleMaster where vhm_picked_by= :picked_by");

		id_query.setParameter("picked_by", driver_id);

		Integer v_id = (Integer) id_query.uniqueResult();
		
		System.err.println(driver_id);

		System.err.println(v_id);
		
	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		
		Date date1 = cal.getTime();
		
		cal.add(Calendar.SECOND, -30);
		Date date = cal.getTime();
		
		System.err.println(date);
		System.err.println(date1);

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT user_master.u_id,user_master.u_name,user_master.m_phone,address,user_lat,user_lang FROM Callbacks where vehicle_master.vhm_id = :id and created_date BETWEEN :from AND :to ORDER BY c_id");

		query.setParameter("id", v_id);
		query.setParameter("from", new java.sql.Date(date.getTime()));
		query.setParameter("to", new java.sql.Date(date1.getTime()));

		

		List<Object[]> obj = query.list();
		
		if(obj == null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Empty List...!");
			jsonObject.put("data", new JSONArray());
			return jsonObject;
			
		}
		

		

			for (Object[] objects : obj) {
				
				JSONObject jsonObject2 = new JSONObject();

				jsonObject2.put("user_id", objects[0]);
				jsonObject2.put("user_name", objects[1]);

				jsonObject2.put("user_phone", objects[2]);
				jsonObject2.put("address", objects[3]);

				jsonObject2.put("user_latitude", objects[4]);
				jsonObject2.put("user_longitude", objects[5]);
				
				jsonArray.put(jsonObject2);

			}
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "success");
			jsonObject.put("data", jsonArray);
			return jsonObject;
			
		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);

		}

		
	}

	@Override
	public JSONObject changeDriverSelfDisableStatus(Integer user_id) {

		JSONObject jsonObject = new JSONObject();
		try {
			UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);

			if (userMaster == null) {

				return SingleTon.APP_TECH_ERROR(null);
			}

			if (userMaster.getU_status() == 1 || userMaster.getU_status() == 2) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Not Verifed...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;
			}

			Query query = sessionFactory.getCurrentSession()
					.createQuery("SELECT vhm_id,vhm_status FROM VehicleMaster WHERE vhm_picked_by = :picked_by");

			query.setParameter("picked_by", user_id);

			List<Object[]> obj = query.list();

			if (!obj.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "You must deselect the Car...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;

			}

		
			Session session = sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			
			userMaster.setMappedWith(null);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Successfully Terminated...!");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			session.update(userMaster);

			tx.commit();
			session.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonObject;

	}

	@Override
	public JSONObject updateBaseKmDetails(Integer user_id, Double base_km, Double base_km_price, Double per_min_price,
			Double base_fair) {

		JSONObject jsonObject = new JSONObject();
		try {

			Query user_query = sessionFactory.getCurrentSession().createQuery(
					"SELECT u_id,u_status,MappedWith,driver_assets.licence_exp FROM UserMaster WHERE u_id= :id AND u_role.rm_id= :role ");

			user_query.setParameter("id", user_id);
			user_query.setParameter("role", 3);

			List<Object[]> obj = user_query.list();

			if (obj == null) {

				return SingleTon.APP_TECH_ERROR(null);

			}

			Object[] object = obj.get(0);

			if (object[1].equals(5)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE,
						"Your Account has been blocked by admin.Kindly contant support...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;

			}

			if (object[1].equals(1) || object[1].equals(2)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Account Not Verified..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;

			}
			if (object[2] == null) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Not Mapped to Any Owner...!");
				return jsonObject;
			}

			if (driverService.checkDrvierAlreadyPickedAnyVehicle((Integer) object[0]) == false) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				jsonObject.put(SingleTon.DRIVER_ACTIVITY_MESSAGE, "Driver Not Picked Car...!");
				return jsonObject;

			}
			
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			
			if(((Date) object[3]).before(calendar.getTime())) {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "License Expired...!");
				return jsonObject;
				
			}
			

			Session session = sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			Query query = session.createQuery("SELECT vhm_id FROM VehicleMaster WHERE vhm_picked_by= :picked_by");

			query.setParameter("picked_by", user_id);

			Integer v_id = (Integer) query.uniqueResult();

			System.out.println("idddd" + v_id);

			VehicleMaster vehicleMaster = session.get(VehicleMaster.class, v_id);

			System.out.println(vehicleMaster.getVhm_status());

			if (vehicleMaster.getVhm_status() == 4) {

				vehicleMaster.setBase_fair(base_fair);
				vehicleMaster.setBase_km(base_km);
				vehicleMaster.setPer_km_price(base_km_price);
				vehicleMaster.setPer_min_price(per_min_price);
				vehicleMaster.setVhm_status(3);

				session.update(vehicleMaster);
				tx.commit();
				session.close();

				jsonObject.put(SingleTon.CONTENT_STATUS, true);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Successfully Updated...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				return jsonObject;

			} else {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Not In Offline...!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;

			}

		} catch (Exception e) {
			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null);

		}

	}

	@Override
	public JSONObject startTrip(JSONObject request, Integer driver_id) {

		JSONObject jsonObject = new JSONObject();

		try {
			
			System.err.println(request);

			UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, driver_id);

			if (userMaster == null) {

				return SingleTon.APP_TECH_ERROR(null);
			}

			Query vehicle_query = sessionFactory.getCurrentSession()
					.createQuery("SELECT vhm_id FROM VehicleMaster WHERE vhm_picked_by= :picked_by");

			vehicle_query.setParameter("picked_by", driver_id);

			Integer v_id = (Integer) vehicle_query.uniqueResult();

			VehicleMaster vehicleMaster = sessionFactory.getCurrentSession().get(VehicleMaster.class, v_id);

			if (vehicleMaster.getVhm_status() != 3) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver not being online.");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;
			}

			Query query = sessionFactory.getCurrentSession().createQuery(
					"FROM UserMaster WHERE m_phone=:m_phone AND u_role.rm_id=:u_role AND u_status =:verify_status ORDER BY u_id DESC");

			query.setBigInteger("m_phone", BigInteger.valueOf(request.getLong(SingleTon.MOBILE)));
			query.setParameter("u_role", 4);
			query.setParameter("verify_status", 2);

			UserMaster customer = (UserMaster) query.setMaxResults(1).uniqueResult();

			if (customer == null) {

				return SingleTon.APP_TECH_ERROR("Invalid user mobile number..!");

			}

			Query vehicle_master_query = sessionFactory.getCurrentSession()
					.createQuery("FROM VehicleMaster WHERE vhm_picked_by =:vhm_picked_by");

			vehicle_master_query.setParameter("vhm_picked_by", driver_id);

			VehicleMaster vehicle_master = (VehicleMaster) vehicle_master_query.uniqueResult();

			if (vehicle_master == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "No Vehicle attached with the driver..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;

			}
			
			
			

			Boolean bool_status = checkBackCodeByUserId(customer, request.getInt(SingleTon.BACKUP_CODE));

			if (bool_status != true) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Backup Code.");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject;
			}

			TripMaster tripMaster = new TripMaster();
			tripMaster.setBck_code(request.getInt(SingleTon.BACKUP_CODE));
			tripMaster.setStarting_point(request.getString(SingleTon.STARTING_POINT));
			tripMaster.setEnding_point(request.getString(SingleTon.ENDING_POINT));
			tripMaster.setStart_lat(request.getDouble(SingleTon.STARTING_LAT));
			tripMaster.setStart_lang(request.getDouble(SingleTon.STARTING_LANG));
			tripMaster.setEnd_lat(request.getDouble(SingleTon.ENDING_LAT));
			tripMaster.setEnd_lang(request.getDouble(SingleTon.ENDING_LANG));
			tripMaster.setEst_amount(request.getDouble(SingleTon.ESTMATED_AMOUNT));
			tripMaster.setBase_fair(request.getDouble(SingleTon.BASE_FAIR));
			tripMaster.setBase_km(request.getDouble(SingleTon.BASE_KM));
			tripMaster.setPer_min_price(request.getDouble(SingleTon.BASE_MIN_PRICE));
			tripMaster.setPer_km_price(request.getDouble(SingleTon.BASE_KM_PRICE));
			tripMaster.setStart_time(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
			
			tripMaster.setCgst(request.getDouble(SingleTon.CGST));
			tripMaster.setSgst(request.getDouble(SingleTon.SGST));

			
			tripMaster.setBitmap_image(request.getString(SingleTon.BITMAP_IMAGE));

			
			tripMaster.setTotal_km(request.getDouble(SingleTon.TOTAL_KM));
			tripMaster.setTax_percent(request.getDouble(SingleTon.TAX_PERCENT));

			tripMaster.setDriver((UserMaster) sessionFactory.getCurrentSession().get(UserMaster.class, driver_id));
			tripMaster.setCustomer(customer);
			tripMaster.setVehicle(vehicle_master);
			TripStatus tripStatus = sessionFactory.getCurrentSession().get(TripStatus.class, 1);
			tripMaster.setTrip_status(tripStatus);
			sessionFactory.getCurrentSession().save(tripMaster);
			updateBackCodeByUserId(customer, request.getInt(SingleTon.BACKUP_CODE));
			
			//update trip status into vehiclemaster
			Session session = sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			
			VehicleMaster vehicleMaster2 = session.get(VehicleMaster.class, v_id);
			
			vehicleMaster2.setVhm_status(5);
			
			tx.commit();
			session.close();
			
			
			Query trip_query = sessionFactory.getCurrentSession().createQuery("SELECT tr_id,start_time FROM TripMaster WHERE driver.u_id= :driver ORDER BY tr_id DESC");
			trip_query.setParameter("driver", driver_id);
			trip_query.setMaxResults(1);
			
			//Integer trip_id = (Integer) trip_query.uniqueResult();
			Object[] object= (Object[]) trip_query.uniqueResult();
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.TRIP_ID, object[0]);
			jsonObject.put(SingleTon.START_TIME, object[1]);
			
			return jsonObject;
		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	private Boolean updateBackCodeByUserId(UserMaster user, Integer code) {
		Session session = sessionFactory.openSession();

		try {
			Integer actvie = 1;

			Integer bck_obj_id = user.getBck_code().getBc_id();

			BackUpCode bck_code = session.get(BackUpCode.class, bck_obj_id);

			System.out.println(bck_code.getBk_1().equals(code));
			System.out.println(bck_code.getS_bk_1().equals(actvie));

			if (bck_code.getBk_1().equals(code) && bck_code.getS_bk_1().equals(actvie)) {

				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_1(0);
				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_2().equals(code) && bck_code.getS_bk_2().equals(actvie)) {

				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_2(0);
				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_3().equals(code) && bck_code.getS_bk_3().equals(actvie)) {

				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_3(0);
				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_4().equals(code) && bck_code.getS_bk_4().equals(actvie)) {

				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_4(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_5().equals(code) && bck_code.getS_bk_5().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_5(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;

			}

			if (bck_code.getBk_6().equals(code) && bck_code.getS_bk_6().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_6(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_7().equals(code) && bck_code.getS_bk_7().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_7(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_8().equals(code) && bck_code.getS_bk_8().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_8(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_9().equals(code) && bck_code.getS_bk_9().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_9(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;
			}

			if (bck_code.getBk_10().equals(code) && bck_code.getS_bk_9().equals(actvie)) {
				Transaction tx = session.beginTransaction();
				bck_code.setS_bk_10(0);

				session.update(bck_code);
				tx.commit();
				session.close();
				return true;

			}

			session.close();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			session.close();
			return false;
		}

	}

	private Boolean checkBackCodeByUserId(UserMaster user, Integer code) {
		Session session = sessionFactory.openSession();

		try {
			Integer actvie = 1;

			Integer bck_obj_id = user.getBck_code().getBc_id();

			BackUpCode bck_code = session.get(BackUpCode.class, bck_obj_id);

			System.out.println(bck_code.getBk_1().equals(code));
			System.out.println(bck_code.getS_bk_1().equals(actvie));

			if (bck_code.getBk_1().equals(code) && bck_code.getS_bk_1().equals(actvie)) {

				System.out.println("trueeeeeeeeee");
				return true;
			}

			if (bck_code.getBk_2().equals(code) && bck_code.getS_bk_2().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_3().equals(code) && bck_code.getS_bk_3().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_4().equals(code) && bck_code.getS_bk_4().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_5().equals(code) && bck_code.getS_bk_5().equals(actvie)) {

				return true;

			}

			if (bck_code.getBk_6().equals(code) && bck_code.getS_bk_6().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_7().equals(code) && bck_code.getS_bk_7().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_8().equals(code) && bck_code.getS_bk_8().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_9().equals(code) && bck_code.getS_bk_9().equals(actvie)) {

				return true;
			}

			if (bck_code.getBk_10().equals(code) && bck_code.getS_bk_10().equals(actvie)) {

				return true;

			}

			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		
	}
	
	@Override
	public JSONObject EndTrip(JSONObject request, Integer user_id) {

	JSONObject jsonObject = new JSONObject();
	try {
	TripMaster tripMaster = (TripMaster) sessionFactory.getCurrentSession().get(TripMaster.class,
	request.getInt(SingleTon.TRIP_ID));
	
	System.err.println(user_id);
	System.err.println(tripMaster.getCustomer().getU_id());


	if (tripMaster == null || tripMaster.getTrip_status().getTs_id() != 1
	|| !tripMaster.getDriver().getU_id().equals(user_id)) {

	jsonObject.put(SingleTon.CONTENT_STATUS, false);
	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Driver or Trip ID.!");
	jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
	return jsonObject;

	}
	if (request.getInt(SingleTon.PAYMENT_TYPE) == 1) {
	
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		TripMaster tripMaster_obj = (TripMaster) session.get(TripMaster.class,
		request.getInt(SingleTon.TRIP_ID));

		TripStatus status = session.get(TripStatus.class, 2);

		tripMaster_obj.setAmount(request.getDouble(SingleTon.AMOUNT));
		tripMaster_obj.setTotal_min(request.getDouble(SingleTon.TOTAL_MIN));
		
		try {
			
		tripMaster_obj.setToll(request.getDouble(SingleTon.TOLL_AMOUNT));
		}catch (Exception e) {

			e.printStackTrace();
		}
		tripMaster_obj.setTotal_km(request.getDouble(SingleTon.TOTAL_KM));
		
		
		
		tripMaster_obj.setTrip_status(status);
		// tripMaster_obj.setEnd_time(end_time);
		// Payment type check
		// 1 for online 2 for COD
		if (request.getInt(SingleTon.PAYMENT_TYPE) == 1) {

		tripMaster_obj.setPayment_method(1);
		}

		String pattern = "MM/dd/yyyy HH:mm:ss";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		Date end_time=simpleDateFormat.parse(request.getString(SingleTon.END_TIME)); 

		tripMaster_obj.setEnd_time(end_time);


		if (request.getInt(SingleTon.PAYMENT_TYPE) == 2) {

		tripMaster_obj.setPayment_method(2);

		}
		
		
		Query query = session.createQuery("SELECT vhm_id FROM VehicleMaster WHERE vhm_picked_by= :picked_by");
		
		query.setParameter("picked_by", user_id);
		
		Integer id = (Integer) query.uniqueResult();
		
		VehicleMaster vehicleMaster = session.get(VehicleMaster.class, id);
		
		vehicleMaster.setVhm_status(3);
		
	
		session.update(tripMaster_obj);
	    session.update(vehicleMaster);
	    
		
		tx.commit();
		session.close();

		jsonObject.put(SingleTon.CONTENT_STATUS, true);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success.!");
		jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		return jsonObject;
	
		
	}
	
	Query amount_query = sessionFactory.getCurrentSession().createQuery("FROM MainWallet WHERE user.u_id= :id ORDER BY m_id DESC");
	
	amount_query.setParameter("id",tripMaster.getCustomer().getU_id());
	amount_query.setMaxResults(1);
	
	MainWallet mainWallet = (MainWallet) amount_query.uniqueResult();
	
	if(mainWallet == null) {
		
		return SingleTon.APP_TECH_ERROR(null);
		
	}
	
	//Double amount = (Double) amount_query.uniqueResult();
	
	Double Trip_amount = request.getDouble(SingleTon.AMOUNT);
	
	if(mainWallet.getClosing()  < Trip_amount) {
		
		jsonObject.put(SingleTon.CONTENT_STATUS, false);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Insufficient Amount.Please add money to wallet..!");
		jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
		return jsonObject;
	}
	
    
	Session session = sessionFactory.openSession();

	Transaction tx = session.beginTransaction();

	TripMaster tripMaster_obj = (TripMaster) session.get(TripMaster.class,
	request.getInt(SingleTon.TRIP_ID));

	TripStatus status = session.get(TripStatus.class, 2);

	tripMaster_obj.setAmount(request.getDouble(SingleTon.AMOUNT));
	tripMaster_obj.setTotal_min(request.getDouble(SingleTon.TOTAL_MIN));
	
	try {
		
	tripMaster_obj.setToll(request.getDouble(SingleTon.TOLL_AMOUNT));
	}catch (Exception e) {

		e.printStackTrace();
	}
	tripMaster_obj.setTotal_km(request.getDouble(SingleTon.TOTAL_KM));
	
	tripMaster_obj.setTrip_status(status);
	// tripMaster_obj.setEnd_time(end_time);
	// Payment type check
	// 1 for online 2 for COD
	if (request.getInt(SingleTon.PAYMENT_TYPE) == 1) {

	tripMaster_obj.setPayment_method(1);
	}

	String pattern = "MM/dd/yyyy HH:mm:ss";

	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	Date end_time=simpleDateFormat.parse(request.getString(SingleTon.END_TIME)); 

	tripMaster_obj.setEnd_time(end_time);


	if (request.getInt(SingleTon.PAYMENT_TYPE) == 2) {

	tripMaster_obj.setPayment_method(2);

	}
	
	
	Query query = session.createQuery("SELECT vhm_id FROM VehicleMaster WHERE vhm_picked_by= :picked_by");
	
	query.setParameter("picked_by", user_id);
	
	Integer id = (Integer) query.uniqueResult();
	
	VehicleMaster vehicleMaster = session.get(VehicleMaster.class, id);
	
	vehicleMaster.setVhm_status(3);
	
	//main wallet transaction
	MainWallet mainWallet2 = new MainWallet();
	
	Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
	
	mainWallet2.setCreated_date(calendar.getTime());
	mainWallet2.setPayment_type(0);
	
	Double closeamount = mainWallet.getClosing();
	
	System.err.println(closeamount);
	
	System.err.println(Trip_amount);
	
	Double changeopen= closeamount-Trip_amount;
	
	mainWallet2.setOpening(closeamount);
	mainWallet2.setOpposition(Trip_amount);
	mainWallet2.setClosing(changeopen);
	mainWallet2.setUser(session.get(UserMaster.class, tripMaster.getCustomer().getU_id()));
	
	
	session.save(mainWallet2);

	session.update(tripMaster_obj);
    session.update(vehicleMaster);
    
	Query admin_query = session.createQuery("FROM AdminWallet ORDER BY aw_id DESC"); 
			
	admin_query.setMaxResults(1);
	
	//admin wallet transaction
	AdminWallet adminWallet = (AdminWallet) admin_query.uniqueResult();
	
	AdminWallet adminWallet2 = new AdminWallet();
	
	adminWallet2.setCreate_date(calendar.getTime());
	adminWallet2.setType(1);
	adminWallet2.setA_opening(adminWallet.getA_closing());
	adminWallet2.setA_opposition(Trip_amount);
	adminWallet2.setA_closing(adminWallet.getA_closing()+Trip_amount);
	adminWallet2.setUser(session.get(UserMaster.class, tripMaster.getCustomer().getU_id()));
	adminWallet2.setDriver(tripMaster.getDriver().getU_id());
	adminWallet2.setOwner(tripMaster.getVehicle().getVhm_owner().getU_id());
	adminWallet2.setTripMaster(session.get(TripMaster.class, tripMaster.getTr_id()));

	session.save(adminWallet2);
    
    
	tx.commit();
	session.close();

	jsonObject.put(SingleTon.CONTENT_STATUS, true);
	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success.!");
	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
	return jsonObject;

	} catch (Exception e) {

	       e.printStackTrace();

	      return SingleTon.APP_TECH_ERROR(null);
	}

	}

	@Override
	public JSONObject deselectCarByDriverId(Integer user_id, Integer vehicle_id) {
		
		JSONObject jsonObject = new JSONObject();
		try {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM TripMaster WHERE trip_status.ts_id =:status and driver.u_id= :id ORDER BY tr_id DESC");

				query.setParameter("status", 1);
				query.setParameter("id", user_id);

				TripMaster tripMaster = (TripMaster) query.uniqueResult();

		        if(tripMaster != null) {
		        	
		        	jsonObject.put(SingleTon.CONTENT_STATUS, false);
		        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "You are in Trip.!");
		        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
		        	return jsonObject;
		        	
		        }
		        
		        Query deselect_query = sessionFactory.getCurrentSession().createQuery("FROM VehicleMaster WHERE vhm_id= :id AND vhm_picked_by= :picked_by");
		        
		        deselect_query.setParameter("id", vehicle_id);  	
		        deselect_query.setParameter("picked_by", user_id);
		        
		        VehicleMaster vehicleMaster = (VehicleMaster) deselect_query.uniqueResult();
		        
		        if(vehicleMaster ==  null) {
		        	
		        	return SingleTon.APP_TECH_ERROR(null);
		        	
		        }
		        
		        if(vehicleMaster.getVhm_status() == 3 || vehicleMaster.getVhm_status() == 4 && vehicleMaster.getVhm_picked_by() != null) {
		        	
		        	Session session = sessionFactory.openSession();

					Transaction tx = session.beginTransaction();
                     
					if(vehicleMaster.getVhm_status() == 3) {
					vehicleMaster.setVhm_status(4);
					}
					vehicleMaster.setVhm_picked_by(null);
					
					tx.commit();
					session.close();
					
					jsonObject.put(SingleTon.CONTENT_STATUS, true);
		        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success.!");
		        	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		        	return jsonObject;
					
		        	
		        }else {
		        	
		        	 return SingleTon.APP_TECH_ERROR(null);
		        }
		        
				
		}catch (Exception e) {

		  e.printStackTrace();
		  return SingleTon.APP_TECH_ERROR(null);
		 
		}
		
	}

	@Override
	public JSONObject addExcistingDriverToOwner(BigInteger mobile, Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT u_id FROM UserMaster WHERE m_phone= :phone and u_role.rm_id= :role");
		
		query.setParameter("phone", mobile);
		query.setParameter("role", 3);
		
		Integer u_id = (Integer) query.uniqueResult();
		
		if(u_id == null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Not Registered in this Number..!");
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
        	return jsonObject;
			
		}
		
        UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, u_id);
		
		if(userMaster.getU_status() == 2 || userMaster.getU_status() == 1) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Not Verified yet..!");
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
        	return jsonObject;
		}
		
		
		
		Query vehicle_query = sessionFactory.getCurrentSession().createQuery("SELECT vhm_id,vhm_status,vhm_picked_by FROM VehicleMaster WHERE vhm_picked_by= :picked_by");
		
		vehicle_query.setParameter("picked_by", u_id);
		
		List<Object[]> object = vehicle_query.list();
		
		try {
		Object[] obj = object.get(0);
		
		if(obj[1].equals(5)) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver In a TRIP..!");
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
        	return jsonObject;
		
		}
		if(obj[2] != null){
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver Picked a Car..!");
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
        	return jsonObject;
		}
		}catch (Exception e) {

		e.printStackTrace();
		}
		
		if(userMaster.getMappedWith() != null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
        	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver already Mapped With Another Owner..!");
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
        	return jsonObject;
		}
		
		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		userMaster.setMappedWith(user_id);
		
		tx.commit();
		
		session.close();
		                                              
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
    	jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
    	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
    	return jsonObject;
		
		
		}catch (Exception e) {
			
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null);
		}
		
	}

	@Override
	public JSONObject getDriverTripHistory(Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		JSONArray jsonArray = new JSONArray();
		
		try {
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT vhm_id FROM VehicleMaster WHERE vhm_picked_by= :picked_by");
	
		query.setParameter("picked_by", user_id);
		
		Integer vehicle_id = (Integer) query.uniqueResult();
		
		if(vehicle_id == null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
        	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put("data", new JSONArray());
			return jsonObject;
		}
		
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		
		Date date1 = cal.getTime();
		
		cal.add(Calendar.HOUR, -24);
		Date date = cal.getTime();
		
		System.err.println(date);
		System.err.println(date.getTime());
		
				
		Query trip_query = sessionFactory.getCurrentSession().createQuery("SELECT tr_id,customer.u_name,vehicle.vhm_number,driver.u_name,starting_point,amount,payment_method,start_time FROM TripMaster WHERE driver.u_id= :did AND vehicle.vhm_id= :vid AND trip_status.ts_id= :status AND start_time BETWEEN :from AND :to");
		
		trip_query.setParameter("did", user_id);
		trip_query.setParameter("vid", vehicle_id);
		trip_query.setParameter("status", 2);
		trip_query.setParameter("from", new java.sql.Date(date.getTime()));
		trip_query.setParameter("to", new java.sql.Date(date1.getTime()));


		List<Object[]> obj = trip_query.list();
		
		for (Object[] objects : obj) {
			
			JSONObject jsonObject2 = new JSONObject();
			
			jsonObject2.put("trip_id", objects[0]);
			jsonObject2.put("customer_name", objects[1]);
			jsonObject2.put("vehicle_number", objects[2]);
			jsonObject2.put("driver_name", objects[3]);
			jsonObject2.put("starting_point", objects[4]);
			jsonObject2.put("amount", objects[5]);
			jsonObject2.put("payment_type", objects[6]);
			jsonObject2.put("start_time", objects[7]);
			
			jsonArray.put(jsonObject2);

		}
		
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
    	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		jsonObject.put("data", jsonArray);
		return jsonObject;
		
		}catch (Exception e) {

		 e.printStackTrace();
		 
		 return SingleTon.APP_TECH_ERROR(null);
		
		}
	}

	@Override
	public JSONObject checkCurrentPasswordByID(Integer user_id, String password) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
	
		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);
		
		System.err.println(bCryptPasswordEncoder.encode(userMaster.getW_credential().getWc_password()));
		System.err.println(bCryptPasswordEncoder.encode(password));

		System.err.println(bCryptPasswordEncoder.matches(password, userMaster.getW_credential().getWc_password()));
		
		if(bCryptPasswordEncoder.matches(password, userMaster.getW_credential().getWc_password())) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
	    	jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password matched");
			return jsonObject;
			
		}
		
		jsonObject.put(SingleTon.CONTENT_STATUS, false);
    	jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password not matched");
		return jsonObject;
		
		}catch (Exception e) {

		e.printStackTrace();
		
		return SingleTon.APP_TECH_ERROR(null);
		
		}
		
	}

	@Override
	public JSONObject getOndemandPlanDetailaById(Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		
		  Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			
			Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_driver.u_id= :driver_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
			
			subcribe_query.setParameter("driver_id", user_id);
			subcribe_query.setParameter("type", 2);

			subcribe_query.setMaxResults(1);
			
			SubscribedList subscribedList1 = (SubscribedList) subcribe_query.uniqueResult();
			
						
			if(subscribedList1 == null || subscribedList1.getTo_date().before(calendar1.getTime())) {
				
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your plan Expired ..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;
				
			}
			
			jsonObject.put("subscribe_id", subscribedList1.getSl_id());
			jsonObject.put("duration", subscribedList1.getSl_duration());
			jsonObject.put("price", subscribedList1.getSl_price());
			jsonObject.put("type_id", subscribedList1.getSl_type().getPt_id());
			jsonObject.put("type_name", subscribedList1.getSl_type().getPt_title());
			jsonObject.put("from_date", subscribedList1.getFrom_date());
			jsonObject.put("to_date", subscribedList1.getTo_date());
			jsonObject.put("driver_id", subscribedList1.getSl_driver().getU_id());

			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your plan Details ..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;
			
			
		
		}catch (Exception e) {
			
			
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
			
		}
	}

	@Override
	public JSONObject getManyTripLocationByLatitude(Integer user_id, Double latitude, Double longtitude) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		JSONArray array = new JSONArray();
		
		
		  Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			
			Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_driver.u_id= :driver_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
			
			subcribe_query.setParameter("driver_id", user_id);
			subcribe_query.setParameter("type", 2);

			subcribe_query.setMaxResults(1);
			
			SubscribedList subscribedList1 = (SubscribedList) subcribe_query.uniqueResult();
			
			System.err.println(subscribedList1.getTo_date());
			System.err.println(calendar1.getTime());
			
			if(subscribedList1 == null || subscribedList1.getTo_date().before(calendar1.getTime())) {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Plan Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

			    jsonObject.put("data",new JSONArray());
			    
			    return jsonObject;
				
				
			}
		
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		
		Date date = calendar.getTime();
		
		
		calendar.add(Calendar.HOUR, -1);
		calendar.add(Calendar.MINUTE, 00);
		
		
		Date date1 = calendar.getTime();
		
		System.err.println(date1);
		System.err.println(new java.sql.Date(date.getTime()));
		
		
		DecimalFormat df = new DecimalFormat("00");

		String fromdate_sql = (date1.getYear() + 1900) + "-" + df.format(date1.getMonth() + 1) + "-"
				+ df.format(date1.getDate()) + " " + date1.getHours() + ":" + df.format(date1.getMinutes()) + ":"
				+ date1.getSeconds();
		String todate_sql = (date.getYear() + 1900) + "-" + df.format(date.getMonth() + 1) + "-"
				+ df.format(date.getDate()) + " " + date.getHours() + ":" + df.format(date.getMinutes()) + ":"
				+ date.getSeconds();

		System.err.println(todate_sql);

		
		String qry = "SELECT start_lat,start_lang, SQRT( POW(69.1 * (start_lat -"+ latitude+"), 2) + POW(69.1 * ("+longtitude+"- start_lang) * COS(start_lat / 57.3), 2)) AS distance FROM trip_master WHERE start_time BETWEEN "+"'"+fromdate_sql+"'"+" AND "+"'"+todate_sql+"'"+" HAVING distance < 0.621371 ORDER BY distance";

		SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(qry);
		
		if(sqlQuery.list() == null) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);


			jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

		    jsonObject.put("data",new JSONArray());
		    
		    return jsonObject;

			
		}

			//array = new JSONArray(gsonBuilder.create().toJson(sqlQuery.list()));
			
			
					jsonObject.put(SingleTon.CONTENT_STATUS, true);

					jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

					jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				    jsonObject.put("data", new JSONArray(gsonBuilder.create().toJson(sqlQuery.list())));

					return jsonObject;


		} catch (Exception e) {
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null);
		}

	}

	@Override
	public void adminTerminateTripByTripId(Integer trip_id, Integer userid) {

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

		Session session = sessionFactory.openSession();
		
		Transaction tx = session.beginTransaction();
		
		TripMaster tripMaster = session.get(TripMaster.class, trip_id);
		
		TripStatus tripStatus = session.get(TripStatus.class, 3);
		
		tripMaster.setTrip_status(tripStatus);
		
		tripMaster.setEnd_time(calendar.getTime());
		tripMaster.setAmount(0.0);
		
		session.update(tripMaster);
		tx.commit();
		session.close();
		
	}
	
}
