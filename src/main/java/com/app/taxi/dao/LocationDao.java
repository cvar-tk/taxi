package com.app.taxi.dao;

import org.json.JSONArray;

public interface LocationDao {

	JSONArray getNearestVehicles(Double latitude, Double longtitude,Integer type);

	JSONArray getNearestVehiclesDetails(Double latitude, Double longtitude, Integer type);

}
