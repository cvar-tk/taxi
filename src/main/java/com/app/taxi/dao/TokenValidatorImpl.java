package com.app.taxi.dao;

import org.hibernate.Query;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("tokenValidatorDao")
@Transactional
@Component
public class TokenValidatorImpl implements TokenValidatorDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Integer getUserIdByToken(String token) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT user_master.u_id FROM UserSession WHERE session_key =:token ORDER BY u_id DESC");

		query.setParameter("token", token);
		query.setMaxResults(1);
		
		Integer user_id = (Integer) query.uniqueResult();

		return user_id;
	}

}
