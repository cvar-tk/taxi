package com.app.taxi.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.FavPlace;
import com.app.taxi.model.UserMaster;


@Repository("FavPlaceDao")
@Transactional
@Component
public class FavPlaceDaoImpl implements FavPlaceDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addPlace(FavPlace favPlace, Integer user_id) {

		if (favPlace.getTitle().equals("Home")) {

			// home content

			Query query = sessionFactory.getCurrentSession()
					.createQuery("DELETE FROM FavPlace where user_id= :user_id AND title= :title");

			query.setParameter("user_id", user_id);

			query.setParameter("title", "Home");

			query.executeUpdate();

		}

		if (favPlace.getTitle().equals("WorkPlace")) {

			Query query = sessionFactory.getCurrentSession()
					.createQuery("DELETE FROM FavPlace where user_id= :user_id AND title= :title");

			query.setParameter("user_id", user_id);

			query.setParameter("title", "WorkPlace");

			query.executeUpdate();

		}

		System.out.println(user_id);
	

		favPlace.setUserMaster(sessionFactory.getCurrentSession().get(UserMaster.class, user_id));

		sessionFactory.getCurrentSession().save(favPlace);
	}

	@Override
	public JSONArray getFavListById(Integer user_id) {

		JSONArray jsonArray = new JSONArray();
		try {
			Query query = sessionFactory.getCurrentSession()
					.createQuery("FROM FavPlace WHERE user_id =:user_id AND fav_status =:status");

			query.setParameter("user_id", user_id);
			query.setParameter("status", 1);

			List<FavPlace> favplace = query.list();

			for (FavPlace obj : favplace) {

				JSONObject object = new JSONObject();

				object.put(SingleTon.ID, obj.getFav_id());
				object.put(SingleTon.ADDRESS, obj.getAddress());
				object.put(SingleTon.LATITIUDE, obj.getFav_lat());
				object.put(SingleTon.LONGTITUDE, obj.getFav_lang());
				object.put(SingleTon.PLACE_TYPE, obj.getTitle());

				jsonArray.put(object);

			}

			return jsonArray;

		} catch (JSONException e) {

			e.printStackTrace();

			return jsonArray;

		}

	}

	@Override
	public void deleteFavPlace(Integer user_id, Integer place_id) {

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Query query = session.createQuery("FROM FavPlace WHERE user_id =:user_id AND fav_id =:fav_id");

		query.setParameter("user_id", user_id);

		query.setParameter("fav_id", place_id);

		FavPlace favPlace = (FavPlace) query.uniqueResult();

		if (favPlace == null) {

			tx.rollback();

			session.clear();
			return;

		}

		favPlace.setFav_status(0);

		session.update(favPlace);

		tx.commit();

		session.close();

	}

}
