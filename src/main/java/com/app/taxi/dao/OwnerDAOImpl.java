package com.app.taxi.dao;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.app.taxi.model.SubscribedList;
import com.app.taxi.model.TripMaster;
import com.app.taxi.model.VehicleMaster;
import com.google.gson.GsonBuilder;

@Repository("OwnerDAO")
@Transactional
@Component
public class OwnerDAOImpl implements OwnerDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private GsonBuilder gsonBuilder;

	@Override
	public JSONObject getOwnerProfileDetails(Integer user_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT u_id,m_phone,u_name,u_pic,u_email,u_status,u_created_date FROM UserMaster WHERE u_id= :id and u_role.rm_id= :role");
		
		query.setParameter("id", user_id);
		query.setParameter("role", 2);
		
		List<Object[]> obj = query.list();
		
		for (Object[] objects : obj) {
			
			try {
				
				jsonObject.put("owner_id", objects[0]);
				jsonObject.put("owner_phone", objects[1]);
				jsonObject.put("owner_name", objects[2]);
				jsonObject.put("owner_pic", SingleTon.IMAGE_PATH+objects[3]);
				jsonObject.put("owner_email", objects[4]);
				jsonObject.put("status", objects[5]);
				jsonObject.put("created_date", objects[6]);
				
				
				Query driver_count = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(u_id) FROM UserMaster WHERE MappedWith= :mapped and u_role.rm_id= :role");

				driver_count.setParameter("mapped", objects[0]);
				driver_count.setParameter("role", 3);
				
				Long driver = (Long) driver_count.uniqueResult();
				
				jsonObject.put("driver_count", driver);
				
				
				Query vehicle_count = sessionFactory.getCurrentSession().createQuery("SELECT COUNT(vhm_id) FROM VehicleMaster WHERE vhm_owner.u_id= :id");
				
				vehicle_count.setParameter("id", objects[0]);
				
				Long vehicle = (Long) vehicle_count.uniqueResult();
				
				jsonObject.put("vehicle_count", vehicle);
				jsonObject.put(SingleTon.CONTENT_STATUS, true);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
				

			}catch (Exception e) {
				
			  e.printStackTrace();
			}
			
		
		}
		
		return jsonObject;
	}

	@Override
	public JSONObject getCarTripHistoryByDate(Integer user_id, Integer car_id, Date date1) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		
		VehicleMaster vehicleMaster = sessionFactory.getCurrentSession().get(VehicleMaster.class, car_id);
		
		if(vehicleMaster == null || !vehicleMaster.getVhm_owner().getU_id().equals(user_id)) {
			
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Car Not Mapped With this Owner...!");
			return jsonObject;
			
		}
		
	    Calendar calender = Calendar.getInstance(TimeZone.getTimeZone("IST"));
	    calender.setTime(date1);
	    
	    
	    Date fromDate = calender.getTime();
	    fromDate.setHours(00);
	    fromDate.setMinutes(00);
	    fromDate.setSeconds(00);
	    
	    Date toDate = calender.getTime();
	    toDate.setHours(23);
	    toDate.setMinutes(59);
	    toDate.setSeconds(58);
	    
	    System.err.println(toDate.getTime());		

	   
	  	

		Query query = sessionFactory.getCurrentSession().createQuery("SELECT tr_id,vehicle.vhm_id,driver.u_name,driver.u_id,starting_point,amount FROM TripMaster WHERE trip_status.ts_id= :status AND vehicle.vhm_id= :id AND start_time BETWEEN :from AND :to ORDER BY tr_id DESC");
	
		query.setParameter("from", new java.sql.Date(fromDate.getTime()));
		query.setParameter("to", new java.sql.Date(toDate.getTime()));
		query.setParameter("status", 2);
		query.setParameter("id", car_id);

		
		List<Object[]> object = query.list();
		
		JSONArray jsonArray = new JSONArray();
		
		for (Object[] objects : object) {
			
			JSONObject jsonObject2 = new JSONObject();
			
			jsonObject2.put("trip_id", objects[0]);
			jsonObject2.put("car_id", objects[1]);
			jsonObject2.put("driver_name", objects[2]);
			jsonObject2.put("driver_id", objects[3]);
			jsonObject2.put("starting_point", objects[4]);
			jsonObject2.put("amount", objects[5]);

			jsonArray.put(jsonObject2);
			
		}
		
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
		jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
		jsonObject.put("data", jsonArray);
		return jsonObject;
		
		}catch (Exception e) {

		e.printStackTrace();
		
		return SingleTon.APP_TECH_ERROR(null);
		}
		
	}

	@Override
	public JSONObject ViewTripHistoryByTripId(Integer trip_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		
		TripMaster tripMaster = sessionFactory.getCurrentSession().get(TripMaster.class, trip_id);
		
		if(tripMaster.getTrip_status().getTs_id() == 1) {
			
			return SingleTon.APP_TECH_ERROR(null);

			
		}else {
		
		jsonObject.put("trip_id", tripMaster.getTr_id());
		jsonObject.put("customer", tripMaster.getCustomer().getU_name());
		jsonObject.put("vehicle", tripMaster.getVehicle().getVhm_number());
		jsonObject.put("driver", tripMaster.getDriver().getU_name());
		jsonObject.put("bck_code", tripMaster.getBck_code());
		jsonObject.put("starting_point", tripMaster.getStarting_point());
		jsonObject.put("ending_point", tripMaster.getEnding_point());
		jsonObject.put("tax_percent", tripMaster.getTax_percent());
		jsonObject.put("start_lat", tripMaster.getStart_lat());
		jsonObject.put("start_lang", tripMaster.getStart_lang());
		jsonObject.put("total_km", tripMaster.getTotal_km());
		jsonObject.put("end_lat", tripMaster.getEnd_lat());
		jsonObject.put("end_lang", tripMaster.getEnd_lang());
		jsonObject.put("start_time", tripMaster.getStart_time());
		jsonObject.put("end_time", tripMaster.getEnd_time());
		jsonObject.put("amount", tripMaster.getAmount());
		jsonObject.put("est_amount", tripMaster.getEst_amount());
		jsonObject.put("base_fair", tripMaster.getBase_fair());
		jsonObject.put("toll", tripMaster.getToll());
		jsonObject.put("per_min_price", tripMaster.getPer_min_price());
		jsonObject.put("per_km_price", tripMaster.getPer_km_price());
		jsonObject.put("base_km", tripMaster.getBase_km());
		jsonObject.put("payment_method", tripMaster.getPayment_method());
		jsonObject.put("total_min", tripMaster.getTotal_min());
		
		
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
		jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		
		return jsonObject;
		

		}
		
		
		}catch (Exception e) {
			
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null);
			
		}
		
	}
	
	@Override
	public JSONObject AdminViewTripHistoryByTripId(Integer trip_id) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		
		TripMaster tripMaster = sessionFactory.getCurrentSession().get(TripMaster.class, trip_id);
		
		
		
		jsonObject.put("trip_id", tripMaster.getTr_id());
		jsonObject.put("customer", tripMaster.getCustomer().getU_name());
		jsonObject.put("customer_mobile", tripMaster.getCustomer().getM_phone());
		jsonObject.put("vehicle", tripMaster.getVehicle().getVhm_number());
		jsonObject.put("vehicle_type", tripMaster.getVehicle().getV_type().getVt_text());

		jsonObject.put("driver", tripMaster.getDriver().getU_name());
		jsonObject.put("driver_mobile", tripMaster.getDriver().getM_phone());

		jsonObject.put("bck_code", tripMaster.getBck_code());
		jsonObject.put("starting_point", tripMaster.getStarting_point());
		jsonObject.put("ending_point", tripMaster.getEnding_point());
		jsonObject.put("tax_percent", tripMaster.getTax_percent());
		jsonObject.put("start_lat", tripMaster.getStart_lat());
		jsonObject.put("start_lang", tripMaster.getStart_lang());
		jsonObject.put("total_km", tripMaster.getTotal_km());
		jsonObject.put("end_lat", tripMaster.getEnd_lat());
		jsonObject.put("end_lang", tripMaster.getEnd_lang());
		jsonObject.put("start_time", tripMaster.getStart_time());
		jsonObject.put("end_time", tripMaster.getEnd_time());
		jsonObject.put("amount", tripMaster.getAmount());
		jsonObject.put("est_amount", tripMaster.getEst_amount());
		jsonObject.put("base_fair", tripMaster.getBase_fair());
		jsonObject.put("toll", tripMaster.getToll());
		jsonObject.put("per_min_price", tripMaster.getPer_min_price());
		jsonObject.put("per_km_price", tripMaster.getPer_km_price());
		jsonObject.put("base_km", tripMaster.getBase_km());
		jsonObject.put("payment_method", tripMaster.getPayment_method());
		jsonObject.put("total_min", tripMaster.getTotal_min());
		jsonObject.put("cgst", tripMaster.getCgst());
		jsonObject.put("sgst", tripMaster.getSgst());
		jsonObject.put("trip_status_id", tripMaster.getTrip_status().getTs_id());
		jsonObject.put("trip_status", tripMaster.getTrip_status().getTs_title());

		
		jsonObject.put("bitmap_image", tripMaster.getBitmap_image());
		
		System.out.println("uiyurywuerywue"+tripMaster.getBitmap_image());
		
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return jsonObject;
		

		
	}

	@Override
	public JSONObject getCarSubscriberPlanDetails(Integer user_id, Integer car_id) {
		
        JSONObject jsonObject = new JSONObject();
		
		try {
		
		  Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			
			Query subcribe_query = sessionFactory.getCurrentSession().createQuery("FROM SubscribedList WHERE sl_vehicle_master.vhm_id= :v_id and sl_type.pt_id= :type ORDER BY sl_id DESC");
			
			subcribe_query.setParameter("v_id", car_id);
			subcribe_query.setParameter("type", 1);

			subcribe_query.setMaxResults(1);
			
			SubscribedList subscribedList1 = (SubscribedList) subcribe_query.uniqueResult();
			
			if(subscribedList1 == null || subscribedList1.getTo_date().before(calendar1.getTime())) {
				
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your plan Expired ..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject;
				
			}
			
			jsonObject.put("subscribe_id", subscribedList1.getSl_id());
			jsonObject.put("duration", subscribedList1.getSl_duration());
			jsonObject.put("price", subscribedList1.getSl_price());
			jsonObject.put("type_id", subscribedList1.getSl_type().getPt_id());
			jsonObject.put("type_name", subscribedList1.getSl_type().getPt_title());
			jsonObject.put("from_date", subscribedList1.getFrom_date());
			jsonObject.put("to_date", subscribedList1.getTo_date());
			jsonObject.put("auto_renewal", subscribedList1.getSl_auto());
			

			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your plan Details ..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject;
			
			
		
		}catch (Exception e) {
			
			
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null);
			
		}
	}

	@Override
	public JSONObject updateCarSubscriberAutoRenewal(Integer user_id, Integer car_id, Integer plan_id,
			Integer auto_renewal) {
		
		JSONObject jsonObject = new JSONObject();
		
		try {
		
	    Session session = sessionFactory.openSession();
	    
	    Transaction tx = session.beginTransaction();
			
	
		
		Query query = session.createQuery("FROM SubscribedList WHERE sl_vehicle_master.vhm_id= :v_id AND sl_type.pt_id= :plan_id ORDER BY sl_id DESC");
		
		query.setParameter("v_id", car_id);
		query.setParameter("plan_id", 1);
		query.setMaxResults(1);
		
		SubscribedList subscribedList1 = (SubscribedList) query.uniqueResult();
		
	
		subscribedList1.setSl_auto(auto_renewal);
		
		
		session.update(subscribedList1);
		tx.commit();
		session.close();
		
		jsonObject.put(SingleTon.CONTENT_STATUS, true);
		jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success ..!");

		jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
		return jsonObject;

		
		}catch (Exception e) {
			
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null);
		}
	}

	@Override
	public void UpdateAutorenewalByMIN() {
		
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		
		Date date = calendar.getTime();
		
		System.err.println(new java.sql.Date(date.getTime()));
		
		Query query = sessionFactory.getCurrentSession().createQuery("SELECT sl_id FROM SubscribedList WHERE to_date <= :date and sl_type.pt_id= :pid and sl_auto= :auto");
		
		query.setParameter("date", new java.sql.Date(date.getTime()));
		query.setParameter("pid", 1);
		query.setParameter("auto", 1);
		
		ArrayList<Integer> integer = new ArrayList<>();
		
		integer.addAll(query.list());
	
		System.err.println(integer);
		
		for (Integer integer2 : integer) {
			
			Session session = sessionFactory.openSession();
			
			Transaction tx = session.beginTransaction();
			
			System.err.println(integer2);
			
			SubscribedList subscribedList  = session.get(SubscribedList.class, integer2);
			
			System.err.println(subscribedList.getTo_date());
			
			Calendar calendar1 = Calendar.getInstance(TimeZone.getTimeZone("IST"));

			
			subscribedList.setFrom_date(calendar1.getTime());

			calendar1.add(Calendar.DATE, subscribedList.getSl_duration());
			
			System.err.println(calendar1.getTime());
			
			subscribedList.setTo_date(calendar1.getTime());
			
			session.update(subscribedList);
			
			tx.commit();
			session.close();
		
		}
	
	}

	@Override
	public JSONArray getOwnerList(Integer page, Integer limit) {
		JSONArray jsonArray = new JSONArray();

		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT u_id,u_name,m_phone,u_email,u_created_date FROM UserMaster WHERE u_role.rm_id= :role order by u_id DESC");

		query.setMaxResults(limit);
		query.setFirstResult(calculateOffset(page, limit));

		query.setParameter("role", 2);
		//query.setParameter("status2", 2);

		List<Object[]> obj = query.list();

		if (obj.isEmpty()) {

			return null;

		}

		try {
			for (Object[] objects : obj) {

				JSONObject jsonObject = new JSONObject();

				jsonObject.put("owner_id", objects[0]);
				jsonObject.put("owner_name", objects[1]);
				jsonObject.put("owner_mobile", objects[2]);
				jsonObject.put("owner_email", objects[3]);
				jsonObject.put("created_date", objects[4]);

				jsonArray.put(jsonObject);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jsonArray;
	}

	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}


	@Override
	public Long getTotalCountOfOwners() {
		
		Query query = sessionFactory.getCurrentSession().createQuery(
				"SELECT COUNT(u_id) FROM UserMaster WHERE u_role.rm_id= :role order by u_id DESC");

		query.setParameter("role", 2);
		
		Long count = (Long) query.uniqueResult();
		
		return count;
		
	}
	
	
}
