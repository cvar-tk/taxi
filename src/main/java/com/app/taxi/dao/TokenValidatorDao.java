package com.app.taxi.dao;



public interface TokenValidatorDao {
	
	public Integer getUserIdByToken(String token);
	
}
