package com.app.taxi.dao;

public interface CommunicationAssetsDao {

	void updateFCMToken(String fcm_token, Integer id);

}
