package com.app.taxi.dao;

import java.util.Calendar;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.model.UserLocation;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleLocation;
import com.app.taxi.model.VehicleMaster;

@Repository("UserAndVehicleLocationDao")
@Transactional
@Component
public class UserAndVehicleLocationDaoImpl implements UserAndVehicleLocationDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void updateUserLocation(Integer user_id, Double latitude, Double longtitude) {

		try {

			Query query = sessionFactory.getCurrentSession()
					.createQuery("SELECT ul_id FROM UserLocation where user.u_id =:user_id");

			query.setParameter("user_id", user_id);

			Integer location_id = (Integer) query.uniqueResult();

			if (location_id == null) {

				UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, user_id);

				if (userMaster == null) {
					return;
				}

				UserLocation userLocation = new UserLocation();

				userLocation.setUl_lat(latitude);
				userLocation.setUl_lang(longtitude);
				userLocation.setUser(userMaster);
				userLocation.setLast_update(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
				sessionFactory.getCurrentSession().saveOrUpdate(userLocation);

				return;

			}

			Session session = sessionFactory.openSession();

			Transaction tx = session.beginTransaction();

			UserLocation userLocation = session.get(UserLocation.class, location_id);

			userLocation.setLast_update(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

			userLocation.setUl_lat(latitude);

			userLocation.setUl_lang(longtitude);

			session.update(userLocation);

			tx.commit();

			session.close();

		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	@Override
	public void updateVehicleLocation(Integer user_id, Double latitude, Double longtitude) {

		Query query = sessionFactory.getCurrentSession()
				.createQuery("SELECT vhm_id from VehicleMaster WHERE vhm_picked_by =:user_id");
		query.setParameter("user_id", user_id);
		Integer vehicle_id = (Integer) query.setMaxResults(1).uniqueResult();
		
         System.out.println(vehicle_id+"---------------------------");
         
		if (vehicle_id == null) {

			return;
		}

		Query vehicle_query = sessionFactory.getCurrentSession()
				.createQuery("SELECT vl_id FROM VehicleLocation where vl_vid.vhm_id =:vehicle_id");

		vehicle_query.setParameter("vehicle_id", vehicle_id);

		
		if (vehicle_query.setMaxResults(1).uniqueResult() == null) {
			System.out.println(vehicle_query.setMaxResults(1) +"---------------------------");

			VehicleLocation vehicleLocation = new VehicleLocation();

			vehicleLocation.setVl_lat(latitude);
			vehicleLocation.setVl_lang(longtitude);
			vehicleLocation.setVl_vid(sessionFactory.getCurrentSession().get(VehicleMaster.class, vehicle_id));
			vehicleLocation.setLast_update(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());

			sessionFactory.getCurrentSession().save(vehicleLocation);
			return;
		}

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		VehicleLocation vehicleLocation = session.get(VehicleLocation.class, (Integer) vehicle_query.setMaxResults(1).uniqueResult());

		vehicleLocation.setVl_lang(longtitude);
		vehicleLocation.setVl_lat(latitude);
		vehicleLocation.setLast_update(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
		session.update(vehicleLocation);
		tx.commit();

		session.close();
	}
}
