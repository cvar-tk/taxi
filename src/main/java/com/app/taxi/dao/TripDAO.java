package com.app.taxi.dao;

import org.json.JSONArray;
import org.json.JSONObject;

public interface TripDAO {

	JSONObject getTripControlDetails(Integer id);

	void AdminUpdateTripControlDetails(Double base_control, Integer status);

	JSONObject getUserTripDetailsByUserId(Integer user_id, Integer pageIndex);

	JSONArray getTripMasterList(Integer page, Integer limit);

	Long getTotalCountTripMaster();

	JSONObject getUserLiveTripDetailsByUserId(Integer user_id);


}
