package com.app.taxi.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.SingleTon;
import com.google.gson.GsonBuilder;

@Repository("LocationDao")
@Transactional
@Component
public class LocationDaoimpl implements LocationDao {

	@Autowired
	private GsonBuilder gsonBuilder;

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public JSONArray getNearestVehicles(Double latitude, Double longtitude,Integer type) {

		JSONArray array = new JSONArray();

		///////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////
		///////// The distance here is in miles .not in KM/////////
		///////////////////////////////////////////////////////////
		
		
		String qry = "SELECT  vlst.vl_lat, vlst.vl_lang,vmt.v_type,vmt.vhm_id, SQRT( POW(69.1 * (vl_lat -"+ latitude+"), 2) + POW(69.1 * ("+longtitude+"- vl_lang) * COS(vl_lat / 57.3), 2)) AS distance FROM vehicle_location vlst INNER JOIN vehicle_master vmt on vlst.vl_vid = vmt.vhm_id WHERE vmt.v_type ="+type+" and vhm_status= 3 HAVING distance < 0.621371 ORDER BY distance ";

		SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(qry);
		
		
		try {

			array = new JSONArray(gsonBuilder.create().toJson(sqlQuery.list()));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return array;
	}

	@Override
	public JSONArray getNearestVehiclesDetails(Double latitude, Double longtitude,Integer type) {

		JSONArray array = new JSONArray();

		///////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////
		///////// The distance here is in miles .not in KM/////////
		///////////////////////////////////////////////////////////
		
		
		String qry = "SELECT vmt.vhm_id,vmt.vhm_picked_by, SQRT( POW(69.1 * (vl_lat -"+ latitude+"), 2) + POW(69.1 * ("+longtitude+"- vl_lang) * COS(vl_lat / 57.3), 2)) AS distance,vmt.base_fair,base_km,per_km_price,per_min_price FROM vehicle_location vlst INNER JOIN vehicle_master vmt on vlst.vl_vid = vmt.vhm_id WHERE vmt.v_type ="+type+" HAVING distance < 0.621 ORDER BY distance,vlst.vl_id DESC";

		SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(qry);
		
		List<Object[]> obj = sqlQuery.list();
		
		for (Object[] objects : obj) {
			
			try {
				
				JSONObject jsonObject = new JSONObject();
				
				jsonObject.put("car_id", objects[0]);
				jsonObject.put("distance", objects[2]);
				jsonObject.put("base_fair", objects[3]);
				jsonObject.put("base_km", objects[4]);
				jsonObject.put("per_km_price", objects[5]);
				jsonObject.put("per_min_price", objects[6]);

				Query query = sessionFactory.getCurrentSession().createQuery("SELECT u_name,u_pic FROM UserMaster WHERE u_id= :id");
				
				query.setParameter("id", (Integer)objects[1]);
				
				List<Object[]> obj1  = query.list();
				
				for (Object[] objects1 : obj1) {
					
					try {
						jsonObject.put("driver_name", objects1[0]);
						jsonObject.put("driver_pic", SingleTon.IMAGE_PATH+objects1[1]);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//jsonObject.put("driver", vehicleService.getVehicleDetailsByPickedBy((Integer)objects[1]));
				
				array.put(jsonObject);
				
			}catch (Exception e) {
				
				e.printStackTrace();
			}
			
			
		}
		
		return array;
	}
	
}
