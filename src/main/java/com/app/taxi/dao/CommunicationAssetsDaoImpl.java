package com.app.taxi.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.taxi.model.UserMaster;

@Repository("CommunicationAssetsDao")
@Transactional
@Component
public class CommunicationAssetsDaoImpl implements CommunicationAssetsDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void updateFCMToken(String fcm_token, Integer id) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		UserMaster userMaster = session.get(UserMaster.class, id);
		userMaster.setFcm_token(fcm_token);
		session.saveOrUpdate(userMaster);
		tx.commit();
		session.close();

	}

}
