package com.app.taxi.dao;

import java.math.BigInteger;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;


public interface DriverDao {

	
	JSONObject addnewDriverForApproval(UserMaster userMaster,WorkersCredential workersCredential, Integer owner_id);

	JSONObject getDriverProfileDetails(Integer user_id);

	JSONArray getNewDrivers(Integer page, Integer limit);

	Long getTotalCountNewDrivers();

	JSONObject getDriverDetailsByDriverId(Integer driver_id);

	void updateDriverDetails(Integer driver_id, String driver_name, Date expiry, Date dob1, String license_no);

	void updatedriverStatus(String reason, String comments, Integer driver_id, Integer status);

	JSONArray getApprovedDriverList(Integer page, Integer limit);

	Long getTotalCountApprovedDrivers();

	JSONObject getDriverDetailsByLicenseNo(String license_number);

	JSONArray filterByDriverDate(Date fromdate, Date todate, Integer pageIndex);


	Long getTotalCountApprovedDriverDate(Date fromdate, Date todate, Integer pageIndex);

	JSONObject ChangeDriverStatusToOnline(Integer user_id);

	JSONObject ChangeDriverStatusToOffOnline(Integer user_id);

	JSONObject selectCarByDriver(Integer user_id, Integer vehicle_id);

	Boolean checkDrvierAlreadyPickedAnyVehicle(Integer driver_id);

	JSONObject getDriverCurrentStatusById(Integer user_id);

	JSONObject getDriverONOFFStatusById(Integer user_id);

	void updateExpiryDateByAdmin(Integer driver_id, Date expiry);

	void ChangeDriverStatusToDisable(Integer check_status, Integer driver_id);

	JSONObject getUserLocationRequest(Integer user_id);

	JSONObject changeDriverSelfDisableStatus(Integer user_id);

	JSONObject updateBaseKmDetails(Integer user_id, Double base_km, Double base_km_price, Double per_min_price,
			Double per_km_price);

	
	JSONObject startTrip(JSONObject request,Integer user_id);

	JSONObject EndTrip(JSONObject request, Integer user_id);

	JSONObject deselectCarByDriverId(Integer user_id, Integer vehicle_id);

	JSONObject addExcistingDriverToOwner(BigInteger mobile, Integer user_id);

	JSONObject getDriverTripHistory(Integer user_id);

	JSONObject checkCurrentPasswordByID(Integer user_id, String password);

	JSONObject getOndemandPlanDetailaById(Integer user_id);

	JSONObject getManyTripLocationByLatitude(Integer user_id, Double latitude, Double longtitude);

	void adminTerminateTripByTripId(Integer trip_id, Integer userid);
}
