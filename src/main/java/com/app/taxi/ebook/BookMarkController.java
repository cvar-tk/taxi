package com.app.taxi.ebook;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.service.UserService;

@Controller
public class BookMarkController {

	@Autowired
	private UserService userService;

	@CrossOrigin
	@ResponseBody
	@PostMapping(value = { "add_bookmark.html" })
	private String POST_LOGIN_PAG(@RequestBody String request) {
		try {
			JSONObject jsonObject = new JSONObject(request);
			JSONObject jsonObject1 = new JSONObject();

			userService.addBookmark(jsonObject.getString("book_id"), jsonObject.getInt("page_id"),
					jsonObject.getString("svg"));

			jsonObject1.put("status", true);
			jsonObject1.put("data", userService.getBookMarksbyBookId(jsonObject.getString("book_id")));
			return jsonObject1.toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("status", false);
				jsonObject.put("error", e.getStackTrace());
				return jsonObject.toString();

			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		return null;

	}

	@CrossOrigin
	@ResponseBody
	@GetMapping(value = { "get_bookmarks.html" })
	private String GET_LOGIN_PAGE(@RequestParam(value = "book_id", required = true) String book_id) {

		try {
			JSONObject jsonObject1 = new JSONObject();
			jsonObject1.put("status", true);
			jsonObject1.put("data", userService.getBookMarksbyBookId(book_id));
			return jsonObject1.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
