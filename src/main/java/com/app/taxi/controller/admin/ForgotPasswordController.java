package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;

@Controller
public class ForgotPasswordController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_REQUEST_CHANAGE_PASSWORD })
	private String GENERATE_FORGOT_PASSWORD(HttpSession httpSession, String message,
			@RequestParam(value = "email_id", required = true) String email_id) {
		
		

		
		
		return userService.requestChangePassword(email_id).toString();

	}

}
