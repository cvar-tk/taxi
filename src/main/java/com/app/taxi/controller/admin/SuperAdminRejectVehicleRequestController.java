package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.VehicleService;

@Controller
public class SuperAdminRejectVehicleRequestController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private SuperAdminNewRequestController superAdminRejectVehicleRequestController;

	@GetMapping(value = { SingleTon.URL_VM_REJECT_REQUEST })
	public ModelAndView GET_VIEW_REQUEST(HttpSession httpSession, String message1, Model model,
			@RequestParam(value = "reason", required = true) String reason,
			@RequestParam(value = "comments", required = false) String comments,
			@RequestParam(value = "vehicle_id", required = true) Integer vehicle_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		if (vehicleService.getVehicleRequestById(vehicle_id) == null) {

			return superAdminRejectVehicleRequestController.GET_NEW_REQUEST(httpSession,"", model,
					"Something went wrong.Kindly try again.!");
		}

		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);

			if (vehicleService.getVehicleRequestById(vehicle_id) == null) {

				return superAdminRejectVehicleRequestController.GET_NEW_REQUEST(httpSession,"", model,
						"Not a valid request.kidnly try again..!");
			}	

		// Delete the account
		vehicleService.upDateVehicleStatusById(vehicle_id, reason, comments, 0);

		return superAdminRejectVehicleRequestController.GET_NEW_REQUEST(httpSession,"", model,
				"Rejected information sent to the user..!");
	}

}
