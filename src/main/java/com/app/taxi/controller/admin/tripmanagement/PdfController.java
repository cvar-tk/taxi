package com.app.taxi.controller.admin.tripmanagement;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TripService;

@Controller
public class PdfController {
	
	@Autowired
	private OwnerService ownerService;

	@GetMapping(value = { SingleTon.URL_ADMIN_INVOICE_PDF })
	public String INVOICE_PDF(HttpSession httpSession, Model model, String message,
			@RequestParam( value = "trip_id" , required = true) Integer trip_id) throws IOException {

		
		JSONObject jsonObject = ownerService.AdminViewTripHistoryByTripId(trip_id);

		String rootPath = System.getProperty("catalina.home");
		
		PDDocument document = new PDDocument();

		PDPage blankPage = new PDPage(PDRectangle.A4);

		document.addPage(blankPage);
		
		PDPageContentStream contents = new PDPageContentStream(document, blankPage);
		
		contents.beginText();
		contents.setFont(PDType1Font.TIMES_BOLD, 10);
		contents.newLineAtOffset(20, 760);
		String Heading = "Greetings from RoopayShoppe.com Web Services.";
		contents.showText(Heading);
		contents.endText();
		
		
		
		try {
			

			String	byteee = jsonObject.getString("bitmap_image");
			
			System.err.println(byteee);
		
			byte[] decodedString = Base64.getMimeDecoder().decode(new String(byteee).getBytes("UTF-8"));
			
			
					    ByteArrayInputStream bais = new ByteArrayInputStream(decodedString);
					    BufferedImage bim = ImageIO.read(bais);
					    PDImageXObject pdImage = LosslessFactory.createFromImage(document, bim);
			
			
			 
				//PDImageXObject pdImage = PDImageXObject.createFromByteArray(document, decodedString, "BMP");
				
				contents.drawImage(pdImage, 20, 780 ,  150, 50);
			
		} catch (JSONException e1) {
			
			e1.printStackTrace();
			

			
		}
				
		
			
			
			
		
		
		contents.close();
		try {

			document.save("C:/Documents/pdf/sample.pdf");

		} catch (IOException e) {

			e.printStackTrace();
		}

		System.out.println("PDF created");

		
			document.close();


		return null;
	}
	
}
