package com.app.taxi.controller.admin.plan;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.PlanService;

@Controller
public class OnDemandPlanManager {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private PlanService planService; 

	@GetMapping(value = { SingleTon.URL_PM_ON_DEMAND_PLANS })
	public ModelAndView GET_LOGIN_PAGE(@RequestParam(value = "message", required = false) String token,
			HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		model.addAttribute(SingleTon.CONTENT_LIST,planService.getAllDemandPlans());

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_ON_DEMAND_PLANS);
	}

}
