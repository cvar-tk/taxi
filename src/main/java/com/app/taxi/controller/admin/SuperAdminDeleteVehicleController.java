package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.VehicleService;

@Controller
public class SuperAdminDeleteVehicleController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private VehicleService vehicleService;

	@PostMapping(value = { SingleTon.URL_VM_DELETE_VEHICLE })
	private ModelAndView DELETE_VECHICLE_DETAILS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "reason", required = true) String reason,
			@RequestParam(value = "comments", required = false) String comments,
			@RequestParam(value = "vehicle_id", required = true) Integer vehicle_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		
		
		//delete account
		vehicleService.upDateVehicleStatusById(vehicle_id,reason,comments, 0);

		
		return new ModelAndView("redirect:" +SingleTon.URL_VM_APPROVED_VEHICLES +"?message="+"Vehicle Details deleted");

	}

}
