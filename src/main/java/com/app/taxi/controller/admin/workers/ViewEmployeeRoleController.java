package com.app.taxi.controller.admin.workers;

import java.math.BigInteger;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.UserService;
import com.google.gson.GsonBuilder;

@Controller
public class ViewEmployeeRoleController {

	
	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private UserService userService;
	
	@Autowired
	private GsonBuilder gsonBuilder;

	@GetMapping(value = { SingleTon.URL_ADMIN_EDIT_MULTIROLE_USER_LIST })
	private ModelAndView GET_ROLES(HttpSession httpSession, String message, Model model,String message1,
			@RequestParam( value = "u_id" , required = true) Integer id) {
		
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);

		
		model.addAttribute("view_list" ,userService.getMultiRoleDetailsById(id));
		
		model.addAttribute(SingleTon.MULTIROLE, gsonBuilder.create().toJson(userService.getMultiRoleMaster()));


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_VIEW_ROLES);

	}
	
	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_MULTIROLE_USER_LIST })
	private ModelAndView UPDATE_ROLE(HttpSession httpSession, String message, Model model,String message1,
			@RequestParam( value = "u_id" , required = true) Integer u_id,
			@RequestParam( value = "name" , required = true) String name,
			@RequestParam( value = "mobile" , required = true) BigInteger mobile,
			@RequestParam( value = "security_pin" , required = true) Integer security_pin,
			@RequestParam( value = "role" , required = true) Integer role) {
				
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
	
		
		
		   userService.updateRoleUserMasterDetailsById(u_id,name,mobile,security_pin,role);
		
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_EDIT_MULTIROLE_USER_LIST+"?u_id="+u_id+"&message="+"Updated Successfully");
	
	}
	
	
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_DEACTIVE_MULTIROLE_USER_LIST })
	private ModelAndView DEACTIVE_role(HttpSession httpSession, String message, Model model,String message1,
			@RequestParam( value = "u_id" , required = true) Integer u_id) {
				
	
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		   userService.DeactiveRoleUserMasterDetailsById(u_id,5);
		
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_EDIT_MULTIROLE_USER_LIST+"?u_id="+u_id+"&message1="+"Account Blocked Successfully");
	
	}
}
