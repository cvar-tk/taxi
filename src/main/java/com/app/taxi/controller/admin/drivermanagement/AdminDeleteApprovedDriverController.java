package com.app.taxi.controller.admin.drivermanagement;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.controller.admin.SuperAdminNewRequestController;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.VehicleService;

@Controller
public class AdminDeleteApprovedDriverController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private DriverService driverService;

	@Autowired
	private NewDriverRequestController newDriverRequestController;

	@GetMapping(value = { SingleTon.URL_ADMIN_DELETE_DRIVER })
	public ModelAndView GET_VIEW_REQUEST(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "reason", required = true) String reason,
			@RequestParam(value = "comments", required = false) String comments,
			@RequestParam(value = "driver_id", required = true) Integer driver_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		// Delete the account
		driverService.updatedriverStatus(reason, comments, driver_id, 0);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST + "?message="
				+ "Driver Account deleted successfully");
	}


}
