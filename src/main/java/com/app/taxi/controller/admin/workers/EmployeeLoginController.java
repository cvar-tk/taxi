package com.app.taxi.controller.admin.workers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.PushNotificationService;
import com.app.taxi.service.UserService;

@Controller
public class EmployeeLoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	

	@GetMapping(value = { SingleTon.URL_EMP_LOGIN })
	public ModelAndView GET_LOGIN_PAGE(HttpSession httpSession, String message, Model model) {

		
		
		if(httpSession != null) {
			
			httpSession.invalidate();
			
		}

	
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);

		return new ModelAndView(SingleTon.TEMPLATE_EMP_LOGIN);
	}

	@PostMapping(value = { SingleTon.URL_EMP_LOGIN })
	private ModelAndView POST_LOGIN_PAGE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "emailid", required = true) String emailid,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "pin", required = true) Integer pin) {

		

		
		return null;
	}
}
