package com.app.taxi.controller.admin.tripcontrol;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.TripService;

@Controller
public class TripControlController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private TripService tripService;

	@GetMapping(value = { SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT })
	private ModelAndView TRIP_CONTROL_MANAGE(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		model.addAttribute("trip" , tripService.getTripControlDetails(1));


		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_TRIP_CONTROL);
		
	}
	
}
