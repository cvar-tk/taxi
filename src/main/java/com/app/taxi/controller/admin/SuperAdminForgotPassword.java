package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;

@Controller
public class SuperAdminForgotPassword {

	@GetMapping(value = { SingleTon.URL_FORGOT_PASSWORD })
	public ModelAndView GET_FORGOT_PASSWORD(HttpSession httpSession, String message, Model model) {
		
		
		

		return new ModelAndView(SingleTon.TEMPLATE_FORGOT_PASSWARD_INFO);
	}

}
