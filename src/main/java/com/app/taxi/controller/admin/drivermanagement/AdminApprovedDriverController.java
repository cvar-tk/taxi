package com.app.taxi.controller.admin.drivermanagement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.DriverService;

@Controller
public class AdminApprovedDriverController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private DriverService driverService;

	@GetMapping(value = { SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST })
	public ModelAndView GET_CUSTOMER_LIST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_APPROVED_DRIVER_REQUEST);

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST_VIEW })
	private ModelAndView GET_DRIVER_VIEW(HttpSession httpSession, String message, Model model,String message1,
			@RequestParam(value = "driver_id", required = true) Integer driver_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE, message1);

		System.out.println(driver_id);

		JSONObject jsonObject = driverService.getDriverDetailsByDriverId(driver_id);

		model.addAttribute("data", jsonObject);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_APPROVED_DRIVER_REQUEST_VIEW);

	}

	@PostMapping(value = { SingleTon.URL_ADMIN_APPROVE_LICENSE_NO })
	private ModelAndView FIND_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "license_number", required = true) String license_number) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject obj = driverService.getDriverDetailsByLicenseNo(license_number);

		if (obj.length() == 0) {

			model.addAttribute(SingleTon.LICENSE_DETAILS, "false");

		} else {
			model.addAttribute(SingleTon.LICENSE_DETAILS, driverService.getDriverDetailsByLicenseNo(license_number));

		}
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_FIND_APLICENSENO);

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_APPROVE_LICENSE_NO })
	private ModelAndView FIND_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST);

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_APPROVE_DRIVER_FILTER })
	private ModelAndView FILTER_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST);

	}

	@PostMapping(value = { SingleTon.URL_ADMIN_APPROVE_DRIVER_FILTER })
	private ModelAndView POST_FILTER_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "pageIndex", required = true) Integer pageIndex,
			@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute("fromdate", from);
		model.addAttribute("todate", to);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_FILTER_APPROVED_DRIVER);

	}

	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_EXPIRY_DATE })
	private ModelAndView UPDATE_EXPIRY_DATE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "license_expiry", required = true) String license_expiry,
			@RequestParam(value = "driver_id", required = true) Integer driver_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		
		
		 SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
			
			Date expiry = null;
			try {
				expiry = formatter2.parse(license_expiry);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		driverService.updateExpiryDateByAdmin(driver_id,expiry);
		
		
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST_VIEW+"?driver_id="+driver_id+"&message="+"Expiry Date Updated Successfully");
		
		
	}

}
