package com.app.taxi.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TripService;
import com.app.taxi.service.UserService;
import com.app.taxi.service.VehicleService;

@Controller
public class ApiRequestManager {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private VehicleService vehicleService;
	
	@Autowired
	private DriverService driverService; 
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TripService tripService;
	
	@Autowired
	private OwnerService ownerService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_NEW_VEHICLE_REQUEST_LIST })
	private String GET_VEHICLE_REQUEST_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("vehicles", vehicleService.getNewApprovalVehicles(page, limit));
			jsonObject.put("totalCount", vehicleService.getTotalCountNewApprovalVehicles());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_APPROVED_VEHICLE_LIST })
	private String GET_APPROVED_VEHICLE_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("vehicles", vehicleService.getApprovedVehicles(page, limit));
			jsonObject.put("totalCount", vehicleService.getTotalCountApprovedVehicles());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_GET_MODELS_BY_MAKE_ID })
	private String GET_MODEL_BY_MAKE_ID(@RequestParam(value = "make_id", required = true) Integer make_id,
			HttpSession httpSession) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		return vehicleService.getVehicleModelsByMakeID(make_id).toString();
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_NEW_DRIVER_LIST})
	private String GET_DRIVER_REQUEST_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("drivers", driverService.getNewDrivers(page, limit));
			jsonObject.put("totalCount", driverService.getTotalCountNewDrivers());
			
			

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_APPROVED_VEHICLE_FILTER_LIST })
	private String FILTER_APPROVED_VEHICLE_LIST(@RequestParam(value = "pageIndex", required = true) Integer pageIndex,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit,
			@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

       SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
		
		Date fromdate = null;
		try {
			fromdate = formatter1.parse(from);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
       SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
		
		Date todate = null;
		try {
			todate = formatter2.parse(to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("vehicles", vehicleService.filterByVehicleDate(fromdate , todate , pageIndex));
			jsonObject.put("totalCount", vehicleService.getFilterTotalCountApprovedVehicles(fromdate , todate , pageIndex));

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_APPROVED_DRIVER_LIST})
	private String GET_APPROVED_DRIVER_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("drivers", driverService.getApprovedDriverList(page, limit));
			jsonObject.put("totalCount", driverService.getTotalCountApprovedDrivers());
			
			System.err.println("asjdhj"+driverService.getApprovedDriverList(page, limit));

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_APPROVED_DRIVER_FILTER_LIST })
	private String FILTER_APPROVED_DRIVER_LIST(@RequestParam(value = "pageIndex", required = true) Integer pageIndex,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit,
			@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

       SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
		
		Date fromdate = null;
		try {
			fromdate = formatter1.parse(from);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
       SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
		
		Date todate = null;
		try {
			todate = formatter2.parse(to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("drivers", driverService.filterByDriverDate(fromdate , todate , pageIndex));
			jsonObject.put("totalCount", driverService.getTotalCountApprovedDriverDate(fromdate , todate , pageIndex));

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_CUSTOMER_LIST})
	private String GET_CUSTOMERS_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("users", userService.getCustomerDetailsList(page, limit));
			jsonObject.put("totalCount", userService.getTotalCustomerCount());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_MULTIROLE_USER_LIST})
	private String GET_MULTIROLE_USERS(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

	/*	/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
*/
		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("roleusers", userService.getMultiRoleUserList(page, limit));
			jsonObject.put("totalCount", userService.getTotalMultiRoleUserCount());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
	
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_TRIP_MASTER_LIST })
	private String GET_TRIP_MASTER_HISTORY_LIST(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("trips", tripService.getTripMasterList(page, limit));
			jsonObject.put("totalCount", tripService.getTotalCountTripMaster());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_ADMIN_API_OWNER_MANAGEMENT_LIST })
	private String GET_OWNER_DETAILS(@RequestParam(value = "pageIndex", required = true) Integer page,
			HttpSession httpSession, @RequestParam(value = "pageSize", required = true) Integer limit) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			return "null";
		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject jsonObject = new JSONObject();

		try {

			jsonObject.put("owners", ownerService.getOwnerList(page, limit));
			jsonObject.put("totalCount", ownerService.getTotalCountOfOwners());

		} catch (JSONException e) {

			e.printStackTrace();
		}

		return jsonObject.toString();
	}
}