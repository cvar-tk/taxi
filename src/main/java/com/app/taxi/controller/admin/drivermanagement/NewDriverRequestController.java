package com.app.taxi.controller.admin.drivermanagement;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.DriverService;

@Controller
public class NewDriverRequestController {


	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private DriverService driverService;

	@GetMapping(value = { SingleTon.URL_ADMIN_NEW_DRIVER_REQUEST })
	public ModelAndView GET_CUSTOMER_LIST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_NEW_DRIVER_REQUEST);

	}
	
	@GetMapping(value = { SingleTon.URL_ADMIN_NEW_DRIVER_REQUEST_VIEW })
	private ModelAndView GET_DRIVER_VIEW(HttpSession httpSession, String message, Model model,
			@RequestParam(value="driver_id" , required = true) Integer driver_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		
		System.out.println(driver_id);
		
		JSONObject jsonObject = driverService.getDriverDetailsByDriverId(driver_id);
		
		model.addAttribute("data" , jsonObject);
		
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_NEW_DRIVER_REQUEST_VIEW);

	}
}
