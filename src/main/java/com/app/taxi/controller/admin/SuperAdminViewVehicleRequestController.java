package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.VehicleService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class SuperAdminViewVehicleRequestController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private VehicleService vehicleService; 
	
	@Autowired
	private GsonBuilder gsonBuilder;
	
	@GetMapping(value = { SingleTon.URL_VM_VIEW_VEHICLE_REQUEST})
	public ModelAndView GET_VIEW_REQUEST(HttpSession httpSession, String message, Model model,@RequestParam(value = "car_id", required = true) Integer car_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);

		model.addAttribute("vehicle_type" , gsonBuilder.create().toJson(vehicleService.getVehicleType()));
		model.addAttribute("vehicle_colour" , gsonBuilder.create().toJson(vehicleService.getVehicleColour()));

		model.addAttribute("data",vehicleService.getVehicleRequestById(car_id));
		model.addAttribute("vehicle_brands",vehicleService.getAllVehicleBrands().toString());
		
		
	return new ModelAndView(SingleTon.TEMPLATE_ADMIN_VIEW_REQUEST);
	}	
}
