package com.app.taxi.controller.admin.tripmanagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.DriverService;

@Controller
public class TripManagementController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private DriverService driverService;

	@GetMapping(value = { SingleTon.URL_ADMIN_TRIP_MASTER_LIST })
	public ModelAndView GET_TRIPMASTER_LIST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_TRIP_LIST);

	}
	
	
}
