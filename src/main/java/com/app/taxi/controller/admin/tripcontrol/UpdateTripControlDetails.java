package com.app.taxi.controller.admin.tripcontrol;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.TripService;

@Controller
public class UpdateTripControlDetails {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private TripService tripService;

	// update base control
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_BASE_CONTROL })
	private ModelAndView UPDATE_BASE_CONTROL(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "base_control", required = true) Double base_control) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////


		tripService.AdminUpdateTripControlDetails(base_control, 1);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"Base Control Updated successfully");

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_BASE_CONTROL })
	private ModelAndView UPDATE_BASE_CONTROL(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////


		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

	}
	
	
	// UPDATE BASE KM
	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_BASE_KM })
	private ModelAndView UPDATE_BASE_km(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "base_km", required = true) Double base_km) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		tripService.AdminUpdateTripControlDetails(base_km, 2);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"Base KM Updated successfully");

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_BASE_KM })
	private ModelAndView REDIRECT_UPDATE_BASE_km(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////


		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

	}
	
	
	//update per min price 
	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_PER_MIN_PRICE })
	private ModelAndView UPDATE_MIN_PRICE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "min_price", required = true) Double
			min_price) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		tripService.AdminUpdateTripControlDetails(min_price, 3);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"Per Min price Updated successfully");

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_PER_MIN_PRICE })
	private ModelAndView REDIRECT_UPDATE_MIN_PRICE(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////


		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

	}
	
	// update gst
	

	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_GST })
	private ModelAndView UPDATE_GST(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "gst", required = true) Double gst) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		tripService.AdminUpdateTripControlDetails(gst, 4);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"GST Updated successfully");

	}

	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_GST })
	private ModelAndView REDIRECT_UPDATE_GST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////


		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

	}
	
	
	
	// update cgst
	

		@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_CGST })
		private ModelAndView UPDATE_CGST(HttpSession httpSession, String message, Model model,
				@RequestParam(value = "cgst", required = true) Double cgst) {

			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

				return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			tripService.AdminUpdateTripControlDetails(cgst, 5);

			return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"CGST Updated successfully");

		}

		@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_CGST })
		private ModelAndView REDIRECT_UPDATE_CGST(HttpSession httpSession, String message, Model model) {

			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

				return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////


			return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

		}

	
	
		// update sgst
		

			@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_SGST })
			private ModelAndView UPDATE_SGST(HttpSession httpSession, String message, Model model,
					@RequestParam(value = "sgst", required = true) Double sgst) {

				/////////////////////////////////////////////////
				///////////// ADMIN SESSION VALIDATION//////////
				////////////////////////////////////////////////
				if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
						|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

					model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

					return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

				}
				/////////////////////////////////////////////////
				/////////////////////////////////////////////////
				/////////////////////////////////////////////////

				tripService.AdminUpdateTripControlDetails(sgst, 6);

				return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT+"?message="+"SGST Updated successfully");

			}

			@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_SGST })
			private ModelAndView REDIRECT_UPDATE_SGST(HttpSession httpSession, String message, Model model) {

				/////////////////////////////////////////////////
				///////////// ADMIN SESSION VALIDATION//////////
				////////////////////////////////////////////////
				if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
						|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

					model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

					return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

				}
				/////////////////////////////////////////////////
				/////////////////////////////////////////////////
				/////////////////////////////////////////////////


				return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_TRIP_CONTROL_MANAGEMENT);

			}

	
	
	
	
	

}
