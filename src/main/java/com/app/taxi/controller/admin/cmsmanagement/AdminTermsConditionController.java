package com.app.taxi.controller.admin.cmsmanagement;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.CMSService;

@Controller
public class AdminTermsConditionController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private CMSService cmsService;

//user
	@GetMapping(value = { SingleTon.URL_ADMIN_GET_TERMS_CONDITION })
	public ModelAndView GET_CUSTOMER_LIST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		String n = cmsService.getTermsAndCondition(4);
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("privacy", n);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.addAttribute("user",jsonObject);
	
	String driver = cmsService.getTermsAndCondition(3);
		
		JSONObject jsonObject1 = new JSONObject();
		try {
			jsonObject1.put("driver_terms",driver);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.addAttribute("driver",jsonObject1);
		
        String owner = cmsService.getTermsAndCondition(2);
		
		JSONObject jsonObject2 = new JSONObject();
		try {
			jsonObject2.put("owner_terms",owner);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("owner",jsonObject2);


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_TERMS_CONDITION);

	}

	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_USER_TERMS_CONDITION })
	public ModelAndView Update_user_privacy_policy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "user_terms", required = true) String terms) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
	
		
		cmsService.updateTermsAndConditionByAdmin(role,terms);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION+"?message="+"User Terms and Condition Updated successfully");

	}
	
	//driver
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_USER_TERMS_CONDITION })
	public ModelAndView Update_user_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION);
		
	}
	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_DRIVER_TERMS_CONDITION })
	public ModelAndView Update_Driver_privacy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "driver_terms", required = true) String terms) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		
		cmsService.updateTermsAndConditionByAdmin(role,terms);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION+"?message="+"Driver Terms and Condition Updated successfully");

	}
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_DRIVER_TERMS_CONDITION })
	public ModelAndView Update_driver_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION);
		
	}
	
	//owner
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_OWNER_TERMS_CONDITION })
	public ModelAndView Update_owner_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION);
		
	}

	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_OWNER_TERMS_CONDITION })
	public ModelAndView Update_Driver_privacy_policy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "owner_terms", required = true) String terms) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		
		cmsService.updateTermsAndConditionByAdmin(role,terms);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_TERMS_CONDITION+"?message="+"Owner Terms And Condition Updated successfully");

	}
}
