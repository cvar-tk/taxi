package com.app.taxi.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.VehicleService;

@Controller
public class ApprovedVehicleController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private VehicleService vehicleService;

	@GetMapping(value = { SingleTon.URL_VM_APPROVED_VEHICLES })
	public ModelAndView GET_VIEW_REQUEST(HttpSession httpSession, String message, Model model , String message1) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_APPROVED_VEHICLE);
	}

	@PostMapping(value = { SingleTon.URL_VM_APPROVE_VEHICLE_NO })
	private ModelAndView FIND_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "vehicle_number", required = true) String vehicle_number) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		//////////////////////////////////////////////// 
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		JSONObject obj = vehicleService.getVechicleDetailsByVechicleNo(vehicle_number);

		if (obj.length() == 0) {

			model.addAttribute(SingleTon.VECHICLE_DETAILS, "false");

		} else {
			model.addAttribute(SingleTon.VECHICLE_DETAILS,
					vehicleService.getVechicleDetailsByVechicleNo(vehicle_number));

		}
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_FIND_APVECHICLENO);

	}

	@GetMapping(value = { SingleTon.URL_VM_APPROVE_VEHICLE_NO })
	private ModelAndView FIND_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		return new ModelAndView("redirect:" + SingleTon.URL_VM_APPROVED_VEHICLES);

	}

	@GetMapping(value = { SingleTon.URL_VM_APPROVE_VEHICLE_FILTER })
	private ModelAndView FILTER_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		return new ModelAndView("redirect:" + SingleTon.URL_VM_APPROVED_VEHICLES);

	}

	@PostMapping(value = { SingleTon.URL_VM_APPROVE_VEHICLE_FILTER })
	private ModelAndView POST_FILTER_VEHICLE_DETAILS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "pageIndex", required = true) Integer pageIndex,
			@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "to", required = true) String to) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute("fromdate", from);
		model.addAttribute("todate", to);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_FILTER_APVECHICLE);

	}

}
