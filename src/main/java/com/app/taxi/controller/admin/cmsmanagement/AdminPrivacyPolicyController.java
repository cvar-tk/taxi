package com.app.taxi.controller.admin.cmsmanagement;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.CMSService;

@Controller
public class AdminPrivacyPolicyController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private CMSService cmsService;


	@GetMapping(value = { SingleTon.URL_ADMIN_GET_PRIVACY_POLICY })
	public ModelAndView GET_CUSTOMER_LIST(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		String n = cmsService.getPrivacyPolicy(4);
		
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("privacy", n);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.addAttribute("user",jsonObject);
	
	String driver = cmsService.getPrivacyPolicy(3);
		
		JSONObject jsonObject1 = new JSONObject();
		try {
			jsonObject1.put("driver_privacy",driver);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.addAttribute("driver",jsonObject1);
		
        String owner = cmsService.getPrivacyPolicy(2);
		
		JSONObject jsonObject2 = new JSONObject();
		try {
			jsonObject2.put("owner_privacy",owner);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		model.addAttribute("owner",jsonObject2);


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_PRIVACY_POLICY);

	}

	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_USER_PRIVACY_POLICY })
	public ModelAndView Update_user_privacy_policy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "user_privacy", required = true) String user_privacy) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		System.out.println(user_privacy);
		
		cmsService.updatePrivacyPolicyByAdmin(role,user_privacy);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY+"?message="+"User Privacy Policy Updated successfully");

	}
	
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_USER_PRIVACY_POLICY })
	public ModelAndView Update_user_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY);
		
	}
	
	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_DRIVER_PRIVACY_POLICY })
	public ModelAndView Update_Driver_privacy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "driver_privacy", required = true) String driver_privacy) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		
		cmsService.updatePrivacyPolicyByAdmin(role,driver_privacy);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY+"?message="+"Driver Privacy Policy Updated successfully");

	}
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_DRIVER_PRIVACY_POLICY })
	public ModelAndView Update_driver_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY);
		
	}
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_UPDATE_OWNER_PRIVACY_POLICY })
	public ModelAndView Update_owner_privacy_policy(HttpSession httpSession, String message, Model model) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
	
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY);
		
	}

	@PostMapping(value = { SingleTon.URL_ADMIN_UPDATE_OWNER_PRIVACY_POLICY })
	public ModelAndView Update_Driver_privacy_policy(HttpSession httpSession, String message, Model model,
			@RequestParam( value= "role", required = true) Integer role,
			@RequestParam( value= "owner_privacy", required = true) String owner_privacy) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
		
		cmsService.updatePrivacyPolicyByAdmin(role,owner_privacy);
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_GET_PRIVACY_POLICY+"?message="+"Owner Privacy Policy Updated successfully");

	}
}
