package com.app.taxi.controller.admin.workers;

import java.math.BigInteger;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.model.MultiRoleMaster;
import com.app.taxi.model.RoleMaster;
import com.app.taxi.model.RoleUserMaster;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;
import com.app.taxi.service.UserService;
import com.google.gson.GsonBuilder;

@Controller
public class RoleManagementController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private UserService userService;
	
	@Autowired
	private GsonBuilder gsonBuilder;

	@GetMapping(value = { SingleTon.URL_ADMIN_ROLE_MANAGEMENT })
	private ModelAndView GET_ROLES(HttpSession httpSession, String message, Model model,String message1) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);

  
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_ROLES_MANAGEMENT);

	}

	
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_CREATE_NEW_ROLE })
	private ModelAndView CREATE_NEW_ROLE(HttpSession httpSession, String message, Model model,String message1) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);
		model.addAttribute(SingleTon.MULTIROLE,gsonBuilder.create().toJson(userService.getMultiRoleMaster()));

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CREATE_ROLE);

	}

	@PostMapping(value = { SingleTon.URL_ADMIN_CREATE_NEW_ROLE })
	private ModelAndView POST_CREATE_NEW_ROLE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "mobile", required = true) BigInteger mobile,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "security_pin", required = true) Integer security_pin,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "confirm_password", required = true) String confirm_password,
			@RequestParam(value = "role", required = true) Integer role) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		if (name.length() < 3 || name.length() > 25) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE, "Name should be 3 to 25 charactor..!");
			model.addAttribute(SingleTon.MULTIROLE,gsonBuilder.create().toJson(userService.getMultiRoleMaster()));

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CREATE_ROLE);

		}

		System.out.println(confirm_password);
		System.out.println(password);
		
		if (!password.equals(confirm_password)) {
			
			System.out.println(confirm_password);
			System.out.println(password);

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE, "Password Should Not Matched..!");
			model.addAttribute(SingleTon.MULTIROLE,gsonBuilder.create().toJson(userService.getMultiRoleMaster()));

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CREATE_ROLE);

		}

		if (userService.checkEmailAlreadyExcist(email) == false) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE, "Email Already Excist..!");
			model.addAttribute(SingleTon.MULTIROLE,gsonBuilder.create().toJson(userService.getMultiRoleMaster()));

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CREATE_ROLE);
		}

		RoleUserMaster userMaster = new RoleUserMaster();
		userMaster.setU_name(name);
		userMaster.setU_email(email);
		userMaster.setM_phone(mobile);
		userMaster.setU_security_pin(security_pin);
		userMaster.setPassword(password);
		userMaster.setU_status(1);

		MultiRoleMaster roleMaster = new MultiRoleMaster();
		roleMaster.setR_id(role);

		userMaster.setU_role(roleMaster);

		
		 userService.adminCreateNewRoleManagement(userMaster,(Integer)httpSession.getAttribute(SingleTon.USER_ID));
		 
		 
		 String contentText = "Account Created in ROOTU .Your Email is "+" "+email+" "+"and Login with this password "+"'"+password+"'"+"and Login Link :"+"'"+"http://localhost:8080/emp-login.html"+"'";

			SingleTon.sendMail(email,"Account Created in ROOTU", contentText);

			model.addAttribute(SingleTon.CONTENT_MESSAGE , "Created Successfully");
			model.addAttribute(SingleTon.MULTIROLE,gsonBuilder.create().toJson(userService.getMultiRoleMaster()));


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CREATE_ROLE);

	}

}
