package com.app.taxi.controller.admin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.model.VehicleColour;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleMasterAssets;
import com.app.taxi.model.VehicleModel;
import com.app.taxi.model.VehicleType;
import com.app.taxi.service.VehicleService;


@Controller
public class SuperAdminApproveVehicleController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private SuperAdminNewRequestController superAdminRejectVehicleRequestController;

	@GetMapping(value = { SingleTon.URL_VM_APPROVE_VEHICLE })
	private ModelAndView GET_DASHBOARD_PAGE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "vehicle_id", required = true) Integer vehicle_id,
			@RequestParam(value = "vehicle_number", required = true) String vehicle_number,
			@RequestParam(value = "reg_number", required = true) String reg_number,
			@RequestParam(value = "owner_name", required = true) String owner_name,
			@RequestParam(value = "model_id", required = true) Integer model_id,
			@RequestParam(value = "registration_date", required = true) String registration_date,
			@RequestParam(value = "insurance_exp_date", required = true) String insurance_exp_date,
			@RequestParam(value = "vehicle_type", required = true) Integer vehicle_type,
			@RequestParam(value = "vehicle_colour" , required = true) Integer vehicle_colour) {

			

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		

		if (vehicleService.getVehicleRequestById(vehicle_id) == null) {

			return superAdminRejectVehicleRequestController.GET_NEW_REQUEST(httpSession,"", model,
					"Something went wrong.Kindly try again.!");
		}

		VehicleMaster master = new VehicleMaster();
		master.setVhm_id(vehicle_id);
		master.setVhm_number(vehicle_number);
		master.setVhm_registration_number(reg_number);
		master.setVhm_name(owner_name);
		
		
		VehicleType vehicleType = new VehicleType();
		vehicleType.setVt_id(vehicle_type);
		master.setV_type(vehicleType);
		
		VehicleColour vehicleColour = new VehicleColour();
		vehicleColour.setC_id(vehicle_colour);
		master.setVhm_colour(vehicleColour);
		

		VehicleModel vehicleModel = new VehicleModel();
		vehicleModel.setVm_id(model_id);
		master.setVhm_model(vehicleModel);
		System.out.println(registration_date);
		System.out.println(insurance_exp_date);
		// master.setRegistration_date(registration_date);
		Date registration_date_op;
		Date insurance_exp_date_op;

		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		try {
			insurance_exp_date_op = format.parse(insurance_exp_date);
			registration_date_op = format.parse(registration_date);

		} catch (ParseException e) {

			e.printStackTrace();

			return null;
		}

		master.setRegistration_date(registration_date_op);
		VehicleMasterAssets vehicleMasterAssets = new VehicleMasterAssets();
		vehicleMasterAssets.setVma_insurance_exp_date(insurance_exp_date_op);
		master.setV_assets(vehicleMasterAssets);
		vehicleService.updateVehicleAfterApproval(master);

		return new ModelAndView("redirect:" + SingleTon.URL_VM_NEW_REQUEST + "?message=" + "Vehicle Details Approved");

	}


}
