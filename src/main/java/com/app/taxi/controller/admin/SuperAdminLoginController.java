package com.app.taxi.controller.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.PushNotificationService;
import com.app.taxi.service.UserService;

@Controller
public class SuperAdminLoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private PushNotificationService pushNotificationService;

	@GetMapping(value = { SingleTon.URL_ADMIN_LOGIN })
	public ModelAndView GET_LOGIN_PAGE(HttpSession httpSession, String message, Model model) {

		List<String> keys = new ArrayList<>();
		
		if(httpSession != null) {
			
			httpSession.invalidate();
			
		}
		
	//	keys.add("eHoiUdL8hpI:APA91bExtxfBjlFWmnv3vZnupPy3J5F_6n4dMVFedUF_bpq60mPFXuw483jtm0bCwrPzvz6z9jUBbWWbnfUpki-s9Ooc1dIVssyYiS5vl1HZDZqwhz7SyMtBVpfHE6ifcbFN1_9a4Yjns0vMY0LxQvVUAWhPoarrfg");
		//pushNotificationService.sendPushNotification(keys, "TESYT", "sdfgbkgdetngjrmtklhjerlkjhklertljktulk");
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);

		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_LOGIN);
	}

	@PostMapping(value = { SingleTon.URL_ADMIN_LOGIN })
	private ModelAndView POST_LOGIN_PAGE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "emailid", required = true) String emailid,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "pin", required = true) Integer pin) {

		model.addAttribute(SingleTon.CONTENT_MESSAGE, "Invalid Username oe Password..!");

		// Result of u_id, w_credential.wc_password,u_status
		Object[] user = userService.getAdminDetailsByCredential(emailid, password, pin);
		
		if (user == null) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Invalid Username or Password..!");

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_LOGIN);

		}

		if (!user[2].equals(1)) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Your Account Disabled..!");

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_LOGIN);

		}

		if (!bCryptPasswordEncoder.matches(password, user[1].toString())) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Invaid Credential ..!");

			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_LOGIN);

		}

		httpSession.setAttribute(SingleTon.SESSION_ROLE, 1);
		httpSession.setAttribute(SingleTon.SESSION_USER_ID, user[0]);

		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_DASHBOARD);
	}
	
	
	
	
	
	
	
	
	
}
