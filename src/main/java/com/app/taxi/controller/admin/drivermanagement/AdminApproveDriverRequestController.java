package com.app.taxi.controller.admin.drivermanagement;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.controller.admin.SuperAdminNewRequestController;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleMasterAssets;
import com.app.taxi.model.VehicleModel;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.VehicleService;

@Controller
public class AdminApproveDriverRequestController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;

	@Autowired
	private DriverService driverService;

	@Autowired
	private SuperAdminNewRequestController superAdminRejectVehicleRequestController;

	@GetMapping(value = { SingleTon.URL_ADMIN_APPROVE_DRIVER })
	private ModelAndView GET_DASHBOARD_PAGE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "driver_id", required = true) Integer driver_id,
			@RequestParam(value = "driver_name", required = true) String driver_name,
			@RequestParam(value = "dob", required = true) String dob,		
			@RequestParam(value = "license_no", required = true) String license_no,
			@RequestParam(value = "license_expiry", required = true) String license_expiry) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}

		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		if (driver_name.length() < 3 || driver_name.length() > 25) {
			
			
			return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_NEW_DRIVER_REQUEST_VIEW+"?driver_id="+driver_id+"&message="+"Name should be 3 to 25 charactor..!");
		}
		
        if ( license_no.length() > 50) {
			
			
			return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_NEW_DRIVER_REQUEST_VIEW+"?driver_id="+driver_id+"&message="+"License should be below 50 charactor..!");
		}
		
		 SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
			
			Date dob1 = null;
			try {
				dob1 = formatter1.parse(dob);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	
		 SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			
			Date expiry = null;
			try {
				expiry = formatter2.parse(license_expiry);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	
			
		
		driverService.updateDriverDetails(driver_id,driver_name,expiry , dob1 ,license_no );
		

		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_NEW_DRIVER_REQUEST+"?message="+"Driver details Approved");

	}
	
	
	@GetMapping(value = { SingleTon.URL_ADMIN_DISABLE_DRIVER })
	public ModelAndView DISABLE_DRIVER_STATUS(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "check_status", required = true) Integer check_status,
			@RequestParam(value = "driver_id", required = true) Integer driver_id) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		driverService.ChangeDriverStatusToDisable(check_status , driver_id);
		
		if(check_status == 2) {
			
		return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST_VIEW + "?driver_id=" + driver_id+"&message="+"Driver Account Enabled");
		
		}else {
			
			return new ModelAndView("redirect:" + SingleTon.URL_ADMIN_APPROVED_DRIVER_REQUEST_VIEW + "?driver_id=" + driver_id+"&message1="+"Driver Account Blocked");

			
		}
	}

	
	
	
}
