package com.app.taxi.controller.admin.plan;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.model.PlanMaster;
import com.app.taxi.model.PlanTypeMaster;
import com.app.taxi.service.PlanService;

@Controller
public class PlanAPIController {

	@Autowired
	private PlanService planService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_CREATE_DEMAND_PLAN })
	private String API_CREATE_POST(@RequestParam(value = "plan_title", required = true) String plan_title,
			@RequestParam(value = "plan_amount", required = true) Float plan_amount,
			@RequestParam(value = "plan_status", required = true) Integer plan_status,
			@RequestParam(value = "plan_validity", required = true) Integer plan_validity,
			@RequestParam(value = "plan_validity_title", required = true) String plan_validity_title,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			if (plan_title.length() < 2 || plan_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_validity_title.length() < 2 || plan_validity_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan validity title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_amount == null || plan_validity == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Amount or Validity should be an number..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			PlanMaster planMaster = new PlanMaster();
			planMaster.setPm_demandtime_duration(plan_validity);
			planMaster.setPm_price(plan_amount);
			planMaster.setStatus(plan_status);
			planMaster.setPm_title(plan_title);
			planMaster.setPm_comments(plan_validity_title);
			planMaster.setPt_type(new PlanTypeMaster());

			planService.addNewPlan(planMaster, 2);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan created successfully..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_CHANGE_DEMAND_PLAN_STATUS })
	private String CHANGE_DEMAND_PLAN_STATUS(@RequestParam(value = "id", required = true) Integer id,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			return planService.changePlanStatusById(id).toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}


	}
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_CHANGE_VEHICLE_PLAN_STATUS })
	private String CHANGE_VEHICLE_PLAN_STATUS(@RequestParam(value = "id", required = true) Integer id,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			return planService.changePlanStatusById(id).toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}


	}
	
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_CREATE_VEHICLE_PLAN })
	private String API_CREATE_VEHICLE_PLAN_POST(@RequestParam(value = "plan_title", required = true) String plan_title,
			@RequestParam(value = "plan_amount", required = true) Float plan_amount,
			@RequestParam(value = "plan_status", required = true) Integer plan_status,
			@RequestParam(value = "plan_validity", required = true) Integer plan_validity,
			@RequestParam(value = "plan_validity_title", required = true) String plan_validity_title,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			if (plan_title.length() < 2 || plan_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_validity_title.length() < 2 || plan_validity_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan validity title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_amount == null || plan_validity == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Amount or Validity should be an number..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			PlanMaster planMaster = new PlanMaster();
			planMaster.setPm_topup_duration(plan_validity);
			planMaster.setPm_price(plan_amount);
			planMaster.setStatus(plan_status);
			planMaster.setPm_title(plan_title);
			planMaster.setPm_comments(plan_validity_title);
			planMaster.setPt_type(new PlanTypeMaster());

			planService.addNewPlan(planMaster, 1);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan created successfully..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_API_EDIT_VEHICLE_PLANS })
	private String API_EDIT_PLANS(@RequestParam(value = "plan_id", required = true) Integer plan_id , HttpSession httpSession) {
		
		JSONObject jsonObject = new JSONObject();

		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			
		return planService.getPlanDetailsById(plan_id).toString();
		
		
		}catch (Exception e) {

		e.printStackTrace();
		
		return SingleTon.APP_TECH_ERROR(null).toString();
		
		}
		
	}
	
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_EDIT_VEHICLE_PLAN })
	private String API_EDIT_VEHICLE_PLAN_POST(@RequestParam(value = "plan_id", required = true) Integer plan_id,
			@RequestParam(value = "plan_title", required = true) String plan_title,
			@RequestParam(value = "plan_amount", required = true) Float plan_amount,
			@RequestParam(value = "plan_status", required = true) Integer plan_status,
			@RequestParam(value = "plan_validity", required = true) Integer plan_validity,
			@RequestParam(value = "plan_validity_title", required = true) String plan_validity_title,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		System.err.println("asgsdgdfgjfgjhchg");
		
		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			if (plan_title.length() < 2 || plan_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_validity_title.length() < 2 || plan_validity_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan validity title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_amount == null || plan_validity == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Amount or Validity should be an number..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}
			
		  

			PlanMaster planMaster = new PlanMaster();
			planMaster.setPm_topup_duration(plan_validity);
			planMaster.setPm_price(plan_amount);
			planMaster.setStatus(plan_status);
			planMaster.setPm_title(plan_title);
			planMaster.setPm_comments(plan_validity_title);
			

			planService.EditPlanDetailsById(planMaster,plan_id);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Updated successfully..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_PM_API_EDIT_ONDEMAND_PLAN })
	private String API_EDIT_ONDEMAND_PLAN_POST(@RequestParam(value = "plan_id", required = true) Integer plan_id,
			@RequestParam(value = "plan_title", required = true) String plan_title,
			@RequestParam(value = "plan_amount", required = true) Float plan_amount,
			@RequestParam(value = "plan_status", required = true) Integer plan_status,
			@RequestParam(value = "plan_validity", required = true) Integer plan_validity,
			@RequestParam(value = "plan_validity_title", required = true) String plan_validity_title,
			HttpSession httpSession) {

		JSONObject jsonObject = new JSONObject();

		System.err.println("asgsdgdfgjfgjhchg");
		
		try {
			/////////////////////////////////////////////////
			///////////// ADMIN SESSION VALIDATION//////////
			////////////////////////////////////////////////
			if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
					|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////
			/////////////////////////////////////////////////

			if (plan_title.length() < 2 || plan_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_validity_title.length() < 2 || plan_validity_title.length() > 25) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan validity title should be 2 to 25 charactors..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (plan_amount == null || plan_validity == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Amount or Validity should be an number..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}
			
		  

			PlanMaster planMaster = new PlanMaster();
			planMaster.setPm_demandtime_duration(plan_validity);
			planMaster.setPm_price(plan_amount);
			planMaster.setStatus(plan_status);
			planMaster.setPm_title(plan_title);
			planMaster.setPm_comments(plan_validity_title);
			

			planService.EditOnDemandPlanDetailsById(planMaster,plan_id);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Plan Updated successfully..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
