package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;

@Controller
public class SuperAdminAccountDetails {

	
	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private UserService userService;

	@GetMapping(value = { SingleTon.URL_ADMIN_CHANGE_PASSWORD })
	private ModelAndView CHANGE_PASSWORD(HttpSession httpSession, String message, Model model , String message1) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);
	}

	
	@PostMapping(value = { SingleTon.URL_ADMIN_CHANGE_PASSWORD })
	private ModelAndView CHANGE_NEW_PASSWORD(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "current_password", required = true) String current_password,
			@RequestParam(value = "new_password", required = true) String new_password,
			@RequestParam(value = "conform_password", required = true) String conform_password) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		if (current_password.length() < 6 || current_password.length() > 12) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Password should be 6-12 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);

		}
		
		
		if (new_password.length() < 6 || new_password.length() > 12) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Password should be 6-12 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);

		}
		
		if (conform_password.length() < 6 || conform_password.length() > 12) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Password should be 6-12 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);

		}
		
		
		if(!new_password.equals(conform_password)) {
			
	       model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Password Not Matched..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);
		}
		
		if(!userService.checkCurrentPasswordByID(Integer.parseInt(httpSession.getAttribute(SingleTon.SESSION_USER_ID).toString()) ,current_password)) {
			
			 model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Please Add Valid Password");
				
				return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);
			
		}
		
		
		userService.updateChangePassword(Integer.parseInt(httpSession.getAttribute(SingleTon.SESSION_USER_ID).toString()) , new_password);
		
		 model.addAttribute(SingleTon.CONTENT_MESSAGE , "Updated Successfully...!");


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PASSWORD);
	}

	
	@GetMapping(value = { SingleTon.URL_ADMIN_CHANGE_PIN })
	private ModelAndView CHANGE_PIN(HttpSession httpSession, String message, Model model , String message1) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);
		
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);
	}

	
	@PostMapping(value = { SingleTon.URL_ADMIN_CHANGE_PIN })
	private ModelAndView CHANGE_NEW_PIN(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "current_pin", required = true) Integer current_pin,
			@RequestParam(value = "new_pin", required = true) Integer new_pin,
			@RequestParam(value = "conform_pin", required = true) Integer conform_pin) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		System.err.println(String.valueOf(current_pin).length());
		
		if (String.valueOf(current_pin).length() != 4) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Pin should be 4 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);

		}
		
		
		if (String.valueOf(new_pin).length() != 4) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Pin should be 4 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);

		}
		
		if (String.valueOf(conform_pin).length() != 4) {

			model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Pin should be 4 charactor..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);

		}
		
		
		if(!new_pin.equals(conform_pin)) {
			
	       model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Pin Should Not Matched..!");
			
			return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);
		}
		
		
		if(!userService.checkCurrentPinByID(Integer.parseInt(httpSession.getAttribute(SingleTon.SESSION_USER_ID).toString()) ,current_pin)) {
			
			 model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , "Please Add Valid Pin");
				
				return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);
			
		}
		
		
		userService.UpdateChangePin(Integer.parseInt(httpSession.getAttribute(SingleTon.SESSION_USER_ID).toString()) , new_pin);
		
		 model.addAttribute(SingleTon.CONTENT_MESSAGE , "Updated Successfully...!");


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_CHANGE_PIN);
	}
	
	
}
