package com.app.taxi.controller.admin.ownermanagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;

@Controller
public class OwnerManageController {
	
	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	

	@GetMapping(value = { SingleTon.URL_ADMIN_OWNER_MANAGEMENT })
	public ModelAndView OWNER_MANAGEMENT(HttpSession httpSession, String message, Model model) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		model.addAttribute(SingleTon.CONTENT_MESSAGE, message);
		
	
	
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_OWNER_MANAGE);

	}
	
	
	
}
