package com.app.taxi.controller.admin;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;

@Controller
public class SuperAdminNewRequestController {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@GetMapping(value = { SingleTon.URL_VM_NEW_REQUEST })
	public ModelAndView GET_NEW_REQUEST(HttpSession httpSession, String message, Model model,String message1) {

		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);


		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_NEW_REQUEST);
	}

}
