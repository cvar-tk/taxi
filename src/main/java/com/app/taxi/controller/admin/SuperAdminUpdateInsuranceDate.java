package com.app.taxi.controller.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder.In;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.VehicleService;

@Controller
public class SuperAdminUpdateInsuranceDate {

	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private VehicleService vehicleService;

	@PostMapping(value = { SingleTon.URL_VM_UPDATE_INSURANCE_DATE })
	private ModelAndView GET_DASHBOARD_PAGE(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "insurance_exp_date", required = true) String insurance_exp_date,
			@RequestParam(value = "vehicle_id", required = true) Integer vehicle_id) {

		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		 SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");
			
			Date insurance_expiry = null;
			try {
				insurance_expiry = formatter2.parse(insurance_exp_date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
		vehicleService.updateVehicleInsuranceExpiryDate(insurance_expiry,vehicle_id);
		

		return new ModelAndView("redirect:"+SingleTon.URL_VM_VIEW_VEHICLE + "?car_id="+vehicle_id+"&message="+"Insurance Expiry Date Updated");
	}

}
