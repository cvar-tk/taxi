package com.app.taxi.controller.admin.tripmanagement;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.controller.admin.SuperAdminLoginController;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TripService;

@Controller
public class ViewTripManagementController {
	
	@Autowired
	private SuperAdminLoginController superAdminLoginController;
	
	@Autowired
	private OwnerService ownerService;
	
	@Autowired
	private DriverService driverService;

	@GetMapping(value = { SingleTon.URL_ADMIN_VIEW_TRIP_MASTER_LIST})
	public ModelAndView GET_VIEW_TRIP_REQUEST(HttpSession httpSession, String message, Model model,String message1,
			@RequestParam(value = "trip_id", required = true) Integer trip_id) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		
		model.addAttribute(SingleTon.CONTENT_MESSAGE , message);
		model.addAttribute(SingleTon.CONTENT_ERROR_MESSAGE , message1);

		
		model.addAttribute("view_trip", ownerService.AdminViewTripHistoryByTripId(trip_id));
		
	    
		return new ModelAndView(SingleTon.TEMPLATE_ADMIN_VIEW_TRIP_LIST);
	}
	

	@GetMapping(value = { SingleTon.URL_ADMIN_TERMINATE_TRIP})
	public ModelAndView ADMIN_TERMINATE_TRIP(HttpSession httpSession, String message, Model model,
			@RequestParam(value = "trip_id", required = true) Integer trip_id) {
		
		/////////////////////////////////////////////////
		///////////// ADMIN SESSION VALIDATION//////////
		////////////////////////////////////////////////
		if (httpSession.getAttribute(SingleTon.SESSION_ROLE) == null
				|| (!httpSession.getAttribute(SingleTon.SESSION_ROLE).equals(1))) {

			model.addAttribute(SingleTon.CONTENT_MESSAGE, "Session Expired.  Login again..!");

			return superAdminLoginController.GET_LOGIN_PAGE(httpSession, "", model);

		}
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////
		/////////////////////////////////////////////////

		driverService.adminTerminateTripByTripId(trip_id , Integer.parseInt(httpSession.getAttribute(SingleTon.SESSION_USER_ID).toString()));
	    
		return new ModelAndView("redirect:"+SingleTon.URL_ADMIN_VIEW_TRIP_MASTER_LIST+"?trip_id="+trip_id+"&message="+"Trip Terminated succesfully..!");
	}
	
}
