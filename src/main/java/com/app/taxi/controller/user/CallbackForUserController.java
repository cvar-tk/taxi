package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.CallbackService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class CallbackForUserController {
	
	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private CallbackService callbackService;
	
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_UPDATE_USERCALLBACK})
	private String USER_CALLBACK(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
		
			Integer vehicle_id = request_obj.getInt(SingleTon.VEHICLE_ID);
			
			String address =  request_obj.getString(SingleTon.FROM_ADDRESS);
			
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);
			
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);


			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			

			return callbackService.addCallbackByVehicleId(vehicle_id,user_id , address, latitude , longtitude).toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
	
	

}
