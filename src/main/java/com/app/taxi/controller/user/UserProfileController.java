package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class UserProfileController {

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private UserService userService; 
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_USER_PROFILE_DETAILS })
	private String POST_GET_OWNER_DETAILS(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		Integer user_id = tokenValidatorService.getUserIdByToken(token);

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return userService.getUserProfileDetails(user_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

	
}
