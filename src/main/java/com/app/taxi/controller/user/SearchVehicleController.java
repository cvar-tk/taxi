package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.LocationService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class SearchVehicleController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private LocationService locationService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_GET_NEAR_VEHICLE_lIST})
	private String POST_ADD_VEHICLE(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);
			Integer type = request_obj.getInt(SingleTon.VEHICLE_TYPE);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

		      jsonObject.put(SingleTon.CONTENT_LIST, locationService.getNearestVehicles(latitude,longtitude,type));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_GET_NEAR_VEHICLE_DEATILS})
	private String GET_NEAREST_VEHICLE_DETAILS(@RequestBody String request) {
		

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);
			Integer type = request_obj.getInt(SingleTon.VEHICLE_TYPE);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
			

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

		      jsonObject.put(SingleTon.CONTENT_LIST, locationService.getNearestVehiclesDetails(latitude,longtitude,type));

			return jsonObject.toString();

		
		
		}catch (Exception e) {

		e.printStackTrace();
		return SingleTon.APP_TECH_ERROR(null).toString();

		}
		
	
	}	
	
}
