package com.app.taxi.controller.user;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.taxi.SingleTon;
import com.app.taxi.model.Feedback;
import com.app.taxi.service.FeedBackService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class UserFeedbackController {
	
	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private FeedBackService feedBackService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_FEEDBACK_CONTROLLER })
	private String POST_ADD_FEEDBACK(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "subject", required = true) String subject,
			@RequestParam(value = "comments", required = true) String comments,
			@RequestParam(value = "file1" , required = false) MultipartFile file1,
			@RequestParam(value = "file2" , required = false) MultipartFile file2,
			@RequestParam(value = "file3" , required = false) MultipartFile file3,
			@RequestParam(value = "file4" , required = false) MultipartFile file4,
			@RequestParam(value = "file5" , required = false) MultipartFile file5) {
		
		Integer user_id = tokenValidatorService.getUserIdByToken(token);
		JSONObject jsonObject = new JSONObject();
		
		try {
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}
			
			
			if (file1.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(file1.getContentType().equalsIgnoreCase("image/png")
					|| file1.getContentType().equalsIgnoreCase("image/jpeg")
					|| file1.getContentType().equalsIgnoreCase("image/jpg")
					|| file1.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}

			if (file2.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(file2.getContentType().equalsIgnoreCase("image/png")
					|| file2.getContentType().equalsIgnoreCase("image/jpeg")
					|| file2.getContentType().equalsIgnoreCase("image/jpg")
					|| file2.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}

			if (file3.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(file3.getContentType().equalsIgnoreCase("image/png")
					|| file3.getContentType().equalsIgnoreCase("image/jpeg")
					|| file3.getContentType().equalsIgnoreCase("image/jpg")
					|| file3.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}

			if (file4.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(file4.getContentType().equalsIgnoreCase("image/png")
					|| file4.getContentType().equalsIgnoreCase("image/jpeg")
					|| file4.getContentType().equalsIgnoreCase("image/jpg")
					|| file4.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}
			
			if (file5.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(file5.getContentType().equalsIgnoreCase("image/png")
					|| file5.getContentType().equalsIgnoreCase("image/jpeg")
					|| file5.getContentType().equalsIgnoreCase("image/jpg")
					|| file5.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}

			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
			
			Feedback feedback = new Feedback();
			feedback.setComments(comments);
			feedback.setSubject(subject);
			feedback.setStatus(1);
			feedback.setFile1(SingleTon.getFilenamebyfile(file1));
			feedback.setFile2(SingleTon.getFilenamebyfile(file2));
			feedback.setFile3(SingleTon.getFilenamebyfile(file3));
			feedback.setFile4(SingleTon.getFilenamebyfile(file4));
			feedback.setFile5(SingleTon.getFilenamebyfile(file5));
			feedback.setCreated_date(calendar.getTime());
			
			feedBackService.AddUserFeedback(feedback , user_id);
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Feedback saved...!");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			
		    return jsonObject.toString();
			
		}catch (Exception e) {

		 e.printStackTrace();
		 
		 return SingleTon.APP_TECH_ERROR(null).toString();
		 
		}
		
		
	
}
	
	
}
	
	
