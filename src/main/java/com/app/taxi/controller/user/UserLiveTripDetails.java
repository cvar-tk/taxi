package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.TripService;
import com.app.taxi.service.VehicleService;

@Controller
public class UserLiveTripDetails {

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private TripService tripService; 
	

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_USER_TRIP_DETAILS})
	private String GET_USER_TRIP_HISTORY(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		try {
			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}
						
			return tripService.getUserLiveTripDetailsByUserId(user_id).toString();
			
	         
			
		}catch (Exception e) {

			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null).toString();
			
		}
		
		

			
	}
}
