package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class UpdateProfileDataController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_UPDATE_USERNAME_EMAIL })
	private String USER_UPDATE_USERNAME_AND_EMAIL(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			String name = request_obj.getString(SingleTon.USER_NAME);
			String email = request_obj.getString(SingleTon.EMAIL);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			userService.updateEmailAndUsername(user_id, name, email);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}
	
	
	
	
	
	
}
