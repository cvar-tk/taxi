package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.model.FavPlace;
import com.app.taxi.service.FavPlaceService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class PlaceManagerController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private FavPlaceService favPlaceService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_ADD_PLACE })
	private String POST_ADD_VEHICLE(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			String title = request_obj.getString(SingleTon.PLACE_TYPE);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);
			String address = request_obj.getString(SingleTon.ADDRESS);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			FavPlace favPlace = new FavPlace();

			favPlace.setFav_lat(latitude);
			favPlace.setFav_lang(longtitude);
			favPlace.setAddress(address);
			favPlace.setTitle(title);
			favPlace.setFav_status(1);
			favPlaceService.addPlace(favPlace, user_id);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			jsonObject.put(SingleTon.CONTENT_LIST, favPlaceService.getFavListById(user_id));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_GET_FAV_PLACE })
	private String GET_FAV__PLACE(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		try {

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			jsonObject.put(SingleTon.CONTENT_LIST, favPlaceService.getFavListById(user_id));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DELETE_PLACE })
	private String GET_DELETE__PLACE(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "place_id", required = true) Integer place_id) {

		JSONObject jsonObject = new JSONObject();

		try {

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			favPlaceService.deleteFavPlace(user_id, place_id);

			jsonObject.put(SingleTon.CONTENT_LIST, favPlaceService.getFavListById(user_id));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

}
