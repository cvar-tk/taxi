package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.VehicleService;

@Controller
public class VehicleOverViewController {

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private VehicleService vehicleService; 
	

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_GET_VEHICLE_DETAILS_BY_VEHICLE_ID})
	private String GET_VEHICLE_DETAILS_BY_VEHICLE_ID(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {
			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Double latitude = request_obj.getDouble(SingleTon.LATITIUDE);
			Double longtitude = request_obj.getDouble(SingleTon.LONGTITUDE);
			Integer vehicle_id = request_obj.getInt(SingleTon.VEHICLE_ID);
			
			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
				
			}
						
			return vehicleService.getVehicleDetailsByVehicleId(vehicle_id,latitude,longtitude).toString();
			
		}catch (Exception e) {

			e.printStackTrace();
			
			return SingleTon.APP_TECH_ERROR(null).toString();
			
		}
		
		

			
	}
}
