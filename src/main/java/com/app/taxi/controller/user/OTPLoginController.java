package com.app.taxi.controller.user;

import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.model.RoleMaster;
import com.app.taxi.model.UserMaster;
import com.app.taxi.service.UserService;

@Controller
public class OTPLoginController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_USER_OTP_LOGIN })
	private String APP_USER_LOGIN(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String phone = request_obj.getString(SingleTon.MOBILE);

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(phone);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject.toString();
			}

			if (!userService.checkAccountExistingByRole(obj_phone, 4)) {

				UserMaster master = new UserMaster();

				master.setM_phone(obj_phone);

				userService.AddNewUser(master);

				jsonObject.put(SingleTon.CONTENT_STATUS, true);

				jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

				return jsonObject.toString();

			}

			userService.sendOTPByID(phone, 4);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();

		}
	}
}
