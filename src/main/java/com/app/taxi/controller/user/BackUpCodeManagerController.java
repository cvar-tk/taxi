package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.BackUpCodeService;
import com.app.taxi.service.TokenValidatorService;
import com.google.gson.GsonBuilder;

@Controller
public class BackUpCodeManagerController {

	@Autowired
	private BackUpCodeService backUpCodeService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private GsonBuilder gsonBuilder;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_USER_GET_BACKUP_CODE })
	private String APP_GET_BACKUP_CODE(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		try {

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			if (backUpCodeService.getBackUpCode(user_id) == null) {

				backUpCodeService.getnerateBackUpCode(user_id);
			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			jsonObject.put("data",
					new JSONObject(gsonBuilder.create().toJson(backUpCodeService.getBackUpCode(user_id))));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_USER_GET_NEW_BACKUP_CODE })
	private String APP_GET_NEW_BACKUP_CODEs(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		try {

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			backUpCodeService.getnerateBackUpCode(user_id);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			jsonObject.put("data",
					new JSONObject(gsonBuilder.create().toJson(backUpCodeService.getBackUpCode(user_id))));

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}
}
