package com.app.taxi.controller.user;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class CheckForUserNameController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private UserService userService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_USER_CHECK_USER_NAME})
	private String POST_LIST_ASSOSIATED_DRIVER(@RequestParam(value = "token", required = true) String token) {

		try {
			JSONObject jsonObject = new JSONObject();

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			String name = userService.getUserNameById(user_id);
			if (name == null || name.equals("")) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

}
