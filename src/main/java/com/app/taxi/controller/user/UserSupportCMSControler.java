package com.app.taxi.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.CMSService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class UserSupportCMSControler {

	@Autowired
	private CMSService cmsService; 
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_USER_GET_PRIVACY_POLICY })
	private String USER_PRIVACY_POLICY() {
		
		return cmsService.getPrivacyPolicy(4);
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_USER_GET_TERMS_AND_CONDITION})
	private String USER_GET_TERMS_AND_CONDITION() {
		
		return cmsService.getTermsAndCondition(4);
	}

	
}
