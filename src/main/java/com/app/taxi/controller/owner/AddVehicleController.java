package com.app.taxi.controller.owner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.taxi.SingleTon;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.VehicleColour;
import com.app.taxi.model.VehicleMaster;
import com.app.taxi.model.VehicleMasterAssets;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;
import com.app.taxi.service.VehicleService;

@Controller
public class AddVehicleController {

	@Autowired
	private VehicleService vehicleService;

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_OWNER_ADD_VEHICLE })
	private String POST_ADD_VEHICLE(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "vehicle_number", required = true) String vechicle_number,
			@RequestParam(value = "registration_number", required = true) String registration_number,
			@RequestParam(value = "colour", required = true) String colour,
			@RequestParam(value = "make", required = true) String make,
			@RequestParam(value = "model", required = true) String model,
			@RequestParam(value = "registration_date", required = true) String registration_date,
			@RequestParam(value = "owner_name", required = true) String owner_name,
			@RequestParam(value = "insurance_exp", required = true) String insurance_exp,
			@RequestParam("rc_book_pdf") MultipartFile rc_book_pdf,
			@RequestParam("insurance") MultipartFile insurance,
			@RequestParam("car_fullview_image") MultipartFile car_fullview_image,
			@RequestParam("car_nameboard_image") MultipartFile car_nameboard_image) {
		try {
			System.out.println(rc_book_pdf.getContentType());
			System.out.println(car_fullview_image.getContentType());
			System.out.println(car_nameboard_image.getContentType());

			JSONObject jsonObject = new JSONObject();

			Integer  user_id =tokenValidatorService.getUserIdByToken(token);
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				return jsonObject.toString();
			}

			System.out.println(token);
			
			
			if(userService.checkOwnerVerifiedById(user_id) == true) {
				
				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Your Account Not Verified Yet..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}
			
			
			if (rc_book_pdf.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "RC Book Pdf is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (insurance.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Insurance copy file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			
			if (car_fullview_image.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Car fullview image is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();

			}

			if (!(car_nameboard_image.getContentType().equalsIgnoreCase("image/png")
					|| car_nameboard_image.getContentType().equalsIgnoreCase("image/jpeg")
					|| car_nameboard_image.getContentType().equalsIgnoreCase("image/jpg")
					|| car_nameboard_image.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "RC Book Nameboard is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}
			
			
			if (!(insurance.getContentType().equalsIgnoreCase("image/png")
					|| insurance.getContentType().equalsIgnoreCase("image/jpeg")
					|| insurance.getContentType().equalsIgnoreCase("image/jpg")
					|| insurance.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Insurance should be Image or Pdf..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(rc_book_pdf.getContentType().equalsIgnoreCase("image/png")
					|| rc_book_pdf.getContentType().equalsIgnoreCase("image/jpeg")
					|| rc_book_pdf.getContentType().equalsIgnoreCase("image/jpg")
					|| rc_book_pdf.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "RC Book should be PDF or Image file..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(car_fullview_image.getContentType().equalsIgnoreCase("image/png")
					|| car_fullview_image.getContentType().equalsIgnoreCase("image/jpeg")
					|| car_fullview_image.getContentType().equalsIgnoreCase("image/jpg")
					|| rc_book_pdf.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid car full image..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(car_nameboard_image.getContentType().equalsIgnoreCase("image/png")
					|| car_nameboard_image.getContentType().equalsIgnoreCase("image/jpeg")
					|| car_nameboard_image.getContentType().equalsIgnoreCase("image/jpg")
					|| rc_book_pdf.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid car full image..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}
			Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));

			VehicleMaster vehicleMaster = new VehicleMaster();
						
			vehicleMaster.setVhm_number(vechicle_number);

			vehicleMaster.setVhm_registration_number(registration_number);

			vehicleMaster.setVhm_make_hint(make);

			vehicleMaster.setVhm_model_hint(model);

			vehicleMaster.setVhm_colour_hint(colour);

			vehicleMaster.setVhm_name(owner_name);

			vehicleMaster.setVhm_status(1);
			
			vehicleMaster.setCreated_date(calendar.getTime());

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			Date obj_registration_date = null;

			String date = registration_date;

			try {

				obj_registration_date = formatter.parse(date);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid Registration Date..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			vehicleMaster.setRegistration_date(obj_registration_date);

			VehicleMasterAssets vehicleMasterAssets = new VehicleMasterAssets();

			Date obj_insurance_exp = null;

			try {

				obj_insurance_exp = formatter.parse(insurance_exp);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid Insurance Date..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			vehicleMasterAssets.setVma_insurance_exp_date(obj_insurance_exp);
			vehicleMasterAssets.setVma_full_view(SingleTon.getFilenamebyfile(car_fullview_image));
			vehicleMasterAssets.setVma_number_view(SingleTon.getFilenamebyfile(car_nameboard_image));
			vehicleMasterAssets.setVma_rc_book(SingleTon.getFilenamebyfile(rc_book_pdf));
			vehicleMasterAssets.setVma_insurance_img(SingleTon.getFilenamebyfile(insurance));
			vehicleMaster.setV_assets(vehicleMasterAssets);

			vehicleService.addNewVehicleforApproval(vehicleMaster,user_id);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Request submitted successfully.!");
			return jsonObject.toString();

		} catch (JSONException e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
}
