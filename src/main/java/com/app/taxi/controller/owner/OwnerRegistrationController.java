package com.app.taxi.controller.owner;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;
import com.app.taxi.service.UserService;

@Controller
public class OwnerRegistrationController {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_OWNER_ADD_NEW_OWNER})
	private String POST_ADD_VEHICLE(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String username = request_obj.getString(SingleTon.USER_NAME);
			String password = request_obj.getString(SingleTon.PASSWORD);
			String phone = request_obj.getString(SingleTon.MOBILE);
			String referal_code = request_obj.getString(SingleTon.REFERARL_CODE);

			if (username.length() < 3 || username.length() > 25) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Name should be 3 to 25 charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
		return jsonObject.toString();
			}

			if (password.length() < 6 || password.length() > 12) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password should be 6-12 charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	return jsonObject.toString();

			}
			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(phone);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
	          return jsonObject.toString();
			}

			if (userService.checkAccountExistingByRole(obj_phone, 2)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "This mobile number already registered.Kindly login.!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
                return jsonObject.toString();

			}

			UserMaster userMaster = new UserMaster();

			userMaster.setU_name(username);
			userMaster.setM_phone(obj_phone);
			userMaster.setU_used_ref_code(referal_code);

			WorkersCredential workersCredential = new WorkersCredential();
			workersCredential.setWc_created_on(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
			workersCredential.setWc_password(bCryptPasswordEncoder.encode(password));
			workersCredential.setWc_updated_on(Calendar.getInstance(TimeZone.getTimeZone("IST")).getTime());
			userMaster.setW_credential(workersCredential);
			userService.AddNewOwner(userMaster);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "User Registered..!");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
}
