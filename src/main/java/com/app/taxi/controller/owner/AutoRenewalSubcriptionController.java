package com.app.taxi.controller.owner;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class AutoRenewalSubcriptionController {

	@Autowired
	private OwnerService ownerService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	// every 1 min method calling
	@GetMapping(value = { SingleTon.URL_APP_AUTO_RENEWAL_METHOD})
	private ModelAndView AUTO_RENEWAL_METHOD(HttpSession httpSession, String message, Model model) {

	
		ownerService.UpdateAutorenewalByMIN();
		
       
		return null;
		
	}
		
}
