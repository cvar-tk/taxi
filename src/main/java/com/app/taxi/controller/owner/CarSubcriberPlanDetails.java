package com.app.taxi.controller.owner;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class CarSubcriberPlanDetails {

	@Autowired
	private OwnerService ownerService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_CAR_SUBCRIBE_PLAN_DETAILS})
	private String POST_DRIVER_ONLINE(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "car_id", required = true) Integer car_id) {

		JSONObject jsonObject = new JSONObject();

		Integer user_id = tokenValidatorService.getUserIdByToken(token);

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return ownerService.getCarSubscriberPlanDetails(user_id , car_id).toString();

			

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
}
