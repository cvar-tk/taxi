package com.app.taxi.controller.owner;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class CarTripHistoryController {
	

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private OwnerService ownerService;
	

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_CAR_TRIP_HISTORY})
	private String OWNER_CAR_TRIP_HISTORY(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "car_id", required = true) Integer car_id,
			@RequestParam(value = "date", required = true) String date) {

		try {
			
			JSONObject jsonObject = new JSONObject();

			Integer  user_id =tokenValidatorService.getUserIdByToken(token);
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				return jsonObject.toString();
			}
  
			 SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MM-yyyy");
				
				Date date1 = null;
				try {
					date1 = formatter1.parse(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		
			
			return ownerService.getCarTripHistoryByDate(user_id,car_id ,date1).toString();


		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}
		
	
	}
	
}
