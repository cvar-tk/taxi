package com.app.taxi.controller.owner;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.VehicleService;

@Controller
public class ViewCarController {

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private VehicleService vehicleService;
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_VIEW_CAR_DETAILS})
	private String VIEW_CAR_DETAILS(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "car_id", required = true) Integer car_id) {

		try {
		
			JSONObject jsonObject = new JSONObject();

			Integer  user_id =tokenValidatorService.getUserIdByToken(token);
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				return jsonObject.toString();
			}
  
			
			return vehicleService.getCarDetailsByCarId(car_id).toString();


		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}
		
		
	
	}
}
