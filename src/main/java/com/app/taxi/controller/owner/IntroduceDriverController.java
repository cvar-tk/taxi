package com.app.taxi.controller.owner;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.taxi.SingleTon;
import com.app.taxi.model.DriverMasterAssets;
import com.app.taxi.model.UserMaster;
import com.app.taxi.model.WorkersCredential;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;
import com.app.taxi.service.VehicleService;

@Controller
public class IntroduceDriverController {

	@Autowired
	private BCryptPasswordEncoder BCryptPasswordEncoder;

	@Autowired
	private UserService userService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private DriverService driverService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_INTRODUCE_DRIVER_APP })
	private String POST_ADD_VEHICLE(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "driver_name", required = true) String driver_name,
			@RequestParam(value = "licence_number", required = true) String licence_number,
			@RequestParam(value = "dob", required = true) String dob,
			@RequestParam(value = "phone", required = true) String phone,
			@RequestParam(value = "gov_proof_number", required = true) String gov_proof_number,
			@RequestParam(value = "licence_exp_date", required = true) String licence_exp_date,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam("driver_photo") MultipartFile driver_photo, 
			@RequestParam("licence") MultipartFile licence,
			@RequestParam("gov_proof") MultipartFile gov_proof) {

		Integer user_id = tokenValidatorService.getUserIdByToken(token);
		JSONObject jsonObject = new JSONObject();

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			if (gov_proof.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "proof document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(gov_proof.getContentType().equalsIgnoreCase("image/png")
					|| gov_proof.getContentType().equalsIgnoreCase("image/jpeg")
					|| gov_proof.getContentType().equalsIgnoreCase("image/jpg")
					|| gov_proof.getContentType().equalsIgnoreCase("application/pdf"))) {
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid proof document file..!");
				return jsonObject.toString();
			}

			if (driver_photo.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Driver photo is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			if (!(driver_photo.getContentType().equalsIgnoreCase("image/png")
					|| driver_photo.getContentType().equalsIgnoreCase("image/jpeg")
					|| driver_photo.getContentType().equalsIgnoreCase("image/jpg"))) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid Driver file format..!");
				return jsonObject.toString();
			}

			if (licence.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Licence document file is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();

			}

			if (!(licence.getContentType().equalsIgnoreCase("image/png")
					|| licence.getContentType().equalsIgnoreCase("image/jpeg")
					|| licence.getContentType().equalsIgnoreCase("image/jpg")
					|| licence.getContentType().equalsIgnoreCase("application/pdf"))) {

				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "invalid licence document file..!");
				return jsonObject.toString();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

			Date obj_licence_exp_date = null;
			Date obj_driver_dob_date = null;

			try {

				obj_licence_exp_date = formatter.parse(licence_exp_date);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid insurance Expiry Date..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			try {

				obj_driver_dob_date = formatter.parse(dob);
				
				System.out.println(obj_driver_dob_date);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid driver birthday..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			if (password.length() < 6 || password.length() > 12) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password should be 6-12 charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();

			}

			if (driver_name.length() < 3 || driver_name.length() > 25) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Name should be 3 to 25 charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			if (gov_proof_number.length() < 2) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Proof number charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}
			if (licence_number.length() < 2) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Licence Number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(phone);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();
			}

			if (userService.checkAccountExistingByRole(obj_phone, 3)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "This mobile number already registered.Kindly login.!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				return jsonObject.toString();

			}

			DriverMasterAssets driverMasterAssets = new DriverMasterAssets();
			driverMasterAssets.setGov_proof_number(gov_proof_number);
			driverMasterAssets.setDma_licence_number(licence_number);
			driverMasterAssets.setDma_dob(obj_driver_dob_date);
			driverMasterAssets.setLicence_exp(obj_licence_exp_date);
			driverMasterAssets.setDma_address_proof(SingleTon.getFilenamebyfile(gov_proof));
			driverMasterAssets.setDma_licence(SingleTon.getFilenamebyfile(licence));

			UserMaster userMaster = new UserMaster();
			userMaster.setM_phone(obj_phone);
			userMaster.setU_name(driver_name);
			userMaster.setU_pic(SingleTon.getFilenamebyfile(driver_photo));
			WorkersCredential workersCredential = new WorkersCredential();
			workersCredential.setWc_password(BCryptPasswordEncoder.encode(password));
			userMaster.setW_credential(workersCredential);
			userMaster.setDriver_assets(driverMasterAssets);
			
			return driverService.addnewDriverForApproval(userMaster, workersCredential, user_id).toString();

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

}
