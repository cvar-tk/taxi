package com.app.taxi.controller.owner;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.PlanService;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class PlanListController {

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private PlanService planService; 

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_RUNNING_PLAN_LIST})
	private String POST_LIST_RUNNING_PLAN_LIST(@RequestParam(value = "token", required = true) String token) {

		try {
			JSONObject jsonObject = new JSONObject();

			Integer owner_id = tokenValidatorService.getUserIdByToken(token);

			if (owner_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			
			
			jsonObject.put(SingleTon.CONTENT_LIST, planService.getActiveRunningPlanList());
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			return jsonObject.toString();

		} catch (Exception e) {
			
			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}	
	
	
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DEMAND_PLAN_LIST})
	private String POST_LIST_DEMAND_PLAN_LIST(@RequestParam(value = "token", required = true) String token) {

		try {
			JSONObject jsonObject = new JSONObject();

			Integer owner_id = tokenValidatorService.getUserIdByToken(token);

			if (owner_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			
			
			jsonObject.put(SingleTon.CONTENT_LIST, planService.getActiveDemandPlanList());
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success..!");
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			return jsonObject.toString();

		} catch (Exception e) {
			
			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

}
