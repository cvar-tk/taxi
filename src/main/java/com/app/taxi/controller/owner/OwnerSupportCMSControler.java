package com.app.taxi.controller.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.CMSService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class OwnerSupportCMSControler {

	@Autowired
	private CMSService cmsService; 
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_OWNER_GET_PRIVACY_POLICY })
	private String OWNER_PRIVACY_POLICY() {
		
		return cmsService.getPrivacyPolicy(2);
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_OWNER_GET_TERMS_AND_CONDITION})
	private String  OWNER_GET_TERMS_AND_CONDITION() {
		
		return cmsService.getTermsAndCondition(2);
	}

	
}
