package com.app.taxi.controller.owner;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.PlanService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class SubscribleRunningPlan {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private PlanService planService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_SUBSCRIBE_RUNNING_PLAN })
	private String API_CREATE_POST(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "plan_id", required = true) Integer plan_id,
			@RequestParam(value = "vehicle_id", required = false) Integer vehicle_id) {

		JSONObject jsonObject = new JSONObject();

		try {

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			return planService.subscribeRunningPlan(user_id, plan_id, vehicle_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
}
