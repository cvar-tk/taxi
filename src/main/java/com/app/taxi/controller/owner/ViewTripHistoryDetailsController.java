package com.app.taxi.controller.owner;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class ViewTripHistoryDetailsController {
	

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private OwnerService ownerService;
	

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_VIEW_TRIP_HISTORY})
	private String OWNER_CAR_TRIP_HISTORY(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "trip_id", required = true) Integer trip_id) {

		try {
			
			JSONObject jsonObject = new JSONObject();

			Integer  user_id =tokenValidatorService.getUserIdByToken(token);
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				return jsonObject.toString();
			}
  
			
			return ownerService.ViewTripHistoryByTripId(trip_id).toString();


		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}
		
	
	}
	
}
