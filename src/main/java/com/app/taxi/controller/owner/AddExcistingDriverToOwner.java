package com.app.taxi.controller.owner;

import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class AddExcistingDriverToOwner {
	

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@Autowired
	private DriverService driverService;
	

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_ADD_EXCISTING_DRIVER})
	private String ADD_EXCISTING_DRIVER(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "mobile", required = true) BigInteger mobile) {

		try {
			
			JSONObject jsonObject = new JSONObject();

			Integer  user_id =tokenValidatorService.getUserIdByToken(token);
			
			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);
				return jsonObject.toString();
			}
  
			
			return driverService.addExcistingDriverToOwner(mobile , user_id).toString();


		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}
		
	
	}
	
}
