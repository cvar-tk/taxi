package com.app.taxi.controller.owner;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.OwnerService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class CarAutoRenewalPlanDetails {

	@Autowired
	private OwnerService ownerService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_CAR_AUTORENEWAL_PLAN})
	private String POST_DRIVER_ONLINE(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "car_id", required = true) Integer car_id,
			@RequestParam(value = "plan_id", required = true) Integer plan_id,
			@RequestParam(value = "auto_renewal", required = true) Integer auto_renewal) {

		JSONObject jsonObject = new JSONObject();

		Integer user_id = tokenValidatorService.getUserIdByToken(token);

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return ownerService.updateCarSubscriberAutoRenewal(user_id , car_id , plan_id , auto_renewal).toString();

			

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
}
