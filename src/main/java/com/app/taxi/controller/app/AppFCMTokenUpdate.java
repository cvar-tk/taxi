package com.app.taxi.controller.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.CommunicationAssetsService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class AppFCMTokenUpdate {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private CommunicationAssetsService communicationAssetsService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_FCM_TOKEN})
	private String POST_LOGIN(@RequestParam(value = "fcm_token", required = true) String fcm_token,
			@RequestParam(value = "access_token", required = true) String access_token) {

		Integer user_id = tokenValidatorService.getUserIdByToken(access_token);

		if (user_id == null) {
			return SingleTon.APP_TECH_SUCESS().toString();
		}

		communicationAssetsService.updateFCMToken(fcm_token, user_id);
		return SingleTon.APP_TECH_SUCESS().toString();

	}
}
