package com.app.taxi.controller.app;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;


@Controller
public class UpdatePasswordByTokenController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_PASSWORD })
	private String UPDATE_PASSWORD(@RequestBody String request) {
		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			String password = request_obj.getString(SingleTon.PASSWORD);

			if (password.length() < 6 || password.length() > 12) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Password should be 6-12 charactor..!");
				return jsonObject.toString();

			}
			
			return userService.UpdatePasswordByToken(token, password).toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}

}
