package com.app.taxi.controller.app;

import java.math.BigInteger;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;

@Controller
public class AppOtpApiController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_OTP_CHECK_ACCOUNT })
	private String APP_CHECK(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {
			
			JSONObject request_obj = new JSONObject(request);

			String phone = request_obj.getString(SingleTon.MOBILE);
			Integer role = request_obj.getInt(SingleTon.ROLE);

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(phone);

			} catch (Exception e) {
				e.printStackTrace();
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject.toString();
			}

			if (!userService.checkAccountExistingByRole(obj_phone, role)) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Mobile Number not registered..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject.toString();

			}
			
			
			userService.sendOTPByID(phone,role);
			
			
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}
	}
}
