package com.app.taxi.controller.app;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class ProfilePictureUploadController {

	@Autowired
	private UserService userService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@PostMapping(value = { SingleTon.ADD_PROFILE_PICTURE })
	private String POST_ADD_VEHICLE(@RequestParam(value = "token", required = true) String token,
			@RequestParam("profile_pic") MultipartFile pic) {

		JSONObject jsonObject = new JSONObject();

		try {
			if (pic.isEmpty()) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Empty is empty..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);
				return jsonObject.toString();
			}

			if (!(pic.getContentType().equalsIgnoreCase("image/png")
					|| pic.getContentType().equalsIgnoreCase("image/jpeg")
					|| pic.getContentType().equalsIgnoreCase("image/jpg"))) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Not a valid Image..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6003);

				return jsonObject.toString();
			}

			if (tokenValidatorService.getUserIdByToken(token) == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}
			
	

			userService.UploadNewPic(tokenValidatorService.getUserIdByToken(token),SingleTon.getFilenamebyfile(pic));
			
			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);
			jsonObject.put(SingleTon.CONTENT_STATUS, true);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Updated Successfully..!");
			
			
			return jsonObject.toString();

		} catch (Exception e) {

			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}

}
