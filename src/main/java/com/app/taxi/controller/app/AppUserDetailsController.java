package com.app.taxi.controller.app;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;

@Controller
public class AppUserDetailsController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.GET_USER_DETAILS })
	private String POST_LOGIN(@RequestBody String request) {
		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);

			return userService.getDetailsBySessionId(token).toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}

}
