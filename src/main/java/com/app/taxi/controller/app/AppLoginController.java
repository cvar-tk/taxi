package com.app.taxi.controller.app;

import java.math.BigInteger;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.Commons;
import com.app.taxi.SingleTon;
import com.app.taxi.service.UserService;

@Controller
public class AppLoginController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_LOGIN })
	private String POST_LOGIN(@RequestBody String request) {

		try {
			JSONObject jsonObject = new JSONObject(request);

			Double mobile = jsonObject.getDouble(SingleTon.MOBILE);
			String password = jsonObject.getString(SingleTon.PASSWORD);
			Integer role = jsonObject.getInt(SingleTon.ROLE);

			return userService.getJSONResultForUserLogin(mobile, password, role).toString();

		} catch (JSONException e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}

	
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_OWNER_APP_OTP })
	private String POST_VERIFY_OWNER_OTP(@RequestBody String request) {

		try {
			JSONObject jsonObject = new JSONObject(request);

			String  mobile = jsonObject.getString(SingleTon.MOBILE);
			String otp = jsonObject.getString(SingleTon.OTP);

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(mobile);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				return jsonObject.toString();
			}
			
			
			return userService.VerifyOwnerOTP(obj_phone,otp).toString();

		} catch (JSONException e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_DRIVER_APP_OTP })
	private String POST_VERIFY_DRIVER_OTP(@RequestBody String request) {

		try {
			JSONObject jsonObject = new JSONObject(request);

			String  mobile = jsonObject.getString(SingleTon.MOBILE);
			String otp = jsonObject.getString(SingleTon.OTP);

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(mobile);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				return jsonObject.toString();
			}
			
			
			return userService.VerifyDriverOTP(obj_phone,otp).toString();

		} catch (JSONException e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}
		
	
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_USER_APP_OTP })
	private String POST_VERIFY_USER_OTP(@RequestBody String request) {

		try {
			JSONObject jsonObject = new JSONObject(request);

			String  mobile = jsonObject.getString(SingleTon.MOBILE);
			String otp = jsonObject.getString(SingleTon.OTP);

			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(mobile);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				return jsonObject.toString();
			}
			
			
			return userService.VerifyUserOTP(obj_phone,otp).toString();

		} catch (JSONException e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}
		
	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_FORGET_PASSSWORD_OTP_VERIFY })
	private String POST_VERIFY_FORGET_PASSWORD_OTP(@RequestBody String request) {

		try {
			JSONObject jsonObject = new JSONObject(request);

			String  mobile = jsonObject.getString(SingleTon.MOBILE);
			String otp = jsonObject.getString(SingleTon.OTP);
			Integer role = jsonObject.getInt(SingleTon.ROLE);


			BigInteger obj_phone;

			try {

				obj_phone = new BigInteger(mobile);

			} catch (Exception e) {
				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Invalid Phone Number..!");
				return jsonObject.toString();
			}
			
			
			return userService.VerifyForgetPasswordOTP(obj_phone,otp,role).toString();

		} catch (JSONException e) {
			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR("").toString();
		}

	}
}
