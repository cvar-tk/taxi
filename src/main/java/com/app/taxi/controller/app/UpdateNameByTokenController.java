package com.app.taxi.controller.app;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.TokenValidatorService;
import com.app.taxi.service.UserService;

@Controller
public class UpdateNameByTokenController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_USERNAME })
	private String UPDATE_NAME(@RequestBody String request) {
		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			String username = request_obj.getString(SingleTon.USER_NAME);

			if (username.length() < 6 || username.length() > 12) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);
				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Username should be 6-12 charactor..!");
				jsonObject.put(SingleTon.RESPONSE_CODE, 6002);

				return jsonObject.toString();

			}

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			userService.UpdateUsernameToken(user_id, username);

			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Success...!");

			jsonObject.put(SingleTon.RESPONSE_CODE, 6001);

			return jsonObject.toString();

		} catch (Exception e) {

			e.printStackTrace();
			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}
}
