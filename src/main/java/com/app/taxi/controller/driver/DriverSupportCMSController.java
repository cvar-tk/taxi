package com.app.taxi.controller.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.CMSService;

@Controller
public class DriverSupportCMSController {


	@Autowired
	private CMSService cmsService; 
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DRIVER_GET_PRIVACY_POLICY })
	private String DRIVER_PRIVACY_POLICY() {
		
		return cmsService.getPrivacyPolicy(3);
	}

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DRIVER_GET_TERMS_AND_CONDITION})
	private String  DRIVER_GET_TERMS_AND_CONDITION() {
		
		return cmsService.getTermsAndCondition(3);
	}

	
}
