package com.app.taxi.controller.driver;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class DriverDeSelectCarController {

	@Autowired
	private DriverService driverService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_DESELECT_CAR_BY_DRIVER })
	private String POST_SELECT_CAR(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();

		try {

			JSONObject request_obj = new JSONObject(request);

			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			Integer vehicle_id = request_obj.getInt(SingleTon.CAR_ID);

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();

			}

			return driverService.deselectCarByDriverId(user_id, vehicle_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();

		}

	}

}
