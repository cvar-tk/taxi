package com.app.taxi.controller.driver;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class DriverUpdateBaseKmDetailsController {

	
	@Autowired
	private DriverService driverService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@PostMapping(value = { SingleTon.URL_APP_UPDATE_BASE_KM_DETAILS})
	private String POST_DRIVER_BASE_KM_DETAILS(@RequestBody String request) {

		JSONObject jsonObject = new JSONObject();
		
		try {
		
			JSONObject request_obj = new JSONObject(request);
			
			String token = request_obj.getString(SingleTon.ACCESS_TOKEN);
			
		    Integer user_id = tokenValidatorService.getUserIdByToken(token);
		    
		    Double base_km = request_obj.getDouble(SingleTon.BASE_KM);
		    
		    Double base_km_price = request_obj.getDouble(SingleTon.BASE_KM_PRICE);

		    Double per_min_price = request_obj.getDouble(SingleTon.BASE_MIN_PRICE);

		    Double base_fair = request_obj.getDouble(SingleTon.BASE_FAIR);


			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return driverService.updateBaseKmDetails(user_id,base_km,base_km_price,per_min_price,base_fair).toString();

			

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
	
	
	
}
