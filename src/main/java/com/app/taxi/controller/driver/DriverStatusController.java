package com.app.taxi.controller.driver;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class DriverStatusController {

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@Autowired
	private DriverService driverService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DRIVER_ACTIVITY_STATUS })
	private String GET_DRIVER_STATUS(@RequestParam(value = "token", required = true) String token) {

		try {

			JSONObject jsonObject = new JSONObject();

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return driverService.getDriverCurrentStatusById(user_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}


	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DRIVER_ON_OFF_STATUS })
	private String GET_ONLINE_STATUS(@RequestParam(value = "token", required = true) String token) {

		try {

			JSONObject jsonObject = new JSONObject();

			Integer user_id = tokenValidatorService.getUserIdByToken(token);

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}

			return driverService.getDriverONOFFStatusById(user_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
	
}
