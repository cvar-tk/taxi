package com.app.taxi.controller.driver;

import java.util.Calendar;
import java.util.TimeZone;

import org.hibernate.Query;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.model.SubscribedList;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class DriverReceiveTripLocation {

	@Autowired
	private DriverService driverService;

	@Autowired
	private TokenValidatorService tokenValidatorService;
	
	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_DRIVER_MANY_TRIP_LOCATION })
	private String GET_CALL_BACK_CONTROLLER(@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "latitude", required = true) Double latitude,
			@RequestParam(value = "longtitude", required = true) Double longtitude) {

		JSONObject jsonObject = new JSONObject();

		Integer user_id = tokenValidatorService.getUserIdByToken(token);

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}
			

			
			return driverService.getManyTripLocationByLatitude(user_id,latitude,longtitude).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}
	
}
