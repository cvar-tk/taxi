package com.app.taxi.controller.driver;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.taxi.SingleTon;
import com.app.taxi.service.DriverService;
import com.app.taxi.service.TokenValidatorService;

@Controller
public class CallBackReciveController {

	@Autowired
	private DriverService driverService;

	@Autowired
	private TokenValidatorService tokenValidatorService;

	@ResponseBody
	@GetMapping(value = { SingleTon.URL_APP_GET_USER_DEATILS_BY_DRIVER })
	private String GET_CALL_BACK_CONTROLLER(@RequestParam(value = "token", required = true) String token) {

		JSONObject jsonObject = new JSONObject();

		Integer user_id = tokenValidatorService.getUserIdByToken(token);

		try {

			if (user_id == null) {

				jsonObject.put(SingleTon.CONTENT_STATUS, false);

				jsonObject.put(SingleTon.CONTENT_MESSAGE, "Session Expired..!");

				jsonObject.put(SingleTon.RESPONSE_CODE, 6005);

				return jsonObject.toString();
			}
			
			return driverService.getUserLocationRequest(user_id).toString();

		} catch (Exception e) {

			e.printStackTrace();

			return SingleTon.APP_TECH_ERROR(null).toString();
		}

	}

}
