package com.app.taxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class TaxiApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(TaxiApplication.class, args);
	}
}
