package com.app.taxi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Callbacks")
public class Callbacks implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "c_id", nullable = false, unique = true)
	private Integer c_id;

	@Column(name = "created_date", nullable = false)
	private Date created_date;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_master", nullable = false)
	private UserMaster user_master;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle_master", nullable = false)
	private VehicleMaster vehicle_master;
	
	@Column(name = "user_lat", nullable = false)
	private Double user_lat;

	@Column(name = "user_lang", nullable = false)
	private Double user_lang;
	
	@Column(name = "address", nullable = false)
	private String address;

	public Integer getC_id() {
		return c_id;
	}

	public void setC_id(Integer c_id) {
		this.c_id = c_id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public UserMaster getUser_master() {
		return user_master;
	}

	public void setUser_master(UserMaster user_master) {
		this.user_master = user_master;
	}

	public VehicleMaster getVehicle_master() {
		return vehicle_master;
	}

	public void setVehicle_master(VehicleMaster vehicle_master) {
		this.vehicle_master = vehicle_master;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Double getUser_lat() {
		return user_lat;
	}

	public void setUser_lat(Double user_lat) {
		this.user_lat = user_lat;
	}

	public Double getUser_lang() {
		return user_lang;
	}

	public void setUser_lang(Double user_lang) {
		this.user_lang = user_lang;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
	
}
