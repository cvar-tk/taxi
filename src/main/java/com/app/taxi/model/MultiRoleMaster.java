package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "multi_role_master")
public class MultiRoleMaster implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "r_id", nullable = false, unique = true)
	private Integer r_id;

	@Column(name = "multi_roles", nullable = false,unique = true)
	private String multi_roles;

	public Integer getR_id() {
		return r_id;
	}

	public void setR_id(Integer r_id) {
		this.r_id = r_id;
	}

	public String getMulti_roles() {
		return multi_roles;
	}

	public void setMulti_roles(String multi_roles) {
		this.multi_roles = multi_roles;
	}

	

}
