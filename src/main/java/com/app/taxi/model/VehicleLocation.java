package com.app.taxi.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_location")
public class VehicleLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vl_id", nullable = false, unique = true)
	private Integer vl_id;

	@Column(name = "vl_lat", nullable = false)
	private Double vl_lat;

	@Column(name = "vl_lang", nullable = false)
	private Double vl_lang;

	@Column(name = "last_update", nullable = false)
	private Date last_update;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "vl_vid", nullable = false,unique =true)
	private VehicleMaster vl_vid;

	public Integer getVl_id() {
		return vl_id;
	}

	public void setVl_id(Integer vl_id) {
		this.vl_id = vl_id;
	}

	public Double getVl_lat() {
		return vl_lat;
	}

	public void setVl_lat(Double vl_lat) {
		this.vl_lat = vl_lat;
	}

	public Double getVl_lang() {
		return vl_lang;
	}

	public void setVl_lang(Double vl_lang) {
		this.vl_lang = vl_lang;
	}

	public Date getLast_update() {
		return last_update;
	}

	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}

	public VehicleMaster getVl_vid() {
		return vl_vid;
	}

	public void setVl_vid(VehicleMaster vl_vid) {
		this.vl_vid = vl_vid;
	}

	
	
	
}
