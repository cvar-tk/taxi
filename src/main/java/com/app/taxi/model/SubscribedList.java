package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subscribed_list")
public class SubscribedList implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "sl_id", nullable = false, unique = true)
	private Integer sl_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sl_vehicle_master", nullable = true)
	private VehicleMaster sl_vehicle_master;
	
	@Column(name = "sl_price", nullable = false)
	private Float sl_price;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "sl_type", nullable = false)
	private PlanTypeMaster sl_type;

	@Column(name = "sl_duration", nullable = false)
	private Integer sl_duration;
	
	@Column(name = "from_date", nullable = false)
	private Date from_date;
	
	@Column(name = "to_date", nullable = false)
	private Date to_date;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sl_driver", nullable = true)
	private UserMaster sl_driver;
	
	@Column(name = "sl_auto", nullable = true)
	private Integer sl_auto;

	public Integer getSl_id() {
		return sl_id;
	}

	public void setSl_id(Integer sl_id) {
		this.sl_id = sl_id;
	}

	public VehicleMaster getSl_vehicle_master() {
		return sl_vehicle_master;
	}

	public void setSl_vehicle_master(VehicleMaster sl_vehicle_master) {
		this.sl_vehicle_master = sl_vehicle_master;
	}

	public Float getSl_price() {
		return sl_price;
	}

	public void setSl_price(Float sl_price) {
		this.sl_price = sl_price;
	}

	public PlanTypeMaster getSl_type() {
		return sl_type;
	}

	public void setSl_type(PlanTypeMaster sl_type) {
		this.sl_type = sl_type;
	}

	public Integer getSl_duration() {
		return sl_duration;
	}

	public void setSl_duration(Integer sl_duration) {
		this.sl_duration = sl_duration;
	}

	public Date getFrom_date() {
		return from_date;
	}

	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getTo_date() {
		return to_date;
	}

	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}

	public UserMaster getSl_driver() {
		return sl_driver;
	}

	public void setSl_driver(UserMaster sl_driver) {
		this.sl_driver = sl_driver;
	}

	public Integer getSl_auto() {
		return sl_auto;
	}

	public void setSl_auto(Integer sl_auto) {
		this.sl_auto = sl_auto;
	}	
	
	
	
}
