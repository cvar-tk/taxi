package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "suppport_cms")
public class SupportCMS implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ss_id", nullable = false, unique = true)
	private Integer ss_id;

	@Column(name = "privacy_policy", nullable = true, columnDefinition = "TEXT")
	private String privacy_policy;

	@Column(name = "terms_condition", nullable = true, columnDefinition = "TEXT")
	private String terms_condition;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "role_master", nullable = false)
	private RoleMaster role_master;

	public Integer getSs_id() {
		return ss_id;
	}

	public void setSs_id(Integer ss_id) {
		this.ss_id = ss_id;
	}

	public String getPrivacy_policy() {
		return privacy_policy;
	}

	public void setPrivacy_policy(String privacy_policy) {
		this.privacy_policy = privacy_policy;
	}

	public String getTerms_condition() {
		return terms_condition;
	}

	public void setTerms_condition(String terms_condition) {
		this.terms_condition = terms_condition;
	}

	public RoleMaster getRole_master() {
		return role_master;
	}

	public void setRole_master(RoleMaster role_master) {
		this.role_master = role_master;
	}
}
