package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "backup_code")
public class BackUpCode implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bc_id", nullable = false, unique = true)
	private Integer bc_id;

	@Column(name = "bk_1", nullable = false)
	private Integer bk_1;

	@Column(name = "s_bk_1", nullable = false)
	private Integer s_bk_1;

	@Column(name = "bk_2", nullable = false)
	private Integer bk_2;

	@Column(name = "s_bk_2", nullable = false)
	private Integer s_bk_2;

	@Column(name = "bk_3", nullable = false)
	private Integer bk_3;

	@Column(name = "s_bk_3", nullable = false)
	private Integer s_bk_3;

	@Column(name = "bk_4", nullable = false)
	private Integer bk_4;

	@Column(name = "s_bk_4", nullable = false)
	private Integer s_bk_4;

	@Column(name = "bk_5", nullable = false)
	private Integer bk_5;

	@Column(name = "s_bk_5", nullable = false)
	private Integer s_bk_5;

	@Column(name = "bk_6", nullable = false)
	private Integer bk_6;

	@Column(name = "s_bk_6", nullable = false)
	private Integer s_bk_6;

	@Column(name = "bk_7", nullable = false)
	private Integer bk_7;

	@Column(name = "s_bk_7", nullable = false)
	private Integer s_bk_7;

	@Column(name = "bk_8", nullable = false)
	private Integer bk_8;

	@Column(name = "s_bk_8", nullable = false)
	private Integer s_bk_8;

	@Column(name = "bk_9", nullable = false)
	private Integer bk_9;
	
	@Column(name = "s_bk_9", nullable = false)
	private Integer s_bk_9;
	
	@Column(name = "bk_10", nullable = false)
	private Integer bk_10;

	@Column(name = "s_bk_10", nullable = false)
	private Integer s_bk_10;

	public Integer getBc_id() {
		return bc_id;
	}

	public void setBc_id(Integer bc_id) {
		this.bc_id = bc_id;
	}

	public Integer getBk_1() {
		return bk_1;
	}

	public void setBk_1(Integer bk_1) {
		this.bk_1 = bk_1;
	}

	public Integer getS_bk_1() {
		return s_bk_1;
	}

	public void setS_bk_1(Integer s_bk_1) {
		this.s_bk_1 = s_bk_1;
	}

	public Integer getBk_2() {
		return bk_2;
	}

	public void setBk_2(Integer bk_2) {
		this.bk_2 = bk_2;
	}

	public Integer getS_bk_2() {
		return s_bk_2;
	}

	public void setS_bk_2(Integer s_bk_2) {
		this.s_bk_2 = s_bk_2;
	}

	public Integer getBk_3() {
		return bk_3;
	}

	public void setBk_3(Integer bk_3) {
		this.bk_3 = bk_3;
	}

	public Integer getS_bk_3() {
		return s_bk_3;
	}

	public void setS_bk_3(Integer s_bk_3) {
		this.s_bk_3 = s_bk_3;
	}

	public Integer getBk_4() {
		return bk_4;
	}

	public void setBk_4(Integer bk_4) {
		this.bk_4 = bk_4;
	}

	public Integer getS_bk_4() {
		return s_bk_4;
	}

	public void setS_bk_4(Integer s_bk_4) {
		this.s_bk_4 = s_bk_4;
	}

	public Integer getBk_5() {
		return bk_5;
	}

	public void setBk_5(Integer bk_5) {
		this.bk_5 = bk_5;
	}

	public Integer getS_bk_5() {
		return s_bk_5;
	}

	public void setS_bk_5(Integer s_bk_5) {
		this.s_bk_5 = s_bk_5;
	}

	public Integer getBk_6() {
		return bk_6;
	}

	public void setBk_6(Integer bk_6) {
		this.bk_6 = bk_6;
	}

	public Integer getS_bk_6() {
		return s_bk_6;
	}

	public void setS_bk_6(Integer s_bk_6) {
		this.s_bk_6 = s_bk_6;
	}

	public Integer getBk_7() {
		return bk_7;
	}

	public void setBk_7(Integer bk_7) {
		this.bk_7 = bk_7;
	}

	public Integer getS_bk_7() {
		return s_bk_7;
	}

	public void setS_bk_7(Integer s_bk_7) {
		this.s_bk_7 = s_bk_7;
	}

	public Integer getBk_8() {
		return bk_8;
	}

	public void setBk_8(Integer bk_8) {
		this.bk_8 = bk_8;
	}

	public Integer getS_bk_8() {
		return s_bk_8;
	}

	public void setS_bk_8(Integer s_bk_8) {
		this.s_bk_8 = s_bk_8;
	}

	public Integer getBk_9() {
		return bk_9;
	}

	public void setBk_9(Integer bk_9) {
		this.bk_9 = bk_9;
	}

	public Integer getS_bk_10() {
		return s_bk_10;
	}

	public void setS_bk_10(Integer s_bk_10) {
		this.s_bk_10 = s_bk_10;
	}

	public Integer getS_bk_9() {
		return s_bk_9;
	}

	public void setS_bk_9(Integer s_bk_9) {
		this.s_bk_9 = s_bk_9;
	}

	public Integer getBk_10() {
		return bk_10;
	}

	public void setBk_10(Integer bk_10) {
		this.bk_10 = bk_10;
	}
	
	
	
}
