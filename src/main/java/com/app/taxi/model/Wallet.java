package com.app.taxi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "wallet")
public class Wallet implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "w_id", nullable = false, unique = true)
	private Integer w_id;

	@Column(name = "opposition", nullable = false)
	private Integer opposition;

	@Column(name = "opening", nullable = false)
	private Integer opening;

	@Column(name = "closeing", nullable = false)
	private Integer closeing;

	@Column(name = "type", nullable = false)
	private Integer type;

	@Column(name = "tx_id", nullable = true)
	private String tx_id;

	@Column(name = "comments", nullable = true)
	private String comments;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user", nullable = false)
	private UserMaster user;

}
