package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.*;

@Entity
@Table(name = "user_status")
public class VehicleStatus implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "s_id", nullable = false, unique = true)
	private Integer s_id;

	@Column(name = "s_name", nullable = false, unique = true)
	private String s_name;

}
