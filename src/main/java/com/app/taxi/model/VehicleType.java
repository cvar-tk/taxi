package com.app.taxi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_type")
public class VehicleType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vt_id", nullable = false, unique = true)
	private Integer vt_id;

	@Column(name = "vt_text", nullable = false)
	private String vt_text;

	public Integer getVt_id() {
		return vt_id;
	}

	public void setVt_id(Integer vt_id) {
		this.vt_id = vt_id;
	}

	public String getVt_text() {
		return vt_text;
	}

	public void setVt_text(String vt_text) {
		this.vt_text = vt_text;
	}	
	
	
	
	
}
