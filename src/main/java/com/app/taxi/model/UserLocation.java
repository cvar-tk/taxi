package com.app.taxi.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_location")
public class UserLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ul_id", nullable = false, unique = true)
	private Integer ul_id;

	@Column(name = "ul_lat", nullable = false)
	private Double ul_lat;

	@Column(name = "ul_lang", nullable = false)
	private Double ul_lang;

	@Column(name = "last_update", nullable = false)
	private Date last_update;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user", nullable = false, unique = true)
	private UserMaster user;

	public Double getUl_lat() {
		return ul_lat;
	}

	public void setUl_lat(Double ul_lat) {
		this.ul_lat = ul_lat;
	}

	public Double getUl_lang() {
		return ul_lang;
	}

	public void setUl_lang(Double ul_lang) {
		this.ul_lang = ul_lang;
	}

	public Date getLast_update() {
		return last_update;
	}

	public void setLast_update(Date last_update) {
		this.last_update = last_update;
	}

	public UserMaster getUser() {
		return user;
	}

	public void setUser(UserMaster user) {
		this.user = user;
	}

}
