package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "role_master")
public class RoleMaster implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rm_id", nullable = false, unique = true)
	private Integer rm_id;

	@Column(name = "roles", nullable = false,unique = true)
	private String roles;

	public Integer getrId() {
		return rm_id;
	}

	public void setrId(Integer rId) {
		this.rm_id = rId;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

}
