package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workers_credential")
public class WorkersCredential implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "wc_id", nullable = false, unique = true)
	private Integer wc_id;

	@Column(name = "wc_password", nullable = true)
	private String wc_password;

	@Column(name = "wc_otp", nullable = true)
	private String wc_otp;
	

	@Column(name = "wc_created_on", nullable = false)
	private Date wc_created_on;

	@Column(name = "wc_updated_on", nullable = false)
	private Date wc_updated_on;

	@Column(name = "wc_otp_expiry_time", nullable = true)
	private Date wc_otp_expiry_time;

	public Integer getWc_id() {
		return wc_id;
	}

	public void setWc_id(Integer wc_id) {
		this.wc_id = wc_id;
	}

	public String getWc_password() {
		return wc_password;
	}

	public void setWc_password(String wc_password) {
		this.wc_password = wc_password;
	}

	public String getWc_otp() {
		return wc_otp;
	}

	public void setWc_otp(String wc_otp) {
		this.wc_otp = wc_otp;
	}

	
	public Date getWc_created_on() {
		return wc_created_on;
	}

	public void setWc_created_on(Date wc_created_on) {
		this.wc_created_on = wc_created_on;
	}

	public Date getWc_updated_on() {
		return wc_updated_on;
	}

	public void setWc_updated_on(Date wc_updated_on) {
		this.wc_updated_on = wc_updated_on;
	}

	public Date getWc_otp_expiry_time() {
		return wc_otp_expiry_time;
	}

	public void setWc_otp_expiry_time(Date wc_otp_expiry_time) {
		this.wc_otp_expiry_time = wc_otp_expiry_time;
	}
	
	
	

}
