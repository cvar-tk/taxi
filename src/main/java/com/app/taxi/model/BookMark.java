package com.app.taxi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bookmarks")
public class BookMark {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bm_id", nullable = false, unique = true)
	private Integer bm_id;

	@Column(name = "book_id")
	private String book_id;
	
	@Column(name = "bookmark_id", nullable = false)
	private Integer bookmark_id;
	
	@Column(name = "image_code", nullable = false)
	private String image_code;

	public String getBook_id() {
		return book_id;
	}

	public void setBook_id(String book_id) {
		this.book_id = book_id;
	}

	

	public Integer getBm_id() {
		return bm_id;
	}

	public void setBm_id(Integer bm_id) {
		this.bm_id = bm_id;
	}

	public Integer getBookmark_id() {
		return bookmark_id;
	}

	public void setBookmark_id(Integer bookmark_id) {
		this.bookmark_id = bookmark_id;
	}

	public String getImage_code() {
		return image_code;
	}

	public void setImage_code(String image_code) {
		this.image_code = image_code;
	}
	
	
	

}
