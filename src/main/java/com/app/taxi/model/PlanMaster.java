package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity


@Table(name = "plan_master")
public class PlanMaster implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pm_id", nullable = false, unique = true)
	private Integer pm_id;

	@Column(name = "pm_title", nullable = false)
	private String pm_title;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "pt_type", nullable = false)
	private PlanTypeMaster pt_type;

	@Column(name = "pm_price", nullable = false)
	private Float pm_price;

	@Column(name = "pm_comments", nullable = false)
	private String pm_comments;
	
	@Column(name = "pm_topup_duration", nullable = true)
	private Integer pm_topup_duration;
	
	@Column(name = "pm_demandtime_duration", nullable = true)
	private Integer pm_demandtime_duration;
	
	@Column(name = "status", nullable = false)
	private Integer status;

	public Integer getPm_id() {
		return pm_id;
	}

	public void setPm_id(Integer pm_id) {
		this.pm_id = pm_id;
	}

	public String getPm_title() {
		return pm_title;
	}

	public void setPm_title(String pm_title) {
		this.pm_title = pm_title;
	}

	public PlanTypeMaster getPt_type() {
		return pt_type;
	}

	public void setPt_type(PlanTypeMaster pt_type) {
		this.pt_type = pt_type;
	}

	public Float getPm_price() {
		return pm_price;
	}

	public void setPm_price(Float pm_price) {
		this.pm_price = pm_price;
	}

	public String getPm_comments() {
		return pm_comments;
	}

	public void setPm_comments(String pm_comments) {
		this.pm_comments = pm_comments;
	}

	public Integer getPm_topup_duration() {
		return pm_topup_duration;
	}

	public void setPm_topup_duration(Integer pm_topup_duration) {
		this.pm_topup_duration = pm_topup_duration;
	}

	public Integer getPm_demandtime_duration() {
		return pm_demandtime_duration;
	}

	public void setPm_demandtime_duration(Integer pm_demandtime_duration) {
		this.pm_demandtime_duration = pm_demandtime_duration;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	
}
