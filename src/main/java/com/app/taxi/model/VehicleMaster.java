package com.app.taxi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vehicle_master")
public class VehicleMaster implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vhm_id", nullable = false, unique = true)
	private Integer vhm_id;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "vhm_owner", nullable = false)
	private UserMaster vhm_owner;

	@Column(name = "vhm_name", nullable = false)
	private String vhm_name;
	
	@Column(name = "vhm_number", nullable = false)
	private String vhm_number;

	@Column(name = "vhm_registration_number", nullable = false)
	private String vhm_registration_number;

	@Column(name = "vhm_picked_by", nullable = true , unique = true)
	private Integer vhm_picked_by;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "registration_date", nullable = false)
	private Date registration_date;

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", nullable = false)
	private Date created_date;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vhm_model", nullable = true)
	private VehicleModel vhm_model;

	@Column(name = "vhm_status", nullable = false)
	private Integer vhm_status;

	/*@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "v_location", nullable = true)
	private VehicleLocation v_location;*/

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "v_assets", nullable = false)
	private VehicleMasterAssets v_assets;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "v_type", nullable = true)
	private VehicleType v_type;

	@Column(name = "base_fair", nullable = true)
	private Double base_fair;

	@Column(name = "per_km_price", nullable = true)
	private Double per_km_price;

	@Column(name = "vhm_make_hint", nullable = false)
	private String vhm_make_hint;

	@Column(name = "vhm_model_hint", nullable = false)
	private String vhm_model_hint;

	@Column(name = "vhm_colour_hint", nullable = false)
	private String vhm_colour_hint;
	
	@Column(name = "base_km", nullable = true)
	private Double base_km;

	@Column(name = "per_min_price", nullable = true)
	private Double per_min_price;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "vhm_colour", nullable = true)
	private VehicleColour vhm_colour;
	
	public Integer getVhm_id() {
		return vhm_id;
	}

	public void setVhm_id(Integer vhm_id) {
		this.vhm_id = vhm_id;
	}

	public String getVhm_name() {
		return vhm_name;
	}

	public void setVhm_name(String vhm_name) {
		this.vhm_name = vhm_name;
	}

	public String getVhm_number() {
		return vhm_number;
	}

	public void setVhm_number(String vhm_number) {
		this.vhm_number = vhm_number;
	}

	public String getVhm_registration_number() {
		return vhm_registration_number;
	}

	public void setVhm_registration_number(String vhm_registration_number) {
		this.vhm_registration_number = vhm_registration_number;
	}

	
	public Double getBase_km() {
		return base_km;
	}

	public void setBase_km(Double base_km) {
		this.base_km = base_km;
	}

	public Double getPer_min_price() {
		return per_min_price;
	}

	public void setPer_min_price(Double per_min_price) {
		this.per_min_price = per_min_price;
	}

	public VehicleColour getVhm_colour() {
		return vhm_colour;
	}

	public void setVhm_colour(VehicleColour vhm_colour) {
		this.vhm_colour = vhm_colour;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public VehicleModel getVhm_model() {
		return vhm_model;
	}

	public void setVhm_model(VehicleModel vhm_model) {
		this.vhm_model = vhm_model;
	}

	public Integer getVhm_status() {
		return vhm_status;
	}

	public void setVhm_status(Integer vhm_status) {
		this.vhm_status = vhm_status;
	}

	/*public VehicleLocation getV_location() {
		return v_location;
	}

	public void setV_location(VehicleLocation v_location) {
		this.v_location = v_location;
	}*/

	public Double getBase_fair() {
		return base_fair;
	}

	public void setBase_fair(Double base_fair) {
		this.base_fair = base_fair;
	}

	public Double getPer_km_price() {
		return per_km_price;
	}

	public void setPer_km_price(Double per_km_price) {
		this.per_km_price = per_km_price;
	}

	public String getVhm_make_hint() {
		return vhm_make_hint;
	}

	public void setVhm_make_hint(String vhm_make_hint) {
		this.vhm_make_hint = vhm_make_hint;
	}

	public String getVhm_model_hint() {
		return vhm_model_hint;
	}

	public void setVhm_model_hint(String vhm_model_hint) {
		this.vhm_model_hint = vhm_model_hint;
	}

	public String getVhm_colour_hint() {
		return vhm_colour_hint;
	}

	public void setVhm_colour_hint(String vhm_colour_hint) {
		this.vhm_colour_hint = vhm_colour_hint;
	}

	public Date getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Date registration_date) {
		this.registration_date = registration_date;
	}

	public VehicleMasterAssets getV_assets() {
		return v_assets;
	}

	public void setV_assets(VehicleMasterAssets v_assets) {
		this.v_assets = v_assets;
	}

	public UserMaster getVhm_owner() {
		return vhm_owner;
	}

	public void setVhm_owner(UserMaster vhm_owner) {
		this.vhm_owner = vhm_owner;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public VehicleType getV_type() {
		return v_type;
	}

	public void setV_type(VehicleType v_type) {
		this.v_type = v_type;
	}

	public Integer getVhm_picked_by() {
		return vhm_picked_by;
	}

	public void setVhm_picked_by(Integer vhm_picked_by) {
		this.vhm_picked_by = vhm_picked_by;
	}

}
