package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_brand")
public class VehicleBrand implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vb_id", nullable = false, unique = true)
	private Integer vb_id;
	
	@Column(name = "vb_name", nullable = false, unique = true)
	private String vb_name;
	
}
