package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "admin_control")
public class AdminControl implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ctrl_id", nullable = false, unique = true)
	private Integer ctrl_id;

	@Column(name = "base_km_control", nullable = false)
	private Integer base_km_control;
	
	@Column(name = "base_km", nullable = false)
	private Double base_km;
	
	@Column(name = "per_min_price", nullable = false)
	private Double per_min_price;
	
	@Column(name = "gst", nullable = false)
	private Double gst;
	
	@Column(name = "cgst", nullable = false)
	private Double cgst;
	
	@Column(name = "sgst", nullable = false)
	private Double sgst;
	

	public Integer getCtrl_id() {
		return ctrl_id;
	}

	public void setCtrl_id(Integer ctrl_id) {
		this.ctrl_id = ctrl_id;
	}

	public Integer getBase_km_control() {
		return base_km_control;
	}

	public void setBase_km_control(Integer base_km_control) {
		this.base_km_control = base_km_control;
	}

	public Double getBase_km() {
		return base_km;
	}

	public void setBase_km(Double base_km) {
		this.base_km = base_km;
	}

	public Double getPer_min_price() {
		return per_min_price;
	}

	public void setPer_min_price(Double per_min_price) {
		this.per_min_price = per_min_price;
	}

	public Double getGst() {
		return gst;
	}

	public void setGst(Double gst) {
		this.gst = gst;
	}

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}

	
	
}
