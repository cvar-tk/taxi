package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "users_master")
public class UserMaster implements Serializable{
	
	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "u_id", nullable = false, unique = true)
	private Integer u_id;

	@Column(name = "m_phone", nullable = true)
	private BigInteger m_phone;

	@Column(name = "u_name", nullable = true)
	private String u_name;

	@Column(name = "u_pic", nullable = true)
	private String u_pic;

	@Column(name = "u_email", nullable = true)
	private String u_email;
	
	@Column(name = "u_security_pin", nullable = true)
	private Integer u_security_pin;

	@Column(name = "u_status", nullable = false)
	private Integer u_status;

	@Column(name = "u_ref_code", nullable = true, length = 6)
	private String u_ref_code;
	
	@Column(name = "fcm_token", nullable = true,columnDefinition="TEXT")
	private String fcm_token;
	
	@Column(name = "u_used_ref_code", length = 6)
	private String u_used_ref_code;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "u_role", nullable = false)
	private RoleMaster u_role;

	@Temporal(TemporalType.DATE)
	@Column(name = "u_created_date", nullable = false)
	private Date u_created_date;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "w_credential", nullable = false)
	private WorkersCredential w_credential;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "bck_code", nullable = true)
	private BackUpCode bck_code;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "driver_assets", nullable = true)
	private DriverMasterAssets driver_assets;
	
	@Column(name = "mapped_with", nullable = true)
	private Integer MappedWith;
	
	@Column(name = "created_by", nullable = true)
	private Integer created_by;

	public Integer getU_id() {
		return u_id;
	}

	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}

	public BigInteger getM_phone() {
		return m_phone;
	}

	public void setM_phone(BigInteger m_phone) {
		this.m_phone = m_phone;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}

	public String getU_pic() {
		return u_pic;
	}

	public void setU_pic(String u_pic) {
		this.u_pic = u_pic;
	}

	public String getU_email() {
		return u_email;
	}

	public String getU_used_ref_code() {
		return u_used_ref_code;
	}

	public void setU_used_ref_code(String u_used_ref_code) {
		this.u_used_ref_code = u_used_ref_code;
	}

	public void setU_email(String u_email) {
		this.u_email = u_email;
	}

	public Integer getU_security_pin() {
		return u_security_pin;
	}

	public void setU_security_pin(Integer u_security_pin) {
		this.u_security_pin = u_security_pin;
	}
	
	public String getFcm_token() {
		return fcm_token;
	}

	public void setFcm_token(String fcm_token) {
		this.fcm_token = fcm_token;
	}

	public Integer getU_status() {
		return u_status;
	}

	public void setU_status(Integer u_status) {
		this.u_status = u_status;
	}

	public String getU_ref_code() {
		return u_ref_code;
	}

	public void setU_ref_code(String u_ref_code) {
		this.u_ref_code = u_ref_code;
	}

	public RoleMaster getU_role() {
		return u_role;
	}

	public void setU_role(RoleMaster u_role) {
		this.u_role = u_role;
	}

	public Date getU_created_date() {
		return u_created_date;
	}

	public void setU_created_date(Date u_created_date) {
		this.u_created_date = u_created_date;
	}

	public WorkersCredential getW_credential() {
		return w_credential;
	}

	public void setW_credential(WorkersCredential w_credential) {
		this.w_credential = w_credential;
	}

	public BackUpCode getBck_code() {
		return bck_code;
	}

	public void setBck_code(BackUpCode bck_code) {
		this.bck_code = bck_code;
	}

	public DriverMasterAssets getDriver_assets() {
		return driver_assets;
	}

	public void setDriver_assets(DriverMasterAssets driver_assets) {
		this.driver_assets = driver_assets;
	}

	public Integer getMappedWith() {
		return MappedWith;
	}

	public void setMappedWith(Integer mappedWith) {
		MappedWith = mappedWith;
	}

	public Integer getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}
	
	
	
	
}
