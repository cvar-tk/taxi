package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehicle_model")
public class VehicleModel implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vm_id", nullable = false, unique = true)
	private Integer vm_id;
	
	@Column(name = "vm_name", nullable = false, unique = true)
	private String vm_name;
		
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vm_brand", nullable = false)
	private VehicleBrand vm_brand;

	public Integer getVm_id() {
		return vm_id;
	}

	public void setVm_id(Integer vm_id) {
		this.vm_id = vm_id;
	}

	public String getVm_name() {
		return vm_name;
	}

	public void setVm_name(String vm_name) {
		this.vm_name = vm_name;
	}

	public VehicleBrand getVm_brand() {
		return vm_brand;
	}

	public void setVm_brand(VehicleBrand vm_brand) {
		this.vm_brand = vm_brand;
	}
	
	

}
