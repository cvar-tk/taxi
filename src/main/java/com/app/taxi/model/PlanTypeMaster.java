package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "plan_type")
public class PlanTypeMaster implements Serializable{
	
	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "pt_id", nullable = false, unique = true)
	private Integer pt_id;

	@Column(name = "pt_title", nullable = false)
	private String pt_title;

	public Integer getPt_id() {
		return pt_id;
	}

	public void setPt_id(Integer pt_id) {
		this.pt_id = pt_id;
	}

	public String getPt_title() {
		return pt_title;
	}

	public void setPt_title(String pt_title) {
		this.pt_title = pt_title;
	}
	
	
	
	
}
