package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Blob;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "trip_master")
public class TripMaster implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "tr_id", nullable = false, unique = true)
	private Integer tr_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer", nullable = false)
	private UserMaster customer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vehicle", nullable = false)
	private VehicleMaster vehicle;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver", nullable = false)
	private UserMaster driver;

	@Column(name = "bck_code", nullable = false)
	private Integer bck_code;

	@Column(name = "starting_point", nullable = false, columnDefinition="TEXT")
	private String starting_point;

	@Column(name = "ending_point", nullable = true, columnDefinition="TEXT")
	private String ending_point;

	@Column(name = "tax_percent", nullable = false)
	private Double tax_percent;

	
	@Column(name = "start_lat", nullable = true)
	private Double start_lat;

	@Column(name = "total_km", nullable = true)
	private Double total_km;

	
	@Column(name = "start_lang", nullable = true)
	private Double start_lang;

	@Column(name = "end_lat", nullable = true)
	private Double end_lat;

	@Column(name = "end_lang", nullable = true)
	private Double end_lang;

	@Column(name = "start_time", nullable = true)
	private Date start_time;

	@Column(name = "end_time", nullable = true)
	private Date end_time;

	@Column(name = "amount", nullable = true)
	private Double amount;
	
	@Column(name = "est_amount", nullable = true)
	private Double est_amount;

	@Column(name = "base_fair", nullable = false)
	private Double base_fair;

	@Column(name = "toll", nullable = true)
	private Double toll;

	@Column(name = "processing_fees", nullable = true)
	private Double processing_fees;
	
	@Column(name = "per_min_price", nullable = false)
	private Double per_min_price;
	
	@Column(name = "per_km_price", nullable = false)
	private Double per_km_price;
	
	@Column(name = "base_km", nullable = false)
	private Double base_km;

	@Column(name = "payment_method", nullable = true)
	private Integer payment_method;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "trip_status", nullable = false)
	private TripStatus trip_status;
	
	@Column(name = "total_min", nullable = true)
	private Double total_min;
	
	@Column(name = "cgst", nullable = true)
	private Double cgst;
	
	@Column(name = "sgst", nullable = true)
	private Double sgst;
	
	
	@Column(name = "bitmap_image", nullable = true ,columnDefinition = "TEXT")
	private String bitmap_image;

	

	public String getBitmap_image() {
		return bitmap_image;
	}

	public void setBitmap_image(String bitmap_image) {
		this.bitmap_image = bitmap_image;
	}

	public Integer getTr_id() {
		return tr_id;
	}

	public void setTr_id(Integer tr_id) {
		this.tr_id = tr_id;
	}

	public UserMaster getCustomer() {
		return customer;
	}

	public void setCustomer(UserMaster customer) {
		this.customer = customer;
	}

	public VehicleMaster getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleMaster vehicle) {
		this.vehicle = vehicle;
	}

	public UserMaster getDriver() {
		return driver;
	}

	public void setDriver(UserMaster driver) {
		this.driver = driver;
	}

	public Integer getBck_code() {
		return bck_code;
	}

	public void setBck_code(Integer bck_code) {
		this.bck_code = bck_code;
	}

	public String getStarting_point() {
		return starting_point;
	}

	public void setStarting_point(String starting_point) {
		this.starting_point = starting_point;
	}

	public String getEnding_point() {
		return ending_point;
	}

	public void setEnding_point(String ending_point) {
		this.ending_point = ending_point;
	}

	public Double getStart_lat() {
		return start_lat;
	}

	public void setStart_lat(Double start_lat) {
		this.start_lat = start_lat;
	}

	public Double getStart_lang() {
		return start_lang;
	}

	public void setStart_lang(Double start_lang) {
		this.start_lang = start_lang;
	}

	public Double getEnd_lat() {
		return end_lat;
	}

	public void setEnd_lat(Double end_lat) {
		this.end_lat = end_lat;
	}

	public Double getEnd_lang() {
		return end_lang;
	}

	public void setEnd_lang(Double end_lang) {
		this.end_lang = end_lang;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public Date getEnd_time() {
		return end_time;
	}

	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getBase_fair() {
		return base_fair;
	}

	public void setBase_fair(Double base_fair) {
		this.base_fair = base_fair;
	}

	public Double getToll() {
		return toll;
	}

	public void setToll(Double toll) {
		this.toll = toll;
	}

	public Double getProcessing_fees() {
		return processing_fees;
	}

	public void setProcessing_fees(Double processing_fees) {
		this.processing_fees = processing_fees;
	}

	public Integer getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(Integer payment_method) {
		this.payment_method = payment_method;
	}

	public TripStatus getTrip_status() {
		return trip_status;
	}

	public void setTrip_status(TripStatus trip_status) {
		this.trip_status = trip_status;
	}

	public Double getEst_amount() {
		return est_amount;
	}

	public void setEst_amount(Double est_amount) {
		this.est_amount = est_amount;
	}

	public Double getBase_km() {
		return base_km;
	}

	public void setBase_km(Double base_km) {
		this.base_km = base_km;
	}

	public Double getPer_min_price() {
		return per_min_price;
	}

	public void setPer_min_price(Double per_min_price) {
		this.per_min_price = per_min_price;
	}

	public Double getTax_percent() {
		return tax_percent;
	}

	public void setTax_percent(Double tax_percent) {
		this.tax_percent = tax_percent;
	}

	public Double getTotal_km() {
		return total_km;
	}

	public void setTotal_km(Double total_km) {
		this.total_km = total_km;
	}

	public Double getPer_km_price() {
		return per_km_price;
	}

	public void setPer_km_price(Double per_km_price) {
		this.per_km_price = per_km_price;
	}

	public Double getTotal_min() {
		return total_min;
	}

	public void setTotal_min(Double total_min) {
		this.total_min = total_min;
	}

	

	public Double getCgst() {
		return cgst;
	}

	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}

	public Double getSgst() {
		return sgst;
	}

	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	
	
	
	
}
