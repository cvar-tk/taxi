package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "vehicle_master_asset")
public class VehicleMasterAssets implements Serializable{

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vma_id", nullable = false, unique = true)
	private Integer vma_id;
	
	@Column(name = "vma_rc_book", nullable = false)
	private String vma_rc_book;

	@Column(name = "vma_full_view", nullable = false)
	private String vma_full_view;
	
	@Column(name = "vma_insurance_img", nullable = false)
	private String vma_insurance_img;

	@Column(name = "vma_number_view", nullable = false)
	private String vma_number_view;

	@Temporal(TemporalType.DATE)
	@Column(name = "vma_insurance_exp_date", nullable = false)
	private Date vma_insurance_exp_date;
	
	
	@Column(name = "reason", nullable = true)
	private String reason;
	
	@Column(name = "comments", nullable = true)
	private String comments;
	

	public Integer getVma_id() {
		return vma_id;
	}

	public void setVma_id(Integer vma_id) {
		this.vma_id = vma_id;
	}

	public String getVma_rc_book() {
		return vma_rc_book;
	}

	public void setVma_rc_book(String vma_rc_book) {
		this.vma_rc_book = vma_rc_book;
	}

	public String getVma_full_view() {
		return vma_full_view;
	}

	public void setVma_full_view(String vma_full_view) {
		this.vma_full_view = vma_full_view;
	}

	public String getVma_number_view() {
		return vma_number_view;
	}

	public void setVma_number_view(String vma_number_view) {
		this.vma_number_view = vma_number_view;
	}

	public Date getVma_insurance_exp_date() {
		return vma_insurance_exp_date;
	}

	public void setVma_insurance_exp_date(Date vma_insurance_exp_date) {
		this.vma_insurance_exp_date = vma_insurance_exp_date;
	}

	public String getVma_insurance_img() {
		return vma_insurance_img;
	}

	public void setVma_insurance_img(String vma_insurance_img) {
		this.vma_insurance_img = vma_insurance_img;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	
	
}
