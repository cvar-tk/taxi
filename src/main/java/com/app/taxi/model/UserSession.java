package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_session")
public class UserSession implements Serializable{
		
		private static final long serialVersionUID = -6470090944414208496L;

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "u_id", nullable = false, unique = true)
		private Integer u_id;
		
		@Column(name = "session_key", nullable = false)
		private String session_key;
		
		@Column(name = "u_created_time", nullable = false)
		private Date u_created_date;
		
		@ManyToOne(fetch = FetchType.LAZY)
		@JoinColumn(name = "user_master", nullable = false)
		private UserMaster user_master;

	

		public String getSession_key() {
			return session_key;
		}

		public void setSession_key(String session_key) {
			this.session_key = session_key;
		}

		public Date getU_created_date() {
			return u_created_date;
		}

		public void setU_created_date(Date u_created_date) {
			this.u_created_date = u_created_date;
		}

		public UserMaster getUser_master() {
			return user_master;
		}

		public void setUser_master(UserMaster user_master) {
			this.user_master = user_master;
		}
		
		

}
