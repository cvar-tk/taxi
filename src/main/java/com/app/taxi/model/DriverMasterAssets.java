package com.app.taxi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "driver_master_asset")
public class DriverMasterAssets implements Serializable{

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dma_id", nullable = false, unique = true)
	private Integer dma_id;
	
	@Column(name = "dma_licence_number", nullable = false)
	private String dma_licence_number;

	@Column(name = "dma_licence", nullable = false)
	private String dma_licence;

	@Column(name = "dma_address_proof", nullable = false)
	private String dma_address_proof;

	@Temporal(TemporalType.DATE)
	@Column(name = "dma_dob", nullable = false)
	private Date dma_dob;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "licence_exp", nullable = false)
	private Date licence_exp;
	
	@Column(name = "gov_proof_number", nullable = false)
	private String gov_proof_number;

	@Column(name = "reason", nullable = true)
	private String reason;

	@Column(name = "comments", nullable = true)
	private String comments;


	public Integer getDma_id() {
		return dma_id;
	}

	public void setDma_id(Integer dma_id) {
		this.dma_id = dma_id;
	}

	public String getDma_licence_number() {
		return dma_licence_number;
	}

	public void setDma_licence_number(String dma_licence_number) {
		this.dma_licence_number = dma_licence_number;
	}

	public String getDma_licence() {
		return dma_licence;
	}

	public void setDma_licence(String dma_licence) {
		this.dma_licence = dma_licence;
	}

	public String getDma_address_proof() {
		return dma_address_proof;
	}

	public void setDma_address_proof(String dma_address_proof) {
		this.dma_address_proof = dma_address_proof;
	}

	public Date getDma_dob() {
		return dma_dob;
	}

	public void setDma_dob(Date dma_dob) {
		this.dma_dob = dma_dob;
	}

	public Date getLicence_exp() {
		return licence_exp;
	}

	public void setLicence_exp(Date licence_exp) {
		this.licence_exp = licence_exp;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getGov_proof_number() {
		return gov_proof_number;
	}

	public void setGov_proof_number(String gov_proof_number) {
		this.gov_proof_number = gov_proof_number;
	}
	
	
	
}
