package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "trip_status")
public class TripStatus implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ts_id", nullable = false, unique = true)
	private Integer ts_id;

	@Column(name = "ts_title", nullable = false, unique = true)
	private String ts_title;

	public Integer getTs_id() {
		return ts_id;
	}

	public void setTs_id(Integer ts_id) {
		this.ts_id = ts_id;
	}

	public String getTs_title() {
		return ts_title;
	}

	public void setTs_title(String ts_title) {
		this.ts_title = ts_title;
	}
	
	
	
	
	
}
