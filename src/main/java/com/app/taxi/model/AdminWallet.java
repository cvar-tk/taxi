package com.app.taxi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "admin_wallet")
public class AdminWallet implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "aw_id", nullable = false, unique = true)
	private int aw_id;
	
	@Column(name = "a_opposition", nullable = false)
	private Double a_opposition;
	
	@Column(name = "a_opening", nullable = false)
	private Double a_opening;
	
	@Column(name = "a_closing", nullable = false)
	private Double a_closing;
	
	@Column(name = "type", nullable = false)
	private Integer type;
	
	@Column(name = "create_date", nullable = false)
	private Date create_date;
	
	@Column(name = "reason", nullable = true)
	private Integer reason;
	
	@Column(name = "referalamount", nullable = true)
	private Integer referalamount;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user", nullable = true)
	private UserMaster user;
	
	@Column(name = "driver", nullable = true)
	private Integer driver;
	
	
	@Column(name = "owner", nullable = true)
	private Integer owner;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tripMaster", nullable = true)
	private TripMaster tripMaster;

	public int getAw_id() {
		return aw_id;
	}

	public void setAw_id(int aw_id) {
		this.aw_id = aw_id;
	}

	public Double getA_opposition() {
		return a_opposition;
	}

	public void setA_opposition(Double a_opposition) {
		this.a_opposition = a_opposition;
	}

	public Double getA_opening() {
		return a_opening;
	}

	public void setA_opening(Double a_opening) {
		this.a_opening = a_opening;
	}

	public Double getA_closing() {
		return a_closing;
	}

	public void setA_closing(Double a_closing) {
		this.a_closing = a_closing;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Integer getReason() {
		return reason;
	}

	public void setReason(Integer reason) {
		this.reason = reason;
	}

	public Integer getReferalamount() {
		return referalamount;
	}

	public void setReferalamount(Integer referalamount) {
		this.referalamount = referalamount;
	}

	public UserMaster getUser() {
		return user;
	}

	public void setUser(UserMaster user) {
		this.user = user;
	}

	public Integer getDriver() {
		return driver;
	}

	public void setDriver(Integer driver) {
		this.driver = driver;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public TripMaster getTripMaster() {
		return tripMaster;
	}

	public void setTripMaster(TripMaster tripMaster) {
		this.tripMaster = tripMaster;
	}
	
	
	
}
