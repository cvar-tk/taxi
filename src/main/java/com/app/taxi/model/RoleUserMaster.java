package com.app.taxi.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name = "role_users_master")
public class RoleUserMaster implements Serializable{
	
	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ur_id", nullable = false, unique = true)
	private Integer ur_id;

	@Column(name = "m_phone", nullable = true)
	private BigInteger m_phone;

	@Column(name = "u_name", nullable = true)
	private String u_name;

	@Column(name = "u_email", nullable = true , unique = true)
	private String u_email;
	
	@Column(name = "u_security_pin", nullable = true)
	private Integer u_security_pin;

	@Column(name = "u_status", nullable = false)
	private Integer u_status;

	@Column(name = "u_ref_code", nullable = true, length = 6)
	private String u_ref_code;

	@Column(name = "password", nullable = false)
	private String password;

	@Temporal(TemporalType.DATE)
	@Column(name = "password_update", nullable = false)
	private Date password_update;
	
	@Column(name = "otp", nullable = true)
	private String otp;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "otp_expiry_time", nullable = true)
	private Date otp_expiry_time;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "u_role", nullable = false)
	private MultiRoleMaster u_role;

	@Temporal(TemporalType.DATE)
	@Column(name = "u_created_date", nullable = false)
	private Date u_created_date;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "created_by", nullable = false)
	private UserMaster created_by;

	public Integer getUr_id() {
		return ur_id;
	}

	public void setUr_id(Integer ur_id) {
		this.ur_id = ur_id;
	}

	public BigInteger getM_phone() {
		return m_phone;
	}

	public void setM_phone(BigInteger m_phone) {
		this.m_phone = m_phone;
	}

	public String getU_name() {
		return u_name;
	}

	public void setU_name(String u_name) {
		this.u_name = u_name;
	}

	public String getU_email() {
		return u_email;
	}

	public void setU_email(String u_email) {
		this.u_email = u_email;
	}

	public Integer getU_security_pin() {
		return u_security_pin;
	}

	public void setU_security_pin(Integer u_security_pin) {
		this.u_security_pin = u_security_pin;
	}

	public Integer getU_status() {
		return u_status;
	}

	public void setU_status(Integer u_status) {
		this.u_status = u_status;
	}

	public String getU_ref_code() {
		return u_ref_code;
	}

	public void setU_ref_code(String u_ref_code) {
		this.u_ref_code = u_ref_code;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getPassword_update() {
		return password_update;
	}

	public void setPassword_update(Date password_update) {
		this.password_update = password_update;
	}

	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public Date getOtp_expiry_time() {
		return otp_expiry_time;
	}

	public void setOtp_expiry_time(Date otp_expiry_time) {
		this.otp_expiry_time = otp_expiry_time;
	}

	public MultiRoleMaster getU_role() {
		return u_role;
	}

	public void setU_role(MultiRoleMaster u_role) {
		this.u_role = u_role;
	}

	public Date getU_created_date() {
		return u_created_date;
	}

	public void setU_created_date(Date u_created_date) {
		this.u_created_date = u_created_date;
	}

	public UserMaster getCreated_by() {
		return created_by;
	}

	public void setCreated_by(UserMaster created_by) {
		this.created_by = created_by;
	}
	
	
	
	
	
	
}
