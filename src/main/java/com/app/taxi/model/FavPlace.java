package com.app.taxi.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fav_place")
public class FavPlace implements Serializable {

	private static final long serialVersionUID = -6470090944414208496L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "fav_id", nullable = false, unique = true)
	private Integer fav_id;

	@Column(name = "fav_lat", nullable = false)
	private Double fav_lat;

	@Column(name = "fav_lang", nullable = false)
	private Double fav_lang;

	@Column(name = "title", nullable = false, length = 500)
	private String title;
	
	@Column(name = "fav_status", nullable = false, length = 500)
	private Integer fav_status;

	@Column(name = "address", nullable = false, length = 500)
	private String address;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = true)
	private UserMaster userMaster;

	public Integer getFav_id() {
		return fav_id;
	}

	public void setFav_id(Integer fav_id) {
		this.fav_id = fav_id;
	}

	public Double getFav_lat() {
		return fav_lat;
	}

	public void setFav_lat(Double fav_lat) {
		this.fav_lat = fav_lat;
	}

	public Double getFav_lang() {
		return fav_lang;
	}

	public void setFav_lang(Double fav_lang) {
		this.fav_lang = fav_lang;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public UserMaster getUserMaster() {
		return userMaster;
	}

	public Integer getFav_status() {
		return fav_status;
	}

	public void setFav_status(Integer fav_status) {
		this.fav_status = fav_status;
	}

	public void setUserMaster(UserMaster userMaster) {
		this.userMaster = userMaster;
	}
	
	

}
