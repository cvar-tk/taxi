package com.app.taxi;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;

public class SingleTon {

	private static SingleTon singleTon;

	/**
	 * Create private constructor
	 */
	private SingleTon() {

	}

	public static final SingleTon getInstance() {

		if (singleTon == null) {
			singleTon = new SingleTon();
		}

		return singleTon;
	}

	//////////////////////////
	public static final String CONTENT_STATUS = "status";
	public static final String CONTENT_SELECT_STATUS = "select_status";
	public static final String CONTENT_MESSAGE = "message";
	public static final String CONTENT_ERROR_MESSAGE = "message1";
	public static final String CONTENT_PICKED_STATUS = "picked_status";


	//////////////////////////

	public static final String IMAGE_PATH = "http://137.74.157.254:8080/resource/";

	
	
	//////////////////////
	public static final String USER_ROLE = "user_role";
	public static final String USER_NAME = "user_name";
	public static final String EMAIL = "email";
	public static final String MOBILE = "mobile_no";
	public static final String PASSWORD = "password";
	public static final String ROLE = "role";
	public static final String USER_ID = "user_id";
	public static final String SESSION_USER_ID = "session_user_id";
	public static final String SESSION_ROLE = "session_role";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String REFERARL_CODE = "referal_code";
	public static final String OTP = "otp";
	public static final String USER_PIC = "picture";
	public static final String USER_STATUS = "user_status";
	public static final String CONTENT_LIST = "content_list";
	public static final String VEHICLE_LIST = "vehicle_list";
	public static final String PLACE_TYPE = "place_type";
	public static final String LATITIUDE = "latitude";
	public static final String LONGTITUDE = "longtitude";
	public static final String ADDRESS = "address";
	public static final String ID = "id";
	public static final String TITLE = "title";
	public static final String PRICE = "price";
	public static final String COMMENTS = "comments";
	public static final String DURATION = "duration";
	public static final String PROFILE_PICTURE = "picture";
	public static final String LINKED_OWNER = "owner_name";
	public static final String VEHICLE_NUMBER = "car_number";
	public static final String VEHICLE_COLOUR = "vehicle_colour";
	public static final String CAR_MODEL_AND_BRAND = "car_model";
	public static final String CAR_ID = "car_id";

	public static final String VECHICLE_DETAILS = "vechicle_no";
	public static final String LICENSE_DETAILS = "license_no";
	public static final String DRIVER_ACTIVITY_CODE = "driver_activity_code";
	public static final String DRIVER_ACTIVITY_MESSAGE = "driver_activity_message";
	public static final String DRIVER_ONDEMAND_CODE = "driver_ondemand_code";
	public static final String DRIVER_ONDEMAND_STATUS = "driver_ondemand_status";

	public static final String VECHICLE_PLAN_CODE = "vehicle_plan_code";
	public static final String VECHICLE_PLAN_STATUS = "vehicle_plan_status";
	
	public static final String ONLINE = "online";
	public static final String VEHICLE_TYPE = "vehicle_type";
	public static final String VEHICLE_ID = "vehicle_id";
	public static final String BASE_KM = "base_km";
	public static final String BASE_FAIR = "base_fair";
	public static final String BASE_KM_PRICE = "base_km_price";
	public static final String BASE_MIN_PRICE = "per_min_price";
	public static final String BACKUP_CODE = "backup_code";
	public static final String STARTING_POINT = "starting_point";
	public static final String ENDING_POINT = "end_point";
	public static final String STARTING_LAT = "start_lat";
	public static final String STARTING_LANG = "start_lang";
	public static final String ENDING_LAT = "ending_lat";
	public static final String ENDING_LANG = "ending_lang";
	public static final String ESTMATED_AMOUNT = "estimated_amount";
	public static final String TAX_PERCENT = "tax_percent";
	public static final String TOTAL_KM = "total_km";
	public static final String TRIP_ID = "trip_id";
	public static final String TRIP_DETAILS = "trip_deatils";
   	public static final String GST = "gst";   
   	public static final String CGST = "cgst";    
   	public static final String SGST = "sgst";    

	public static final String BITMAP_IMAGE = "bitmap_image";

   	public static final String CURRENT_TIME = "current_time";
	public static final String MULTIROLE = "multi_role";
	public static final String TOTAL_MIN = "total_min";

   	public static final String AMOUNT = "amount";
   	public static final String TOLL_AMOUNT = "toll_amount";
   	public static final String PAYMENT_TYPE = "payment_type";
   	public static final String START_TIME = "start_time";
   	public static final String END_TIME = "end_time";
   	
   	public static final String FROM_ADDRESS = "address";
	public static final String TRIP_STATUS = "trip_status";
	public static final String TRIP_CODE = "trip_code";

   	
	/////////////////////

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////// URL PATH STARTS /////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	public static final String URL_FILE_PATH = "res-target.html/{file_name}";
	public static final String URL_APP_FCM_TOKEN = "update_fcm_token.html";
	public static final String URL_APP_UPDATE_USERNAME = "update-user-name.html";

	public static final String URL_ADMIN_LOGIN = "admin-login.html";
	public static final String URL_ADMIN_DASHBOARD = "admin-dashboard.html";
	public static final String URL_FORGOT_PASSWORD = "admin-forgot-password.html";
	public static final String URL_VM_NEW_REQUEST = "vm-new-requests.html";
	public static final String URL_VM_REJECT_REQUEST = "vm-reject-request.html";
	public static final String URL_VM_VIEW_VEHICLE_REQUEST = "vm-view-vehicle.html";
	public static final String URL_VM_VIEW_VEHICLE = "vm-view-approved-vehicle.html";
	
	public static final String URL_ADMIN_CHANGE_PASSWORD = "admin-change-password.html";
	public static final String URL_ADMIN_CHANGE_PIN = "admin-change-pin.html";


	public static final String URL_EMP_LOGIN = "emp-login.html";
	public static final String URL_APP_GET_SERVER_TIME = "api-get-server-time.html";
	
	public static final String URL_VM_APPROVED_VEHICLES = "vm-approved-vehicles.html";
	public static final String URL_VM_APPROVE_VEHICLE = "vm-approve-vehicle.html";
	public static final String URL_VM_REPORTS = "vm-reports.html";
	public static final String URL_PM_VEHICLE_PLANS = "pm-vehicle-plans.html";
	public static final String URL_PM_ON_DEMAND_PLANS = "pm-on-demand-plans.html";
	public static final String URL_PM_API_CREATE_DEMAND_PLAN = "api-create-on-demand-plan.html";
	public static final String URL_PM_API_CREATE_VEHICLE_PLAN = "api-create-vehicle-plan.html";
	public static final String URL_PM_API_CHANGE_DEMAND_PLAN_STATUS = "api-change-demand-plan-status.html";
	public static final String URL_API_EDIT_VEHICLE_PLANS = "api-edit-plan-master.html";
	public static final String URL_PM_API_EDIT_VEHICLE_PLAN = "api-edit-vehicle-plan.html";
	public static final String URL_PM_API_EDIT_ONDEMAND_PLAN = "api-edit-ondemand-plan.html";

	public static final String URL_PM_API_CHANGE_VEHICLE_PLAN_STATUS = "api-change-vehicle-plan-status.html";
	public static final String URL_ADMIN_ROLE_MANAGEMENT = "admin-role-management.html";
	public static final String URL_ADMIN_CREATE_NEW_ROLE = "admin-create-new-role.html";

	public static final String URL_ADMIN_USER_MANAGEMENT = "admin-user-management.html";
	public static final String URL_ADMIN_NEW_DRIVER_REQUEST = "admin-new-driver-request.html";
	public static final String URL_ADMIN_NEW_DRIVER_REQUEST_VIEW = "admin-view-driver-request.html";
	public static final String URL_ADMIN_APPROVED_DRIVER_REQUEST = "admin-approved-driver-request.html";
	public static final String URL_ADMIN_APPROVED_DRIVER_REQUEST_VIEW = "admin-view-approved-driver-request.html";
	public static final String URL_APP_END_TRIP = "api-stop-trip.html";

	public static final String URL_ADMIN_TRIP_CONTROL_MANAGEMENT = "admin-trip-control.html";
	public static final String URL_ADMIN_UPDATE_BASE_CONTROL = "admin-update-base-control.html";
	public static final String URL_ADMIN_UPDATE_BASE_KM = "admin-update-base-km.html";
	public static final String URL_ADMIN_UPDATE_PER_MIN_PRICE = "admin-update-per-min-price.html";
	public static final String URL_ADMIN_UPDATE_GST = "admin-update-gst.html";
	public static final String URL_ADMIN_UPDATE_CGST = "admin-update-cgst.html";
	public static final String URL_ADMIN_UPDATE_SGST = "admin-update-sgst.html";

	//tripmaster
	public static final String URL_ADMIN_TRIP_MASTER_LIST = "admin-trip_master_list.html";
	public static final String URL_ADMIN_API_TRIP_MASTER_LIST = "admin-api-trip-master-list.html";
	public static final String URL_ADMIN_VIEW_TRIP_MASTER_LIST = "admin-view-trip-list.html";
	public static final String URL_ADMIN_TERMINATE_TRIP = "admin-terminate-trip.html";

	
	public static final String URL_ADMIN_API_NEW_VEHICLE_REQUEST_LIST = "admin-api-new-vehicle-request-list.html";
	public static final String URL_ADMIN_API_APPROVED_VEHICLE_LIST = "admin-api-approved-vehicle-request-list.html";
	public static final String URL_ADMIN_API_NEW_DRIVER_LIST = "admin-api-new-driver-request-list.html";
	public static final String URL_ADMIN_API_APPROVED_DRIVER_LIST = "admin-api-approved-driver-request-list.html";
	public static final String URL_ADMIN_API_CUSTOMER_LIST = "admin-api-new-customers-list.html";
	public static final String URL_ADMIN_API_MULTIROLE_USER_LIST = "admin-api-multirole-details.html";
	public static final String URL_ADMIN_EDIT_MULTIROLE_USER_LIST = "admin-view-multirole-user.html";
	public static final String URL_ADMIN_UPDATE_MULTIROLE_USER_LIST = "admin-update-role-view.html";
	public static final String URL_ADMIN_DEACTIVE_MULTIROLE_USER_LIST = "admin-deactive-role-user.html";

	
	
	
	public static final String URL_APP_LOGIN = "api-login.html";
	public static final String URL_OWNER_APP_OTP = "api-owner-otp.html";
	public static final String URL_DRIVER_APP_OTP = "api-driver-otp.html";
	public static final String URL_APP_OTP_CHECK_ACCOUNT = "api-check-account.html";
	public static final String URL_APP_UPDATE_PASSWORD = "api-update-password.html";
	public static final String URL_APP_DRIVER_ON_OFF_STATUS = "api-get-driver-on-off-status.html";
	public static final String URL_APP_FORGET_PASSSWORD_OTP_VERIFY = "api-forget-password-otp-verify.html";
	public static final String URL_APP_CHECK_CURRENT_PASSWORD = "api-check_current_password.html";

	public static final String URL_APP_OWNER_ADD_VEHICLE = "add-new-vehicle.html";
	public static final String URL_APP_OWNER_LIST_VEHICLE = "list-all-vehicle.html";
	public static final String URL_APP_OWNER_LIST_DRIVER = "list-all-driver.html";
	public static final String URL_APP_OWNER_ADD_NEW_OWNER = "add-new-owner.html";
	public static final String URL_APP_SUBSCRIBE_RUNNING_PLAN = "subscribe-running-plan.html";
	public static final String URL_INTRODUCE_DRIVER_APP = "add-new-driver-for-approval.html";
	public static final String URL_APP_GET_OWNER_DETAILS = "api-get-owner-details.html";
	
	public static final String URL_APP_USER_PROFILE_DETAILS = "api-get-user-profile-details.html";
	public static final String URL_APP_USER_FEEDBACK_CONTROLLER = "add-new-user-feedback.html";

	public static final String URL_APP_USER_ADD_PLACE = "add-fav-place.html";
	public static final String URL_APP_USER_OTP_LOGIN = "api-user-otp-login-account.html";
	public static final String URL_APP_USER_CHECK_USER_NAME = "api-user-check-user-name.html";
	public static final String URL_APP_USER_UPDATE_USERNAME_EMAIL = "api-user-update-username-email.html";
	public static final String URL_APP_USER_UPDATE_USERCALLBACK = "api-get-user-callback.html";
	public static final String URL_USER_GET_BACKUP_CODE = "api-user-get-backup-code.html";
	public static final String URL_USER_GET_NEW_BACKUP_CODE = "api-user-get-new-backup-code.html";
	public static final String URL_APP_UPDATE_USER_LOCATION = "api-update-user-location.html";
	public static final String URL_APP_UPDATE_VEHICLE_LOCATION = "api-update-vehicle-location.html";
	public static final String URL_APP_UPDATE_USER_EMAIL = "api-update-user-email.html";
	public static final String URL_APP_USER_GET_NEAR_VEHICLE_lIST = "api-get-nearest-vehicle.html";
	public static final String URL_APP_RUNNING_PLAN_LIST = "api-get-running-plan-list.html";
	public static final String URL_APP_DEMAND_PLAN_LIST = "api-get-demand-plan-list.html";
	public static final String URL_APP_GET_DRIVER_DETAILS = "api-get-driver-details.html";
	public static final String URL_APP_SELECT_CAR_BY_DRIVER = "api-select-car-by-driver.html";
	public static final String URL_APP_DESELECT_CAR_BY_DRIVER = "api-deselect-car-by-driver.html";

	public static final String URL_APP_DRIVER_ONDEMAND_PLAN = "api-driver-ondemand-plan-details.html";
	public static final String URL_APP_DRIVER_MANY_TRIP_LOCATION = "api-driver-many-trip-location.html";

	public static final String URL_APP_CAR_SUBCRIBE_PLAN_DETAILS = "api-car-subcribe-plan-details.html";
	public static final String URL_APP_CAR_AUTORENEWAL_PLAN = "api-car-autorenewal-plan.html";
	
	public static final String URL_APP_AUTO_RENEWAL_METHOD = "api-autorenewal-method.html";

	
	public static final String URL_APP_MAKE_DRIVER_ONLINE = "api-make-driver-online.html";
	public static final String URL_APP_MAKE_DRIVER_OFFLINE = "api-make-driver-offline.html";
	public static final String URL_APP_DRIVER_ACTIVITY_STATUS = "api-driver-activity-status.html";
	public static final String URL_APP_GET_VEHICLE_DETAILS_BY_VEHICLE_ID = "api-get-vehicle-details-by-vehicle-id.html";
	public static final String URL_APP_GET_USER_DEATILS_BY_DRIVER = "api-get-user-details-by-driver.html";
	public static final String URL_APP_UPDATE_BASE_KM_DETAILS = "api-update-base-km-details.html";
	public static final String URL_APP_START_TRIP = "api-start-trip.html";
	public static final String URL_APP_SELF_DISABLE_DRIVER = "api-self-disable-driver.html";
	public static final String URL_APP_DRIVER_TRIP_HISTORY = "api-driver-trip-history.html";
	public static final String URL_APP_USER_TRIP_HISTORY = "api-user-trip-history.html";
	public static final String URL_APP_USER_TRIP_DETAILS = "api-user-trip-details.html";

	public static final String URL_USER_APP_OTP = "api-user-send-otp.html";
	public static final String URL_APP_GET_FAV_PLACE = "list-fav-place.html";
	public static final String URL_APP_DELETE_PLACE = "delete-fav-place.html";

	public static final String ADD_PROFILE_PICTURE = "add-profile-picture.html";
	public static final String GET_USER_DETAILS = "get-user-details.html";
	public static final String URL_GET_MODELS_BY_MAKE_ID = "get-models-by-make-id.html";
	public static final String URL_ADMIN_REQUEST_CHANAGE_PASSWORD = "request-for-password-change.html";
	public static final String URL_APP_VIEW_CAR_DETAILS = "api-get-car-details.html";
	public static final String URL_APP_VIEW_DRIVER_DETAILS = "api-owner-get-driver-detail.html";
	public static final String URL_APP_ADD_EXCISTING_DRIVER = "api-add-excisting-driver-to-owner.html";
	public static final String URL_APP_CAR_TRIP_HISTORY = "api-car-trip-history.html";
	public static final String URL_APP_VIEW_TRIP_HISTORY = "api-view-trip-history.html";

	public static final String URL_VM_APPROVE_VEHICLE_NO = "vm-approve-vehicle-no.html";
	public static final String URL_VM_APPROVE_VEHICLE_FILTER = "vm-filter-approve-vehicle.html";
	public static final String URL_VM_UPDATE_INSURANCE_DATE = "update-insurance-expiry-date.html";
	
	public static final String URL_ADMIN_INVOICE_PDF = "pdf-create.html";

	
	public static final String URL_ADMIN_API_APPROVED_VEHICLE_FILTER_LIST = "admin-api-approved-vehicle-filter-list.html";
	public static final String URL_ADMIN_APPROVE_DRIVER = "admin-approve-driver.html";
	public static final String URL_ADMIN_REJECT_DRIVER = "admin-reject-driver-request.html";
	public static final String URL_ADMIN_APPROVE_LICENSE_NO = "admin-approve-license-no.html";
	public static final String URL_ADMIN_APPROVE_DRIVER_FILTER = "admin-filter-approve-driver.html";
	public static final String URL_ADMIN_API_APPROVED_DRIVER_FILTER_LIST = "admin-api-approved-driver-filter-list.html";
	public static final String URL_VM_DELETE_VEHICLE = "vm-delete-request.html";
	public static final String URL_ADMIN_DELETE_DRIVER = "admin-delete-driver-request.html";
	public static final String URL_ADMIN_UPDATE_EXPIRY_DATE = "admin-update-expiry-date.html";
	public static final String URL_ADMIN_DISABLE_DRIVER = "admin-disable-driver-request.html";
	public static final String URL_APP_GET_NEAR_VEHICLE_DEATILS = "api-get-nearest-vehicle-details.html";
	// CMS ADMIN PANEL URL STARTS
	public static final String URL_ADMIN_GET_PRIVACY_POLICY = "admin-privacy-policy.html";
	public static final String URL_ADMIN_UPDATE_USER_PRIVACY_POLICY = "admin-user-privacy-policy.html";
	public static final String URL_ADMIN_UPDATE_DRIVER_PRIVACY_POLICY = "admin-update-driver-privacy-policy.html";
	public static final String URL_ADMIN_UPDATE_OWNER_PRIVACY_POLICY = "admin-owner-privacy-policy.html";

	public static final String URL_ADMIN_GET_TERMS_CONDITION = "admin-terms-and-condition.html";
	public static final String URL_ADMIN_UPDATE_USER_TERMS_CONDITION = "admin-user-terms-condition.html";
	public static final String URL_ADMIN_UPDATE_DRIVER_TERMS_CONDITION = "admin-driver-terms-condition.html";
	public static final String URL_ADMIN_UPDATE_OWNER_TERMS_CONDITION = "admin-owner-terms-condition.html";

	// CMS ADMIN PANEL URL END
	
	
	//owner management
	public static final String URL_ADMIN_OWNER_MANAGEMENT = "admin-owner-management.html";
	public static final String URL_ADMIN_API_OWNER_MANAGEMENT_LIST = "admin-api-owner-list-list.html";

	

	// CMS APP URL STARTS
	public static final String URL_APP_USER_GET_PRIVACY_POLICY = "api-get-user-privacy-policy.html";
	public static final String URL_APP_USER_GET_TERMS_AND_CONDITION = "api-get-user-terms-and-condition.html";
	public static final String URL_APP_OWNER_GET_PRIVACY_POLICY = "api-get-owner-privacy-policy.html";
	public static final String URL_APP_OWNER_GET_TERMS_AND_CONDITION = "api-get-owner-terms-and-condition.html";
	public static final String URL_APP_DRIVER_GET_PRIVACY_POLICY = "api-get-driver-privacy-policy.html";
	public static final String URL_APP_DRIVER_GET_TERMS_AND_CONDITION = "api-get-driver-terms-and-condition.html";
	// CMS APP URL ENDS

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////// URL PATH END ////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////// TEMPLATE PATH STARTS ////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	public static final String TEMPLATE_ADMIN_LOGIN = "admin/login";
	public static final String TEMPLATE_EMP_LOGIN = "admin/emp_login";

	public static final String TEMPLATE_ADMIN_DASHBOARD = "admin/dashboard";
	public static final String RESPONSE_CODE = "response_code";
	public static final String TEMPLATE_ADMIN_NEW_REQUEST = "admin/newvehiclerequest";
	public static final String TEMPLATE_ADMIN_VIEW_REQUEST = "admin/viewrequest";
	public static final String TEMPLATE_ADMIN_VIEW_VEHICLE = "admin/viewvehicledetails";
	public static final String TEMPLATE_ADMIN_APPROVED_VEHICLE = "admin/approvedvehicle";
	public static final String TEMPLATE_FORGOT_PASSWARD_INFO = "admin/infoforgotpassword";
	public static final String TEMPLATE_ADMIN_VEHICLE_PLANS = "admin/vehicleplans";
	public static final String TEMPLATE_ADMIN_ON_DEMAND_PLANS = "admin/ondemandplans";
	public static final String TEMPLATE_ADMIN_NEW_DRIVER_REQUEST = "admin/newdriverrequest";
	public static final String TEMPLATE_ADMIN_NEW_DRIVER_REQUEST_VIEW = "admin/viewdriverdetails";
	public static final String TEMPLATE_ADMIN_APPROVED_DRIVER_REQUEST = "admin/approved_driverlist";
	public static final String TEMPLATE_ADMIN_APPROVED_DRIVER_REQUEST_VIEW = "admin/approved_driverview";
	
	
	public static final String TEMPLATE_ADMIN_CHANGE_PASSWORD = "admin/change_password";
	public static final String TEMPLATE_ADMIN_CHANGE_PIN = "admin/change_pin";

	

	public static final String TEMPLATE_ADMIN_FIND_APVECHICLENO = "admin/find_apvehicleno";
	public static final String TEMPLATE_ADMIN_FILTER_APVECHICLE = "admin/filter_approvedvehicle";
	public static final String TEMPLATE_ADMIN_FIND_APLICENSENO = "admin/driver_aplicenseno";

	public static final String TEMPLATE_ADMIN_FILTER_APPROVED_DRIVER = "admin/filter_approveddriver";
	public static final String TEMPLATE_ADMIN_CUSTOMERS_LIST = "admin/manage_user";
	public static final String TEMPLATE_ADMIN_PRIVACY_POLICY = "admin/privacy_policy";
	public static final String TEMPLATE_ADMIN_TERMS_CONDITION = "admin/terms_condition";
	
	public static final String TEMPLATE_ADMIN_TRIP_CONTROL = "admin/trip_control";
	public static final String TEMPLATE_ADMIN_ROLES_MANAGEMENT = "admin/role_manager";
	public static final String TEMPLATE_ADMIN_CREATE_ROLE = "admin/create_role";
	public static final String TEMPLATE_ADMIN_VIEW_ROLES = "admin/view_emprole";

	//tripmaster
	public static final String TEMPLATE_ADMIN_TRIP_LIST = "admin/trip_list";
	public static final String TEMPLATE_ADMIN_VIEW_TRIP_LIST = "admin/view_triplist";

	
	//owner management
	public static final String TEMPLATE_ADMIN_OWNER_MANAGE = "admin/owner_manage";

	
	
	// public static final String API_TECHNICAL_ERROR_MSG = '{"status":false}';

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////// TEMPLATE PATH END ///////////////////////
	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	// Email Sending
	public static Boolean sendMail(String toEmail, String headerText, String Contenttext) {

		// Sender's email ID
		final String fromEmail = "asharaf@technokryon.com";

		// Password
		final String passWord = "Techno2020"; // correct password for gmail id

		Properties props = new Properties();

		props.setProperty("mail.transport.protocol", "smtp");

		props.setProperty("mail.host", "smtp.zoho.com");

		props.put("mail.smtp.auth", "true");

		props.put("mail.smtp.port", "465");

		props.put("mail.debug", "true");

		props.put("mail.smtp.socketFactory.port", "465");

		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

		props.put("mail.smtp.socketFactory.fallback", "false");

		javax.mail.Session session = javax.mail.Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(fromEmail, passWord);
			}
		});

		try {
			// session.setDebug(true);
			Transport transport = session.getTransport();

			InternetAddress addressFrom = new InternetAddress(fromEmail);

			MimeMessage message = new MimeMessage(session);

			message.setSender(addressFrom);

			message.setSubject(headerText);

			message.setContent(Contenttext, "text/plain");

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

			transport.connect();

			Transport.send(message);

			transport.close();
		} catch (AddressException e) {

			e.printStackTrace();

			return false;

		} catch (MessagingException e) {

			e.printStackTrace();

			return false;
		}

		return true;
	}

	public static JSONObject APP_TECH_ERROR(String error) {

		JSONObject jsonObject = new JSONObject();

		System.out.println("SingleTon.APP_TECH_ERROR()-" + error);

		try {
			jsonObject.put(SingleTon.CONTENT_STATUS, false);
			jsonObject.put(SingleTon.RESPONSE_CODE, 6004);
			jsonObject.put(SingleTon.CONTENT_MESSAGE, "Something went wrong..! Try after sometime.");
			if (error != null) {

				System.out.println("SingleTon.APP_TECH_ERROR()" + error);
			}
			return jsonObject;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static JSONObject APP_TECH_SUCESS() {

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put(SingleTon.CONTENT_STATUS, true);

			return jsonObject;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// EMAIL VALID CHECKING
	public static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public static String getFilenamebyfile(MultipartFile file) {

		String rootPath = System.getProperty("catalina.home");

		if (!file.isEmpty()) {

			// Create the file on server
			Calendar cal = Calendar.getInstance();

			String name = cal.get(Calendar.DATE) + "-" + cal.get(Calendar.MONTH) + "-" + cal.get(Calendar.YEAR) + "-"
					+ cal.getTimeInMillis() + "ROOTU" + "." + FilenameUtils.getExtension(file.getOriginalFilename());

			// String name = file.getOriginalFilename();

			try {

				byte[] bytes = file.getBytes();

				File dir = new File(rootPath + File.separator + "ROOTU");

				if (!dir.exists())

					dir.mkdirs();

				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);

				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));

				stream.write(bytes);

				stream.close();

				System.out.println("You successfully uploaded file=" + name + " on " + rootPath);

				return name;

			} catch (Exception e) {

				System.out.println("You failed to upload " + name + " => " + e.getMessage());

				return null;

			}

		}
		return null;
	}

	private static HttpURLConnection con;

	public static boolean sendphone(String tophone, String contentText) {

		String url = "http://api.msg91.com/api/sendhttp.php?mobiles=" + tophone
				+ "&authkey=224402AlzXeXbB7T5b4324fd&message=" + contentText + "&sender=ROOOTU";

		System.out.println(url);

		try {

			URL myurl = new URL(url);
			con = (HttpURLConnection) myurl.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", "text/plain");
			con.setRequestProperty("Content-Language", "en-US");

			con.setUseCaches(false);
			con.setDoOutput(true);

			StringBuilder content;
			System.out.println(con.getURL());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String line;
			content = new StringBuilder();

			while ((line = in.readLine()) != null) {
				content.append(line);
				content.append(System.lineSeparator());
			}

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
