-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 07, 2018 at 07:46 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `root_taxi`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE IF NOT EXISTS `activity` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`a_id`),
  KEY `FK32t902l214em2bw3i6k3cxspb` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `backup_code`
--

DROP TABLE IF EXISTS `backup_code`;
CREATE TABLE IF NOT EXISTS `backup_code` (
  `bc_id` int(11) NOT NULL AUTO_INCREMENT,
  `bk_1` int(11) NOT NULL,
  `bk_2` int(11) NOT NULL,
  `bk_3` int(11) NOT NULL,
  `bk_4` int(11) NOT NULL,
  `bk_5` int(11) NOT NULL,
  `bk_6` int(11) NOT NULL,
  `bk_7` int(11) NOT NULL,
  `bk_8` int(11) NOT NULL,
  `bk_9` int(11) NOT NULL,
  `s_bk_1` int(11) NOT NULL,
  `s_bk_10` int(11) NOT NULL,
  `s_bk_2` int(11) NOT NULL,
  `s_bk_3` int(11) NOT NULL,
  `s_bk_4` int(11) NOT NULL,
  `s_bk_5` int(11) NOT NULL,
  `s_bk_6` int(11) NOT NULL,
  `s_bk_7` int(11) NOT NULL,
  `s_bk_8` int(11) NOT NULL,
  `bk_10` int(11) NOT NULL,
  `s_bk_9` int(11) NOT NULL,
  PRIMARY KEY (`bc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backup_code`
--

INSERT INTO `backup_code` (`bc_id`, `bk_1`, `bk_2`, `bk_3`, `bk_4`, `bk_5`, `bk_6`, `bk_7`, `bk_8`, `bk_9`, `s_bk_1`, `s_bk_10`, `s_bk_2`, `s_bk_3`, `s_bk_4`, `s_bk_5`, `s_bk_6`, `s_bk_7`, `s_bk_8`, `bk_10`, `s_bk_9`) VALUES
(1, 8722, 9860, 3133, 6275, 4249, 545, 2601, 9274, 1111, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4983, 1),
(2, 8885, 818, 4751, 6045, 3989, 549, 1544, 8170, 8399, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9717, 1),
(3, 5420, 31, 7365, 2107, 1637, 4481, 7455, 4753, 1501, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9715, 1),
(4, 3917, 7530, 7703, 9859, 1298, 9369, 9563, 285, 4835, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1587, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

DROP TABLE IF EXISTS `bookmarks`;
CREATE TABLE IF NOT EXISTS `bookmarks` (
  `bm_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` varchar(255) DEFAULT NULL,
  `bookmark_id` int(11) NOT NULL,
  `image_code` varchar(255) NOT NULL,
  PRIMARY KEY (`bm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `driver_master_asset`
--

DROP TABLE IF EXISTS `driver_master_asset`;
CREATE TABLE IF NOT EXISTS `driver_master_asset` (
  `dma_id` int(11) NOT NULL AUTO_INCREMENT,
  `dma_address_proof` varchar(255) NOT NULL,
  `dma_dob` date NOT NULL,
  `dma_licence` varchar(255) NOT NULL,
  `dma_licence_number` varchar(255) NOT NULL,
  `licence_exp` date NOT NULL,
  PRIMARY KEY (`dma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fav_place`
--

DROP TABLE IF EXISTS `fav_place`;
CREATE TABLE IF NOT EXISTS `fav_place` (
  `fav_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(500) NOT NULL,
  `fav_lang` double NOT NULL,
  `fav_lat` double NOT NULL,
  `title` varchar(500) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fav_status` int(11) NOT NULL,
  PRIMARY KEY (`fav_id`),
  KEY `FKdjrpad4opes5updsx93pxt0cg` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fav_place`
--

INSERT INTO `fav_place` (`fav_id`, `address`, `fav_lang`, `fav_lat`, `title`, `user_id`, `fav_status`) VALUES
(2, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(3, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(4, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(5, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(6, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(7, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(8, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(9, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(10, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0),
(11, '11/50, North street', 80.24785, 12.993177, 'Home', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `plan_master`
--

DROP TABLE IF EXISTS `plan_master`;
CREATE TABLE IF NOT EXISTS `plan_master` (
  `pm_id` int(11) NOT NULL AUTO_INCREMENT,
  `pm_comments` varchar(255) NOT NULL,
  `pm_demandtime_duration` int(11) DEFAULT NULL,
  `pm_price` float NOT NULL,
  `pm_title` varchar(255) NOT NULL,
  `pm_topup_duration` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `pt_type` int(11) NOT NULL,
  PRIMARY KEY (`pm_id`),
  KEY `FKlrgcj9tkl0a5fw72n53htorge` (`pt_type`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan_master`
--

INSERT INTO `plan_master` (`pm_id`, `pm_comments`, `pm_demandtime_duration`, `pm_price`, `pm_title`, `pm_topup_duration`, `status`, `pt_type`) VALUES
(1, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2),
(17, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2),
(18, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2),
(19, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2),
(20, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2),
(21, 'sfhfh', 46, 12, 'xcbxf', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `plan_type`
--

DROP TABLE IF EXISTS `plan_type`;
CREATE TABLE IF NOT EXISTS `plan_type` (
  `pt_id` int(11) NOT NULL AUTO_INCREMENT,
  `pt_title` varchar(255) NOT NULL,
  PRIMARY KEY (`pt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plan_type`
--

INSERT INTO `plan_type` (`pt_id`, `pt_title`) VALUES
(1, 'Running Plan'),
(2, 'On-Demand');

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
CREATE TABLE IF NOT EXISTS `role_master` (
  `rm_id` int(11) NOT NULL AUTO_INCREMENT,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rm_id`),
  UNIQUE KEY `UK_jyh957up1fivh61bnjvn1uesf` (`roles`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_master`
--

INSERT INTO `role_master` (`rm_id`, `roles`) VALUES
(1, 'Administrator'),
(3, 'Driver'),
(2, 'Owner'),
(4, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `subscribed_list`
--

DROP TABLE IF EXISTS `subscribed_list`;
CREATE TABLE IF NOT EXISTS `subscribed_list` (
  `sl_id` int(11) NOT NULL AUTO_INCREMENT,
  `sl_duration` double NOT NULL,
  `sl_price` float NOT NULL,
  `sl_status` int(11) NOT NULL,
  `sl_type` int(11) NOT NULL,
  `sl_vehicle_master` int(11) NOT NULL,
  PRIMARY KEY (`sl_id`),
  KEY `FKmxekfkiqdg7pk5gcf5fs5vy0u` (`sl_type`),
  KEY `FK6sm3eg0nmr66lf5u3idegi5vy` (`sl_vehicle_master`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trip_master`
--

DROP TABLE IF EXISTS `trip_master`;
CREATE TABLE IF NOT EXISTS `trip_master` (
  `tr_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `base_fair` double NOT NULL,
  `bck_code` int(11) NOT NULL,
  `end_lang` double NOT NULL,
  `end_lat` double NOT NULL,
  `end_time` datetime(6) NOT NULL,
  `ending_point` varchar(255) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `processing_fees` double NOT NULL,
  `start_lang` double NOT NULL,
  `start_lat` double NOT NULL,
  `start_time` datetime(6) NOT NULL,
  `starting_point` varchar(255) NOT NULL,
  `toll` double NOT NULL,
  `customer` int(11) NOT NULL,
  `driver` int(11) NOT NULL,
  `trip_status` int(11) DEFAULT NULL,
  `vehicle` int(11) NOT NULL,
  PRIMARY KEY (`tr_id`),
  KEY `FKblgh41v76c2rid9asstgiiv6e` (`customer`),
  KEY `FK7b2ftyslval7f6vblde5u9ekn` (`driver`),
  KEY `FKditsyqvat2mvep7b776q0mj5x` (`trip_status`),
  KEY `FKa206o3hmkbvnk7diqknxef9jl` (`vehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trip_status`
--

DROP TABLE IF EXISTS `trip_status`;
CREATE TABLE IF NOT EXISTS `trip_status` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_title` varchar(255) NOT NULL,
  PRIMARY KEY (`ts_id`),
  UNIQUE KEY `UK_j2fsp284cef1nfqkf3pds704u` (`ts_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users_master`
--

DROP TABLE IF EXISTS `users_master`;
CREATE TABLE IF NOT EXISTS `users_master` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `m_phone` double DEFAULT NULL,
  `u_created_date` date NOT NULL,
  `u_email` varchar(255) DEFAULT NULL,
  `u_name` varchar(255) DEFAULT NULL,
  `u_pic` varchar(255) DEFAULT NULL,
  `u_ref_code` varchar(6) DEFAULT NULL,
  `u_security_pin` int(11) DEFAULT NULL,
  `u_status` int(11) NOT NULL,
  `bck_code` int(11) DEFAULT NULL,
  `u_role` int(11) NOT NULL,
  `w_credential` int(11) NOT NULL,
  `u_used_ref_code` varchar(6) DEFAULT NULL,
  `mapped_with` int(11) DEFAULT NULL,
  `fcm_token` text,
  `driver_assets` int(11) DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  KEY `FKfpnr2tao9mnrd1pafcdypvq2b` (`bck_code`),
  KEY `FK621bj0bg1r145f7jefgiduywk` (`u_role`),
  KEY `FKt09djqef7ytxre57xjdgswigj` (`w_credential`),
  KEY `FK9xme9997le51amewj3buo9bq6` (`driver_assets`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_master`
--

INSERT INTO `users_master` (`u_id`, `m_phone`, `u_created_date`, `u_email`, `u_name`, `u_pic`, `u_ref_code`, `u_security_pin`, `u_status`, `bck_code`, `u_role`, `w_credential`, `u_used_ref_code`, `mapped_with`, `fcm_token`, `driver_assets`) VALUES
(1, 9566753347, '2018-07-07', 'alwinraja.kryon@gmail.com', 'Allwin Raja', NULL, NULL, 9566, 1, NULL, 1, 2, 'AUXHUK', NULL, NULL, NULL),
(2, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 1, 3, 'AUXHUK', NULL, NULL, NULL),
(3, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 1, 4, 'AUXHUK', NULL, NULL, NULL),
(4, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 1, 5, 'AUXHUK', NULL, NULL, NULL),
(5, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 1, 6, 'AUXHUK', NULL, NULL, NULL),
(6, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 1, 7, 'AUXHUK', NULL, NULL, NULL),
(7, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 2, NULL, 1, 8, 'AUXHUK', NULL, NULL, NULL),
(8, 956675367, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 2, 9, 'AUXHUK', NULL, NULL, NULL),
(9, 9564675367, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 2, 10, 'AUXHUK', NULL, NULL, NULL),
(10, 9566753347, '2018-07-07', NULL, 'Allwin Raja', NULL, NULL, NULL, 2, NULL, 2, 11, 'AUXHUK', NULL, NULL, NULL),
(11, 94564675367, '2018-07-07', 'alwinraja.kryon@gmail.com', 'Allwin Raja', NULL, NULL, NULL, 1, NULL, 2, 12, 'AUXHUK', NULL, NULL, NULL),
(12, 9566753347, '2018-08-02', NULL, 'sfdgdsg', NULL, NULL, NULL, 2, 4, 4, 15, NULL, NULL, NULL, NULL),
(13, 6555445544, '2018-08-02', NULL, NULL, NULL, NULL, NULL, 1, NULL, 4, 16, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

DROP TABLE IF EXISTS `user_location`;
CREATE TABLE IF NOT EXISTS `user_location` (
  `ul_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_update` datetime(6) NOT NULL,
  `ul_lang` double NOT NULL,
  `ul_lat` double NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`ul_id`),
  UNIQUE KEY `UK_edk7ku1jv4y8ah9h0jrgpci7u` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_location`
--

INSERT INTO `user_location` (`ul_id`, `last_update`, `ul_lang`, `ul_lat`, `user`) VALUES
(1, '2018-08-07 13:10:42.444000', 423.3453657, 235.35676578, 10),
(2, '2018-08-07 13:11:16.292000', 423.3453657, 235.35676578, 11);

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

DROP TABLE IF EXISTS `user_session`;
CREATE TABLE IF NOT EXISTS `user_session` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_key` varchar(255) NOT NULL,
  `u_created_time` datetime(6) NOT NULL,
  `user_master` int(11) NOT NULL,
  PRIMARY KEY (`u_id`),
  KEY `FKi1dsrwx06tfing4p9f44nyybd` (`user_master`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`u_id`, `session_key`, `u_created_time`, `user_master`) VALUES
(1, '$2a$10$RG0SJpoXzE3T6X7US5ylfeuohC2G0sV53kqeo9dkbjOAtvdara.Xq', '2018-07-05 12:09:49.902000', 2),
(2, '$2a$10$TEpWSvN/3NATMZr1CyeboOJ8mtsZB9s3m.S1FlFZD9Huo8C8zuvXm<<1530773304298>>', '2018-07-05 12:18:24.298000', 2),
(3, '$2a$10$z/wiFxajgai3TN6ernL.kOJI4UuW3ChjMyBI6fVDum/8MTvbfXV3O<<1530773326994>>', '2018-07-05 12:18:46.994000', 2),
(4, '$2a$10$BWV4Zt0Bemdd8SEdeLRI2.jKXIg6O0hwGoXH2Pm9Hm/crv2l6kry2<<1530773392822>>', '2018-07-05 12:19:52.822000', 1),
(5, '$2a$10$.gNzrd954BD7BK5i0lNDnuVkWIlNoirr7ZAa92/DdNecHssGNneNq<<1530964753736>>', '2018-07-07 17:29:13.736000', 10),
(6, '$2a$10$TlgIuePjudk08ouU3d3oBuTlqd2SX/xWRjn0/Kx7lHafouaG1sCsi<<1530964787140>>', '2018-07-07 17:29:47.140000', 11),
(7, '$2a$10$iVswe3t7oXZC/PhbP7JpBO8dmM5ynp41DjesK3/MKVAffEpOt7g2K<<1530965161655>>', '2018-07-07 17:36:01.655000', 11),
(8, '$2a$10$c93cy.ObHSOqQHhpOee4KerXfkwOo5a/6w8GSf5YrRW95YqSkXBKq--1533217188181--', '2018-08-02 19:09:48.181000', 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

DROP TABLE IF EXISTS `user_status`;
CREATE TABLE IF NOT EXISTS `user_status` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(255) NOT NULL,
  PRIMARY KEY (`s_id`),
  UNIQUE KEY `UK_1557y72psvuuh345j431o8vpq` (`s_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_brand`
--

DROP TABLE IF EXISTS `vehicle_brand`;
CREATE TABLE IF NOT EXISTS `vehicle_brand` (
  `vb_id` int(11) NOT NULL AUTO_INCREMENT,
  `vb_name` varchar(255) NOT NULL,
  PRIMARY KEY (`vb_id`),
  UNIQUE KEY `UK_950pq62x3bpwk6ijgm0l9vifg` (`vb_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_brand`
--

INSERT INTO `vehicle_brand` (`vb_id`, `vb_name`) VALUES
(2, 'dhdgh'),
(1, 'sdb');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_location`
--

DROP TABLE IF EXISTS `vehicle_location`;
CREATE TABLE IF NOT EXISTS `vehicle_location` (
  `vl_id` int(11) NOT NULL AUTO_INCREMENT,
  `last_update` datetime(6) NOT NULL,
  `vl_lang` double NOT NULL,
  `vl_lat` double NOT NULL,
  PRIMARY KEY (`vl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_master`
--

DROP TABLE IF EXISTS `vehicle_master`;
CREATE TABLE IF NOT EXISTS `vehicle_master` (
  `vhm_id` int(11) NOT NULL AUTO_INCREMENT,
  `base_fair` double DEFAULT NULL,
  `per_km_price` double DEFAULT NULL,
  `registration_date` date NOT NULL,
  `vhm_colour` varchar(255) DEFAULT NULL,
  `vhm_colour_hint` varchar(255) NOT NULL,
  `vhm_make_hint` varchar(255) NOT NULL,
  `vhm_model_hint` varchar(255) NOT NULL,
  `vhm_name` varchar(255) NOT NULL,
  `vhm_number` varchar(255) NOT NULL,
  `vhm_registration_number` varchar(255) NOT NULL,
  `vhm_status` int(11) NOT NULL,
  `v_assets` int(11) NOT NULL,
  `v_location` int(11) DEFAULT NULL,
  `vhm_model` int(11) DEFAULT NULL,
  `vhm_owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`vhm_id`),
  KEY `FKe6sy48yvatvwn9chdu7hvictv` (`v_assets`),
  KEY `FKm0j6k0lcsticbggcmbfibn95i` (`v_location`),
  KEY `FKqacc3cjgh5wylfwp7f7n0tkmt` (`vhm_model`),
  KEY `FKef9g0pt3htwppxopnwgbjudtg` (`vhm_owner`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_master`
--

INSERT INTO `vehicle_master` (`vhm_id`, `base_fair`, `per_km_price`, `registration_date`, `vhm_colour`, `vhm_colour_hint`, `vhm_make_hint`, `vhm_model_hint`, `vhm_name`, `vhm_number`, `vhm_registration_number`, `vhm_status`, `v_assets`, `v_location`, `vhm_model`, `vhm_owner`) VALUES
(1, NULL, NULL, '0015-01-08', 'Red', 'Red', 'Hundai', 'i10', 'Allwin Raja', 'TN AU67 5476 ', '251471THF785447854', 2, 8, NULL, 4, 11);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_master_asset`
--

DROP TABLE IF EXISTS `vehicle_master_asset`;
CREATE TABLE IF NOT EXISTS `vehicle_master_asset` (
  `vma_id` int(11) NOT NULL AUTO_INCREMENT,
  `vma_full_view` varchar(255) NOT NULL,
  `vma_insurance_exp_date` date NOT NULL,
  `vma_number_view` varchar(255) NOT NULL,
  `vma_rc_book` varchar(255) NOT NULL,
  `vma_insurance_img` varchar(255) NOT NULL,
  PRIMARY KEY (`vma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_master_asset`
--

INSERT INTO `vehicle_master_asset` (`vma_id`, `vma_full_view`, `vma_insurance_exp_date`, `vma_number_view`, `vma_rc_book`, `vma_insurance_img`) VALUES
(1, '/resource/6-6-2018-1530880780232CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530880780236CLS.png', '/resource/6-6-2018-1530880780237CLS.pdf', ''),
(2, '/resource/6-6-2018-1530880798188CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530880798191CLS.png', '/resource/6-6-2018-1530880798191CLS.pdf', ''),
(3, '/resource/6-6-2018-1530881121731CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530881121735CLS.png', '/resource/6-6-2018-1530881121735CLS.pdf', ''),
(4, '/resource/6-6-2018-1530881151029CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530881151033CLS.png', '/resource/6-6-2018-1530881151034CLS.pdf', ''),
(5, '/resource/6-6-2018-1530881335126CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530881335131CLS.png', '/resource/6-6-2018-1530881335131CLS.pdf', ''),
(6, '/resource/6-6-2018-1530881406977CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530881406981CLS.png', '/resource/6-6-2018-1530881406981CLS.pdf', ''),
(7, '/resource/6-6-2018-1530881457574CLS.jpg', '2019-05-30', '/resource/6-6-2018-1530881457578CLS.png', '/resource/6-6-2018-1530881457579CLS.pdf', ''),
(8, '/resource/6-6-2018-1530881499727CLS.jpg', '2013-08-10', '/resource/6-6-2018-1530881499730CLS.png', '/resource/6-6-2018-1530881499731CLS.pdf', '');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_model`
--

DROP TABLE IF EXISTS `vehicle_model`;
CREATE TABLE IF NOT EXISTS `vehicle_model` (
  `vm_id` int(11) NOT NULL AUTO_INCREMENT,
  `vm_name` varchar(255) NOT NULL,
  `vm_brand` int(11) NOT NULL,
  PRIMARY KEY (`vm_id`),
  UNIQUE KEY `UK_palctj7f5k1b0rpdtdxunm5bq` (`vm_name`),
  KEY `FKhoeswc2dpax1k170ktlidyqcx` (`vm_brand`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_model`
--

INSERT INTO `vehicle_model` (`vm_id`, `vm_name`, `vm_brand`) VALUES
(3, 'dfgdfg', 1),
(4, 'erfgerheh', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

DROP TABLE IF EXISTS `wallet`;
CREATE TABLE IF NOT EXISTS `wallet` (
  `w_id` int(11) NOT NULL AUTO_INCREMENT,
  `closeing` int(11) NOT NULL,
  `opening` int(11) NOT NULL,
  `opposition` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`w_id`),
  KEY `FKtoafts568cvrpmcic6173dn8q` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `workers_credential`
--

DROP TABLE IF EXISTS `workers_credential`;
CREATE TABLE IF NOT EXISTS `workers_credential` (
  `wc_id` int(11) NOT NULL AUTO_INCREMENT,
  `wc_created_on` datetime(6) NOT NULL,
  `wc_otp` varchar(255) DEFAULT NULL,
  `wc_otp_expiry_time` datetime(6) DEFAULT NULL,
  `wc_password` varchar(255) DEFAULT NULL,
  `wc_updated_on` datetime(6) NOT NULL,
  PRIMARY KEY (`wc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workers_credential`
--

INSERT INTO `workers_credential` (`wc_id`, `wc_created_on`, `wc_otp`, `wc_otp_expiry_time`, `wc_password`, `wc_updated_on`) VALUES
(1, '2018-07-05 00:00:00.000000', '5847', '2018-07-05 00:00:00.000000', '$2a$10$TB/tZJXuTfOPy68AWL9BJ.6Tp/M1cVvrl7A2uKKZF14vHTr35hRI6', '2018-07-05 00:00:00.000000'),
(2, '2018-07-07 15:59:17.877000', NULL, NULL, '$2a$10$TB/tZJXuTfOPy68AWL9BJ.6Tp/M1cVvrl7A2uKKZF14vHTr35hRI6', '2018-07-07 15:59:17.982000'),
(3, '2018-07-07 15:59:20.038000', NULL, NULL, '$2a$10$bgOuAbd4K7f8Kngl1zMti.gJ9hGzRJO3rfUsoSEPl41H1l1JxEfTm', '2018-07-07 15:59:20.132000'),
(4, '2018-07-07 15:59:21.292000', NULL, NULL, '$2a$10$lDdFl37jRgLlbTZmaG9YUO8puoapQBvMf52dKd9hqcXAaaILR9OCe', '2018-07-07 15:59:21.386000'),
(5, '2018-07-07 15:59:22.058000', NULL, NULL, '$2a$10$jVDO2Co0j.KRIumhfxewVuJ8qvl43YQk9u3hC1AwSOMOvuyI17blG', '2018-07-07 15:59:22.153000'),
(6, '2018-07-07 16:01:50.340000', NULL, NULL, '$2a$10$nvPc1bFz30YnsPChLrKAH.MIxgZzn7932aAICIdKZlVvOaI7LrT3S', '2018-07-07 16:01:50.434000'),
(7, '2018-07-07 16:01:51.240000', NULL, NULL, '$2a$10$faULqwx149nRuOov68kxVuX7bGi7eSGaDs1P.v6foUAw81JAyGJuW', '2018-07-07 16:01:51.338000'),
(8, '2018-07-07 16:01:52.331000', NULL, NULL, '$2a$10$aRLFF0Hkr05KdlemfQPaA.Tc.ZQVm9u/QpN0rvzlg5NDOQperEpFS', '2018-07-07 16:01:52.428000'),
(9, '2018-07-07 16:18:38.024000', NULL, NULL, '$2a$10$aV8/wTXrrSkVp9MI64hqUe6r3/maRKBmKxKlq5jsRUrTdcwXF2hVm', '2018-07-07 16:18:38.124000'),
(10, '2018-07-07 16:18:49.033000', NULL, NULL, '$2a$10$baquNA.7IZ6nzH/C4k0yhONlKKm/G0GkunjbkWyymrHVoawCjkwfK', '2018-07-07 16:18:49.130000'),
(11, '2018-07-07 16:29:45.779000', NULL, NULL, '$2a$10$/.X338g/LskyK7NkgfDyOuT38iFc2jV65L8ja6sYVYbbNpE6.hxd2', '2018-07-07 16:29:45.882000'),
(12, '2018-07-07 17:29:30.274000', NULL, NULL, '$2a$10$AYc4HthYVG9UE6yAhc46aetfc6Hm9Mi3GErBF3XJiAQkIbmG7vDe6', '2018-07-07 17:29:30.372000'),
(13, '2018-08-02 16:12:54.485000', '851113', '2018-08-02 16:17:54.485000', NULL, '2018-08-02 16:12:54.485000'),
(14, '2018-08-02 16:13:42.187000', '874658', '2018-08-02 16:18:42.187000', NULL, '2018-08-02 16:13:42.187000'),
(15, '2018-08-02 16:14:24.084000', '373801', '2018-08-02 19:14:07.446000', NULL, '2018-08-02 16:14:24.084000'),
(16, '2018-08-02 19:05:08.393000', '460100', '2018-08-02 19:11:55.585000', NULL, '2018-08-02 19:05:08.393000');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `FK32t902l214em2bw3i6k3cxspb` FOREIGN KEY (`user`) REFERENCES `users_master` (`u_id`);

--
-- Constraints for table `fav_place`
--
ALTER TABLE `fav_place`
  ADD CONSTRAINT `FKdjrpad4opes5updsx93pxt0cg` FOREIGN KEY (`user_id`) REFERENCES `users_master` (`u_id`);

--
-- Constraints for table `plan_master`
--
ALTER TABLE `plan_master`
  ADD CONSTRAINT `FKlrgcj9tkl0a5fw72n53htorge` FOREIGN KEY (`pt_type`) REFERENCES `plan_type` (`pt_id`);

--
-- Constraints for table `subscribed_list`
--
ALTER TABLE `subscribed_list`
  ADD CONSTRAINT `FK6sm3eg0nmr66lf5u3idegi5vy` FOREIGN KEY (`sl_vehicle_master`) REFERENCES `vehicle_master` (`vhm_id`),
  ADD CONSTRAINT `FKmxekfkiqdg7pk5gcf5fs5vy0u` FOREIGN KEY (`sl_type`) REFERENCES `plan_type` (`pt_id`);

--
-- Constraints for table `trip_master`
--
ALTER TABLE `trip_master`
  ADD CONSTRAINT `FK7b2ftyslval7f6vblde5u9ekn` FOREIGN KEY (`driver`) REFERENCES `users_master` (`u_id`),
  ADD CONSTRAINT `FKa206o3hmkbvnk7diqknxef9jl` FOREIGN KEY (`vehicle`) REFERENCES `vehicle_master` (`vhm_id`),
  ADD CONSTRAINT `FKblgh41v76c2rid9asstgiiv6e` FOREIGN KEY (`customer`) REFERENCES `users_master` (`u_id`),
  ADD CONSTRAINT `FKditsyqvat2mvep7b776q0mj5x` FOREIGN KEY (`trip_status`) REFERENCES `trip_status` (`ts_id`);

--
-- Constraints for table `users_master`
--
ALTER TABLE `users_master`
  ADD CONSTRAINT `FK621bj0bg1r145f7jefgiduywk` FOREIGN KEY (`u_role`) REFERENCES `role_master` (`rm_id`),
  ADD CONSTRAINT `FK9xme9997le51amewj3buo9bq6` FOREIGN KEY (`driver_assets`) REFERENCES `driver_master_asset` (`dma_id`),
  ADD CONSTRAINT `FKfpnr2tao9mnrd1pafcdypvq2b` FOREIGN KEY (`bck_code`) REFERENCES `backup_code` (`bc_id`),
  ADD CONSTRAINT `FKt09djqef7ytxre57xjdgswigj` FOREIGN KEY (`w_credential`) REFERENCES `workers_credential` (`wc_id`);

--
-- Constraints for table `user_location`
--
ALTER TABLE `user_location`
  ADD CONSTRAINT `FKsnds0ljpohbq0m5sihmw6k4j3` FOREIGN KEY (`user`) REFERENCES `users_master` (`u_id`);

--
-- Constraints for table `user_session`
--
ALTER TABLE `user_session`
  ADD CONSTRAINT `FKi1dsrwx06tfing4p9f44nyybd` FOREIGN KEY (`user_master`) REFERENCES `users_master` (`u_id`);

--
-- Constraints for table `vehicle_master`
--
ALTER TABLE `vehicle_master`
  ADD CONSTRAINT `FKe6sy48yvatvwn9chdu7hvictv` FOREIGN KEY (`v_assets`) REFERENCES `vehicle_master_asset` (`vma_id`),
  ADD CONSTRAINT `FKef9g0pt3htwppxopnwgbjudtg` FOREIGN KEY (`vhm_owner`) REFERENCES `users_master` (`u_id`),
  ADD CONSTRAINT `FKm0j6k0lcsticbggcmbfibn95i` FOREIGN KEY (`v_location`) REFERENCES `vehicle_location` (`vl_id`),
  ADD CONSTRAINT `FKqacc3cjgh5wylfwp7f7n0tkmt` FOREIGN KEY (`vhm_model`) REFERENCES `vehicle_model` (`vm_id`);

--
-- Constraints for table `vehicle_model`
--
ALTER TABLE `vehicle_model`
  ADD CONSTRAINT `FKhoeswc2dpax1k170ktlidyqcx` FOREIGN KEY (`vm_brand`) REFERENCES `vehicle_brand` (`vb_id`);

--
-- Constraints for table `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `FKtoafts568cvrpmcic6173dn8q` FOREIGN KEY (`user`) REFERENCES `users_master` (`u_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
