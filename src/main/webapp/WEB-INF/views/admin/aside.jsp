
<!-- ===== Left-Sidebar ===== -->
<aside class="sidebar">
	<div class="scroll-sidebar">
		<nav class="sidebar-nav">
			<ul id="side-menu">
				<li><a class="waves-effect"
					href="${pageContext.request.contextPath}/admin-dashboard.html"
					aria-expanded="false"><i class="icon-screen-desktop fa-fw"></i>
						<span class="hide-menu"> Dashboard </span></a></li>
				<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span
						class="hide-menu"> Vehicle Manager </span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="vm-new-requests.html">New Requests</a></li>
						<li><a href="vm-approved-vehicles.html">Approved Vehicles</a></li>
						<li><a href="#">Reports</a></li>
					</ul></li>
				<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-notebook fa-fw"></i> <span
						class="hide-menu"> Driver Manager </span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="admin-new-driver-request.html">New Requests</a></li>
						<li><a href="admin-approved-driver-request.html">Approved Drivers</a></li>
						<li><a href="#">Reports</a></li>
					</ul></li>
				<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-docs fa-fw"></i> <span
						class="hide-menu"> Plan Manager</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a
							href="${pageContext.request.contextPath}/pm-vehicle-plans.html">Vehicle
								Plans</a></li>
						<li><a
							href="${pageContext.request.contextPath}/pm-on-demand-plans.html">On-Demand
								Map Plans</a></li>
						<li><a href="#">Reports</a></li>
					</ul></li>
				<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-grid fa-fw"></i> <span
						class="hide-menu"> User Management</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a
							href="${pageContext.request.contextPath}/admin-user-management.html">Customers</a></li>
				       <li><a href="${pageContext.request.contextPath}/admin-owner-management.html">Owners</a></li> 
							
						 <li><a href="${pageContext.request.contextPath}/admin-role-management.html">Roles</a></li> 
						
					</ul></li>
					
						<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-grid fa-fw"></i> <span
						class="hide-menu">Trip Manager</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a
							href="${pageContext.request.contextPath}/admin-trip_master_list.html">Trip Request</a></li>
						
					</ul></li>
					
					<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-layers fa-fw"></i> <span
						class="hide-menu"> CMS</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a
							href="${pageContext.request.contextPath}/admin-privacy-policy.html">Privacy Policy</a></li>
						<li><a
							href="${pageContext.request.contextPath}/admin-terms-and-condition.html">Terms And Condition</a></li>
					</ul></li>
					
					<li><a class="waves-effect" href="javascript:void(0);"
					aria-expanded="false"><i class="icon-layers fa-fw"></i> <span
						class="hide-menu"> Controls</span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a
							href="${pageContext.request.contextPath}/admin-trip-control.html">Trip Control</a></li>
						
					</ul></li>
			</ul>
		</nav>
	</div>
</aside>
<!-- ===== Left-Sidebar-End ===== -->