<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">
<!-- ===== DatePicker ===== -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


          <link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">

<style type="text/css">
hr {
	margin-top: 0px;
	margin-bottom: 0px;
	border: 0;
	border-top: 1px solid #eee;
}
</style>
</head>
<body class="mini-sidebar fix-header" ng-app="myApp"
	ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">
		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />
		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<ul class="breadcrumb">
					<li><a href="#">Dashboard</a></li>
					<li><a href="#">Approved Request</a></li>
					<li><a href="#">View</a></li>
				</ul>
				<!-- .row -->
				<div class="row white-box">
				<div class="form-group">
						<div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>					
                       	
                       		</div>
					<div class=" col-md-12">
						<div class=" col-md-6">
							  <h2><i class="fa fa-user-circle" aria-hidden="true"><span style="padding:10px;">Driver Details</span></i></h2>							
							
							
								<div class="form-group">
									<label class="col-md-12">Driver Name</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.d_name}}" required="required"
											name="driver_name">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-md-12">Date Of Birth</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.dateofbirth}}" required="required"
											name="dob" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">License Number</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.license_no}}" required="required"
											name="license_no" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12">Owner Name 
									</label>
									<div class="col-sm-12 bottom-pad-20">

										<input type="text" class="form-control"
											value="{{data.owner_name}}" required="required"
											name="owner_name" readonly="readonly">
									</div>
								</div>
								
								<form action="admin-update-expiry-date.html" method="post">
								
								<input type="hidden" name="driver_id" name="driver_id" value="{{data.d_id}}">
								<div class="form-group">
									<label class="col-sm-12">License Expiry Date

									</label>
									<div class="col-md-8 bottom-pad-20">
            <input type="text" class="form-control" name="license_expiry" placeholder="License Expiry Date" id="div_from" value="{{data.license_expiry}}"/> 

									</div>
									
									<div class="col-md-4">
									<button type="submit"
										class="btn btn-success waves-effect waves-light m-r-10"
										>Update Date</button>
								</div>
									
								</div>
								
								</form>
								
								
<div class="form-group" ng-hide="data.d_email == 'false'">
									<label class="col-sm-12">Email ID

									</label>
									<div class="col-sm-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.d_email}}" required="required"
											name="email">
									</div>
								</div>
						</div>
						<div class="col-md-6">
							<div class="col-md-6">
								<div class="row">
									<h5 style="font-size: 13px">Registered On
										{{data.created_date}}</h5>
								</div>
								<div class="row">
									<h3 class="box-title m-b-0 ">
										Registered Account : <strong>{{data.d_phone}}</strong>
									</h3>
									<a href="#">
										<button type="button">Transaction</button>
									</a> <a href="#">
										<button type="button">Activity</button>
									</a>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="user-thumb text-center">
										<img alt="thumbnail" class="img-circle" width="100" style="max-height: 100px;"
											src="{{data.d_picture}}">
										<h3>{{data.d_name}}</h3>
									</div>
								</div>
							</div>
							
							
							
							<div class="col-md-2">
								<div class="row btn pull-right">
								
									<label class="switch"> 
						 	<a href="${pageContext.request.contextPath}/admin-disable-driver-request.html?check_status={{c}}&driver_id={{data.d_id}}">
									
									<input type="checkbox" value="" name="checked" id="check">
									
										<span class="slider round"></span>
										
										</a>
									</label>
									
									
								</div>
								
									
							</div>
							
							<div class="clearfix"></div>
							<br>
							
							<div class="row">
									<div class="col-md-3 col-xs-6 b-r">
									<strong>License Proof</strong> <br> <br> <a
										href="{{data.license_image}}" target="_blank"
										class="text-muted">Click to view</a>
								</div>
								<div class="col-md-3 col-xs-6">
									<strong>Address Proof</strong> <br> <br> <a
										href="{{data.address_proof}}" target="_blank"
										class="text-muted">Click to view</a>
								</div>
								
								
							</div>
						</div>
					</div>
					<br>
					<div class="text-right">
						<button type="button"
							class="btn btn-inverse waves-effect waves-light"
							onclick="openRejectModel()">Delete Account</button>
					</div>
					<!-- /.row -->
				</div>
				<!-- /.container-fluid -->
				<jsp:include page="sidebar.jsp" />
				<jsp:include page="footer.jsp" />
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->
		
		
		<!-- /.modal -->
		<div id="reject_model" class="modal fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<form method="get"
					action="${pageContext.request.contextPath}/admin-delete-driver-request.html">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×</button>
							<h4 class="modal-title">Are you sure..?</h4>
						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="recipient-name" class="control-label">Reason:</label>
								<input type="text" class="form-control" name="reason"
									id="id_reason" required="required">
							</div>
							<div class="form-group">
								<label for="message-text" class="control-label">Comments:</label>
								<textarea class="form-control" id="id_comments" name="comments"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="driver_id" value="{{data.d_id}}"
								required="required">

							<button type="button" class="btn btn-default waves-effect"
								data-dismiss="modal">Close</button>
							<button type="submit"
								class="btn btn-danger waves-effect waves-light">Confirm</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		
		
		
		
		<!-- ===== jQuery ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
		<!-- ===== Bootstrap JavaScript ===== -->
	
		<!-- ===== Slimscroll JavaScript ===== -->
		<script
			src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
		<!-- ===== Wave Effects JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/waves.js"></script>
		<!-- ===== Menu Plugin JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
		<!-- ===== Custom JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/custom.js"></script>
		<!-- ===== Plugin JS ===== -->
		<!-- ===== Style Switcher JS ===== -->
		
		   <script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
          <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
		<script
			src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		<!-- ===== Style Switcher JS ===== -->
		<script src="${pageContext.request.contextPath}/js/angular.js"></script>
			<script
			src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	  
	  <script type="text/javascript">
      var date = $('#div_from').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'dd-mm-yy',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
		<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function ($scope, $http) {
                    	
                var d = ${data};
                        $scope.data = d;
                        
                        console.log($scope.data);
                  	  console.log(d.d_email);
                  	  var s = d.d_status;
                  	  
                  	  if(s == 4){
                  	
                  	  document.getElementById('check').checked=true;
                  	  
                  	document.getElementById('check').value="4";
                  	  
                  	 
                  	  
                  	  }else{
                  		  
                  		document.getElementById('check').checked=false;
 
                  		document.getElementById('check').value="5";
                  	  }
                  	
                  	  
                  	if( document.getElementById('check').checked == true){
                  		
                  		$scope.c = "5";
                  	}else{
                  		
                  		$scope.c = "4";
                  
                  	}
                  	 
                  	 
                  	  

                    });
              </script>
              
              		<script type="text/javascript">
function openRejectModel(){
	$('#id_reason').val("");
	$('#id_comments').val("");
	
$('#reject_model').modal('show');
}
</script>



              
</body>
</html>