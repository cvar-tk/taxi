
<!-- ===== Right-Sidebar ===== -->
<div class="right-sidebar">
	<div class="slimscrollright">
		<div class="rpanel-title">
			Settings <span><i class="icon-close right-side-toggler"></i></span>
		</div>
		<div class="r-panel-body">

			<ul>
			<li> <a href="admin-change-password.html"><span><i class="fa fa-key " aria-hidden="true"></i></span>    Change Password</a></li>
			<li> <a href="admin-change-pin.html"><span><i class="fa fa-tag " aria-hidden="true"></i></span>    Change PIN</a></li>
		 <li> <a href="admin-login.html"><span><i class="fa fa-power-off " aria-hidden="true" style="padding-left: 1px;"></i></span>    Logout</a></li>
			
			</ul>
		</div>
	</div>
</div>
<!-- ===== Right-Sidebar-End ===== -->