<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">
</head>

<body class="mini-sidebar fix-header" ng-app="vehicleApp"
	ng-controller="vehicleCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">
		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />
		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid ">
				<div class="row ">
					<div class="col-md-12">
						<div class="white-box">
							<div class="row">
								<div class="col-md-4">
									<h3 class="box-title">Home > Plan Manager > Vehicle Plans</h3>
								</div>
								<div class="col-md-8">
									<div class="col-md-12" style="padding: 0;">
										<div class="col-md-10"></div>
										<div class="col-md-2">
											<button class="btn btn-block btn-success"
												onclick="opencreateModel()">Create</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-xs-12"
										style="padding-left: 20%; padding-right: 20%;">
										<span class="badge btn-success" style="width: 100%;">${message}</span>
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12" style="padding: 26px;">

										<div class="row">

											<div class="col-md-4 col-sm-6" ng-repeat="plan in plans">

												<div
													ng-class="{ 'white-box bg-plan-active-box color-box' : plan.status == 1,'white-box bg-plan-deactive-box color-box' : plan.status == 2 }">
													<!-- Content goes here...-->
													<div class="col-md-12" style="padding-left: 0px">
														<div class="col-md-8" style="padding-left: 0px">
															<h1 class="text-white font-light m-b-0">{{plan.price}}
																INR</h1>
															<h3 class="text-white font-light m-b-0">{{plan.duration}}
																Days</h3>
														</div>
														<div class="col-md-4" style="position: relative;"
															ng-if="plan.status == 1">

															<label class="switch"
																style="position: absolute; right: 0; top: 10px">


																<input type="checkbox" checked="checked"
																value="{{plan.id}}" onchange="changeActiveStatus(this)">


																<span class="slider round"></span>
															</label>
														</div>

														<div class="col-md-4" style="position: relative;"
															ng-if="plan.status == 2">

															<label class="switch"
																style="position: absolute; right: 0; top: 10px">


																<input type="checkbox" value="{{plan.id}}"
																onchange="changeActiveStatus(this)"> <span
																class="slider round"></span>
															</label>
														</div>
													</div>
													<span class="hr-line"></span>
													<p class="cb-text">{{plan.title}}</p>
													<h6 class="text-white font-semibold">
														0 - <span class="font-light">Subscribers</span>
													</h6>
													<hr>
													<span class="font-light">{{plan.comments}}</span>
													
													
                                                 <span style="float: right;color: #fff"  ng-click="editShowModel(plan.id)" class="fa fa-edit"  ></span>
												
												
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<jsp:include page="sidebar.jsp" />
				</div>
				<!-- /.container-fluid -->
				<jsp:include page="footer.jsp" />
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->
		<!-- /.modal -->
		<div id="create_model" class="modal fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">×</button>
						<h4 class="modal-title">Create new vehicle plan :</h4>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<span class="badge btn-danger" style="width: 100%" id="message"></span>
						</div>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-name" class="control-label">Title:</label>
							<input type="text" class="form-control" id="id_plan_title"
								required="required">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Amount:</label> <input
								type="number" min="1" class="form-control" id="id_plan_amount"
								required="required">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Validity
								Title:</label> <input type="text" class="form-control" name="reason"
								id="id_plan_validity_title" required="required">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Validity(Days):</label>
							<input type="number" min="1" class="form-control" name="reason"
								id="id_plan_validity_time" required="required">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Status:</label> <select
								class="form-control" required="required" id="id_plan_status">
								<option selected="selected" disabled="disabled" value=""
									id="id_plan_status">-- Select Status --</option>
								<option selected="selected" value="1">Enable</option>
								<option selected="selected" value="2">Disable</option>
							</select>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default waves-effect"
								data-dismiss="modal">Close</button>
							<button type="button"
								class="btn btn-danger waves-effect waves-light"
								onclick="createPlan()">Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="edit_model" class="modal fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">×</button>
						<h4 class="modal-title">Edit vehicle plan :</h4>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<span class="badge btn-danger" style="width: 100%" id="message1"></span>
						</div>
					</div>
					
					<input type="hidden" value="{{plan_master.plan_id}}" id="plans_id" name="edit_id">
					
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-name" class="control-label">Title:</label>
							<input type="text" class="form-control" id="id_title"
								value="{{plan_master.plan_title}}">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Amount:</label> <input
								type="number" min="1" class="form-control" id="id_amount"
								value="{{plan_master.plan_price}}">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Validity
								Title:</label> <input type="text" class="form-control" name="reason"
								id="id_validity_title" value="{{plan_master.plan_comments}}">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Validity(Days):</label>
							<input type="number" min="1" class="form-control" name="reason"
								id="id_validity_time" value="{{plan_master.plan_totup_duration}}">
						</div>
						<div class="form-group">
							<label for="message-text" class="control-label">Status:</label> <select
								class="form-control"  id="id_status" >
								<option selected="selected" disabled="disabled">-- Select Status --</option>
								<option value="1">Enable</option>
								<option value="2">Disable</option>
							</select>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default waves-effect"
								data-dismiss="modal">Close</button>
							<button type="button"
								class="btn btn-danger waves-effect waves-light"
								onclick="editPlan()">Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
							<input type="hidden" value="" id="edit_id" name="edit_id">
		
		<!-- ==============================
         Required JS Files
         =============================== -->
		<!-- ===== jQuery ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js">
            </script>
		<!-- ===== Bootstrap JavaScript ===== -->
		<script
			src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js">
            </script>
		<!-- ===== Slimscroll JavaScript ===== -->
		<script
			src="${pageContext.request.contextPath}/js/jquery.slimscroll.js">
            </script>
		<!-- ===== Wave Effects JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/waves.js">
            </script>
		<!-- ===== Menu Plugin JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/sidebarmenu.js">
            </script>
		<!-- ===== Custom JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/custom.js">
            </script>
		<!-- ===== Plugin JS ===== -->
		<!-- ===== Style Switcher JS ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js">
            </script>
		<script src="${pageContext.request.contextPath}/js/angular.js">
            </script>
		<script
			src="${pageContext.request.contextPath}/js/ui-bootstrap-tpls-0.13.4.min.js">
            </script>

		<script type="text/javascript">
                console.log(${content_list});

                var app = angular.module('vehicleApp', ['ui.bootstrap']);

                app.controller('vehicleCtrl', function($scope, $http) {

                    $scope.plans = ${content_list};


                 $scope.editShowModel= function (id) {
                	 
                	 console.log(id);
                        
                        	 $('#edit_id').val(id);
                            $('#edit_model').modal('show');
                            
                            var plan_id = document.getElementById('edit_id').value;
                            
                            console.log(plan_id);
                           
                            $scope.getlist = function() {

                				$http.get(
                						"${pageContext.request.contextPath}/api-edit-plan-master.html?plan_id="+ plan_id ).then(
                						function(response) {
                							
                							console.log(response);
                							
                							console.log(response.data);
                							  $scope.plan_master = response.data;
                							  
                							  var status = response.data.status;
                							  
                							  console.log(status);
                							  
                							  var s = 1;
                							  
                							  if(status == 2){
                								  console.log("shdafh");
                								  s = 2;
                							  }
                							  
                							  $('#id_status').val(s);
                							
                							
                						}, function(err) {
                							var error = err;
                							console.log(error);

                						});

                			}

                			$scope.getlist();
 
                        
                        
                        }
                        
                    
                    
                });
                
                
               function changeActiveStatus(context){
                	console.log(context.value);
                	
                	   $.ajax({
                           type: 'POST',
                           url: '${pageContext.request.contextPath}/api-change-vehicle-plan-status.html?id='+context.value,                         
                           dataType: 'json',
						   success: function(data) {
                               console.log(data);

                               if (data.status) {

                                   window.location = "${pageContext.request.contextPath}/pm-vehicle-plans.html?message="+data.message;
                                   return;
                               }

                               console.log(data.message);
                           }
                       });
                	
                }
            </script>



		<script type="text/javascript">
                function opencreateModel() {
                    $('#id_plan_title').val("");
                    $('#id_plan_amount').val("");
                    $('#id_plan_validity_title').val("");
                    $('#id_plan_validity_time').val("");
                    $('#id_plan_status').val("");
                    $('#create_model').modal('show');
                }


                function createPlan() {

                    var plan_title = $('#id_plan_title').val();
                    var plan_amount = $('#id_plan_amount').val();
                    var plan_validity_title = $('#id_plan_validity_title').val();
                    var plan_validity = $('#id_plan_validity_time').val();
                    var plan_status = $('#id_plan_status').val();

                    var jsonData = {
                        "plan_title": plan_title,
                        "plan_amount": plan_amount,
                        "plan_validity_title": plan_validity_title,
                        "plan_validity": plan_validity,
                        "plan_status": plan_status
                    };

                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/api-create-vehicle-plan.html',
                        data: jsonData,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);

                            if (data.status) {

                                window.location = "${pageContext.request.contextPath}/pm-vehicle-plans.html?message=Plan created successfully..!";
                                return;
                            }

                            console.log(data.message);
                            document.getElementById("message").innerHTML = data.message;
                            setTimeout(hidemessage, 6000);

                        }
                    });


                    console.log(jsonData);
                }

                function hidemessage() {
                    document.getElementById("message").innerHTML = "";
                }
                
                </script>
                
                <script>
                
                
                function editPlan() {
                	
                	console.log("editplan");
                	 var id = $('#plans_id').val();

                    var title = $('#id_title').val();
                    var amount = $('#id_amount').val();
                    var validity_title = $('#id_validity_title').val();
                    var validity = $('#id_validity_time').val();
                    var status = $('#id_status').val();

                    var jsonData = {
                    		"plan_id": id,
                        "plan_title": title,
                        "plan_amount": amount,
                        "plan_validity_title": validity_title,
                        "plan_validity": validity,
                        "plan_status": status
                    };

                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/api-edit-vehicle-plan.html',
                        data:jsonData,
                        dataType: 'json',
                        success: function(response) {
                            console.log(response);

                            if (response.status) {

                                window.location = "${pageContext.request.contextPath}/pm-vehicle-plans.html?message=Plan Updated Successfully..!";
                                return;
                            }

                            console.log(response.message);
                            console.log(response.message);
                            document.getElementById("message1").innerHTML = response.message;
                            setTimeout(hidemessage, 6000);

                        }
                    });


                    console.log(jsonData);
                }
                function hidemessage() {
                    document.getElementById("message1").innerHTML = "";
                }
                
            </script>
</body>

</html>