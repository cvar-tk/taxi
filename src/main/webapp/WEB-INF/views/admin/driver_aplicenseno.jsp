<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/plugins/images/favicon.png">
      <title>RootU</title>
      <!-- ===== Bootstrap CSS ===== -->
      <link href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
      <!-- ===== Plugin CSS ===== -->
      <!-- ===== Animation CSS ===== -->
      <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet">
      <!-- ===== Custom CSS ===== -->
      <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
      <!-- ===== Color CSS ===== -->
      <link href="${pageContext.request.contextPath}/css/colors/red.css" id="theme" rel="stylesheet">
            <!-- ===== Date Range picker ===== -->    
          <link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">
      
      
      
      
      
   </head>
   <body class="mini-sidebar fix-header"  ng-app="myApp" ng-controller="driverCtrl">
      <!-- Preloader -->
      <div class="preloader">
         <div class="cssload-speeding-wheel"></div>
      </div>
      <div id="wrapper">
         <jsp:include page="nav.jsp" />
         <jsp:include page="aside.jsp" />
         <!-- Page Content -->
         <div class="page-wrapper">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-md-12">
                    <div class="white-box">
                    <div class="form-group">
							<div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>	
                    <div class="row">
                                   <div class="col-md-4">                                           
                            <h3 class="box-title">Home > Approved Driver</h3>                            
                            </div>
                            <div class="col-md-8">    
                            <div class="col-md-12">   
                           <form action="admin-filter-approve-driver.html?pageIndex=1" method="post">                               
                              <div class="col-md-5">
                                        <div class="input-daterange input-group" id="date-range">
                                            <input type="text" class="form-control" name="from" placeholder="From" id="div_from"/> <span class="input-group-addon bg-info b-0 text-white">to</span>
                                            <input type="text" class="form-control" name="to" placeholder="To" id="div_to" /> 
                                            </div>
                              </div>
                              <div class="col-md-2">
                                      <button class="btn btn-block btn-success">Filter</button> 
                              </div>
                              
                               </form>
                              <form action="admin-approve-license-no.html" method="post">
                               <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="License No" name="license_number" required="required""/> 
                              </div>
                              <div class="col-md-2">
                                      <button class="btn btn-block btn-info" type="submit">Go</button> 
                              </div>
                              </form>
                               </div>                                
                            </div>
                            
                            </div>
                            <div class="table-responsive">
                                      <table class="table" >  
                        <thead>  
                           <tr>
                                         <th>S.No</th>
											<th>Licence Number</th>										
											<th>Driver Name</th>
											<th>Owner Name</th>
											<th>Ph. Number</th>
											<th>#</th>
                                        </tr> 
                        </thead>  
                        <tbody>  
                            <tr >  
                               	<td>{{$index + 1}}</td>
											<td>{{driver.license_no}}</td>
											<td>{{driver.d_name}}</td>
											<td>{{driver.owner_name}}</td>
											<td>{{driver.d_phone}}</td>
											<td><a
												href="${pageContext.request.contextPath}/admin-view-approved-driver-request.html?driver_id={{driver.d_id}}"><i
													class="fa fa-edit"></i></a></td>
													
													 </tr>  
                        </tbody>  
                      
                    </table>  
                        
                            </div>
                        </div>
                  </div>
               </div>
               <jsp:include page="sidebar.jsp" />
            </div>
            <!-- /.container-fluid -->
            <jsp:include page="footer.jsp" />
         </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- ==============================
         Required JS Files
         =============================== -->
         
         
      
      
           
      <!-- ===== jQuery ===== -->
      <script src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
      <!-- ===== Bootstrap JavaScript ===== -->
      <script src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- ===== Slimscroll JavaScript ===== -->
      <script src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
      <!-- ===== Wave Effects JavaScript ===== -->
      <script src="${pageContext.request.contextPath}/js/waves.js"></script>
      <!-- ===== Menu Plugin JavaScript ===== -->
      <script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
      <!-- ===== Custom JavaScript ===== -->
      <script src="${pageContext.request.contextPath}/js/custom.js"></script>
      <!-- ===== Plugin JS ===== -->
      
           <script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
          <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
      
      <!-- ===== Style Switcher JS ===== -->
      <script src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
      
     <script src="${pageContext.request.contextPath}/js/angular.js"></script>
     <script src="${pageContext.request.contextPath}/js/ui-bootstrap-tpls-0.13.4.min.js"></script>
<%--     <script src="${pageContext.request.contextPath}/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
 --%>          <script src="${pageContext.request.contextPath}/plugins/components/moment/moment.js"></script>
 
 
    <script type="text/javascript">
var date = $('#div_from').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'dd-mm-yy',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
	  
	  <script type="text/javascript">
var date = $('#div_to').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'dd-mm-yy',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
      
      <script type="text/javascript">
      var app = angular.module('myApp', ['ui.bootstrap']);  
      
      app.controller('driverCtrl', function ($scope, $http) {  
       
                                 $scope.driver = ${license_no} ;
                          
    
         
          
          
      });  
      
      </script>
      
      
   </body>
</html>