<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

</head>

<body class="mini-sidebar fix-header">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">

		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />

				<!--     Page Content  -->
		<div class="page-wrapper">
			<div class="container-fluid">
			<div class="col-sm-12">
				<div class="white-box">
 <div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>
                       	</div>
					<div class="row mobile-form">
						
						
						<div class="col-sm-6">

							<h4 class="box-title">Privacy Policy</h4>
							
							
						</div>
						<form action="admin-user-privacy-policy.html" method="post">
						
						<input type="hidden" name="role" value = "4">

							<div class="col-sm-12">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3">User Privacy Policy*</label>
										<div class="col-md-9">
											<textarea id="user_privacy" class="form-control" type="text"
												 name="user_privacy" required></textarea>
										
										</div>
									</div>


								</div>

							</div>




<div class="col-sm-1" style="float:right; margin-top:20px;">
      <button type="submit" class="btn btn-info btn-outline-info waves-effect md-trigger cbtn"  id="p_name" name="product_name">Submit</button>
      </div>
       
                         

						</form>
						
						<div>
						<form action="admin-update-driver-privacy-policy.html" method="post">
						
												<input type="hidden" name="role" value = "3">
						
							<div class="col-sm-12" style="    margin-top: 30px;">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3">Driver Privacy Policy*</label>
										<div class="col-md-9">
											<textarea id="driver_privacy" class="form-control" type="text"
												 name="driver_privacy" required></textarea>
										
										</div>
									</div>


								</div>

							</div>




<div class="col-sm-1" style="float:right; margin-top:20px;">
      <button type="submit" class="btn btn-info btn-outline-info waves-effect md-trigger cbtn"  id="p_name" name="product_name">Submit</button>
      </div>
      
       	</form>
                         </div>
						
					
						
					<div>
						<form action="admin-owner-privacy-policy.html" method="post">
						
												<input type="hidden" name="role" value = "2">
						
							<div class="col-sm-12" style="    margin-top: 30px;">
								<div class="row">
									<div class="form-group">
										<label class="col-md-3">Owner Privacy Policy*</label>
										<div class="col-md-9">
											<textarea id="owner_privacy" class="form-control" type="text"
												 name="owner_privacy" required></textarea>
										
										</div>
									</div>


								</div>

							</div>




<div class="col-sm-1" style="float:right; margin-top:20px;">
      <button type="submit" class="btn btn-info btn-outline-info waves-effect md-trigger cbtn"  id="p_name" name="product_name">Submit</button>
      </div>
      
       	</form>
                         </div>
							
						
					
						
					</div>
				
			<!-- /.row -->




			<div class="right-sidebar">
				<div class="slimscrollright">
					<div class="rpanel-title">
						Settings <span><i class="icon-close right-side-toggler"></i></span>
					</div>
					<div class="r-panel-body">

						<ul>
							<li><a href="admin-login.html"><span><i
										class="fa fa-power-off " aria-hidden="true"></i></span> Logout</a></li>


						</ul>
					</div>
				</div>
			</div>


</div>
</div>
</div>
</div>
	</div>
	
	
	
	<!-- ===== jQuery ===== -->
	       <script src="${pageContext.request.contextPath}/ckeditor/ckeditor.js"></script>
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- ===== Bootstrap JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ===== Slimscroll JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!-- ===== Wave Effects JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- ===== Menu Plugin JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!-- ===== Custom JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!-- ===== Plugin JS ===== -->
	<!-- ===== Style Switcher JS ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		
	  <script>
	  
              var v = ${user};
            	 
            	
             document.getElementById('user_privacy').innerHTML =v.privacy;
             
             var d = ${driver};
             
             document.getElementById('driver_privacy').innerHTML =d.driver_privacy;

             var o = ${owner};
             
             document.getElementById('owner_privacy').innerHTML =o.owner_privacy;

             
             
             </script> 	
		<script>
                        $(document).ready(function () {
                            CKEDITOR.replace('user_privacy');

                        });
                    </script>
                 
                     <script>
                        function loadDescription() {
                           
                            var dfg = $('#user_privacy').val();
                            console.log($('#user_privacy'));
                        }
                    </script>    
		
		
		<script>
                        $(document).ready(function () {
                            CKEDITOR.replace('driver_privacy');

                        });
                    </script>
                 
                     <script>
                        function loadDescription() {
                           
                            var dfg = $('#driver_privacy').val();
                            console.log($('#driver_privacy'));
                        }
                    </script>    
		
			<script>
                        $(document).ready(function () {
                            CKEDITOR.replace('owner_privacy');

                        });
                    </script>
                 
                     <script>
                        function loadDescription() {
                           
                            var dfg = $('#owner_privacy').val();
                            console.log($('#owner_privacy'));
                        }
                    </script>    
		
		
		
		
</body>

</html>