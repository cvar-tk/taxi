<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">
<!-- ===== DatePicker ===== -->
<
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


          <link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">


<style type="text/css">
hr {
	margin-top: 0px;
	margin-bottom: 0px;
	border: 0;
	border-top: 1px solid #eee;
}


#ui-datepicker-div{
  width:15% !important;
}
</style>



</head>

<body class="mini-sidebar fix-header" ng-app="myApp"
	ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">
		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />
		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<ul class="breadcrumb">
					<li><a href="#">Dashboard</a></li>
					<li><a href="#">New Request</a></li>
					<li><a href="#">View</a></li>
				</ul>
			
								<form action="admin-approve-driver.html" method="get">
								
				
							<div class="row white-box">
							
							<div class="form-group">
						<div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>							</div>
					</div>
						<div class=" col-md-12">
							<div class=" col-md-6">
							
                           <h2><i class="fa fa-user-circle" aria-hidden="true"><span style="padding:10px;">Driver Details</span></i></h2>							
							
							
								<div class="form-group">
									<label class="col-md-12">Driver Name</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.d_name}}" required="required"
											name="driver_name">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-md-12">Date Of Birth</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.dateofbirth}}" required="required" id="dob"
											name="dob">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">License Number</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.license_no}}" required="required"
											name="license_no">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12">Owner Name 
									</label>
									<div class="col-sm-12 bottom-pad-20">

										<input type="text" class="form-control"
											value="{{data.owner_name}}" required="required"
											name="owner_name" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12">License Expiry Date

									</label>
									<div class="col-sm-12 bottom-pad-20">
            <input type="text" class="form-control" name="license_expiry" placeholder="License Expiry Date" id="div_from" value="{{data.license_expiry}}"/> 

									</div>
								</div>
								
								
<div class="form-group" ng-hide="data.d_email == 'false'">
									<label class="col-sm-12">Email ID

									</label>
									<div class="col-sm-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.d_email}}" readonly="readonly"
											name="email">
									</div>
								</div>
							
							</div>
							<div class=" col-md-6">
								<h3 class="box-title m-b-0">
									Registered Account : <strong>{{data.d_phone}}</strong>
								</h3>
								
								
									<div class="col-md-3 col-xs-6">
								
									<div class="user-thumb text-center" style="width: 200%;">
										<img alt="thumbnail" class="img-circle" width="50" style="max-height: 90px; width: 90px;"
											src="{{data.d_picture}}">
										<h3>{{data.d_name}}</h3>
									</div>
								
							</div>
								<br>
								<div class="col-sm-3"></div>
								
									<div class="col-md-3 col-xs-6 b-r">
									<strong>License Proof</strong> <br> <br> <a
										href="{{data.license_image}}" target="_blank"
										class="text-muted">Click to view</a>
								</div>
								<div class="col-md-3 col-xs-6">
									<strong>Address Proof</strong> <br> <br> <a
										href="{{data.address_proof}}" target="_blank"
										class="text-muted">Click to view</a>
								</div>
								
							
								
								
								</div>
							
						</div>
						<br>
						
						<div class="text-right">
				   	<input type="hidden" name="driver_id" value="{{data.d_id}}">
								
							<button type="submit"
								class="btn btn-success waves-effect waves-light m-r-10">Approve</button>
							
								
								
							<button type="button"
								class="btn btn-inverse waves-effect waves-light"
								onclick="openRejectModel()">Reject</button>
								
								
						</div>
						<!-- /.row -->
					</div>
				</form>
				<!-- /.container-fluid -->
				<jsp:include page="sidebar.jsp" />

				<jsp:include page="footer.jsp" />
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->

		<!-- /.modal -->
		<div id="reject_model" class="modal fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<form method="get"
					action="${pageContext.request.contextPath}/admin-reject-driver-request.html">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×</button>
							<h4 class="modal-title">Are you sure..?</h4>
						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="recipient-name" class="control-label">Reason:</label>
								<input type="text" class="form-control" name="reason"
									id="id_reason" required="required">
							</div>
							<div class="form-group">
								<label for="message-text" class="control-label">Comments:</label>
								<textarea class="form-control" id="id_comments" name="comments"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="driver_id" value="{{data.d_id}}"
								required="required">

							<button type="button" class="btn btn-default waves-effect"
								data-dismiss="modal">Close</button>
							<button type="submit"
								class="btn btn-danger waves-effect waves-light">Confirm</button>
						</div>
					</div>
				</form>
			</div>
		</div>


		<!-- ==============================
         Required JS Files
         =============================== -->
		<!-- ===== jQuery ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
		
		
		<!-- ===== Slimscroll JavaScript ===== -->
		<script
			src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
		<!-- ===== Wave Effects JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/waves.js"></script>
		<!-- ===== Menu Plugin JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
		<!-- ===== Custom JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/custom.js"></script>
		
		<script
			src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
			
			    <script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
          <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
      
      <!-- ===== Bootstrap JavaScript ===== -->
      <script
			src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
		<!-- ===== Style Switcher JS ===== -->
		<script src="${pageContext.request.contextPath}/js/angular.js"></script>
		
      
      <script type="text/javascript">
		
			 
			 function openRejectModel(){
		 
	$('#id_reason').val("");
	$('#id_comments').val("");
	
	console.log("reject");
	
	//$('#reject_model').style.display = "block";
	
	$('#reject_model').modal('show');
      }
		 
</script>
        <script type="text/javascript">
var date = $('#dob').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'yy-mm-dd',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script>   
      
    <script type="text/javascript">
var date = $('#div_from').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'yy-mm-dd',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
		<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function ($scope, $http) {
                    	
                var d = ${data};
                        $scope.data = d;
                        
                        console.log($scope.data);
                        
                        if(d.d_email == false){
                  	  console.log(d.d_email);

                       
                      
                        $scope.email = Object.keys(d.d_email).length;
                      
                        console.log(Object.keys(d.d_email).length);

                        }

                    });
              </script>

		
</body>

</html>