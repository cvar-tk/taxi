<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

</head>

<body class="mini-sidebar fix-header">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<section id="wrapper" class="login-register">
		<div class="login-box">
			<div class="white-box">
				<form class="form-horizontal form-material" id="loginform"
					action="admin-login.html" method="post"
					onsubmit="return loginFormValidation()">
					<h3 class="box-title m-b-20">Sign-In to Panel</h3>
					<div class="form-group">
						<div class="col-xs-12">
							<span class="badge btn-danger" style="width: 100%">${message}</span>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" name="emailid" id="emailid"
								type="email" required="required" maxlength="50"
								placeholder="Email Address">
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="password" id="password"
								type="password" required="required" maxlength="15"
								placeholder="Password">
						</div>
					</div>


					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" name="pin" id="pin" type="password"
								required="required" maxlength="4" placeholder="PIN">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-12">
							<div class="checkbox checkbox-primary pull-left p-t-0">
								<input id="customCheck1" type="checkbox"> <label
									for="checkbox-signup"> Remember me </label>
							</div>
							<a href="javascript:void(0)" id="to-recover"
								class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>
								Forgot Password?</a>
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button
								class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light"
								id="login" type="submit">Log In</button>
						</div>
					</div>

				</form>
				<form class="form-horizontal" id="recoverform"
					action="request-for-password-change.html" onsubmit="return false">
					<div class="form-group ">
						<div class="col-xs-12">
							<h3>Recover Password</h3>
							<p class="text-muted">Enter your Email and instructions will
								be sent to you!</p>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" type="email" required="required"
								placeholder="Email" name="email_id" id="id_email">
						</div>
					</div>
					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button
								class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light"
								type="submit" onclick="checkemail()">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<!-- jQuery -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Menu Plugin JavaScript -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!--slimscroll JavaScript -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!--Style Switcher -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>

	<script src="${pageContext.request.contextPath}/js/cookie.js"></script>


	//Script for remember me
	<script type="text/javascript">

$(document).ready(function() {
        console.log("method");

    var remember = $.cookie('remember');
            console.log(remember);

    if (remember == 'true')
    {
        console.log("retrived");

        var email = $.cookie('emailid');
        var password = $.cookie('password');
        // autofill the fields
        $('#emailid').val(email);
        $('#password').val(password);
        $("#customCheck1").prop("checked", true);
    	console.log(emailid);
    	console.log(password);

    }

$("#login").click(function() {
    if ($('#customCheck1').is(':checked')) {
    console.log("saved");

        var emailid = $('#emailid').val();
        var password = $('#password').val();

        // set cookies to expire in 14 days
        $.cookie('emailid', emailid, { expires: 14 });
        $.cookie('password', password, { expires: 14 });
        $.cookie('remember', true, { expires: 14 });
    }
    else
    {
        // reset cookies
        $.cookie('emailid', null);
        $.cookie('password', null);
        $.cookie('remember', null);
    }
});
});

</script>

	<script type="text/javascript">
    
    function loginFormValidation(){
    	
    	var isnum = /^\d+$/.test(document.getElementById('pin').value);

    	console.log(isnum);
    	
    	if(isnum){
    		
    		return true;
    		
    	}
    	
    	document.getElementById('pin').value ="";
    	return false;
    }
    
    </script>

	<script type="text/javascript">
    
    function checkemail() {
		
    	$.ajax({
    	    url: '${pageContext.request.contextPath}/request-for-password-change.html?email_id='+$('#id_email').val(),
    	    success: function (response) {
    	    	console.log(response);
    	        $('#post').html(response.responseText);
    	    },
    	    error: function (jqXHR, exception) {
    	        var msg = '';
    	        if (jqXHR.status === 0) {
    	            msg = 'Not connect.\n Verify Network.';
    	        } else if (jqXHR.status == 404) {
    	            msg = 'Requested page not found. [404]';
    	        } else if (jqXHR.status == 500) {
    	            msg = 'Internal Server Error [500].';
    	        } else if (exception === 'parsererror') {
    	            msg = 'Requested JSON parse failed.';
    	        } else if (exception === 'timeout') {
    	            msg = 'Time out error.';
    	        } else if (exception === 'abort') {
    	            msg = 'Ajax request aborted.';
    	        } else {
    	            msg = 'Uncaught Error.\n' + jqXHR.responseText;
    	        }
    	        console.log(msg);
    	    }
    	});
    	
	}
    
    </script>

</body>

</html>