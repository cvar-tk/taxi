<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">
<!-- ===== DatePicker ===== -->
          <link href="${pageContext.request.contextPath}/css/jquery-ui.css" rel="stylesheet">


<style type="text/css">
hr {
	margin-top: 0px;
	margin-bottom: 0px;
	border: 0;
	border-top: 1px solid #eee;
}
</style>

</head>

<body class="mini-sidebar fix-header" ng-app="myApp"
	ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">
		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />
		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<ul class="breadcrumb">
					<li><a href="#">Dashboard</a></li>
					<li><a href="#">New Request</a></li>
					<li><a href="#">View</a></li>
				</ul>
				<!-- .row -->
				<form action="vm-approve-vehicle.html" method="get">
					<div class="row white-box">
						<div class=" col-md-12">
							<div class=" col-md-6">
								<div class="form-group">
									<label class="col-md-12">Vehicle Number</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.vhm_number}}" required="required"
											name="vehicle_number">
									</div>
								</div>
								<div class="form-group ">
									<label class="col-md-12">Registration Number</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.vhm_registration_number}}" required="required"
											name="reg_number">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Registered owner name</label>
									<div class="col-md-12 bottom-pad-20">
										<input type="text" class="form-control"
											value="{{data.vhm_owner_name}}" required="required"
											name="owner_name">
									</div>
								</div>
								
									<div class="form-group">
									<label class="col-sm-12">Vehicle Type 
									</label>
									<div class="col-sm-12 bottom-pad-20">

										<select class="form-control" name="vehicle_type" required="required">
											<option selected="selected" disabled="disabled" value="">--
												Select Type --</option>
											<option ng:repeat="type in vehicle_type"
												value="{{type.vt_id}}">{{type.vt_text}}</option>
										</select> <span class="help-block"></span>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-12">Vehicle Colour 
									</label>
									<div class="col-sm-12 bottom-pad-20">

										<select class="form-control" name="vehicle_colour" required="required">
											<option selected="selected" disabled="disabled" value="">--
												Select Type --</option>
											<option ng:repeat="colour in vehicle_colour"
												value="{{colour.c_id}}">{{colour.colour}}</option>
										</select> <span class="help-block"></span>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-12">Brand - <strong>{{data.vhm_make_hint}}</strong>
									</label>
									<div class="col-sm-12 bottom-pad-20">

										<select class="form-control" id="id_brand" required="required">
											<option selected="selected" disabled="disabled" value="">--
												Select brand --</option>
											<option ng:repeat="brand in vehicle_brands"
												value="{{brand.vb_id}}">{{brand.vb_name}}</option>
										</select> <span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12">Model - <strong>{{data.vhm_model_hint}}</strong>

									</label>
									<div class="col-sm-12 bottom-pad-20">
										<select class="form-control" required="required"
											name="model_id">
											<option selected="selected" disabled="disabled" value="">--
												Select model --</option>
											<option ng:repeat="model in models_list"
												value="{{model.vm_id}}">{{model.vm_name}}
										</select> <span class="help-block"></span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-12">Registered Date - <strong>{{data.registration_date}}</strong>
									</label>
									<div class="col-md-12 bottom-pad-20">
										<div class="input-group col-md-12">
											<input type="text" class="form-control"
												id="datepicker-autoclose1" placeholder="dd/mm/yyyy"
												onkeydown="return false" required="required"
												name="registration_date">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-12">Insurance Exp. Date - <strong>{{data.vhm_insurance_exp}}</strong>
									</label>
									<div class="col-md-12 bottom-pad-20">
										<div class="input-group col-md-12">
											<input type="text" class="form-control"
												id="datepicker-autoclose2" placeholder="dd/mm/yyyy"
												onkeydown="return false" required="required"
												name="insurance_exp_date">
										</div>
									</div>
								</div>
							</div>
							<div class=" col-md-6">
								<h3 class="box-title m-b-0">
									Registered Account : <strong>9568574584</strong>
								</h3>
								<br>
								<div class="row">
									<div class="col-md-3 col-xs-6 b-r">
										<strong>Car Full View</strong> <br> <br> <a
											href="{{data.car_full_view}}" target="_blank"
											class="text-muted">Click to view</a>
									</div>
									<div class="col-md-3 col-xs-6 b-r">
										<strong>RC Book</strong> <br> <br> <a
											href="{{data.car_rc_book}}" target="_blank"
											class="text-muted">Click to view</a>
									</div>
									<div class="col-md-3 col-xs-6 b-r">
										<strong>Car Name Board</strong> <br> <br> <a
											href="{{data.car_name_board_img}}" target="_blank"
											class="text-muted">Click to view</a>
									</div>
									<div class="col-md-3 col-xs-6 b-r">
										<strong>Insurance</strong> <br> <br> <a
											href="{{data.insurance_img}}" target="_blank"
											class="text-muted">Click to view</a>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="text-right">
							<input type="hidden" name="vehicle_id" value="{{data.vhm_id}}"
								required="required">

							<button type="submit"
								class="btn btn-success waves-effect waves-light m-r-10">Approve</button>
							<button type="button"
								class="btn btn-inverse waves-effect waves-light"
								onclick="openRejectModel()">Reject</button>
						</div>
						<!-- /.row -->
					</div>
				</form>
				<!-- /.container-fluid -->
				<jsp:include page="sidebar.jsp" />

				<jsp:include page="footer.jsp" />
			</div>
			<!-- /#page-wrapper -->
		</div>
		<!-- /#wrapper -->

		<!-- /.modal -->
		<div id="reject_model" class="modal fade" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<form method="get"
					action="${pageContext.request.contextPath}/vm-reject-request.html">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">×</button>
							<h4 class="modal-title">Are you sure..?</h4>
						</div>
						<div class="modal-body">

							<div class="form-group">
								<label for="recipient-name" class="control-label">Reason:</label>
								<input type="text" class="form-control" name="reason"
									id="id_reason" required="required">
							</div>
							<div class="form-group">
								<label for="message-text" class="control-label">Comments:</label>
								<textarea class="form-control" id="id_comments" name="comments"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" name="vehicle_id" value="{{data.vhm_id}}"
								required="required">

							<button type="button" class="btn btn-default waves-effect"
								data-dismiss="modal">Close</button>
							<button type="submit"
								class="btn btn-danger waves-effect waves-light">Confirm</button>
						</div>
					</div>
				</form>
			</div>
		</div>


		<!-- ==============================
         Required JS Files
         =============================== -->
		<!-- ===== jQuery ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
		<!-- ===== Bootstrap JavaScript ===== -->
		
		<!-- ===== Slimscroll JavaScript ===== -->
		<script
			src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
		<!-- ===== Wave Effects JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/waves.js"></script>
		<!-- ===== Menu Plugin JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
		<!-- ===== Custom JavaScript ===== -->
		<script src="${pageContext.request.contextPath}/js/custom.js"></script>
		<!-- ===== Plugin JS ===== -->
		
		  <script src="${pageContext.request.contextPath}/js/jquery-1.12.4.js"></script>
          <script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
		<!-- ===== Style Switcher JS ===== -->
		<script
			src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		<!-- ===== Style Switcher JS ===== -->
		<script src="${pageContext.request.contextPath}/js/angular.js"></script>
		<!-- Date Picker Plugin JavaScript -->
		
	<script
			src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>

		<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function ($scope, $http) {
                        $scope.data = ${data};
                        $scope.vehicle_brands = ${vehicle_brands};
                        console.log($scope.data);
                        console.log($scope.vehicle_brands);

                        var brand = document.getElementById('id_brand');

                        brand.addEventListener("change", function () {
                            var make_id = brand.options[brand.selectedIndex].value;

                            console.log(make_id);

                            $http.get("${pageContext.request.contextPath}/get-models-by-make-id.html?make_id=" + make_id)
                                .then(function (response) {
                                    $scope.models_list = response.data;
                                    console.log($scope.models_list);
                                });

                        });
                        
                        var type = ${vehicle_type};
                        
                        $scope.vehicle_type = type;
                        
                        
                        var colour = ${vehicle_colour};
                        $scope.vehicle_colour = colour;

                        
                        

                    });
                </script>
			  <script type="text/javascript">
var date = $('#datepicker-autoclose1').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'dd-mm-yy',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
	  
	  	  <script type="text/javascript">
var date = $('#datepicker-autoclose2').datepicker({ 
	
	  defaultDate: "+1w",
	   dateFormat: 'dd-mm-yy',
      autoclose: true,
	    changeMonth: true,
		changeYear: true,
		
	  
	 
	 });
	  </script> 
	  
		<script type="text/javascript">
function openRejectModel(){
	$('#id_reason').val("");
	$('#id_comments').val("");
	
$('#reject_model').modal('show');
}
</script>
</body>

</html>