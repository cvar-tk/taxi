<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

</head>

<body class="mini-sidebar fix-header">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">

		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />

		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="white-box">
							<h3 class="box-title">Under Development</h3>
						</div>
					</div>
				</div>

				<jsp:include page="sidebar.jsp" />


			</div>
			<!-- /.container-fluid -->
			<jsp:include page="footer.jsp" />

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- ==============================
        Required JS Files
    =============================== -->
	<!-- ===== jQuery ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- ===== Bootstrap JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ===== Slimscroll JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!-- ===== Wave Effects JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- ===== Menu Plugin JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!-- ===== Custom JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!-- ===== Plugin JS ===== -->
	<!-- ===== Style Switcher JS ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>