<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

<link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/components/dropify/dist/css/dropify.min.css">


<style>

.ee{
    margin-bottom: 30px;
}

.bt1{

    left: 130px;
}

@media screen and (max-width:600px) {
    .ee{
        margin-bottom: 0px !important;
    }
 .ee .col-md-2 {
     width:50% !important;
 }
 } 
</style>


</head>

<body class="mini-sidebar fix-header" ng-app="myApp" ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">

		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />

		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="white-box">
						
						
						<div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>
                       	</div>
                       	
							<h3 class="box-title"> Role Management > View Request</h3>
							
							<div class="row">
							
				<form action="admin-update-role-view.html" method="post">
							
							<input type="hidden" id="u_id" name="u_id" value="{{data.u_id}}">
							
							<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Name*</label>
										
										<div class="col-md-6">
											<input id="name" class="form-control" type="text" value="{{data.u_name}}"
												 name="name" required>
										</div>
										</div>
								</div>
														
						</div>	
							
							<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Mobile*</label>
										
										<div class="col-md-6">
											<input id="mobile" class="form-control" step= "0.01" type="number" value="{{data.u_phone}}"
												 name="mobile" required>
										</div>
										</div>
								</div>		
						</div>	
						
							<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Email*</label>
										
										<div class="col-md-6">
											<input id="email" class="form-control" type="email" value="{{data.u_email}}"
												 name="email" readonly="readonly">
										</div>
										</div>
								</div>		
						</div>	
						
						
					
					<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Security PIN*</label>
										
										<div class="col-md-6">
											<input id="security_pin" class="form-control" type="number" value="{{data.security_pin}}"
												 name="security_pin" required>
										</div>
										</div>
								</div>		
						</div>	
					
					<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Status*</label>
										
										<div class="col-md-6">
											<input id="status" ng-if = "data.status == 1" class="form-control" type="text" value="Pending"
												 name="status" readonly="readonly">
												 <input id="status" ng-if = "data.status == 2" class="form-control" type="text" value="Active"
												 name="status" readonly="readonly">
												  <input id="status" ng-if = "data.status == 5" class="form-control" type="text" value="Deactive"
												 name="status" readonly="readonly">
										</div>
										</div>
								</div>		
						</div>	
					
					
					
					
					<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									
										<label class="col-md-2">Roles*</label>
										
										<div class="col-md-6">
											<select id="role" class="form-control" ng-model="role"
												 name="role" required>
												  <option value="null" selected="selected" disabled="disabled">--Select--</option>
												    <option ng:repeat="d in dd" ng-selected="role == d.r_id" 
												value="{{d.r_id}}">{{d.multi_roles}}</option>
												 
												 
												 </select>
										</div>
										</div>
								</div>		
						</div>	
					
								
								<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-3 col-lg-1 col-sm-1 col-sm-offset-7 col-md-1 col-md-offset-7 col-lg-offset-8 col-xs-offset-2">
									<button class="btn btn-primary" type="submit">Update</button>
									</div>
									
									</form>
									
									<a href="admin-deactive-role-user.html?u_id={{data.u_id}}">
									<div class="col-xs-3 col-sm-3 col-md-1 col-lg-1 col-xs-offset-1 col-md-offset-1 col-lg-offset-0">
									<button class="btn btn-danger" type="button" >Deactive</button>
									</div>
									</a>
								</div>			
								</div>
							
						
					</div>
				</div>

				<jsp:include page="sidebar.jsp" />

</div>
</div>
			</div>
			<!-- /.container-fluid -->
			<jsp:include page="footer.jsp" />

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- ==============================
        Required JS Files
    =============================== -->
	<!-- ===== jQuery ===== -->
		<!-- Booststrap   -->
		

	
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- ===== Bootstrap JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ===== Slimscroll JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!-- ===== Wave Effects JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- ===== Menu Plugin JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!-- ===== Custom JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!-- ===== Plugin JS ===== -->
	<!-- ===== Style Switcher JS ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		
		 <script src="${pageContext.request.contextPath}/plugins/components/dropify/dist/js/dropify.min.js"></script>
    
       <script src="${pageContext.request.contextPath}/js/angular.js"></script>
     <script src="${pageContext.request.contextPath}/js/ui-bootstrap-tpls-0.13.4.min.js"></script>
     
     
       <script type="text/javascript">
      var app = angular.module('myApp', ['ui.bootstrap']);  
      
      app.controller('myCtrl', function ($scope, $http) {  
    	 
    	  var v = ${view_list};
    	  
    	  $scope.data = ${view_list};
    	  
    	  console.log($scope.data.role_id);
    	  
    	  $scope.role = $scope.data.role_id;
    	
    	      var value = v.role_id;
    	    
    	     
           $scope.dd = ${multi_role};
    	  
    	  console.log($scope.dd);
    	  
    	  
    	  
    	  
      });
        
    	  
    	  </script>
     
     
    <script>
    
    $(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
   
    </script>
    
		
	
		
		
</body>

</html>