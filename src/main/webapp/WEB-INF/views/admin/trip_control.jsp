<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

<style>

.ee{

    margin-bottom: 30px;

}

</style>


</head>

<body class="mini-sidebar fix-header" ng-app="myApp"
	ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">

		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />

		<!-- Page Content -->
		<div class="page-wrapper" style="min-height: 421px;">
			<div class="container-fluid">
				<div class="row">
				
					<div class="col-md-12">
						<div class="white-box">
						
						<div class="col-xs-12">
<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>
                       	</div>
                       	
							<h3 class="box-title">Admin Trip Control</h3>
							
							<div class="row">
							
							
							
							<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-base-control.html" method="post">
									
										<label class="col-md-2">Base Control</label>
										
										<div class="col-md-6">
										
										<select class="form-control" name="base_control" required="required" id="base_control">
											<option selected="selected" disabled="disabled" value="">--
												Select Type --</option>
											<option 
												value="0">Manual</option>
												<option 
												value="1">Automatic</option>
										</select> 
										
										
											<!-- <input id="base_control" class="form-control" type="number" step="0.01"
											
											 value="{{data.base_control}}"
												 name="base_control" required> -->
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										</form>
										
									</div>


								</div>

							</div>
							
								<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-base-km.html" method="post">
									
										<label class="col-md-2">Base KM</label>
										<div class="col-md-6">
											<input id="base_km" class="form-control" type="number" step="0.01" value="{{data.base_km}}"
												 name="base_km" required>
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										</form>
									</div>


								</div>

							</div>
							
							
							
							
							
							
								<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-per-min-price.html" method="post">
									
										<label class="col-md-2">Per Min Price</label>
										<div class="col-md-6">
											<input id="min_price" class="form-control" type="number" step="0.01" value="{{data.per_min_price}}"
												 name="min_price" required>
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										
										</form>
									</div>


								</div>

							</div>
							
							
							<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-gst.html" method="post">
									
										<label class="col-md-2">GST(%)</label>
										<div class="col-md-6">
											<input id="gst" class="form-control" type="number" step="0.01" value="{{data.gst}}"
												 name="gst" required>
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										
										</form>
									</div>


								</div>

							</div>
							
							
								<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-cgst.html" method="post">
									
										<label class="col-md-2">CGST(%)</label>
										<div class="col-md-6">
											<input id="gst" class="form-control" type="number" step="0.01" value="{{data.cgst}}"
												 name="cgst" required>
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										
										</form>
									</div>


								</div>

							</div>
							
							
								<div class="col-sm-12 ee">
								<div class="row">
									<div class="form-group">
									<div class="col-sm-2"></div>
									
									<form action="admin-update-sgst.html" method="post">
									
										<label class="col-md-2">SGST(%)</label>
										<div class="col-md-6">
											<input id="gst" class="form-control" type="number" step="0.01" value="{{data.sgst}}"
												 name="sgst" required>
										
										</div>
										<div class="col-sm-2">
										
										<button type="submit" class="btn btn-primary">Update</button>
										
										</div>
										
										</form>
									</div>


								</div>

							</div>
							
							
							
						</div>
					
				</div>

</div>
				<jsp:include page="sidebar.jsp" />

</div>
			</div>
			<!-- /.container-fluid -->
			<jsp:include page="footer.jsp" />

		</div>
		<!-- /#page-wrapper -->
	</div>
	<!-- /#wrapper -->
	<!-- ==============================
        Required JS Files
    =============================== -->
	<!-- ===== jQuery ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- ===== Bootstrap JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ===== Slimscroll JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!-- ===== Wave Effects JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- ===== Menu Plugin JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!-- ===== Custom JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!-- ===== Plugin JS ===== -->
	<!-- ===== Style Switcher JS ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		<script src="${pageContext.request.contextPath}/js/angular.js"></script>	
		
		
		<script>
                    var app = angular.module('myApp', []);
                    app.controller('myCtrl', function ($scope, $http) {
                    	
                var d = ${trip};
                        $scope.data = d;
                        
                        console.log($scope.data);
                        
                      var base=d.base_control;
                      
                      var va = 0;
                      
                      if(base != 0){
                    	  
                    	  va=1;
                      }
                        
                       $('#base_control').val(va); 
                        
                    });
              </script>
		
		
		
		
		
		
		
</body>



</html>