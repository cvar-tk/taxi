<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<link rel="icon" type="image/png" sizes="16x16"
	href="${pageContext.request.contextPath}/plugins/images/favicon.png">
<title>RootU</title>
<!-- ===== Bootstrap CSS ===== -->
<link
	href="${pageContext.request.contextPath}/bootstrap/dist/css/bootstrap.min.css"
	rel="stylesheet">
<!-- ===== Plugin CSS ===== -->
<!-- ===== Animation CSS ===== -->
<link href="${pageContext.request.contextPath}/css/animate.css"
	rel="stylesheet">
<!-- ===== Custom CSS ===== -->
<link href="${pageContext.request.contextPath}/css/style.css"
	rel="stylesheet">
<!-- ===== Color CSS ===== -->
<link href="${pageContext.request.contextPath}/css/colors/red.css"
	id="theme" rel="stylesheet">

<link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/components/dropify/dist/css/dropify.min.css">


<style>

.ef{
    margin-bottom: 30px;
}

.ee{
    margin-bottom: 20px;
}

.bt1{

    left: 130px;
}

@media screen and (max-width:600px) {
    /* .ee{
        margin-bottom: 0px !important;
    } */
 .ee .col-md-2 {
     width:50% !important;
 }
 
 } 
 
 @media screen and (max-width:767px) {
 .small-device-br{
 	margin-bottom:80px;
 }
 }
 
</style>


</head>

<body class="mini-sidebar fix-header" ng-app="myApp" ng-controller="myCtrl">
	<!-- Preloader -->
	<div class="preloader">
		<div class="cssload-speeding-wheel"></div>
	</div>
	<div id="wrapper">

		<jsp:include page="nav.jsp" />
		<jsp:include page="aside.jsp" />

		<!-- Page Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="white-box">
						
						
						<div class="col-xs-12">
						<span class="badge btn-success" style="width: 100%">${message}</span>
                       	<span class="badge btn-danger" style="width: 100%">${message1}</span>
                       	</div>
                       	<div class="row ef">
                       	<div class="col-sm-6">
							<h3 class="box-title"> View Trip Details</h3>
							</div>
							<div class="col-sm-6" ng-show="data.trip_status_id == 1">
		           			<a href="admin-terminate-trip.html?trip_id={{data.trip_id}}"><button class="btn btn-primary" type="submit" style="float:right;">End Trip</button></a>
							</div>
							</div>
							
							<div class="row">
							<div class="col-sm-12 col-sm-offset-1 ef">
							
							<div class="col-sm-6 ee">
								<div class="row">
									
										<!-- WORKING -->
										
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Customer Name :</b></label>
										
										<div class="col-xs-8 col-sm-3 col-md-1 col-lg-1 ee">
											<label id="name">{{data.customer}}</label>
											
										</div>
										
								</div>
					
								<div class="row">
									
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Customer Mobile :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name">{{data.customer_mobile}}</label>
											
										</div>
										
								</div>
						
								<div class="row">
									
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Driver Name :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name">{{data.driver}}</label>
											
										
										</div>
								</div>
								
								<div class="row">
									
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Driver Mobile :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.driver_mobile}}</label>
											
										
										</div>
								</div>
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Vehicle Number :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.vehicle}}</label>
											
										</div>
								</div>
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Vehice Type :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.vehicle_type}}</label>
											
										</div>
								</div>
								
								
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Start Time :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name">{{data.start_time}}</label>
											
										</div>
								</div>
								
								
								<div class="row" ng-hide="data.end_time == null">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;" >End time :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name">{{data.end_time}}</label>
											
										</div>
								</div>
								
									<div class="row" ng-hide="data.amount == null">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;" >Amount :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name">{{data.amount}}</label>
											
										</div>
								</div>
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">CGST(%) :</b></label>
										
										<div class="col-xs-8 col-md-8 ee" >
											<label  id="name">{{data.cgst}}</label>
											
										</div>
								</div>
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">SGST(%) :</b></label>
										
										<div class="col-xs-8 col-md-8 ee" >
											<label  id="name">{{data.sgst}}</label>
											
										</div>
								</div>
								
								<div class="row small-device-br">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;" ng-hide="data.payment_method == null">Payment Method :</b></label>
										
										<div class="col-xs-8 col-md-8 ee" >
											<label ng-if="data.payment_method == 1" id="name">Cash</label>
											<label ng-if="data.payment_method == 2" id="name">Online</label>
										</div>
								</div>
								
								
								</div>
						
					
						
						
							<div class="col-sm-6 ee ">
								<div class="row ">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Base Fair :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.base_fair}}</label>
											
										</div>
								</div>
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Base KM :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.base_km}}</label>
											
										</div>
								</div>
								<div class="row">
									
										<label class="col-xs-4 col-md-3"><b style="font-weight: 700;">Start Point :</b></label>
										
										<div class="col-xs-7 col-md-6 col-md-offset-1 ee">
											<label id="name"  >{{data.starting_point}}</label>
											
										
										</div>
								</div>
								<div class="row" ng-hide="data.ending_point == null">
									
										<label class="col-xs-4 col-md-3"><b style="font-weight: 700;" >End point :</b></label>
										
										<div class="col-xs-7 col-md-6 col-md-offset-1 ee">
											<label id="name"  >{{data.ending_point}}</label>
											
										</div>
								</div>
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Tax (%) :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.tax_percent}}</label>
											
										</div>
								</div>
						<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Estimated Amount :</b></label>
										
										<div class="col-xs-8 col-md-8 ee">
											<label id="name"  >{{data.est_amount}}</label>
											
										</div>
								</div>
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Per KM Price :</b></label>
										
										<div class="col-xs-8 col-md-8 ee ">
											<label id="name">{{data.per_km_price}}</label>
											
										</div>
								</div>
								
								<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Per Minute Price :</b></label>
										
										<div class="col-xs-8 col-md-8 ee ">
											<label id="name">{{data.per_min_price}}</label>
											
										</div>
								</div>
								
									<div class="row">
									
										<label class="col-xs-4 col-md-4"><b style="font-weight: 700;">Toll :</b></label>
										
										<div class="col-xs-8 col-md-8 ee ">
											<label id="name">{{data.toll}}</label>
											
										</div>
								</div>
						</div>	
						<!-- ENDS -->
							
							
						</div>
						
						<div class="col-sm-10 col-sm-offset-1 ef">
						
						 <div id="map" style="width: 100%; height: 400px;"></div>
						 
						 </div>
						
						
						
					</div>
				</div>

				<jsp:include page="sidebar.jsp" />

				</div>
				</div>
				</div>
			<!-- /.container-fluid -->
			<jsp:include page="footer.jsp" />

		</div>
		<!-- /#page-wrapper -->
	</div>
	
	
	<!-- /#wrapper -->
	<!-- ==============================
        Required JS Files
    =============================== -->
	<!-- ===== jQuery ===== -->
		<!-- Booststrap   -->
		
<input type = "hidden" id="jsonarray" value="">
	
	<script
		src="${pageContext.request.contextPath}/plugins/components/jquery/dist/jquery.min.js"></script>
	<!-- ===== Bootstrap JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- ===== Slimscroll JavaScript ===== -->
	<script
		src="${pageContext.request.contextPath}/js/jquery.slimscroll.js"></script>
	<!-- ===== Wave Effects JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/waves.js"></script>
	<!-- ===== Menu Plugin JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/sidebarmenu.js"></script>
	<!-- ===== Custom JavaScript ===== -->
	<script src="${pageContext.request.contextPath}/js/custom.js"></script>
	<!-- ===== Plugin JS ===== -->
	<!-- ===== Style Switcher JS ===== -->
	<script
		src="${pageContext.request.contextPath}/plugins/components/styleswitcher/jQuery.style.switcher.js"></script>
		
		 <script src="${pageContext.request.contextPath}/plugins/components/dropify/dist/js/dropify.min.js"></script>
    
       <script src="${pageContext.request.contextPath}/js/angular.js"></script>
     <script src="${pageContext.request.contextPath}/js/ui-bootstrap-tpls-0.13.4.min.js"></script>
 
 <script src="http://maps.google.com/maps/api/js?key=AIzaSyBXkYGxXa1mijBUY_qkzyGRtYcc-micZ1k" 
          type="text/javascript"></script>

 <!-- <script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXkYGxXa1mijBUY_qkzyGRtYcc-micZ1k"
	async defer></script> -->
	
       <script type="text/javascript">
      var app = angular.module('myApp', ['ui.bootstrap']);  
      
      app.controller('myCtrl', function ($scope, $http) {  
    	 
    	  var v = ${view_trip};
    	  
    	  $scope.data = ${view_trip};
    	  
    	  console.log($scope.data);
    	  
    	  var array=[];
    	  
    	  var jsonarray = [];
    	  
    	  jsonarray.push($scope.data.start_lat);
    	  jsonarray.push($scope.data.start_lang);
    	  jsonarray.push(1);

 var jsonarray1 = [];
    	  
    	  jsonarray1.push($scope.data.end_lat);
    	  jsonarray1.push($scope.data.end_lang);
    	  jsonarray1.push(2);

        array.push(jsonarray);
        array.push(jsonarray1);
        
        console.log(array);
      
        var locations = array;
        
        var iconGreen = '${pageContext.request.contextPath}/plugins/images/green-marker.png';
        var iconRed = '${pageContext.request.contextPath}/plugins/images/red-marker.png';     

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: new google.maps.LatLng(13.0827,80.2707),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

         
        	
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[0][0], locations[0][1] ),
            map: map,
            icon: iconGreen
          });
          
          marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[1][0], locations[1][1] ),
              map: map,
              icon: iconRed
            });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
             // infowindow.setContent(locations[i][0]);
             
             console.log(marker);
             infowindow.open(map, marker); 
            
            }
          })(marker, i));
        
        
        
        
        
      });
        
     
    	  </script>
     
     
    <script>
    
    $(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
   
    </script>
    
    
 
     
	
		
		
</body>

</html>